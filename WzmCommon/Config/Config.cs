﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace WzmCommon
{

    public class ConfigManager
    {

        private static IDictionary<string, string> _settings = new Dictionary<string, string>();
        /// <summary>
        /// 
        /// </summary>
        private static XDocument _config;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public static void LoadConfig(string path)
        {
            _config = XDocument.Load(path);
            var appSettings = _config
                   .Descendants()
                   .Elements()
                   .FirstOrDefault(e => e.Name.LocalName == "appSettings");
            if (null != appSettings)
            {
                foreach (var e in appSettings
                    .Elements()
                    .Where(e => e.Name.LocalName == "add"))
                {
                    _settings[e.Attribute("key").Value] = e.Attribute("value").Value;
                }
            }
        }

        public static string GetSetting(string key)
        {
            return GetSetting(key, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetSetting(string key, string defaultValue)
        {
            if (!_settings.ContainsKey(key))
            {
                if (null == defaultValue) throw new Exception(string.Format("La llave {0} no existe.", key));
                return defaultValue;
            }
            return _settings[key];
        }

        public static bool ContainsKey(string key)
        {
            return _settings.ContainsKey(key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IDictionary<string, string> Settings
        {
            get
            {
                var appSettings = _config
                    .Descendants()
                    .Elements()
                    .FirstOrDefault(e => e.Name.LocalName == "appSettings");
                var result = new Dictionary<string, string>();
                if (null != appSettings)
                {
                    foreach (var e in appSettings
                        .Elements()
                        .Where(e => e.Name.LocalName == "add"))
                    {
                        result.Add(e.Attribute("key").Value, e.Attribute("value").Value);
                    }
                }
                return result;
            }
        }
                
        public static void PrintSettings()
        {
            Console.WriteLine("Settings loaded.");
            Console.WriteLine("===================================");
            foreach (var k in Settings.Keys)
            {
                Console.WriteLine("{0} = \"{1}\"", k, Settings[k]);
            }
            Console.WriteLine("===================================");
        }
        public static string PrintSettingsToString()
        {
            string output = "";
            foreach (var k in Settings.Keys)
            {
                output += String.Format("{0} = \"{1}\"", k, Settings[k]);
            }
            return output;
        }
        
    }
}

