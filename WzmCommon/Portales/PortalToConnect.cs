﻿namespace WzmCommon.Portales
{
    public static class PortalToConnect
    {
        private static int _userPerPortal = 4;
        //private static int _extraUsersPerPortal = 1;
        public static int PortalQty(int conectedUsers)
        {
            if (conectedUsers <= _userPerPortal)
            {
                return 1;
            }

            if (conectedUsers <= _userPerPortal * 2)
            {
                return 2;
            }
            if (conectedUsers <= _userPerPortal * 3)
            {
                return 3;
            }
            if (conectedUsers <= _userPerPortal * 4)
            {
                return 4;
            }
            if (conectedUsers <= _userPerPortal * 5)
            {
                return 5;
            }
            if (conectedUsers <= _userPerPortal * 6)
            {
                return 6;
            }
            return 0;
        }
    }
}
