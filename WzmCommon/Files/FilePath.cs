﻿using System;
using System.IO;

namespace WzmCommon.Files
{
    public static class FilePath
    {
        private static string TimePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\time";
        private static string TempTimePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\tempTime";
        private static string LastDayPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\date";
        private static string ActivationCodePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "activacion.ac");
        private static string AppSettings = Path.Combine(Directory.GetCurrentDirectory(), "App_Data", "wzm.config");
        private static string LicensePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "license.lic");
        private static string TempLicense = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "temp.lic");

        public static string GetFilePath(Files file)
        {

            switch (file)
            {
                case Files.Time:
                    return TimePath;

                case Files.TempTime:
                    return TempTimePath;
                case Files.LastDay:
                    return LastDayPath;
                case Files.Activation:
                    return ActivationCodePath;
                case Files.License:
                    return LicensePath;
                case Files.TempLicense:

                default:
                    return TempLicense;
            }
        }


        public static bool ExistFile(Files file)
        {
            switch (file)
            {
                case Files.Time:
                    return File.Exists(TimePath);
                case Files.TempTime:
                    return File.Exists(TempTimePath);
                case Files.LastDay:
                    return File.Exists(LastDayPath);
                case Files.Activation:
                    return File.Exists(ActivationCodePath);
                case Files.License:
                    return File.Exists(LicensePath);
                case Files.TempLicense:

                default:
                    return File.Exists(TempLicense);
            }
        }

    }

    public enum Files
    {
        Time, TempTime, LastDay, Activation, License, TempLicense
    }
}
