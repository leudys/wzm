﻿namespace WzmCommon.Files
{
    public static class FileName
    {
        public static string ConstructFileName(string deviceMac, int randomNumber)
        {
            return $"{string.Concat(deviceMac.Split(':'))}_{randomNumber.ToString()}";


        }
    }
}
