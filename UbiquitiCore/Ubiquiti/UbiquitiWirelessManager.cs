﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Models;
using wzmData.Enum;
using SshCore;
using wzmData.Ssh;
using wzmData.Ubiquiti;
using UbiquitiCore.Utils;

namespace wzmCore.Ubiquiti
{
    public class UbiquitiWirelessManager
    {
        /// <summary>
        /// Obitene el modo en que está operando la Wireless del Dispositivo.
        /// </summary>
        /// <param name="ubiquitiCredentials">Credenciales del Dispositivo</param>
        /// <returns></returns>
        public async Task<WirelessMode> GetWirelessMode(UbiquitiCredentials ubiquitiCredentials)
        {
            Login login = new Login() { User = ubiquitiCredentials.User, Password = ubiquitiCredentials.Password, Port = 22, Ip = ubiquitiCredentials.Dispositivo.Ip };
            var connection = new SshConnection();
            await connection.Initialize(login);

            var deviceInfo = connection.GetJson(CommandType.UbiquitiObject);

            if (deviceInfo != null)
            {
                var info = (UbiquitiObject)deviceInfo;

                string mode = UbiquitiUtils.GetModeString(info.Wireless.Mode == "ap", info.Wireless.Wds, info.Wireless.Aprepeater);

                //if (mode.Equals("AP-Repeater") || mode.Equals("Access Point WDS"))
                //{
                //    return WirelessMode.ApRepeater;
                //}
                if (mode.Equals("Access Point"))
                {
                    return WirelessMode.Ap;

                }
                return WirelessMode.Station;
            }

            return WirelessMode.Desconocido;
        }

        /// <summary>
        /// Obitene el modo en que está operando la Wireless del Dispositivo.
        /// </summary>
        /// <param name="ubiquitiObject">Información del Dispositivo</param>
        /// <returns></returns>
        public WirelessMode GetWirelessMode(UbiquitiObject ubiquitiObject)
        {
            string mode = UbiquitiUtils.GetModeString(ubiquitiObject.Wireless.Mode == "ap", ubiquitiObject.Wireless.Wds, ubiquitiObject.Wireless.Aprepeater);

            if (mode.Equals("Access Point"))
            {
                return WirelessMode.Ap;

            }
            return WirelessMode.Station;
        }

        /// <summary>
        /// Cambia el Modo de la Wireless <see cref="WirelessMode"/>
        /// </summary>
        /// <param name="stationDeviceCredential">Credenciales de Ubiquiti del Equipo a Cambiar</param>
        /// <param name="apDeviceCredential">Credenciales de Ubiquiti del AP</param>
        /// <param name="fileName">Nombre aleatorio</param>
        /// <returns></returns>
        public async Task SetWirelessToStationMode(UbiquitiCredentials stationDeviceCredential, UbiquitiCredentials apDeviceCredential, string fileName)
        {


            FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
       FileAccess.Write);
            ScpConnection scpConnection = new ScpConnection();
            SshConnection sshConnection = new SshConnection();
            Login login = new Login() { User = apDeviceCredential.User, Password = apDeviceCredential.Password, Port = 22, Ip = apDeviceCredential.Dispositivo.Ip };

            List<Task> list = new List<Task>();
            list.Add(scpConnection.Initialize(login));
            list.Add(sshConnection.Initialize(login));

            await Task.WhenAll(list);
            await scpConnection.DownloadConfiguration(downloadStream);
            UbiquitiObject apInformation = (UbiquitiObject)sshConnection.GetJson(CommandType.UbiquitiObject);

            if (apInformation != null)
            {
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                File.Delete($"App_Data/Temp_Data/{fileName}");

                if (str.Any(c => c.Contains("radio.1.mode")))
                {
                    var radioMode = str.FirstOrDefault(c => c.Contains("radio.1.mode")).Split('=');
                    var oldValue = radioMode[1];

                    if (!oldValue.Equals("managed"))
                    {
                        radioMode[1] = "managed";

                        var index = str.IndexOf($"radio.1.mode={oldValue}");

                        str.RemoveAt(index);
                        str.Insert(index, $"radio.1.mode={radioMode[1]}");
                    }


                }
                else
                {
                    str.Add("radio.1.mode=managed");
                }
                if (str.Any(c => c.Contains("wireless.1.ssid")))
                {
                    var ssid = str.FirstOrDefault(c => c.Contains("wireless.1.ssid")).Split('=');

                    if (ssid.Length > 0)
                    {
                        string oldSsid = ssid[1];

                        ssid[1] = apInformation.Wireless.Essid;

                        var index = str.IndexOf($"wireless.1.ssid={oldSsid}");

                        str.RemoveAt(index);
                        str.Insert(index, $"wireless.1.ssid={ssid[1]}");

                    }
                    else
                    {
                        var index = str.IndexOf("wireless.1.ssid=");
                        str.RemoveAt(index);
                        str.Insert(index, $"wireless.1.ssid={apInformation.Wireless.Essid}");
                    }
                }
                else
                {
                    str.Add($"wireless.1.ssid={apInformation.Wireless.Essid}");
                }
                if (str.Any(c => c.Contains("radio.1.freq")))
                {
                    var freq = str.FirstOrDefault(c => c.Contains("radio.1.freq")).Split('=');

                    if (freq.Length > 0)
                    {
                        string oldFreq = freq[1];

                        freq[1] = apInformation.Wireless.Frequency.Split(' ')[0];

                        var index = str.IndexOf($"radio.1.freq={oldFreq}");

                        str.RemoveAt(index);
                        str.Insert(index, $"radio.1.freq={freq[1]}");

                    }
                    else
                    {
                        var index = str.IndexOf("radio.1.freq=");
                        str.RemoveAt(index);
                        str.Insert(index, $"radio.1.freq={apInformation.Wireless.Frequency.Split(' ')[0]}");
                    }
                }
                else
                {
                    str.Add($"wireless.1.ssid={apInformation.Wireless.Essid}");
                }
                File.WriteAllText($"App_Data/Temp_Data/{fileName}", string.Join(' ', str.ToArray()));

                FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

                await scpConnection.UploadConfiguration(source, false);
                await sshConnection.CommitChanges(true);

            }
        }

        //public async Task SetWirelessToStationMode(UbiquitiCredentials stationDeviceCredential, MikrotikCredentials apDeviceCredential, string fileName)
        //{


        //    //     FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
        //    //FileAccess.Write);
        //    //     ScpConnection scpConnection = new ScpConnection();
        //    //     SshConnection sshConnection = new SshConnection();


        //    //     List<Task> list = new List<Task>();
        //    //     list.Add(scpConnection.Initialize(stationDeviceCredential));
        //    //     list.Add(sshConnection.Initialize(apDeviceCredential));

        //    //     await Task.WhenAll(list);
        //    //     await scpConnection.DownloadConfiguration(downloadStream);
        //    //     UbiquitiObject apInformation = (UbiquitiObject)sshConnection.GetJson(CommandType.UbiquitiObject);

        //    //     if (apInformation != null)
        //    //     {
        //    //         var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();

        //    //         if (str.Any(c => c.Contains("radio.1.mode")))
        //    //         {
        //    //             var radioMode = str.FirstOrDefault(c => c.Contains("radio.1.mode")).Split('=');
        //    //             var oldValue = radioMode[1];

        //    //             if (!oldValue.Equals("managed"))
        //    //             {
        //    //                 radioMode[1] = "managed";

        //    //                 var index = str.IndexOf($"radio.1.mode={oldValue}");

        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"radio.1.mode={radioMode[1]}");
        //    //             }


        //    //         }
        //    //         else
        //    //         {
        //    //             str.Add("radio.1.mode=managed");
        //    //         }
        //    //         if (str.Any(c => c.Contains("wireless.1.ssid")))
        //    //         {
        //    //             var ssid = str.FirstOrDefault(c => c.Contains("wireless.1.ssid")).Split('=');

        //    //             if (ssid.Length > 0)
        //    //             {
        //    //                 string oldSsid = ssid[1];

        //    //                 ssid[1] = apInformation.Wireless.Essid;

        //    //                 var index = str.IndexOf($"wireless.1.ssid={oldSsid}");

        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"wireless.1.ssid={ssid[1]}");

        //    //             }
        //    //             else
        //    //             {
        //    //                 var index = str.IndexOf("wireless.1.ssid=");
        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"wireless.1.ssid={apInformation.Wireless.Essid}");
        //    //             }
        //    //         }
        //    //         else
        //    //         {
        //    //             str.Add($"wireless.1.ssid={apInformation.Wireless.Essid}");
        //    //         }
        //    //         if (str.Any(c => c.Contains("radio.1.freq")))
        //    //         {
        //    //             var freq = str.FirstOrDefault(c => c.Contains("radio.1.freq")).Split('=');

        //    //             if (freq.Length > 0)
        //    //             {
        //    //                 string oldFreq = freq[1];

        //    //                 freq[1] = apInformation.Wireless.Frequency.Split(' ')[0];

        //    //                 var index = str.IndexOf($"radio.1.freq={oldFreq}");

        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"radio.1.freq={freq[1]}");

        //    //             }
        //    //             else
        //    //             {
        //    //                 var index = str.IndexOf("radio.1.freq=");
        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"radio.1.freq={apInformation.Wireless.Frequency.Split(' ')[0]}");
        //    //             }
        //    //         }
        //    //         else
        //    //         {
        //    //             str.Add($"wireless.1.ssid={apInformation.Wireless.Essid}");
        //    //         }
        //    //     }
        //}

        //public async Task SetWirelessToStationMode(MikrotikCredentials stationDeviceCredential, MikrotikCredentials apDeviceCredential, string fileName)
        //{


        //    //     FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
        //    //FileAccess.Write);
        //    //     ScpConnection scpConnection = new ScpConnection();
        //    //     SshConnection sshConnection = new SshConnection();


        //    //     List<Task> list = new List<Task>();
        //    //     list.Add(scpConnection.Initialize(stationDeviceCredential));
        //    //     list.Add(sshConnection.Initialize(apDeviceCredential));

        //    //     await Task.WhenAll(list);
        //    //     await scpConnection.DownloadConfiguration(downloadStream);
        //    //     UbiquitiObject apInformation = (UbiquitiObject)sshConnection.GetJson(CommandType.UbiquitiObject);

        //    //     if (apInformation != null)
        //    //     {
        //    //         var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();

        //    //         if (str.Any(c => c.Contains("radio.1.mode")))
        //    //         {
        //    //             var radioMode = str.FirstOrDefault(c => c.Contains("radio.1.mode")).Split('=');
        //    //             var oldValue = radioMode[1];

        //    //             if (!oldValue.Equals("managed"))
        //    //             {
        //    //                 radioMode[1] = "managed";

        //    //                 var index = str.IndexOf($"radio.1.mode={oldValue}");

        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"radio.1.mode={radioMode[1]}");
        //    //             }


        //    //         }
        //    //         else
        //    //         {
        //    //             str.Add("radio.1.mode=managed");
        //    //         }
        //    //         if (str.Any(c => c.Contains("wireless.1.ssid")))
        //    //         {
        //    //             var ssid = str.FirstOrDefault(c => c.Contains("wireless.1.ssid")).Split('=');

        //    //             if (ssid.Length > 0)
        //    //             {
        //    //                 string oldSsid = ssid[1];

        //    //                 ssid[1] = apInformation.Wireless.Essid;

        //    //                 var index = str.IndexOf($"wireless.1.ssid={oldSsid}");

        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"wireless.1.ssid={ssid[1]}");

        //    //             }
        //    //             else
        //    //             {
        //    //                 var index = str.IndexOf("wireless.1.ssid=");
        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"wireless.1.ssid={apInformation.Wireless.Essid}");
        //    //             }
        //    //         }
        //    //         else
        //    //         {
        //    //             str.Add($"wireless.1.ssid={apInformation.Wireless.Essid}");
        //    //         }
        //    //         if (str.Any(c => c.Contains("radio.1.freq")))
        //    //         {
        //    //             var freq = str.FirstOrDefault(c => c.Contains("radio.1.freq")).Split('=');

        //    //             if (freq.Length > 0)
        //    //             {
        //    //                 string oldFreq = freq[1];

        //    //                 freq[1] = apInformation.Wireless.Frequency.Split(' ')[0];

        //    //                 var index = str.IndexOf($"radio.1.freq={oldFreq}");

        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"radio.1.freq={freq[1]}");

        //    //             }
        //    //             else
        //    //             {
        //    //                 var index = str.IndexOf("radio.1.freq=");
        //    //                 str.RemoveAt(index);
        //    //                 str.Insert(index, $"radio.1.freq={apInformation.Wireless.Frequency.Split(' ')[0]}");
        //    //             }
        //    //         }
        //    //         else
        //    //         {
        //    //             str.Add($"wireless.1.ssid={apInformation.Wireless.Essid}");
        //    //         }
        //    //     }
        //}

        public async Task SetWirelessToApMode(UbiquitiCredentials deviceCredential, string fileName)
        {


            FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
       FileAccess.Write);
            ScpConnection scpConnection = new ScpConnection();
            Login login = new Login() { User = deviceCredential.User, Password = deviceCredential.Password, Port = 22, Ip = deviceCredential.Dispositivo.Ip };
            await scpConnection.Initialize(login);
            await scpConnection.DownloadConfiguration(downloadStream);

            var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").Where(c => c.Contains("radio.1.mode") || c.Contains("wireless.1.ap")).ToList();

            var radioMode = str.FirstOrDefault(c => c.Contains("radio.1.mode")).Split('=');

            if (!radioMode[1].Equals("master"))
            {
                // Hacer la pincha
            }
        }

        //public async Task SetWirelessToApMode(MikrotikCredentials deviceCredential)
        //{


        //}

        public async Task SetWirelessToRepeaterMode(UbiquitiCredentials deviceCredential, string fileName)
        {

            FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
       FileAccess.Write);
            ScpConnection scpConnection = new ScpConnection();
            Login login = new Login() { User = deviceCredential.User, Password = deviceCredential.Password, Port = 22, Ip = deviceCredential.Dispositivo.Ip };
            await scpConnection.Initialize(login);
            await scpConnection.DownloadConfiguration(downloadStream);

            var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").Where(c => c.Contains("radio.1.mode") || c.Contains("wireless.1.ap")).ToList();

            var radioMode = str.FirstOrDefault(c => c.Contains("radio.1.mode")).Split('=');

           
        }

        //public async Task SetWirelessToRepeaterMode(MikrotikCredentials deviceCredential)
        //{



        //}

        //public async Task SetWirelessMode(UbiquitiCredentials ubiquitiCredentials)
        //{

        //}
    }
}
