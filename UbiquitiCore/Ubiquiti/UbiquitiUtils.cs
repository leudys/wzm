﻿using System;

namespace UbiquitiCore.Utils
{
    public static class UbiquitiUtils
    {
        private static decimal _prevCpuTotal;
        private static decimal _prevCpuBusy;
        private static decimal _prevCpuUsage;
        private static decimal _prevCpuUptime;

        public static decimal Update_Mem_Usage(decimal freeram, decimal totalram)
        {
            var b = Math.Round((totalram - freeram) / totalram * 100);
            if (b < 0)
            {
                b = 0;
            }
            else
            {
                if (b > 100)
                {
                    b = 100;
                }
            }
            return b;
        }



        public static void Reset()
        {
            _prevCpuBusy = 0;
            _prevCpuBusy = 0;
            _prevCpuUsage = 0;
            _prevCpuUptime = 0;
        }

        public static decimal Update_Cpu_Usage(decimal cputotal, decimal cpubusy, decimal uptime)
        {
            var c = _prevCpuUsage;
            decimal a;
            if (cputotal != 0 && cputotal != _prevCpuTotal)
            {
                a = Math.Round((100 * (cpubusy - _prevCpuBusy)) / (cputotal - _prevCpuTotal));
            }
            else
            {
                a = _prevCpuUsage;
            }
            _prevCpuTotal = cputotal;
            _prevCpuBusy = cpubusy;
            if (a < 0)
            {
                a = 0;
            }
            else
            {
                if (a > 100)
                {
                    a = 100;
                }
            }
            if ((_prevCpuUsage != a) && (uptime - _prevCpuUptime >= 2))
            {
                _prevCpuUptime = uptime;
                c = a;
            }
            _prevCpuUsage = c;
            return c;
        }

        internal static string GetModeString(bool c, bool d, bool b)
        {
            string a;
            if (c)
            {
                if (d)
                {
                    a = b ? ("AP-Repeater") : ("Access Point WDS");

                }
                else
                {
                    a = ("Access Point");
                }
            }
            else
            {
                a = d ? ("Estación WDS") : ("Estación");
            }
            return a;
        }

        public static string GetTime(int f, string h, string c)
        {
            var g = (f / 86400) | 0;
            var b = "" + (((f / 3600) | 0) % 24);
            var e = "" + (((f / 60) | 0) % 60);
            var d = "" + (f % 60);
            var a = (b.Length < 2 ? "0" : "") + b + ":" + (e.Length < 2 ? "0" : "") + e + ":" + (d.Length < 2 ? "0" : "") + d;
            if (g > 0)
            {
                a = "" + g + ' ' + ((g == 1) ? h : c) + ' ' + a;
            }
            return a;
        }
    }
}
