﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Models;
using wzmData.Enum;
using SshCore;
using wzmData.Ubiquiti;
using wzmData.User;
using wzmData.Ssh;

namespace wzmCore.Ubiquiti
{
    public class UbiquitiACLManager
    {
        /// <summary>
        /// Obtiene los usuarios que se encuentran en un dispositivo
        /// </summary>
        /// <param name="deviceCredential"></param>
        /// <returns></returns>
        public async Task<List<UbiquitiMac>> GetDeviceUsersAsync(UbiquitiCredentials deviceCredential, string fileName)
        {
            try
            {
                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
                   FileAccess.Write);
                ScpConnection scpConnection = new ScpConnection();
                Login login = new Login() { User = deviceCredential.User, Password = deviceCredential.Password, Port = 22, Ip = deviceCredential.Dispositivo.Ip };
                await scpConnection.Initialize(login);
                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();
                scpConnection.Dispose();
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").Where(c => c.Contains("wireless.1.mac_acl")).ToList();
                var macList = GetUsers(str);
                return macList;
            }
            catch (Exception)
            {


            }
            finally
            {
                File.Delete($"App_Data/Temp_Data/{fileName}");

            }

            return null;
        }

        /// <summary>
        /// Obtiene un usuario dada su mac
        /// </summary>
        /// <param name="deviceCredential"></param>
        /// <param name="userMac"></param>
        /// <returns></returns>
        public async Task<UbiquitiMac> GetUserByMacAsync(UbiquitiCredentials deviceCredential, string userMac, string fileName)
        {
            try
            {
                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
                   FileAccess.Write);
                ScpConnection scpConnection = new ScpConnection();
                Login login = new Login() { User = deviceCredential.User, Password = deviceCredential.Password, Port = 22, Ip = deviceCredential.Dispositivo.Ip };
                await scpConnection.Initialize(login);
                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();
                scpConnection.Dispose();
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");
                var id = str.Where(c => c.Contains(userMac)).FirstOrDefault().Split('.')[3];
                var user = str.Where(c => c.Contains("wireless.1.mac_acl." + id)).Select(c =>
                  {
                      var c1 = c.Split('=');
                      var c2 = c1[0].Split('.');
                      return new Test { id = c2[3], key = c2[4], value = c1[1] };
                  }).GroupBy(c => c.id).ToList();
                UbiquitiMac ubiquitiMacViewModel = new UbiquitiMac();
                foreach (var item in user)
                {
                    string comment = string.Empty;
                    string mac = string.Empty;
                    StatusEnum status = StatusEnum.Deshabilitado;
                    foreach (var u in item)
                    {
                        if (u.key == "comment")
                        {
                            comment = u.value;

                        }
                        if (u.key == "mac")
                        {
                            mac = u.value;

                        }
                        if (u.key == "status")
                        {
                            if (u.value != "disabled")
                            {
                                status = StatusEnum.Habilitado;

                            }
                            else
                            {
                                status = StatusEnum.Deshabilitado;

                            }
                        }
                    }
                    ubiquitiMacViewModel.Mac = mac; ubiquitiMacViewModel.Comment = comment; ubiquitiMacViewModel.Status = status;
                }
                File.Delete($"App_Data/Temp_Data/{fileName}");
                scpConnection.Dispose();
                return ubiquitiMacViewModel;
            }
            catch (Exception)
            {
                File.Delete($"App_Data/Temp_Data/{fileName}");

                return null;
            }


        }

        /// <summary>
        /// Obtiene un usuario dada su mac
        /// </summary>
        /// <param name="scpConnection">Conexion SCP ya establecida con el dispositivo</param>
        /// <param name="userMac">MAC del usuario a obtener</param>
        /// <returns></returns>
        public async Task<UbiquitiMac> GetDeviceByMacAsync(ScpConnection scpConnection, string userMac, string fileName)
        {
            try
            {
                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
                   FileAccess.Write);

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();

                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

                var id = str.Where(c => c.Contains(userMac)).FirstOrDefault().Split('.')[3];
                var user = str.Where(c => c.Contains("wireless.1.mac_acl." + id)).Select(c =>
                {
                    var c1 = c.Split('=');
                    var c2 = c1[0].Split('.');
                    return new Test { id = c2[3], key = c2[4], value = c1[1] };
                }).GroupBy(c => c.id).ToList();
                UbiquitiMac ubiquitiMacViewModel = new UbiquitiMac();
                foreach (var item in user)
                {
                    string comment = string.Empty;
                    string mac = string.Empty;
                    StatusEnum status = StatusEnum.Deshabilitado;
                    foreach (var u in item)
                    {
                        if (u.key == "comment")
                        {
                            comment = u.value;

                        }
                        if (u.key == "mac")
                        {
                            mac = u.value;

                        }
                        if (u.key == "status")
                        {
                            if (u.value != "disabled")
                            {
                                status = StatusEnum.Habilitado;

                            }
                            else
                            {
                                status = StatusEnum.Deshabilitado;

                            }
                        }
                    }
                    ubiquitiMacViewModel.Mac = mac; ubiquitiMacViewModel.Comment = comment; ubiquitiMacViewModel.Status = status;
                }
                File.Delete($"App_Data/Temp_Data/{fileName}");
                return ubiquitiMacViewModel;
            }
            catch (Exception)
            {
                File.Delete($"App_Data/Temp_Data/{fileName}");
                return null;
            }


        }

        /// <summary>
        /// Elimina un usuario del dispositivo
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public async Task DeleteDeviceAsync(UserDevice device, string fileName)
        {
            var credential = device.ApplicationUser.Dispositivo.DeviceCredential.Where(c => c is UbiquitiCredentials).FirstOrDefault(c => c.DispositivoId.Equals(device.ApplicationUser.Dispositivo.Id)) as UbiquitiCredentials;
            Login login = new Login() { User = credential.User, Password = credential.Password, Port = 22, Ip = credential.Dispositivo.Ip };
            FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
                  FileAccess.Write);
            //Establezco conexiones con el dispositivo tanto como SCP como SSH
            ScpConnection scpConnection = new ScpConnection();
            SshConnection sshConnection = new SshConnection();
            List<Task> connectionList = new List<Task>() { scpConnection.Initialize(login), sshConnection.Initialize(login) };
            await Task.WhenAll(connectionList);
            await scpConnection.DownloadConfiguration(downloadStream);
            downloadStream.Close();
            var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

            string id = str.FirstOrDefault(c => c.Contains(device.Mac)).Split('.')[3];
            string[] user = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
            var tempString = File.ReadAllText($"App_Data/Temp_Data/{fileName}");
            File.Delete($"App_Data/Temp_Data/{fileName}");

            tempString = tempString.Replace(user[0], null);
            tempString = tempString.Replace(user[1], null);
            tempString = tempString.Replace(user[2], null);
            File.WriteAllText($"App_Data/Temp_Data/{fileName}", tempString);

            FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

            await scpConnection.UploadConfiguration(source, true);
            await sshConnection.CommitChanges(true);
            scpConnection.Dispose();
            sshConnection.Dispose();
            source.Dispose();
            File.Delete($"App_Data/Temp_Data/{fileName}");
        }

        /// <summary>
        /// Elimina un usuario del dispositivo
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public async Task DeleteDeviceAsync(UserDevice device, SshConnection sshConnection, ScpConnection scpConnection, string fileName)
        {
            var credential = device.ApplicationUser.Dispositivo.DeviceCredential.Where(c => c is UbiquitiCredentials).FirstOrDefault(c => c.DispositivoId.Equals(device.ApplicationUser.Dispositivo)) as UbiquitiCredentials;
            FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
                  FileAccess.Write);
            //Establezco conexiones con el dispositivo tanto como SCP como SSH

            await scpConnection.DownloadConfiguration(downloadStream);
            downloadStream.Close();
            var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

            string id = str.FirstOrDefault(c => c.Contains(device.Mac)).Split('.')[3];
            string[] user = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
            var tempString = File.ReadAllText($"App_Data/Temp_Data/{fileName}");
            File.Delete($"App_Data/Temp_Data/{fileName}");

            tempString = tempString.Replace(user[0], null);
            tempString = tempString.Replace(user[1], null);
            tempString = tempString.Replace(user[2], null);
            File.WriteAllText($"App_Data/Temp_Data/{fileName}", tempString);

            FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

            await scpConnection.UploadConfiguration(source, true);
            await sshConnection.CommitChanges(true);
            scpConnection.Dispose();
            sshConnection.Dispose();
            source.Dispose();
            File.Delete($"App_Data/Temp_Data/{fileName}");
        }

        /// <summary>
        /// Elimina un usuario del dispositivo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="sshConnection"></param>
        /// <param name="scpConnection"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task DeleteDeviceAsync(UbiquitiMac usuario, SshConnection sshConnection, ScpConnection scpConnection, string fileName)
        {

            FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
                  FileAccess.Write);
            //Establezco conexiones con el dispositivo tanto como SCP como SSH

            await scpConnection.DownloadConfiguration(downloadStream);
            downloadStream.Close();
            var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

            string id = str.FirstOrDefault(c => c.Contains(usuario.Mac)).Split('.')[3];
            string[] user = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
            var tempString = File.ReadAllText($"App_Data/Temp_Data/{fileName}");
            File.Delete($"App_Data/Temp_Data/{fileName}");

            tempString = tempString.Replace(user[0], null);
            tempString = tempString.Replace(user[1], null);
            tempString = tempString.Replace(user[2], null);
            File.WriteAllText($"App_Data/Temp_Data/{fileName}", tempString);

            FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

            await scpConnection.UploadConfiguration(source, true);
            await sshConnection.CommitChanges(true);

            source.Dispose();
            File.Delete($"App_Data/Temp_Data/{fileName}");
        }

        /// <summary>
        /// Añade un usuario al dispositivo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public async Task<UserValidation> AddDeviceAsync(UserDevice device, UbiquitiCredentials credential, string fileName)
        {
            UserValidation userValidation = new UserValidation();
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
          FileAccess.Write);
                //Establezco conexiones con el dispositivo tanto como SCP como SSH
                ScpConnection scpConnection = new ScpConnection();
                SshConnection sshConnection = new SshConnection();
                Login login = new Login() { User = credential.User, Password = credential.Password, Port = 22, Ip = credential.Dispositivo.Ip };
                List<Task> connectionList = new List<Task>() { scpConnection.Initialize(login), sshConnection.Initialize(login) };
                await Task.WhenAll(connectionList);
                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();

                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var tempStr = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var users = GetUsers(tempStr);

                if (!users.Any(c => c.Mac.Equals(device.Mac)))
                {

                    int lastId = users.Max(c => c.Id);
                    var lastuser = users.Find(c => c.Id == lastId);

                    int indexComment = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.comment={lastuser.Comment}");
                    int indexMac = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.mac={lastuser.Mac}");
                    int indexStatus = 0;
                    switch (lastuser.Status)
                    {
                        case StatusEnum.Habilitado:
                            indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=enabled"); break;
                        case StatusEnum.Deshabilitado:
                            indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=disabled"); break;
                        default:
                            break;
                    }
                    List<int> indexes = new List<int>() { indexComment, indexMac, indexStatus };
                    int nextIndex = indexes.Max() + 1;

                    int nextId = lastId + 1;
                    var newUser = new List<string>() { $"wireless.1.mac_acl.{nextId}.comment={device.Comment}", $"wireless.1.mac_acl.{nextId}.mac={device.Mac}", $"wireless.1.mac_acl.{nextId}.status=enabled" };
                    str.InsertRange(nextIndex, newUser);

                    File.Delete($"App_Data/Temp_Data/{fileName}");

                    string newConfig = string.Join(Environment.NewLine, str.ToArray());

                    File.WriteAllText($"App_Data/Temp_Data/{fileName}", newConfig);
                    FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);
                    await scpConnection.UploadConfiguration(source, true);
                    await sshConnection.CommitChanges(true);

                    source.Dispose();
                    scpConnection.Dispose();
                    sshConnection.Dispose();

                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    return userValidation;
                }
                else
                {
                    scpConnection.Dispose();
                    sshConnection.Dispose();

                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = "El usuario ya se encuentra el dispositivo Ubiquiti." });
                    return userValidation;
                }

            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = e.Message });

                return userValidation;
            }

        }

        /// <summary>
        /// /// Añade un usuario al dispositivo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="scpConnection"></param>
        /// <param name="sshConnection"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<UserValidation> AddDeviceAsync(UserDevice device, ScpConnection scpConnection, SshConnection sshConnection, string fileName)
        {
            UserValidation userValidation = new UserValidation();
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
          FileAccess.Write);
                //Establezco conexiones con el dispositivo tanto como SCP como SSH

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();

                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var tempStr = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var users = GetUsers(tempStr);

                if (!users.Any(c => c.Mac.Equals(device.Mac)))
                {

                    UbiquitiMac lastuser = new UbiquitiMac();

                    List<string> newUser = new List<string>();
                    if (users.Count() > 0)
                    {
                        int nextIndex = 0;
                        int lastId = users.Max(c => c.Id);
                        lastuser = users.Find(c => c.Id == lastId);

                        int indexComment = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.comment={lastuser.Comment}");
                        int indexMac = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.mac={lastuser.Mac}");
                        int indexStatus = 0;
                        switch (lastuser.Status)
                        {
                            case StatusEnum.Habilitado:
                                indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=enabled"); break;
                            case StatusEnum.Deshabilitado:
                                indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=disabled"); break;
                            default:
                                break;
                        }
                        List<int> indexes = new List<int>() { indexComment, indexMac, indexStatus };
                        nextIndex = indexes.Max() + 1;

                        int nextId = lastId + 1;
                        newUser = new List<string>() { $"wireless.1.mac_acl.{nextId}.comment={device.Comment}", $"wireless.1.mac_acl.{nextId}.mac={device.Mac}", $"wireless.1.mac_acl.{nextId}.status=enabled" };
                        str.InsertRange(nextIndex, newUser);
                    }
                    else
                    {
                        newUser = new List<string>() { $"wireless.1.mac_acl.{1}.comment={device.Comment}", $"wireless.1.mac_acl.{1}.mac={device.Mac}", $"wireless.1.mac_acl.{1}.status=enabled" };
                        str.InsertRange(1, newUser);
                    }






                    File.Delete($"App_Data/Temp_Data/{fileName}");

                    string newConfig = string.Join(Environment.NewLine, str.ToArray());

                    File.WriteAllText($"App_Data/Temp_Data/{fileName}", newConfig);
                    FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);
                    await scpConnection.UploadConfiguration(source, true);
                    await sshConnection.CommitChanges(true);

                    source.Dispose();


                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    return userValidation;
                }
                else
                {
                    scpConnection.Dispose();
                    sshConnection.Dispose();

                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = "El usuario ya se encuentra el dispositivo Ubiquiti." });
                    return userValidation;
                }

            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = e.Message });

                return userValidation;
            }

        }

        /// <summary>
        /// /// Añade un usuario al dispositivo
        /// </summary>
        /// <param name="userName">Nombre de usuario</param>
        /// <param name="userMAc">MAC del dispositivo del usuario</param>
        /// <param name="scpConnection"></param>
        /// <param name="sshConnection"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<UserValidation> AddDeviceAsync(string userName, string userMAc, ScpConnection scpConnection, SshConnection sshConnection, string fileName)
        {
            UserValidation userValidation = new UserValidation();
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
          FileAccess.Write);
                //Establezco conexiones con el dispositivo tanto como SCP como SSH

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();

                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var tempStr = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var users = GetUsers(tempStr);

                if (!users.Any(c => c.Mac.Equals(userMAc)))
                {

                    UbiquitiMac lastuser = new UbiquitiMac();

                    List<string> newUser = new List<string>();
                    if (users.Count() > 0)
                    {
                        int nextIndex = 0;
                        int lastId = users.Max(c => c.Id);
                        lastuser = users.Find(c => c.Id == lastId);

                        int indexComment = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.comment={lastuser.Comment}");
                        int indexMac = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.mac={lastuser.Mac}");
                        int indexStatus = 0;
                        switch (lastuser.Status)
                        {
                            case StatusEnum.Habilitado:
                                indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=enabled"); break;
                            case StatusEnum.Deshabilitado:
                                indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=disabled"); break;
                            default:
                                break;
                        }
                        List<int> indexes = new List<int>() { indexComment, indexMac, indexStatus };
                        nextIndex = indexes.Max() + 1;

                        int nextId = lastId + 1;
                        newUser = new List<string>() { $"wireless.1.mac_acl.{nextId}.comment={userName}", $"wireless.1.mac_acl.{nextId}.mac={userMAc}", $"wireless.1.mac_acl.{nextId}.status=enabled" };
                        str.InsertRange(nextIndex, newUser);
                    }
                    else
                    {
                        newUser = new List<string>() { $"wireless.1.mac_acl.{1}.comment={userName}", $"wireless.1.mac_acl.{1}.mac={userMAc}", $"wireless.1.mac_acl.{1}.status=enabled" };
                        str.InsertRange(1, newUser);
                    }

                    File.Delete($"App_Data/Temp_Data/{fileName}");

                    string newConfig = string.Join(Environment.NewLine, str.ToArray());

                    File.WriteAllText($"App_Data/Temp_Data/{fileName}", newConfig);
                    FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);
                    await scpConnection.UploadConfiguration(source, true);
                    await sshConnection.CommitChanges(true);

                    source.Dispose();


                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    return userValidation;
                }
                else
                {
                    scpConnection.Dispose();
                    sshConnection.Dispose();

                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = "El usuario ya se encuentra el dispositivo Ubiquiti." });
                    return userValidation;
                }

            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = e.Message });

                return userValidation;
            }

        }


        /// <summary>
        /// Añade un usuario al dispositivo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="sshConnection"></param>
        /// <param name="scpConnection"></param>
        /// <returns></returns>
        public async Task<UserValidation> AddDeviceAsync(UbiquitiMac usuario, SshConnection sshConnection, ScpConnection scpConnection, string fileName)
        {
            UserValidation userValidation = new UserValidation();
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
          FileAccess.Write);

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();

                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var tempStr = File.ReadAllLines($"App_Data/Temp_Data/{fileName}").ToList();
                var users = GetUsers(tempStr);

                if (!users.Any(c => c.Mac == usuario.Mac))
                {

                    int lastId = users.Max(c => c.Id);
                    var lastuser = users.Find(c => c.Id == lastId);

                    int indexComment = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.comment={lastuser.Comment}");
                    int indexMac = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.mac={lastuser.Mac}");
                    int indexStatus = 0;
                    switch (lastuser.Status)
                    {
                        case StatusEnum.Habilitado:
                            indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=enabled"); break;
                        case StatusEnum.Deshabilitado:
                            indexStatus = str.IndexOf($"wireless.1.mac_acl.{lastuser.Id}.status=disabled"); break;
                        default:
                            break;
                    }
                    List<int> indexes = new List<int>() { indexComment, indexMac, indexStatus };
                    int nextIndex = indexes.Max() + 1;

                    int nextId = lastId + 1;
                    var newUser = new List<string>() { $"wireless.1.mac_acl.{nextId}.comment={usuario.Comment}", $"wireless.1.mac_acl.{nextId}.mac={usuario.Mac}", $"wireless.1.mac_acl.{nextId}.status=enabled" };
                    str.InsertRange(nextIndex, newUser);

                    File.Delete($"App_Data/Temp_Data/{fileName}");

                    string newConfig = string.Join(Environment.NewLine, str.ToArray());

                    File.WriteAllText($"App_Data/Temp_Data/{fileName}", newConfig);
                    FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);
                    await scpConnection.UploadConfiguration(source, true);
                    await sshConnection.CommitChanges(true);

                    source.Dispose();

                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    return userValidation;
                }
                else
                {

                    File.Delete($"App_Data/Temp_Data/{fileName}");
                    userValidation.Failed(new UserValidationErrors()
                    {
                        Code = "ACL_Ubiquiti",
                        Description = "El usuario ya se encuentra en el dispositivo Ubiquiti."
                    });
                    return userValidation;
                }

            }
            catch (Exception e)
            {

                userValidation.Failed(new UserValidationErrors() { Code = "ACL_Ubiquiti", Description = e.Message });
                return userValidation;
            }

        }



        /// <summary>
        /// Actualiza el usuario en el Dispositivo
        /// </summary>
        /// <param name="scpConnection"></param>
        /// <param name="sshConnection"></param>
        /// <param name="oldUser"></param>
        /// <param name="newUser"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDeviceAsync(ScpConnection scpConnection, SshConnection sshConnection, UbiquitiMac oldUser, UbiquitiMac newUser, string fileName)
        {
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
    FileAccess.Write);

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

                string id = str.FirstOrDefault(c => c.Contains(oldUser.Mac)).Split('.')[3];
                string[] userInDevice = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
                string[] userToBeUpdated = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();

                int StatusIndex = 0;
                int CommentIndex = 0;
                int MacIndex = 0;
                for (int i = 0; i < userToBeUpdated.Length; i++)
                {
                    if (userToBeUpdated[i].Contains("status"))
                    {
                        StatusIndex = i;

                    }
                    if (userToBeUpdated[i].Contains("comment"))
                    {
                        CommentIndex = i;
                    }
                    if (userToBeUpdated[i].Contains("mac"))
                    {
                        MacIndex = i;
                    }
                }
                string[] status = userToBeUpdated[StatusIndex].Split('=');
                string[] comment = userToBeUpdated[CommentIndex].Split('=');
                string[] mac = userToBeUpdated[MacIndex].Split('=');

                if (status.Length != 0)
                {
                    status[1] = "disabled";
                }
                if (comment.Length != 0)
                {
                    comment[1] = newUser.Comment;
                }
                if (mac.Length != 0)
                {
                    mac[1] = newUser.Mac;
                }

                userToBeUpdated[StatusIndex] = $"{status[0]}={status[1]}";
                userToBeUpdated[MacIndex] = $"{mac[0]}={mac[1]}";
                userToBeUpdated[CommentIndex] = $"{comment[0]}={comment[1]}";

                var tempString = File.ReadAllText($"App_Data/Temp_Data/{fileName}");
                File.Delete($"App_Data/Temp_Data/{fileName}");

                for (int i = 0; i < userInDevice.Length; i++)
                {
                    tempString = tempString.Replace(userInDevice[i], userToBeUpdated[i]);
                }


                File.WriteAllText($"App_Data/Temp_Data/{fileName}", tempString);

                FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

                await scpConnection.UploadConfiguration(source, true);
                await sshConnection.CommitChanges(true);

                source.Dispose();
                File.Delete($"App_Data/Temp_Data/{fileName}");


                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Deshabilita un usuario en el Dispositivo
        /// </summary>
        /// <param name="scpConnection"></param>
        /// <param name="sshConnection"></param>
        /// <param name="userMac"></param>
        /// <returns></returns>
        public async Task<bool> DisableUserAsync(ScpConnection scpConnection, SshConnection sshConnection, string userMac, string fileName)
        {
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
    FileAccess.Write);

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

                string id = str.FirstOrDefault(c => c.Contains(userMac)).Split('.')[3];
                string[] enabledUser = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
                string[] userToBeDisabled = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
                int index = 0;
                for (int i = 0; i < enabledUser.Length; i++)
                {
                    if (enabledUser[i].Contains("status"))
                    {
                        index = i;
                        break;
                    }
                }
                string[] status = enabledUser.FirstOrDefault(c => c.Contains("status")).Split('=');


                if (status.Length != 0)
                {
                    if (status[1].Contains("enabled"))
                    {
                        status[1] = "disabled";
                    }



                    userToBeDisabled[index] = $"{status[0]}={status[1]}";

                    var tempString = File.ReadAllText($"App_Data/Temp_Data/{fileName}");
                    File.Delete($"App_Data/Temp_Data/{fileName}");

                    tempString = tempString.Replace(enabledUser[0], userToBeDisabled[0]);
                    tempString = tempString.Replace(enabledUser[1], userToBeDisabled[1]);
                    tempString = tempString.Replace(enabledUser[2], userToBeDisabled[2]);
                    File.WriteAllText($"App_Data/Temp_Data/{fileName}", tempString);

                    FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

                    await scpConnection.UploadConfiguration(source, true);
                    await sshConnection.CommitChanges(true);

                    source.Dispose();
                    File.Delete($"App_Data/Temp_Data/{fileName}");

                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        /// <summary>
        /// Habilita un usuario en el Dispositivo
        /// </summary>
        /// <param name="scpConnection"></param>
        /// <param name="sshConnection"></param>
        /// <param name="userMac"></param>
        /// <returns></returns>
        public async Task<bool> EnableUserAsync(ScpConnection scpConnection, SshConnection sshConnection, string userMac, string fileName)
        {
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
    FileAccess.Write);

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");

                string id = str.FirstOrDefault(c => c.Contains(userMac)).Split('.')[3];
                string[] enabledUser = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
                string[] userToBeDisabled = str.Where(c => c.Contains(string.Format("wireless.1.mac_acl." + "{0}", id))).ToArray();
                int index = 0;
                for (int i = 0; i < enabledUser.Length; i++)
                {
                    if (enabledUser[i].Contains("status"))
                    {
                        index = i;
                        break;
                    }
                }
                string[] status = enabledUser.FirstOrDefault(c => c.Contains("status")).Split('=');


                if (status.Length != 0)
                {
                    if (status[1].Contains("disabled"))
                    {
                        status[1] = "enabled";
                    }



                    userToBeDisabled[index] = $"{status[0]}={status[1]}";

                    var tempString = File.ReadAllText($"App_Data/Temp_Data/{fileName}");
                    File.Delete($"App_Data/Temp_Data/{fileName}");

                    tempString = tempString.Replace(enabledUser[0], userToBeDisabled[0]);
                    tempString = tempString.Replace(enabledUser[1], userToBeDisabled[1]);
                    tempString = tempString.Replace(enabledUser[2], userToBeDisabled[2]);
                    File.WriteAllText($"App_Data/Temp_Data/{fileName}", tempString);

                    FileStream source = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.Open, FileAccess.Read);

                    await scpConnection.UploadConfiguration(source, true);
                    await sshConnection.CommitChanges(true);

                    source.Dispose();
                    File.Delete($"App_Data/Temp_Data/{fileName}");

                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        /// <summary>
        /// Comprueba que exista un usuario en el dispositivo dado su MAC.
        /// </summary>
        /// <param name="scpConnection">Conexión SCP del dispositivo</param>
        /// <param name="sshConnection">Conexión SSH del dispositivo</param>
        /// <param name="userMac">MAC del usuario</param>
        /// <returns>true or false</returns>
        public async Task<bool> FindDevice(ScpConnection scpConnection, string userMac, string fileName)
        {
            try
            {

                FileStream downloadStream = new FileStream($"App_Data/Temp_Data/{fileName}", FileMode.OpenOrCreate,
    FileAccess.Write);

                await scpConnection.DownloadConfiguration(downloadStream);
                downloadStream.Close();
                var str = File.ReadAllLines($"App_Data/Temp_Data/{fileName}");
                File.Delete($"App_Data/Temp_Data/{fileName}");
                return str.Any(c => c.Contains(userMac));

            }
            catch (Exception)
            {

                return false;
            }

        }


        /// <summary>
        /// Obtiene los usuarios que se encuentren en la lista ACL del dispositivo
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        private List<UbiquitiMac> GetUsers(List<string> config)
        {
            var users = new List<UbiquitiMac>();
            if (config != null)
            {
                if (config.Count > 0)
                {
                    if (config.FirstOrDefault(c => c.Contains("wireless.1.mac_acl.status")).Split('=')[1] != "disabled")
                    {
                        if (config.FirstOrDefault(c => c.Contains("wireless.1.mac_acl.policy")).Split('=')[1] != "deny")
                        {
                            config.Remove("wireless.1.mac_acl.status=enabled");
                            config.Remove("wireless.1.mac_acl.policy=allow");
                            var wirelessAcl = config.Where(
                                c => c.Contains("wireless.1.mac_acl")).Select(
                                c =>
                                {
                                    var c1 = c.Split('=');
                                    var c2 = c1[0].Split('.');
                                    return new Test { id = c2[3], key = c2[4], value = c1[1] };
                                }).GroupBy(c => c.id).ToList();



                            foreach (var acl in wirelessAcl)
                            {
                                string comment = string.Empty;
                                string mac = string.Empty;
                                StatusEnum status = StatusEnum.Deshabilitado;
                                foreach (var item in acl)
                                {

                                    if (item.key == "comment")
                                    {
                                        comment = item.value;

                                    }
                                    if (item.key == "mac")
                                    {
                                        mac = item.value;

                                    }
                                    if (item.key == "status")
                                    {
                                        if (item.value != "disabled")
                                        {
                                            status = StatusEnum.Habilitado;

                                        }
                                        else
                                        {
                                            status = StatusEnum.Deshabilitado;

                                        }
                                    }
                                }
                                users.Add(new UbiquitiMac() { Id = int.Parse(acl.Key), Comment = comment, Mac = mac, Status = status });

                            }
                        }
                    }
                }


            }

            users.OrderBy(c => c.Id);

            return users;
        }

        private int GetId(string v)
        {
            return int.Parse(v.Split('.')[3]);
        }

        private StatusEnum GetStatus(string v)
        {
            if (v.Split('.')[4].Split('=')[1] == "enabled")
            {
                return StatusEnum.Habilitado;
            }
            return StatusEnum.Deshabilitado;
        }

        private string GetMac(string v)
        {
            return v.Split('.')[4].Split('=')[1];
        }

        private string GetComment(string v)
        {
            return v.Split('.')[4].Split('=')[1];
        }


    }

    public class Test
    {
        public string id { get; set; }

        public string key { get; set; }

        public string value { get; set; }
    }
}
