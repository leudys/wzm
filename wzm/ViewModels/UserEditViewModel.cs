﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzm.ViewModels
{
    public class UserEditViewModel
    {
        public string Id { get; set; }

        [Display(Name ="Alias en el Hotpost")]
        public string HotspotUser { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string UserName { get; set; }

        [Display(Name = "Fecha de Creación")]
        [DataType(DataType.Date)]
        public DateTime? Fecha { get; set; }

        public List<int> Servicios { get; set; }
              
        [Display(Name = "Clasificador de Pago")]
        public int? ClasificadorPagoId { get; set; }

        [Display(Name = "Pago del Mes por Adelantado")]
        public bool PagoAdelantado { get; set; }

    }
}
