﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace wzm.ViewModels
{
    public class FileUploadViewModel
    {
        [Required(ErrorMessage = "El campo Licencia es olbigatoio")]
        public IFormFile UploadLicense { get; set; }
    }
}
