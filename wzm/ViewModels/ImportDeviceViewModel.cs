﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzm.ViewModels
{
    public class ImportDeviceViewModel
    {
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Usuario")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Remote("IsMacValid", "Remote")]
        [Display(Name = "MAC")]
        public string Mac { get; set; }

        [Display(Name ="Dispositivo Administrador")]
        public int? AdminDeviceId { get; set; }

        [Display(Name = "Tipo de Conexión")]
        public DeviceConecctionType DeviceConecctionType { get; set; }

    }
}
