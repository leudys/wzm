﻿using System.ComponentModel.DataAnnotations;

namespace wzm.ViewModels
{
    public class ResetPassowrdViewModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva Contraseña")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("NewPassword", ErrorMessage = "El campo Contraseña y Confirmar Contraseña no coinciden.")]
        public string ConfirmPassword { get; set; }
    }
}
