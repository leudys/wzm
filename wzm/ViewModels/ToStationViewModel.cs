﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace wzm.ViewModels
{
    public class ToStationViewModel
    {
        public int DeviceId { get; set; }

        [Display(Name="Ap")]
        public int? ApId { get; set; }

        [Remote("IsMacValid", "Remote")]
        [StringLength(17, ErrorMessage = "El campo {0} ha de tener 17 caracteres", MinimumLength = 17)]
        public string Mac { get; set; }

        public string SSID { get; set; }

        public bool Publico { get; set; }
    }
}
