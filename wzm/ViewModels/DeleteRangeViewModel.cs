﻿namespace wzm.ViewModels
{
    public class DeleteRangeViewModel
    {

        public string Desde { get; set; }

        public string Hasta { get; set; }
    }
}
