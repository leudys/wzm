﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace wzm.ViewModels
{
    public class SyncDeviceViewModel
    {
        [Display(Name = "Dispositivo Administrador")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public int AdminDeviceId { get; set; }

        [Display(Name = "Eliminar dispositivo del sistema si no existe en el dispositivo.")]

        public bool Delete { get; set; }
    }
}
