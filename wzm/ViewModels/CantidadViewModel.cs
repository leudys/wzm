﻿
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace wzm.ViewModels
{
    public class CantidadViewModel
    {
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Remote("Cantidad", "Remote")]
        public int Cantidad { get; set; }

        [Remote("ValidateIp", "Remote")]       
        [Display(Name ="Gateway")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Ip { get; set; }

        [Display(Name = "Aplicar Calidad de Servicio")]
        public bool QoS { get; set; }

    }
}
