﻿using System.ComponentModel.DataAnnotations;

namespace wzm.ViewModels
{
    public class IpAddressViewModel
    {
        [Display(Name="Dirección Ip")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Ip { get; set; }

        [Display(Name = "Interfaz")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Interface { get; set; }
    }
}
