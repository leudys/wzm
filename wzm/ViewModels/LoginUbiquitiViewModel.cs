﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wzm.ViewModels
{
    internal class LoginUbiquitiViewModel
    {
        public string User { get; set; }

        public int Port { get; set; }

        public string Password { get; set; }

        public string Ip { get; set; }
    }
}
