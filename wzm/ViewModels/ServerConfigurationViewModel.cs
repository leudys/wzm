﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wzm.ViewModels
{
    public class ServerConfigurationViewModel
    {
        public string Ip { get; set; }

        public int HttpPort { get; set; }

        public int HttpsPort { get; set; }
    }
}
