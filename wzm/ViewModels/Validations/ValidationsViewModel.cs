﻿namespace wzm.ViewModels.Validations
{
    public class ValidationsViewModel
    {
        public bool Validate { get; set; }

        public string Message { get; set; }
    }
}
