﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzm.ViewModels
{
    public class UsuarioViewModel
    {
        public string Id { get; set; }

   
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Usuario")]
        [Remote("ValidateUserName", "Remote")]
        public string UserName { get; set; }
        [Display(Name = "Alias del Hostpot")]
        public string HotspotUser { get; set; }

        [Display(Name = "Cliente Desde")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public DateTime? ClienteDesde { get; set; }

        [Display(Name = "Pago del Mes por Adelantado")]
        public bool PagoAdelantado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public StatusEnum Estado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Estado del Pago")]
        public UserPaymentState UserPaymentState { get; set; }


        [Display(Name = "Dispositivo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Remote("DeviceHasCredential", "Remote")]
        public int DispositivoId { get; set; }

        [Display(Name = "Clasificador de Pago")]
        public int? ClasificadorPagoId { get; set; }

        //[Display(Name = "Portal de Origen")]
        //[Required(ErrorMessage = "El campo {0} es obligatorio")]
        //public string PortalDeOrigen { get; set; }

        //[Remote("ValidateIp", "Remote", AdditionalFields = "Id")]
        //public string Ip { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public List<int> Servicios { get; set; }

        [StringLength(100, ErrorMessage = "El {0} debe tener al menos {2} y máximo {1} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Compare("Password", ErrorMessage = "La contraseña y la confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }
    }
}
