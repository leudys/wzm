﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzm.ViewModels
{
    public class ImportMultipleDeviceViewModel
    {
        [Required]
        [Display(Name ="Dispositivo")]
        public int DispositivoId { get; set; }

       [Display(Name ="Dispositivo Administrador")]
        public int? AdminDeviceId { get; set; }

        //[Required]
        //[Display(Name ="Dispositivo Etecsa")]
        //public int MikrotikEtecsaId { get; set; }


        //public bool Gaiming { get; set; }
    }
}
