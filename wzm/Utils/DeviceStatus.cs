﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Models;
using wzmData.Enum;
using wzmCore.Extensions;
using wzmData.Ubiquiti;
using UbiquitiCore.Discovery;
using wzmData.Ubiquiti.UbiquitiProperties;

namespace wzm.Utils
{
    public static class DeviceStatus
    {
        private static ApplicationDbContext ApplicationDbContext { get; set; }

        public static bool ChangeDeviceStatus(string mac, int dispositivoId, ApplicationDbContext applicationDbContext)
        {
            var device = applicationDbContext.Dispositivos.FirstOrDefault(c => c.Mac.Contains(mac));
            if (device is ApDevice)
            {
                if (applicationDbContext.UbiquitiCredentials.Any(c => c.DispositivoId == device.Id))
                {


                    var credential = applicationDbContext.UbiquitiCredentials.FirstOrDefault(c => c.DispositivoId == device.Id);

                    MakeDeviceChanges(device, credential, applicationDbContext);

                }

                return true;

            }
            if (device is StationDevice)
            {
                var parentDevice = applicationDbContext.Dispositivos.Find(dispositivoId);


                var client = device as StationDevice;
                client.ApDeviceId = dispositivoId;
                applicationDbContext.Update(client);
                applicationDbContext.SaveChanges();
                return true;



            }

            return false;
        }

        public static bool ChangeDeviceStatus(int dispositivoId, ApplicationDbContext applicationDbContext)
        {
            var device = applicationDbContext.Dispositivos.Find(dispositivoId);
            if (device is ApDevice)
            {




                if (applicationDbContext.UbiquitiCredentials.Any(c => c.DispositivoId == device.Id))
                {


                    var credential = applicationDbContext.UbiquitiCredentials.FirstOrDefault(c => c.DispositivoId == device.Id);

                    MakeDeviceChanges(device, credential, applicationDbContext);

                }

                return true;


            }
            if (device is StationDevice)
            {
                var client = applicationDbContext.StationDevices.Find(device.Id);
                client.ApDeviceId = dispositivoId;
                applicationDbContext.Update(client);
                applicationDbContext.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Cambia el modo de Ap,ApRepeater a Estación
        /// </summary>
        /// <param name="macApPublico">MAC del AP Público</param>
        /// <param name="deviceId">Dispositivo que se cambió hacia estación</param>
        /// <param name="applicationDbContext"></param>
        /// <returns></returns>
        public static async Task<bool> ChangeToStationAsync(ApDevice ap, int deviceId, ApplicationDbContext applicationDbContext)
        {
            try
            {

                var device = applicationDbContext.Dispositivos.Find(deviceId);


                StationDevice stationDevice = new StationDevice();

                if (applicationDbContext.Dispositivos.Any(c => c.Mac.Equals(ap.Mac)))
                {
                    var apDevice = applicationDbContext.Dispositivos.FirstOrDefault(c => c.Mac.Equals(ap.Mac));
                    stationDevice.ApDeviceId = apDevice.Id;
                    stationDevice.SSID = apDevice.SSID;
                    stationDevice.Hostname = device.Hostname;
                    stationDevice.Mac = device.Mac;
                    stationDevice.Ip = device.Ip;
                    stationDevice.WirelessSecurity = apDevice.WirelessSecurity;
                    stationDevice.WirelessMode = WirelessMode.Station;
                    stationDevice.Firmware = device.Firmware;
                    stationDevice.Platform = device.Platform;
                }

                applicationDbContext.Add(stationDevice);
                await applicationDbContext.SaveChangesAsync();

                if (applicationDbContext.GetApplicationClients().Any(c => c.DispositivoId == deviceId))
                {
                    var clients = applicationDbContext.GetApplicationClients().Where(c => c.DispositivoId == deviceId);

                    if (clients != null)
                    {
                        if (clients.Count() > 0)
                        {
                            foreach (var client in clients)
                            {
                                client.DispositivoId = stationDevice.Id;
                                
                                applicationDbContext.Users.Update(client);
                                applicationDbContext.SaveChanges();
                            }
                        }
                    }
                }

                applicationDbContext.Remove(device);
                await applicationDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Cambia el modo de Ap,ApRepeater a Estación
        /// </summary>
        /// <param name="macApPublico">MAC del AP Público</param>
        /// <param name="ssid">SSID del AP Público</param>
        /// <param name="deviceId">Dispositivo que se cambió hacia estación</param>
        /// <param name="applicationDbContext"></param>
        /// <returns></returns>
        public static async Task<bool> ChangeToStationAsync(string macApPublico, string ssid, int deviceId, ApplicationDbContext applicationDbContext)
        {
            try
            {
                var device = applicationDbContext.Dispositivos.Find(deviceId);

                StationDevice stationDevice = new StationDevice()
                {
                    SSID = ssid,
                    WirelessSecurity = WirelessSecurity.Ninguna,
                    Mac = device.Mac,
                    Ip = device.Ip,
                    Publico = true,
                    MacApPublico = macApPublico,
                    Hostname = device.Hostname,
                    WirelessMode = WirelessMode.Station,
                    Firmware = device.Firmware,
                    Platform = device.Platform,
                };


                applicationDbContext.Add(stationDevice);
                await applicationDbContext.SaveChangesAsync();
                var users = applicationDbContext.GetApplicationClients();
                if (users.Any(c => c.DispositivoId == deviceId))
                {
                    var clients = users.Where(c => c.DispositivoId == deviceId);

                    if (clients != null)
                    {
                        if (clients.Count() > 0)
                        {
                            foreach (var client in clients)
                            {
                                client.DispositivoId = stationDevice.Id;
                                
                                applicationDbContext.Users.Update(client);
                                applicationDbContext.SaveChanges();
                            }
                        }
                    }
                }

                applicationDbContext.Remove(device);
                await applicationDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>
        /// Cambia el modo de Estacion,ApRepeater a ApRepeater
        /// </summary>
        /// <param name="deviceId">Id del Dispositivo a Cambiar</param>
        /// <param name="applicationDbContext"></param>
        /// <returns></returns>
        public static async Task<bool> ChangeToApAsync(int deviceId, ApplicationDbContext applicationDbContext)
        {
            try
            {

                var device = applicationDbContext.Dispositivos.Find(deviceId);


                ApDevice apDevice = new ApDevice();

                apDevice.SSID = device.SSID;
                apDevice.WirelessSecurity = device.WirelessSecurity;
                apDevice.Mac = device.Mac;
                apDevice.Ip = device.Ip;
                apDevice.WirelessMode = WirelessMode.Ap;

                apDevice.Hostname = device.Hostname;
                apDevice.Platform = device.Platform;
                apDevice.Ubicacion = device.Ubicacion;


                applicationDbContext.Dispositivos.Add(apDevice);
                await applicationDbContext.SaveChangesAsync();
                var users = applicationDbContext.GetApplicationClients();
                if (users.Any(c => c.DispositivoId == deviceId))
                {
                    var clients = users.Where(c => c.DispositivoId == deviceId);

                    if (clients != null)
                    {
                        if (clients.Count() > 0)
                        {
                            foreach (var client in clients)
                            {
                                client.DispositivoId = apDevice.Id;
                                applicationDbContext.Users.Update(client);
                                applicationDbContext.SaveChanges();
                            }
                        }
                    }
                }

                applicationDbContext.Dispositivos.Remove(device);
                await applicationDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        ///  /// Cambia el modo de Estacion,ApRepeater a ApRepeater
        /// </summary>
        /// <param name="deviceId">Id del Dispositivo a Cambiar</param>
        /// <param name="macs">Lista de dispositivos a añadir como repetidores</param>
        /// <param name="applicationDbContext"></param>
        /// <returns></returns>
        public static bool ChangeToApRepeater(int deviceId, List<string> macs, ApplicationDbContext applicationDbContext)
        {
            try
            {

                var device = applicationDbContext.Dispositivos.Find(deviceId);

                applicationDbContext.SaveChanges();
                var users = applicationDbContext.GetApplicationClients();
                if (users.Any(c => c.DispositivoId == deviceId))
                {
                    var clients = users.Where(c => c.DispositivoId == deviceId);

                    if (clients != null)
                    {
                        if (clients.Count() > 0)
                        {
                            foreach (var client in clients)
                            {

                                applicationDbContext.Users.Update(client);
                                applicationDbContext.SaveChanges();
                            }
                        }
                    }
                }

                applicationDbContext.Remove(device);
                applicationDbContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Actualiza los del datos del dispositvo en el sistema
        /// </summary>
        /// <param name="dispositivo">Dispositivo que se va a actualizar en el sistema</param>
        /// <param name="ubiquitiObject">Información del Dispositivo</param>
        /// <param name="apInfo">Información del AP.</param>
        /// /// <param name="apInfo"></param>
        public static void UpdateStationDevice(StationDevice dispositivo, UbiquitiObject ubiquitiObject, List<Device> devices, ApInfo apInfo, ApplicationDbContext context)
        {


            if (!dispositivo.SSID.Equals(ubiquitiObject.Wireless.Essid))
            {
                dispositivo.SSID = ubiquitiObject.Wireless.Essid;
            }
            if (!dispositivo.Hostname.Equals(ubiquitiObject.Host.Hostname))
            {
                dispositivo.Hostname = ubiquitiObject.Host.Hostname;
                context.StationDevices.Update(dispositivo);
                context.SaveChangesAsync();
            }
            if (dispositivo.ApDeviceId != null)
            {
                if (!dispositivo.ApDevice.Mac.Equals(apInfo.Mac))
                {
                    if (!context.Dispositivos.Any(c => c.Mac.Equals(apInfo.Mac)))
                    {
                        if (!devices.Any(c => c.FormatedMacAddress.Equals(apInfo.Mac)))
                        {
                            // Si la MAC del AP no se encuentra en los dispositivos guardados ni en los disposivitos escaneados, entonces es muy probable que estemos frente a un dispositivo conectado a una red publica.
                            dispositivo.ApDeviceId = null;
                            dispositivo.Publico = true;
                            dispositivo.MacApPublico = apInfo.Mac;
                            context.Dispositivos.Update(dispositivo);
                            context.SaveChanges();
                        }
                        else
                        {
                            var ap = devices.FirstOrDefault(c => c.FormatedMacAddress.Equals(apInfo.Mac));

                            ApDevice newDevice = new ApDevice()
                            {
                                SSID = ap.SSID,
                                WirelessSecurity = WirelessSecurity.Ninguna,
                                Mac = ap.FormatedMacAddress,
                                Ip = ap.IpAddress,
                                WirelessMode = WirelessMode.Station,
                                Hostname = ap.Hostname,
                                Platform = ap.Platform,

                            };

                            context.Dispositivos.Add(newDevice);
                            context.SaveChanges();
                            dispositivo.ApDeviceId = newDevice.Id;
                            context.Dispositivos.Update(dispositivo);
                            context.SaveChanges();
                        }
                    }
                    else
                    {

                    }
                }
            }
            else
            {
                if (context.ApDevices.Any(c => c.Mac.Equals(apInfo.Mac)))
                {
                    var ap = context.ApDevices.FirstOrDefault(c => c.Mac.Equals(apInfo.Mac));

                    dispositivo.ApDeviceId = ap.Id;
                    dispositivo.ApDevice = ap;
                    context.StationDevices.Update(dispositivo);
                    context.SaveChanges();
                }
            }

            
        }

     

        private static void MakeDeviceChanges(Dispositivo device, UbiquitiCredentials ubiquitiCredentials, ApplicationDbContext applicationDbContext)
        {

        }
    }
}
