﻿using Microsoft.AspNetCore.Mvc;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.ViewComponents
{
    public class Internet : ViewComponent
    {
        private readonly ApplicationDbContext _dbContext;

        private readonly IWzmCore _wzmCore;

        public Internet(ApplicationDbContext db, IWzmCore mikrotikManager)
        {
            _dbContext = db;
            _wzmCore = mikrotikManager;
        }

        public bool Working { get; set; }

        public IViewComponentResult Invoke(string wlan)
        {
            //var credential = ConvertType.Convert(_dbContext.MikrotikCredentials.First(c => c.Selected==true));

            //using (ITikConnection conection = ConnectionFactory.OpenConnection(TikConnectionType.Api, credential.Ip, credential.User, credential.Password))
            //{
            //    var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
            //    string markRoute = routes.First(c => c.Gateway.Contains(wlan)).MarkRoute;
            //    string i = _wzmCore.MikrotikManager.InternetInfo.HasInternet(conection, markRoute);
            //    var internet = new InterViewModel() { Internet = i };
            //    return View(internet);
            //};

            return View();
        }
    }
}
