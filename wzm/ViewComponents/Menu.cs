﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.ViewComponents
{
    public class Menu : ViewComponent
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUsers> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IWzmCore _wzmCore;

        public Menu(ApplicationDbContext db, UserManager<ApplicationUsers> userManager, RoleManager<IdentityRole> roleManager,IWzmCore wzmCore)
        {
            _dbContext = db;
            _userManager = userManager;
            _roleManager = roleManager;
            _wzmCore = wzmCore;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {


            if (User.IsInRole("Admin"))
            {
                if (!_wzmCore.LicenseManager.Validated)
                {
                    return View("EmptyMenu");
                }
                else
                {
                    return View("AdminMenu");
                }

            }
            if (User.IsInRole("User"))
            {
                return View("EmptyMenu");
            }
            return View("EmptyMenu");

        }
    }
}
