﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using wzmData.Models;

namespace wzm
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddEnvironmentVariables()
            .AddJsonFile("certificado.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"certificado.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
            .Build();
            var certificateSettings = config.GetSection("certificateSettings");
            string certificateFileName = certificateSettings.GetValue<string>("filename");
            string certificatePassword = certificateSettings.GetValue<string>("password");
            var certificate = new X509Certificate2(certificateFileName, certificatePassword);
            var host = CreateWebHostBuilder(args, certificate).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    context.Database.Migrate();
                    Seed.Initialize(services);


                    if (!Directory.Exists("App_Data"))
                    {
                        Directory.CreateDirectory("App_Data");
                    }
                    if (!Directory.Exists("App_Data/Temp_Data"))
                    {
                        Directory.CreateDirectory("App_Data/Temp_Data");
                    }

                    host.Run();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine();
                   
                }
            }


        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args, X509Certificate2 certificate) =>
            WebHost.CreateDefaultBuilder(args)
            .UseKestrel((hostingContext, options) =>
            {
                var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
                .Build();

                var serverConfig = config.GetSection("ServerConfiguration");
                var ip = serverConfig.GetValue<string>("ip");

                options.Listen(IPAddress.Parse(ip), int.Parse(serverConfig.GetValue<string>("httpPort")));
                options.Listen(IPAddress.Parse(ip), int.Parse(serverConfig.GetValue<string>("httpsPort")), listenOptions =>
                {
                    listenOptions.UseHttps(certificate);
                });
            })
            .ConfigureLogging((hostingContext, logging) =>
            {
                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                logging.AddConsole();
                logging.AddDebug();

                logging.AddEventSourceLogger();

            })
           .CaptureStartupErrors(true)
            .UseStartup<Startup>();
    }
}
