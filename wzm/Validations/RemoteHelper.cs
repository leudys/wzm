﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using tik4net;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Validations
{
    public static class RemoteHelper
    {
        public static bool ValidateIpAddress(string ip, string id,ApplicationDbContext dbContext,IWzmCore wzmCore)
        {
            try
            {
                IPAddress iPAddress;
                IPAddress.TryParse(ip, out iPAddress);

                if (iPAddress == null)
                {
                    return false;
                }

                if (ip.Split('.').Length >= 4)
                {
                    if (id != null)
                    {
                        if (dbContext.UsersDevices.Any(c => c.Ip.Equals(ip) && !c.Id.Equals(id)))
                        {
                            return false;
                        }

                        var mikrotikCredential = dbContext.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                        using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                        {
                            if (wzmCore.MikrotikManager.Ip.DhcpServer.IsStaticIpInDhcpRange(ip, connection))
                            {
                                return false;
                            }

                        }
                    }
                    else
                    {
                        if (dbContext.UsersDevices.Any(c => c.Ip.Equals(ip)))
                        {
                            return false;
                        }



                        var mikrotikCredential = dbContext.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                        using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                        {
                            if (wzmCore.MikrotikManager.Ip.DhcpServer.IsStaticIpInDhcpRange(ip, connection))
                            {
                                return false;
                            }

                            var leases = wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(connection);
                            var arp = wzmCore.MikrotikManager.Ip.IpArp.GetAllArp(connection);

                            if (arp.Any(c => c.Address.Equals(ip) && c.Interface.Equals("ether1")) || leases.Any(c => c.Address.Equals(ip)))
                            {
                                return false;

                            }
                        }
                    }


                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
