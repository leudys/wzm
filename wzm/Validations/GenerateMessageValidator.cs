﻿using wzmCore.Enum;

namespace wzm.Validations
{
    public class GenerateMessageValidator
    {

        public static string Generate(MessageEnum messageType)
        {
            switch (messageType)
            {
                case MessageEnum.Null:
                    return "No se ha encontrado la Base de Datos.";
                case MessageEnum.NotSelected:
                    return "Por favor seleccione una credencial del Mikrotik.";
                case MessageEnum.NotCredentialFound:
                    return "No se ha encontrado credenciales para loguearse al equipo de Mikrotik.";
                default:
                    return "";
            }
        }

    }
}
