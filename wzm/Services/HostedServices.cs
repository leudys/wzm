﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using tik4net;
using wzm.Services.Helpers;
using wzmCore.Administration;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmData.Enum;
using wzmCore.Services;
using wzmCore.Utils;
using wzmCore.Services.System;
using wzmCore.Data;
using wzmCore.Extensions;

namespace wzm.Services
{

    internal class HostedServices : IHostedService
    {

        #region Private Memebers
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IServiceScopeFactory _scope;
        private readonly IEnumerable<JobSchedule> _jobSchedules;
        private readonly ILogger<HostedServices> _logger;
        private readonly IWzmCore _wzmCore;
        private readonly IConfiguration _configuration;
        private Timer _timer;
        private TimeChanged _timeChanged;

        #endregion

        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="jobSchedules"></param>
        /// <param name="jobFactory"></param>
        /// <param name="scope"></param>
        /// <param name="logger"></param>
        public HostedServices(ISchedulerFactory schedulerFactory, IConfiguration configuration, IEnumerable<JobSchedule> jobSchedules, IJobFactory jobFactory, IServiceScopeFactory scope, ILogger<HostedServices> logger, IWzmCore wzmCore)
        {
            _schedulerFactory = schedulerFactory;

            _jobFactory = jobFactory;

            _scope = scope;

            _jobSchedules = jobSchedules;

            _logger = logger;

            _wzmCore = wzmCore;

            _configuration = configuration;

        }

        private IScheduler Scheduler { get; set; }

        private void DoWork(object state)
        {
            _timeChanged.CheckTimeUpdate();
        }

        /// <summary>
        /// Inicia todos los servicios
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task StartAsync(CancellationToken cancellationToken)
        {

            if (_wzmCore.LicenseManager.Validate())
            {
                _timeChanged = new TimeChanged(_wzmCore, _logger, cancellationToken);
                _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(10));

                Scheduler = await _schedulerFactory.GetScheduler(cancellationToken);
                Scheduler.JobFactory = _jobFactory;

                foreach (var item in ServicesGenerator.GetStaticServices())
                {
                    await CreateJob(item, cancellationToken);
                }

                await Scheduler.Start(cancellationToken);
                Thread.Sleep(3000);

                var service = _scope.CreateScope();
                try
                {
                    using (var context = new ApplicationDbContext(service.ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
                    {
                        var services = context.ApplicationServices.Where(c => c.State != ServicesStatus.Stopped).ToList();

                        await BalanceoStartupChecker(context, cancellationToken);

                        await CreateJobs(services, context, cancellationToken);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e.Message);
                }
            }
            else
            {
                _logger.LogCritical("La licencia no es válida.");

            }


        }


        /// <summary>
        /// Añade un Servicio al Scheduler
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="cronExpression"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task CreateJob(ServiceModel service, CancellationToken cancellationToken)
        {
            try
            {
                var job = new JobSchedule(service.ServiceType, cronExpression: service.CronExpression);

                var Rjob = CreateJob(job, service.ServiceType);
                var Rtrigger = CreateTrigger(job);
                await Scheduler.ScheduleJob(Rjob, Rtrigger, cancellationToken);

                if (service.ServiceType != typeof(LicenseChecker))
                {
                    _logger.LogInformation($"El servicio {service.ServiceType} se ha iniciado.");
                }

            }
            catch (Exception e)
            {

                _logger.LogError($"Error iniciando servicio: {service.ServiceType.Name}{Environment.NewLine}{e.Message}");
            }



        }

        /// <summary>
        /// Eliminar un Servicio del Scheduler
        /// </summary>
        /// <param name="serviceType">Servicio a Eliminar</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteJob(ServiceType serviceType, CancellationToken cancellationToken)
        {
            JobKey jobKey = new JobKey(string.Empty);

            switch (serviceType)
            {
                case ServiceType.ReconexionAutomatica:
                    jobKey = new JobKey(nameof(ReconnectionByPingJob));
                    if (IsJobRunning(jobKey))
                    {
                        await Scheduler.Interrupt(jobKey);
                        await Scheduler.DeleteJob(jobKey);
                        jobKey = new JobKey(nameof(ReconnectionByDefaultJob));
                        await Scheduler.Interrupt(jobKey);
                        await Scheduler.DeleteJob(jobKey);
                        _logger.LogInformation($"El servicio {serviceType} se ha detenido.");
                    }
                    break;
                case ServiceType.Vip:
                    jobKey = new JobKey(nameof(VipJob));
                    break;
                case ServiceType.AntiClon:
                    jobKey = new JobKey(nameof(AntiClonJob));
                    break;
                //case ServiceType.Vpn:
                //    jobKey = new JobKey(nameof(VpnHelper));
                //    break;
                case ServiceType.BalanceadorNTH:
                    jobKey = new JobKey(nameof(BalanceoNTHJob));
                    break;
                case ServiceType.BalanceadorPCC:
                    jobKey = new JobKey(nameof(BalanceoPCCJob));
                    break;
                case ServiceType.Morosos:
                    jobKey = new JobKey(nameof(MorososJob));
                    break;

                case ServiceType.LicenseChecker:
                    jobKey = new JobKey(nameof(LicenseChecker));
                    break;
                case ServiceType.SimpleQueueLimit:
                    jobKey = new JobKey(nameof(SimpleQueueJob));
                    break;

                default:
                    break;
            }

            if (IsJobRunning(jobKey))
            {
                await Scheduler.Interrupt(jobKey);
                await Scheduler.DeleteJob(jobKey);
                _logger.LogInformation($"El servicio {serviceType} se ha detenido.");
            }
        }

        /// <summary>
        /// Comprueba que un servicio se esté ejecutando.
        /// </summary>
        /// <param name="jobKey"></param>
        /// <returns></returns>
        public bool IsJobRunning(JobKey jobKey)
        {
            // Check if job is running
            var triggers = Scheduler.GetTriggersOfJob(jobKey).Result;
            var isRunning = false;
            foreach (var trigger in triggers)
            {
                var triggerState = Scheduler.GetTriggerState(trigger.Key).Result;
                if (triggerState == TriggerState.Paused || triggerState == TriggerState.Blocked || triggerState == TriggerState.Complete || triggerState == TriggerState.Error || triggerState == TriggerState.Normal)
                {
                    isRunning = true;
                }
            }

            return isRunning;
        }

        /// <summary>
        /// Para todos los Servicios al cerrarse la aplicación.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task StopAsync(CancellationToken cancellationToken)
        {

            try
            {
                if (Scheduler != null)
                {
                    bool vipJob = IsJobRunning(new JobKey("wzmVipJob"));
                    bool balanceoNTH = IsJobRunning(new JobKey("wzmBalanceoNTHJob"));
                    bool balanceoPCC = IsJobRunning(new JobKey("wzmBalanceoPCCJob"));
                    await Scheduler.Shutdown(cancellationToken);


                    await RemoveServicesChanges(balanceoPCC, balanceoNTH);
                }


            }
            catch (Exception)
            {

            }



        }

        /// <summary>
        /// Elimina el NAT y Mangle
        /// </summary>
        /// <param name="balanceoType">Tipo de Balanceo a eliminar</param>
        public void RemoveBalanceo(string balanceoType)
        {
            using (var context = new ApplicationDbContext(_scope.CreateScope().ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                var _credential = context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                using (var connection = ConnectionFactory.OpenConnection(TikConnectionType.Api, _credential.Dispositivo.Ip, _credential.User, _credential.Password))
                {


                    var nat = new Nat();
                    var mangle = new Mangle();
                    if (nat.GetAllNats(connection).Any(c => c.Comment.Equals(balanceoType)))
                    {
                        nat.DeleteNatMultiByComment(connection, balanceoType);
                    }
                    if (mangle.GetAllMangle(connection).Any(c => c.Comment.Equals(balanceoType)))
                    {
                        mangle.DeleteMangleMultiByComment(connection, balanceoType);
                    }

                }
                var hostServices = context.HostsServices.Where(c => c.IsBalanced);
                var dispositivosBalanceo = hostServices.Select(c => c.Device).ToList();
                var credencials = context.MikrotikCredentials.FirstOrDefault(c => c.Selected);
                using (ITikConnection connection = ConnectionFactory.OpenConnection(credencials.Dispositivo.Firmware.GetApiVersion(), credencials.Dispositivo.Ip, credencials.User, credencials.Password))
                {
                    foreach (var item in dispositivosBalanceo)
                    {
                        _wzmCore.UserManagement.ReturnToOriginalPortal(item, connection);

                        var host = hostServices.FirstOrDefault(c => c.DeviceId.Equals(item.Id));
                        host.IsBalanced = false;
                        host.PortalActual = host.Device.PortalDeOrigen;

                        context.Update(host);
                        context.SaveChanges();
                    }

                }

            }
        }

        /// <summary>
        /// Añade todos los servicios al Scheduler
        /// </summary>
        /// <param name="services"></param>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task CreateJobs(List<ApplicationServices> services, ApplicationDbContext context, CancellationToken cancellationToken)
        {
            foreach (var item in services)
            {
                switch (item.ServiceType)
                {
                    case ServiceType.ReconexionAutomatica:

                        await CreateJob(ServicesGenerator.GetServiceModel(typeof(ReconnectionByPingJob)), cancellationToken);
                        await CreateJob(ServicesGenerator.GetServiceModel(typeof(ReconnectionByDefaultJob)), cancellationToken);
                        break;
                    case ServiceType.Vip:
                        await CreateJob(ServicesGenerator.GetServiceModel(typeof(VipJob)), cancellationToken);
                        break;
                    case ServiceType.NautaConnector:
                        await CreateJob(ServicesGenerator.GetServiceModel(typeof(NautaConnectorJob)), cancellationToken);
                        break;

                    case ServiceType.SimpleQueueLimit:
                        await CreateJob(ServicesGenerator.GetServiceModel(typeof(SimpleQueueJob)), cancellationToken);
                        break;
                    case ServiceType.Morosos:

                        var diasPagos = context.ClasificadorPagos.Where(c => c.PaymentType != PaymentType.Gratis).GroupBy(c => c.DiaPago).Select(c => c.Key).ToList();

                        if (diasPagos.Count() != 0)
                        {
                            string dias = string.Empty;
                            foreach (var dia in diasPagos)
                            {
                                if (dia != 0)
                                {
                                    if (!dias.Equals(dia))
                                    {
                                        dias += dia + ",";
                                    }

                                }

                            }
                            dias = dias.Remove(dias.Length - 1);

                            var morosoService = item as MorososService;

                            //await CreateJob(new ServiceModel() { ServiceType = typeof(MorososJob), CronExpression = "0/5 * * * * ?" }, cancellationToken);

                            await CreateJob(new ServiceModel() { ServiceType = typeof(MorososJob), CronExpression = $"{morosoService.Horario.Seconds} {morosoService.Horario.Minutes} {morosoService.Horario.Hours} {dias} * ? *" }, cancellationToken);

                        }

                        break;
                    case ServiceType.CambiarMac:
                        break;

                    case ServiceType.AntiClon:
                        await CreateJob(ServicesGenerator.GetServiceModel(typeof(AntiClonJob)), cancellationToken);
                        break;

                    case ServiceType.UbiquitiScan:

                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Comprueba que los dos balanceos no se encuentren activos.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task BalanceoStartupChecker(ApplicationDbContext context, CancellationToken cancellationToken)
        {

            var pcc = context.ApplicationServices.FirstOrDefault(c => c.ServiceType == ServiceType.BalanceadorPCC);
            var nth = context.ApplicationServices.FirstOrDefault(c => c.ServiceType == ServiceType.BalanceadorNTH);
            if (nth.State == ServicesStatus.Running && pcc.State == ServicesStatus.Running)
            {
                nth.State = ServicesStatus.Stopped;
                pcc.State = ServicesStatus.Stopped;
                context.ApplicationServices.Update(nth);
                context.ApplicationServices.Update(pcc);
                await context.SaveChangesAsync();

                _logger.LogInformation("Se ha detectado que los dos Balanceos se iban a iniciar. Esto trae conflictos con WZM, ambos se han detenido.");

            }
            if (nth.State == ServicesStatus.Running)
            {
                await CreateJob(new ServiceModel() { ServiceType = typeof(BalanceoNTHJob), CronExpression = "0/5 * * * * ?" }, cancellationToken);

            }
            else
            {
                if (pcc.State == ServicesStatus.Running)
                {
                    await CreateJob(new ServiceModel() { ServiceType = typeof(BalanceoPCCJob), CronExpression = "0/5 * * * * ?" }, cancellationToken);

                }
            }
        }

        /// <summary>
        /// Retorna los usuarios a su portal de origen para que no se queden corgados, 
        /// así como elimina los mangles y nat generados por los balanceadores
        /// </summary>
        /// <param name="balanceoNTH"></param>
        /// <param name="balanceoPCC"></param>
        /// <returns></returns>
        private async Task RemoveServicesChanges(bool balanceoNTH, bool balanceoPCC)
        {
            using (var context = new ApplicationDbContext(_scope.CreateScope().ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                var _credential = context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                using (var connection = ConnectionFactory.OpenConnection(TikConnectionType.Api, _credential.Dispositivo.Ip, _credential.User, _credential.Password))
                {

                    UserManagement userManagement = new UserManagement();

                    //Devuelvo todos los usuarios a su portal de origen, en caso de que alguno se haya quedado fuera del mismo.

                    var services = context.HostsServices.Include(c => c.Device).ThenInclude(c => c.ApplicationUser).Where(c => c.IsBalanced || c.HasInternet || !c.PortalActual.Equals(c.Device.PortalDeOrigen)).ToList();


                    if (services != null)
                    {


                        if (services.Count > 0)
                        {

                            if (services.Any(c => c.HasInternet))
                            {
                                userManagement.ReturnToOriginalPortal(false, services, connection);

                                foreach (var item in services)
                                {
                                    item.HasInternet = false;
                                    item.IsBalanced = false;
                                    context.HostsServices.Update(item);
                                    await context.SaveChangesAsync();
                                }
                            }


                        }
                    }
                    if (balanceoNTH)
                    {
                        RemoveBalanceo("wzm_balanceo_NTH");
                    }
                    if (balanceoPCC)
                    {
                        RemoveBalanceo("wzm_balanceo_PCC");
                    }
                }



            }
        }

        private static ITrigger CreateTrigger(JobSchedule schedule)
        {
            return TriggerBuilder
                .Create()
                .WithIdentity($"{schedule.JobType.FullName}.trigger")
                .WithCronSchedule(schedule.CronExpression)
                .WithDescription(schedule.CronExpression)
                .Build();
        }

        private static IJobDetail CreateJob(JobSchedule schedule, Type type)
        {
            var jobType = schedule.JobType;

            return JobBuilder
                .Create(jobType)
                .WithIdentity(jobType.Name)
                .WithDescription(jobType.Name)
                .Build();
        }

    }
}
