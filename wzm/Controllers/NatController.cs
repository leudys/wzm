﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects.Ip.Firewall;
using wzm.Extensions;
using wzm.Validations;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class NatController : Controller
    {
        ApplicationDbContext _db;

        private readonly IWzmCore _wzmCore;
        public NatController(ApplicationDbContext db, IWzmCore mikrotikManager)
        {
            _db = db;

            _wzmCore = mikrotikManager;
        }

        // GET: Nat
        public IActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }
            var n = new List<FirewallNat>();
            var c = _db.MikrotikCredentials.FirstOrDefault(d => d.Selected == true);
            using (var con = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                n = _wzmCore.MikrotikManager.Firewall.Nat.GetAllNats(con);
            }

            return View(n);
        }

        // GET: Nat/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Nat/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Nat/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Nat/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Nat/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Nat/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Nat/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}