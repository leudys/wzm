﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using wzmData.Models;

namespace wzm.Controllers
{
    public class ResetTimeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ResetTimeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ResetTime
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ResetTime.Include(r => r.Time).Include(r => r.User).ThenInclude(c => c.Device);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ResetTime/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var resetTime = await _context.ResetTime
                .Include(r => r.Time)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (resetTime == null)
            {
                return NotFound();
            }

            return View(resetTime);
        }

        // GET: ResetTime/Create
        public IActionResult Create()
        {
            ViewData["TimeId"] = new SelectList(_context.Time, "Id", "GetTime");
            ViewData["UserId"] = new SelectList(_context.HostsServices.Include(c => c.Device), "Id", "Name");
            return View();
        }

        // POST: ResetTime/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserId,TimeId")] ResetTime resetTime)
        {
            if (ModelState.IsValid)
            {
                _context.Add(resetTime);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TimeId"] = new SelectList(_context.Time, "Id", "Id", resetTime.TimeId);
            ViewData["UserId"] = new SelectList(_context.HostsServices, "Id", "Name", resetTime.UserId);
            return View(resetTime);
        }

        // GET: ResetTime/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var resetTime = await _context.ResetTime.FindAsync(id);
            if (resetTime == null)
            {
                return NotFound();
            }
            ViewData["TimeId"] = new SelectList(_context.Time, "Id", "Id", resetTime.TimeId);
            ViewData["UserId"] = new SelectList(_context.Usuarios, "Id", "Id", resetTime.UserId);
            return View(resetTime);
        }

        // POST: ResetTime/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,TimeId")] ResetTime resetTime)
        {
            if (id != resetTime.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(resetTime);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ResetTimeExists(resetTime.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TimeId"] = new SelectList(_context.Time, "Id", "Id", resetTime.TimeId);
            ViewData["UserId"] = new SelectList(_context.Usuarios, "Id", "Id", resetTime.UserId);
            return View(resetTime);
        }

        // GET: ResetTime/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var resetTime = await _context.ResetTime
                .Include(r => r.Time)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (resetTime == null)
            {
                return NotFound();
            }

            return View(resetTime);
        }

        // POST: ResetTime/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var resetTime = await _context.ResetTime.FindAsync(id);
            _context.ResetTime.Remove(resetTime);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ResetTimeExists(int id)
        {
            return _context.ResetTime.Any(e => e.Id == id);
        }
    }
}
