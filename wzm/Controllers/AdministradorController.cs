﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using wzm.ViewModels;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministradorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;
        private readonly UserManager<ApplicationUsers> _userManager;
        private readonly RoleManager<IdentityRole> _rolesManager;

        public AdministradorController(ApplicationDbContext context, IWzmCore wzmCore, UserManager<ApplicationUsers> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _wzmCore = wzmCore;

            _userManager = userManager;
            _rolesManager = roleManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var usuarios = _userManager.Users.ToList();
            var administradores = new List<ApplicationUsers>();
            foreach (var item in usuarios)
            {
                var roles = await _userManager.GetRolesAsync(item);

                if (roles.Any(c => c.Contains("Admin")))
                {
                    administradores.Add(item);
                }

            }

            return View(administradores);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            ApplicationUsers user = await _userManager.FindByIdAsync(id);
            UserEditViewModel viewmodel = new UserEditViewModel()
            {
                Id = user.Id,

                UserName = user.UserName,


            };
            var mk = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(TikConnectionType.Api, mk.Dispositivo.Ip, mk.User, mk.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute");
                return View(viewmodel);

            };


        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, [Bind("Id,UserName")] UserEditViewModel viewModel)
        {
            ApplicationUsers user = await _userManager.FindByIdAsync(viewModel.Id);


            if (ModelState.IsValid)
            {
                if (!user.UserName.Equals(viewModel.UserName))
                {
                    user.UserName = viewModel.UserName;
                }


                await _userManager.UpdateAsync(user);
                TempData["success"] = "La operación se ha realizado correctamente";
                return RedirectToAction(nameof(Index));
            }

            return View(viewModel);


        }

        [HttpGet]
        public async Task<IActionResult> ResetPassword(string id)
        {
            ApplicationUsers user = await _userManager.FindByIdAsync(id);
            ResetPassowrdViewModel viewmodel = new ResetPassowrdViewModel() { UserId = user.Id };
            ViewBag.UserName = user.UserName;
            return View(viewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPassowrdViewModel viewModel)
        {

            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _userManager.FindByIdAsync(viewModel.UserId);
            if (user == null)
            {
                return NotFound($"No se puede obtener el usuario con ID '{_userManager.GetUserId(User)}'.");
            }

            await _userManager.RemovePasswordAsync(user);
            var addPasswordResult = await _userManager.AddPasswordAsync(user, viewModel.NewPassword);


            if (!addPasswordResult.Succeeded)
            {
                foreach (var error in addPasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return View();
            }

            TempData["success"] = "La operación se ha realizado correctamente";
            return RedirectToAction(nameof(Index));

        }


    }
}
