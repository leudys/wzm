﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using tik4net;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    public class PortalExceptionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IWzmCore _wzmCore;
        public PortalExceptionsController(ApplicationDbContext context, IWzmCore wzmCore)
        {
            _context = context;
            _wzmCore = wzmCore;
        }

        // GET: PortalExceptions
        public async Task<IActionResult> Index()
        {
            return View(await _context.PortalExceptions.ToListAsync());
        }

        // GET: PortalExceptions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portalException = await _context.PortalExceptions
                .FirstOrDefaultAsync(m => m.Id == id);
            if (portalException == null)
            {
                return NotFound();
            }

            return View(portalException);
        }

        // GET: PortalExceptions/Create
        public IActionResult Create()
        {
            var c = _context.MikrotikCredentials.Include(e => e.Dispositivo).First(e => e.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Portal"] = new SelectList(routes, "MarkRoute", "MarkRoute");
                return View();
            }
        }

        // POST: PortalExceptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Portal,ServiceType")] PortalException portalException)
        {
            if (ModelState.IsValid)
            {
                _context.Add(portalException);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            var c = _context.MikrotikCredentials.Include(e => e.Dispositivo).First(e => e.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Portal"] = new SelectList(routes, "MarkRoute", "MarkRoute", portalException.Portal);
                return View(portalException);
            }

        }

        // GET: PortalExceptions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portalException = await _context.PortalExceptions.FindAsync(id);
            if (portalException == null)
            {
                return NotFound();
            }
            return View(portalException);
        }

        // POST: PortalExceptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Portal,ServiceType")] PortalException portalException)
        {
            if (id != portalException.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(portalException);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PortalExceptionExists(portalException.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(portalException);
        }

        // GET: PortalExceptions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portalException = await _context.PortalExceptions
                .FirstOrDefaultAsync(m => m.Id == id);
            if (portalException == null)
            {
                return NotFound();
            }

            return View(portalException);
        }

        // POST: PortalExceptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var portalException = await _context.PortalExceptions.FindAsync(id);
            _context.PortalExceptions.Remove(portalException);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PortalExceptionExists(int id)
        {
            return _context.PortalExceptions.Any(e => e.Id == id);
        }
    }
}
