﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Data.Mikrotik;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class IpArpController : Controller
    {
        ApplicationDbContext _db { get; set; }
        private readonly IWzmCore _wzmCore;
        public IpArpController(ApplicationDbContext db, IWzmCore mikrotikManager)
        {
            _wzmCore = mikrotikManager;
            _db = db;
        }
        // GET: IpArp
        public ActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }
            var ipArp = _wzmCore.MikrotikManager.Ip.IpArp.GetAllArp(_db.MikrotikCredentials.FirstOrDefault(c => c.Selected));
            return View(ipArp);
        }

        // GET: IpArp/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: IpArp/Create
        public ActionResult Create()
        {
            var interfaces = _wzmCore.MikrotikManager.Interface.GetInterfaces(_db.MikrotikCredentials.First(d => d.Selected));
            interfaces.OrderBy(d => d.Id);
            ViewData["Interfaces"] = new SelectList(interfaces, "Id", "Name");
            return View();
        }

        // POST: IpArp/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Address,Interface,MacAddress")] IpArpModel ipArp)
        {
            try
            {
                _wzmCore.MikrotikManager.Ip.IpArp.Add(_db.MikrotikCredentials.FirstOrDefault(c => c.Selected), ipArp);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: IpArp/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: IpArp/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: IpArp/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: IpArp/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}