﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using tik4net;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    public class VpnNacionalInstanceController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;
        public VpnNacionalInstanceController(ApplicationDbContext context,IWzmCore wzmCore)
        {
            _context = context;
            _wzmCore = wzmCore;
        }

        // GET: VpnNacionalInstance
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.VpnServerNacional.Include(v => v.CuentaNauta).Include(v => v.Portal);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: VpnNacionalInstance/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vpnServerNacional = await _context.VpnServerNacional
                .Include(v => v.CuentaNauta)
                .Include(v => v.Portal)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vpnServerNacional == null)
            {
                return NotFound();
            }

            return View(vpnServerNacional);
        }

        // GET: VpnNacionalInstance/Create
        public IActionResult Create()
        {
            ViewData["CuentaNautaId"] = new SelectList(_context.CuentaNautas, "Id", "User");
            var mk = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true);
            
            ViewData["PortalId"] = new SelectList(_context.Portales, "Id", "Interfaz");
            return View();
        }

        // POST: VpnNacionalInstance/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CuentaNautaId,PortalId,DisconnectOnPingFailed,Id,User,Password,Host,PersistentConnection,AsInternet")] VpnServerNacional vpnServerNacional)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vpnServerNacional);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CuentaNautaId"] = new SelectList(_context.CuentaNautas, "Id", "User", vpnServerNacional.CuentaNautaId);
            ViewData["PortalId"] = new SelectList(_context.Portales, "Id", "Interfaz", vpnServerNacional.PortalId);
            return View(vpnServerNacional);
        }

        // GET: VpnNacionalInstance/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vpnServerNacional = await _context.VpnServerNacional.FindAsync(id);
            if (vpnServerNacional == null)
            {
                return NotFound();
            }
            ViewData["CuentaNautaId"] = new SelectList(_context.CuentaNautas, "Id", "User", vpnServerNacional.CuentaNautaId);
            ViewData["PortalId"] = new SelectList(_context.Portales, "Id", "Interfaz", vpnServerNacional.PortalId);
            return View(vpnServerNacional);
        }

        // POST: VpnNacionalInstance/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CuentaNautaId,DisconnectOnPingFailed,PortalId,Id,User,Password,Host,PersistentConnection,AsInternet")] VpnServerNacional vpnServerNacional)
        {
            if (id != vpnServerNacional.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vpnServerNacional);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VpnServerNacionalExists(vpnServerNacional.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CuentaNautaId"] = new SelectList(_context.CuentaNautas, "Id", "User", vpnServerNacional.CuentaNautaId);
            ViewData["PortalId"] = new SelectList(_context.Portales, "Id", "Interfaz", vpnServerNacional.PortalId);
            return View(vpnServerNacional);
        }

        // GET: VpnNacionalInstance/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vpnServerNacional = await _context.VpnServerNacional
                .Include(v => v.CuentaNauta)
                .Include(v => v.Portal)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vpnServerNacional == null)
            {
                return NotFound();
            }

            return View(vpnServerNacional);
        }

        // POST: VpnNacionalInstance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vpnServerNacional = await _context.VpnServerNacional.FindAsync(id);
            _context.VpnServerNacional.Remove(vpnServerNacional);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VpnServerNacionalExists(int id)
        {
            return _context.VpnServerNacional.Any(e => e.Id == id);
        }
    }
}
