﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using wzmData.Models;

namespace wzm.Controllers
{
    public class PortalesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PortalesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Portales
        public async Task<IActionResult> Index()
        {
            return View(await _context.Portales.ToListAsync());
        }

        // GET: Portales/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portalesEtecsa = await _context.Portales
                .FirstOrDefaultAsync(m => m.Id == id);
            if (portalesEtecsa == null)
            {
                return NotFound();
            }

            return View(portalesEtecsa);
        }

        // GET: Portales/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Portales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Interfaz,Route")] PortalesEtecsa portalesEtecsa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(portalesEtecsa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(portalesEtecsa);
        }

        // GET: Portales/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portalesEtecsa = await _context.Portales.FindAsync(id);
            if (portalesEtecsa == null)
            {
                return NotFound();
            }
            return View(portalesEtecsa);
        }

        // POST: Portales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Interfaz,DhcpClient,Nat,Mangle,Route,Internet,Enabled")] PortalesEtecsa portalesEtecsa)
        {
            if (id != portalesEtecsa.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(portalesEtecsa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PortalesEtecsaExists(portalesEtecsa.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(portalesEtecsa);
        }

        // GET: Portales/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var portalesEtecsa = await _context.Portales
                .FirstOrDefaultAsync(m => m.Id == id);
            if (portalesEtecsa == null)
            {
                return NotFound();
            }

            return View(portalesEtecsa);
        }

        // POST: Portales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var portalesEtecsa = await _context.Portales.FindAsync(id);
            _context.Portales.Remove(portalesEtecsa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PortalesEtecsaExists(int id)
        {
            return _context.Portales.Any(e => e.Id == id);
        }
    }
}
