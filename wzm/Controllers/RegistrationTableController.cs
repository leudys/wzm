﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Data.Mikrotik;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RegistrationTableController : Controller
    {

        private bool _registrationTableIsRunning { get; set; }

        private readonly IWzmCore _wzmCore;
        ApplicationDbContext _db;
        public RegistrationTableController(IWzmCore wzmCore, ApplicationDbContext db)
        {
            _wzmCore = wzmCore;
            _db = db;
        }

        // GET: RegistrationTable
        public IActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }
            List<WirelessRegistrationModel> wireless = new List<WirelessRegistrationModel>();

            wireless = _wzmCore.MikrotikManager.Interface.Wireless.RegistrationTable.GetRergistrationTable(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true), wireless);
            return View(wireless);
        }

        [HttpGet]
        public IActionResult AjaxHandler()
        {

            List<WirelessRegistrationModel> wireless = new List<WirelessRegistrationModel>();

            wireless = _wzmCore.MikrotikManager.Interface.Wireless.RegistrationTable.GetRergistrationTable(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true), wireless);

            return PartialView("_RegistrationPartial", wireless.OrderBy(c => c.Id));
        }
    }
}