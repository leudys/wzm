﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Data.Mikrotik;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class WirelessController : Controller
    {

        ApplicationDbContext _db;

        private readonly IWzmCore _wzmCore;

        public WirelessController(ApplicationDbContext db, IWzmCore wzmCore)
        {
            _db = db;

            _wzmCore = wzmCore;
        }

        // GET: Wireless
        public IActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return RedirectToAction("Index", "MikrotikCredentials");
            }
            var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);

            var wireless = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWirelessViewModel(credential);
            wireless.OrderBy(c => c.Id);
            return View(wireless);
        }


        public IActionResult ConfigMikrotik()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ConfigMikrotik([Bind("RangoInicialDhcp,RangoFinalDhcp")]ConfigMikrotikModel configMikrotikViewModel)
        {
            var v = CredentialValidator.ValidateMKCrendential(_db);
            if (!v.Validate)
            {
                TempData["errorCredenciales"] = v.Message;

                return RedirectToAction("Index", "MikrotikCredentials");
            }
            try
            {
                int.Parse(configMikrotikViewModel.RangoInicialDhcp);
                int.Parse(configMikrotikViewModel.RangoFinalDhcp);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "El campo Rango Inicial y Rango Final deben ser números enteros.");
                return View(configMikrotikViewModel);
            }
            if (int.Parse(configMikrotikViewModel.RangoInicialDhcp) >= int.Parse(configMikrotikViewModel.RangoFinalDhcp))
            {
                ModelState.AddModelError("", "El campo Rango Inicial debe ser menor que el campo Rango Final.");
                return View(configMikrotikViewModel);
            }

            var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
            if (_wzmCore.MikrotikManager.ConfigMikrotik.ConfiguredToEtecsa(credential))
            {
                TempData["error"] = "Este dispositivo ya se encuentra configurado para WIFI_ETECSA";
                return RedirectToAction(nameof(Index));
            }
            else
            {

                string[] tp = credential.Dispositivo.Ip.Split('.');
                string ri = tp[0] + "." + tp[1] + "." + tp[2] + "." + configMikrotikViewModel.RangoInicialDhcp;
                string rf = tp[0] + "." + tp[1] + "." + tp[2] + "." + configMikrotikViewModel.RangoFinalDhcp;
                configMikrotikViewModel.RangoInicialDhcp = ri;
                configMikrotikViewModel.RangoFinalDhcp = rf;
                if (ModelState.IsValid)
                {

                    _wzmCore.MikrotikManager.ConfigMikrotik.Config(credential, configMikrotikViewModel);
                    TempData["success"] = "La configuración para WIFI_ETECSA se ha realizado con éxito.";
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(configMikrotikViewModel);
                }
            }


        }

        [HttpGet]
        public IActionResult DesableWlan(string id)
        {
            if (id == string.Empty || id == null)
            {
                return NotFound();
            }
            try
            {
                var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);

                _wzmCore.MikrotikManager.Interface.Wireless.DesableOrEnable(id, true, credential);
                TempData["success"] = "La " + id + " se ha deshabilitado correctamente";


            }
            catch (Exception e)
            {
                TempData["error"] = e.Message;
            }

            return RedirectToAction("Index");



        }

        [HttpGet]
        public IActionResult EnableWlan(string id)
        {
            if (id == string.Empty || id == null)
            {
                return NotFound();
            }
            try
            {
                var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);

                _wzmCore.MikrotikManager.Interface.Wireless.DesableOrEnable(id, false, credential);
                TempData["success"] = "La " + id + " se ha habilitado correctamente";



            }
            catch (Exception e)
            {

                TempData["error"] = e.Message;
            }

            return RedirectToAction("Index");

        }
    }
}