using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Interfaces;
using wzmData.Enum;
using wzmData.Models;

namespace wzm.Controllers
{
    public class HotSpotController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;
        public HotSpotController(ApplicationDbContext dbContext, IWzmCore wzmCore)
        {
            _context = dbContext;
            _wzmCore = wzmCore;
        }
        // GET: HotSpot
        public ActionResult Index()
        {
            var adminDevice = _context.AdminDevices.Include(c => c.DeviceCredential).FirstOrDefault(c => c.Fabricante == DeviceTypeEnum.Mikrotik);
           
            var users = _wzmCore.MikrotikManager.Ip.HotSpot.ListUsers((MikrotikCredentials)adminDevice.DeviceCredential.FirstOrDefault());
            return View(users);
        }

        // GET: HotSpot/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HotSpot/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HotSpot/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HotSpot/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HotSpot/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HotSpot/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HotSpot/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Active()
        {
            var adminDevice = _context.AdminDevices.Include(c => c.DeviceCredential).FirstOrDefault(c => c.Fabricante == DeviceTypeEnum.Mikrotik);           
            var users = _wzmCore.MikrotikManager.Ip.HotSpot.ListActiveUsers((MikrotikCredentials)adminDevice.DeviceCredential.FirstOrDefault());
            return View(users);
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              