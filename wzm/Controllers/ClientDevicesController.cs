﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzm.Extensions;
using wzm.Validations;
using wzm.ViewModels;
using wzmCore.Data;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Enum;
using wzmData.Models;
using wzmData.ViewModels;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ClientDevicesController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IWzmCore _wzmCore;
        private readonly UserManager<ApplicationUsers> _userManager;
        public ClientDevicesController(ApplicationDbContext applicationDbContext, IWzmCore wzmCore, UserManager<ApplicationUsers> userManager)
        {
            _db = applicationDbContext;
            _wzmCore = wzmCore;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients().Where(c => c.Estado == StatusEnum.Habilitado), "Id", "UserName");
            return View(_db.UsersDevices.ToList());
        }

        [HttpGet]
        public IActionResult GetDhcpServer(int id)
        {
            var adminDevice = _db.AdminDevices.Find(id);
            var roles = _db.AdminDevicesRoles.Include(c => c.DeviceRole).Where(c => c.AdminDeviceId.Equals(id)).ToList();

            if (roles.Any(c => c.DeviceRole.Role == Roles.DHCP))
            {
                if (_db.DeviceCredentials.Include(c => c.Dispositivo).Any(c => c.Dispositivo.Id.Equals(id)))
                {
                    var d = _db.DeviceCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Dispositivo.Id.Equals(id));

                    using (ITikConnection conection = ConnectionFactory.OpenConnection(d.Dispositivo.Firmware.GetApiVersion(), d.Dispositivo.Ip, d.User, d.Password))
                    {
                        ViewData["DhcpServer"] = new SelectList(conection.LoadAll<IpDhcpServer>().Where(c => c.Disabled == false), "Name", "Name");
                        return PartialView("_DhcpPartial");
                    }
                }
            }

            return PartialView("_DhcpPartial");
        }

        [HttpGet]
        public IActionResult SincWithDevice()
        {
            ViewData["Admin"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo");
            return View();
        }

        [HttpPost]
        public IActionResult SyncWithDevice(string ind)
        {
            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
            var cm = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(1));


            var devices = _db.UsersDevices.ToList();


            var sync = _wzmCore.UserManagement.SyncWithMkDevice(devices, cm, mikrotikCredential, false);


            foreach (var item in sync.ToUpdate)
            {
                _db.Update(item);
                _db.SaveChanges();
            }

            foreach (var item in sync.ToAdd)
            {
                _db.Add(item);
                _db.SaveChanges();
            }
            foreach (var item in sync.ToDelete)
            {
                _db.Remove(item);
                _db.SaveChanges();
            }

            return PartialView("SyncDevice", sync);
        }

        [HttpGet]
        public JsonResult UserServices(string id, string service)
        {

            var services = _db.ClientServices.Include(c => c.Service).Where(c => c.UserId.Equals(id)).ToList();

            if (service.Equals("internet"))
            {
                if (services.Any(c => c.Service.UserServiceType == UserServiceType.Internet))
                {
                    return Json(data: true);

                }
                else
                {
                    return Json(data: false);
                }
            }
            if (service.Equals("game"))
            {
                if (services.Any(c => c.Service.UserServiceType == UserServiceType.Games))
                {
                    return Json(data: true);

                }
                else
                {
                    return Json(data: false);
                }
            }
            return Json("ok");

        }

        [HttpGet]
        public IActionResult Create()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["error"] = _db.ValidateMKCrendential().Message;
                return RedirectToAction(nameof(Index));
            }
            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Admin"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo");
                ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute");
                ViewData["DhcpServer"] = new SelectList(conection.LoadAll<IpDhcpServer>().Where(c => c.Disabled == false), "Name", "Name");
                ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients().Where(c => c.DispositivoId != null), "Id", "UserName");
                return View();
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Mac,Ip,TipoIp,PublicIp,NationalNetwork,AddressList,DhcpServer,AdminDeviceId,DeviceConnectionType,ApplicationUserId,PortalDeOrigen,Comment")] UserDevice userDevice)
        {
            if (userDevice.TipoIp == IpTypeEnum.Fija)
            {
                if (userDevice.Ip == null)
                {
                    ModelState.AddModelError("", "El campo Ip es obligatorio.");

                }

            }
            if (userDevice.TipoIp == IpTypeEnum.Automatica)
            {
                if (userDevice.DhcpServer == null)
                {
                    ModelState.AddModelError("", "El campo Dhcp Server es obligatorio.");

                }
            }

            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["error"] = _db.ValidateMKCrendential().Message;
                return RedirectToAction(nameof(Index));
            }

            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
            var user = await _userManager.FindByIdAsync(userDevice.ApplicationUserId);
            var dispositivo = _db.Dispositivos.Include(c => c.DeviceCredential).FirstOrDefault(c => c.Id.Equals(user.DispositivoId));

            if (ModelState.IsValid)
            {
                DeviceOperations deviceOperation = new DeviceOperations();
                if (userDevice.AdminDeviceId != null)
                {
                    if (_db.AdminDevicesRoles.Include(c => c.DeviceRole).Where(c => c.AdminDeviceId.Equals(userDevice.AdminDeviceId)).Any(c => c.DeviceRole.Role == Roles.DHCP))
                    {
                        if (!_db.MikrotikCredentials.Any(c => c.DispositivoId.Equals(userDevice.AdminDeviceId)))
                        {
                            TempData["error"] = "El dispositivo Administrador seleccionado no posee credenciales.";
                            return RedirectToAction(nameof(Index));
                        }
                        else
                        {
                            var cm = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(userDevice.AdminDeviceId));
                            deviceOperation = _wzmCore.UserManagement.DeviceManager.AddDeviceIp(userDevice, cm);

                            if (userDevice.NationalNetwork)
                            {
                                var national = new DeviceToNationalNetwork()
                                {

                                    PublicIp = userDevice.PublicIp,
                                    UserDevice = userDevice
                                };
                                _wzmCore.UserManagement.DeviceManager.ToNationalNetwork(national, cm);
                            }

                            if (deviceOperation.Validation.Succeeded)
                            {
                                if (userDevice.PortalDeOrigen != null)
                                {
                                    deviceOperation = _wzmCore.UserManagement.DeviceManager.AddDeviceToFirewallAddressList(userDevice, mikrotikCredential);

                                }

                            }

                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "El dispositivo seleccionado no posee el rol de DHCP Server.");
                    }
                }
                else
                {
                    deviceOperation = _wzmCore.UserManagement.DeviceManager.AddDeviceToMikrotik(userDevice, mikrotikCredential);
                }


                if (deviceOperation.Validation.Succeeded)
                {
                    if (userDevice.DeviceConnectionType == DeviceConecctionType.Wifi)
                    {
                        if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti && (dispositivo.Platform.Contains("M2") || dispositivo.Platform.Contains("M5")))
                        {
                            if (dispositivo.WirelessSecurity == WirelessSecurity.ACL || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WPA)
                            {
                                var cu = _db.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(user.DispositivoId));
                                if (cu != null)
                                {
                                    if (cu is UbiquitiCredentials)
                                    {
                                        cu = cu as UbiquitiCredentials;

                                        var validation = await _wzmCore.UserManagement.DeviceManager.AddDeviceToUbiquitiAsync(userDevice, cu, _wzmCore.Random.Next());

                                        if (validation.Succeeded)
                                        {
                                            await _db.AddAsync(userDevice);
                                            await _db.SaveChangesAsync();

                                            if (userDevice.NationalNetwork)
                                            {
                                                var national = new DeviceToNationalNetwork()
                                                {
                                                    UserDeviceId = userDevice.Id,
                                                    PublicIp = userDevice.PublicIp,

                                                };

                                                await _db.AddAsync(national);
                                                await _db.SaveChangesAsync();
                                            }

                                            TempData["success"] = "Se ha añadido el dispositivo correctamente";
                                            return RedirectToAction("Index");
                                        }
                                        else
                                        {
                                            TempData["error"] = validation.ToString();
                                            return RedirectToAction("Index");
                                        }
                                    }
                                }
                                else
                                {
                                    ModelState.AddModelError("", $"El dispositivo {dispositivo.GetDispositivo} no posee credenciales.");
                                    using (ITikConnection conection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                                    {
                                        var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                                        ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute", userDevice.PortalDeOrigen);
                                        ViewData["DhcpServer"] = new SelectList(conection.LoadAll<IpDhcpServer>(), "Name", "Name", userDevice.DhcpServer);
                                        ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients().Where(c => c.DispositivoId != null), "Id", "UserName", userDevice.ApplicationUserId);
                                        return View(userDevice);
                                    }
                                }
                            }
                        }
                        else
                        {
                            await _db.AddAsync(userDevice);
                            await _db.SaveChangesAsync();
                            TempData["success"] = "Se ha añadido el dispositivo correctamente";
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        await _db.AddAsync(userDevice);
                        await _db.SaveChangesAsync();
                        if (userDevice.NationalNetwork)
                        {
                            var national = new DeviceToNationalNetwork()
                            {
                                UserDeviceId = userDevice.Id,
                                PublicIp = userDevice.PublicIp,

                            };

                            await _db.AddAsync(national);
                            await _db.SaveChangesAsync();
                        }
                        TempData["success"] = "Se ha añadido el dispositivo correctamente";
                        return RedirectToAction("Index");
                    }
                }
                ModelState.AddModelError("", deviceOperation.Validation.ToString());
            }


            using (ITikConnection conection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute", userDevice.PortalDeOrigen);
                ViewData["DhcpServer"] = new SelectList(conection.LoadAll<IpDhcpServer>(), "Name", "Name", userDevice.DhcpServer);
                ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients().Where(c => c.DispositivoId != null), "Id", "UserName", userDevice.ApplicationUserId);
                return View(userDevice);
            }

        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var dispositivo = _db.UsersDevices.FirstOrDefault(c => c.Id.Equals(id));

            if (dispositivo == null)
            {
                return NotFound();
            }

            if (dispositivo.ApplicationUserId != null)
            {
                ViewData["Usuarios"] = new SelectList(_userManager.Users, "Id", "UserName", dispositivo.ApplicationUserId);

                return View(dispositivo);
            }
            ViewData["Usuarios"] = new SelectList(_userManager.Users, "Id", "UserName");

            return View(dispositivo);




        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DeviceConnectionType,Ip,TipoIp,Comment,ApplicationUserId,DhcpServer,Mac,PortalDeOrigen,HasInternet,Disabled")] UserDevice dispositivo)
        {

            if (ModelState.IsValid)
            {
                _db.Update(dispositivo);
                await _db.SaveChangesAsync();
                TempData["success"] = "Dispositivo actualizado correctamente.";
                return RedirectToAction(nameof(Index));
            }
            ViewData["Usuarios"] = new SelectList(_userManager.Users, "Id", "UserName", dispositivo.ApplicationUserId);
            return View(dispositivo);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["error"] = _db.ValidateMKCrendential().Message;
                return RedirectToAction(nameof(Index));
            }

            var dispositivo = await _db.UsersDevices.FindAsync(id);

            if (dispositivo == null)
            {
                return NotFound();
            }



            return View(dispositivo);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dispositivo = await _db.UsersDevices.FindAsync(id);

            if (dispositivo.ApplicationUserId != null)
            {
                var user = await _userManager.FindByIdAsync(dispositivo.ApplicationUserId);

                if (user.DispositivoId != null)
                {
                    var userDevice = _db.Dispositivos.Find(user.DispositivoId);

                    if (dispositivo.DeviceConnectionType == DeviceConecctionType.Wifi)
                    {
                        if (userDevice.Fabricante == DeviceTypeEnum.Ubiquiti && (userDevice.Platform.Contains("M2") || userDevice.Platform.Contains("M5")))
                        {
                            if (userDevice.WirelessSecurity == WirelessSecurity.ACL || userDevice.WirelessSecurity == WirelessSecurity.ACL_WEP || userDevice.WirelessSecurity == WirelessSecurity.ACL_WPA)
                            {
                                if (_db.DeviceCredentials.Any(c => c.DispositivoId.Equals(userDevice.Id)))
                                {
                                    var credenciales = _db.DeviceCredentials.FirstOrDefault(c => c.DispositivoId.Equals(userDevice.Id)) as UbiquitiCredentials;

                                    await _wzmCore.UserManagement.DeviceManager.DeleteDeviceFromUbiquiti(dispositivo.Mac, credenciales, _wzmCore.Random.Next());
                                }
                            }
                        }
                    }
                }


            }
            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
            _wzmCore.UserManagement.DeviceManager.DeleteDeviceFromMikrotik(dispositivo.Mac, dispositivo.Ip, mikrotikCredential);
            _db.UsersDevices.Remove(dispositivo);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }

        [HttpGet]
        public async Task<IActionResult> DispositivosSinUsuario()
        {
            return View(await _db.UsersDevices.Where(c => c.ApplicationUserId == null).ToListAsync());
        }

        [HttpGet]
        public IActionResult ImportFromUbiquiti()
        {

            var dispositivos = _db.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap).ToList();
            ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo");
            //ViewData["Etecsa"]= _db..Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik && c.S)
            ViewData["Dispositivos"] = new SelectList(dispositivos.Where(c => c.WirelessSecurity == WirelessSecurity.ACL || c.WirelessSecurity == WirelessSecurity.ACL_WEP || c.WirelessSecurity == WirelessSecurity.ACL_WPA), "Id", "GetDispositivo");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ImportFromUbiquiti(ImportMultipleDeviceViewModel import)
        {
            var v = _db.ValidateMKCrendential();

            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction("Index");
            }
            var dispositvo = _db.Dispositivos.Find(import.DispositivoId);
            if (dispositvo != null)
            {
                if (!_db.DeviceCredentials.Any(c => c.DispositivoId.Equals(import.DispositivoId)))
                {
                    TempData["error"] = "El dispositivo seleccionado no posee credenciales.";
                    return RedirectToAction("Index");
                }
                if (import.AdminDeviceId != null)
                {
                    if (_db.AdminDevicesRoles.Any(c => c.AdminDeviceId.Equals(import.AdminDeviceId)))
                    {
                        if (_db.AdminDevicesRoles.Include(c => c.DeviceRole).Where(c => c.AdminDeviceId.Equals(import.AdminDeviceId)).Any(c => c.DeviceRole.Role == Roles.DHCP))
                        {
                            if (!_db.DeviceCredentials.Any(c => c.DispositivoId.Equals(import.AdminDeviceId)))
                            {
                                TempData["error"] = "El dispositivo seleccionado no posee credenciales.";
                                return RedirectToAction("Index");
                            }
                            var mikrotikEtecsa = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                            var adminDevice = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(import.AdminDeviceId));
                            var cred = _db.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(import.DispositivoId));
                            var dispositivosEnBd = _db.UsersDevices.ToList();
                            var userDevices = await _wzmCore.UserManagement.ImportFromUbiquitiAsync(mikrotikEtecsa, adminDevice, dispositivosEnBd, dispositvo, _db.Dispositivos.ToList(), _wzmCore.LicenseManager.GetLicenseType(), cred, _wzmCore.Random.Next());

                            if (userDevices.UserValidation.Succeeded)
                            {

                                if (userDevices.Dispositivos.Count > 0)
                                {

                                    int cantUsers = userDevices.Dispositivos.Count;
                                    int tempAdded = 0;

                                    foreach (var item in userDevices.Dispositivos)
                                    {
                                        await _db.AddAsync(item);
                                        tempAdded++;
                                        await _db.SaveChangesAsync();

                                    }


                                    TempData["success"] = $"Se han importado {tempAdded} dispositivos.";
                                    return RedirectToAction("Index");


                                }
                                else
                                {
                                    TempData["success"] = "Todos los dispositivos de este Dispositivo ya se encuentran en el sistema.";
                                    return RedirectToAction("Index");
                                }
                            }

                            else
                            {
                                TempData["error"] = userDevices.UserValidation.ToString();
                                return RedirectToAction("Index");
                            }

                        }
                        else
                        {
                            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                            var cred = _db.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(import.DispositivoId));
                            var dispositivosEnBd = _db.UsersDevices.ToList();
                            var userDevices = await _wzmCore.UserManagement.ImportFromUbiquitiAsync(mikrotikCredential, dispositivosEnBd, dispositvo, _db.Dispositivos.ToList(), _wzmCore.LicenseManager.GetLicenseType(), cred, _wzmCore.Random.Next());

                            if (userDevices.UserValidation.Succeeded)
                            {

                                if (userDevices.Dispositivos.Count > 0)
                                {

                                    int cantUsers = userDevices.Dispositivos.Count;
                                    int tempAdded = 0;

                                    foreach (var item in userDevices.Dispositivos)
                                    {
                                        await _db.AddAsync(item);
                                        tempAdded++;
                                        await _db.SaveChangesAsync();

                                    }
                                    TempData["success"] = $"Se han importado {tempAdded} dispositivos.";
                                    return RedirectToAction("Index");


                                }
                                else
                                {
                                    TempData["success"] = "Todos los dispositivos de este Dispositivo ya se encuentran en el sistema.";
                                    return RedirectToAction("Index");
                                }
                            }

                            else
                            {
                                TempData["error"] = userDevices.UserValidation.ToString();
                                return RedirectToAction("Index");
                            }
                        }
                    }
                    else
                    {
                        var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                        var cred = _db.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(import.DispositivoId));
                        var dispositivosEnBd = _db.UsersDevices.ToList();
                        var userDevices = await _wzmCore.UserManagement.ImportFromUbiquitiAsync(mikrotikCredential, dispositivosEnBd, dispositvo, _db.Dispositivos.ToList(), _wzmCore.LicenseManager.GetLicenseType(), cred, _wzmCore.Random.Next());

                        if (userDevices.UserValidation.Succeeded)
                        {

                            if (userDevices.Dispositivos.Count > 0)
                            {

                                int cantUsers = userDevices.Dispositivos.Count;
                                int tempAdded = 0;

                                foreach (var item in userDevices.Dispositivos)
                                {
                                    await _db.AddAsync(item);
                                    tempAdded++;
                                    await _db.SaveChangesAsync();

                                }
                                TempData["success"] = $"Se han importado {tempAdded} dispositivos.";
                                return RedirectToAction("Index");


                            }
                            else
                            {
                                TempData["success"] = "Todos los dispositivos de este Dispositivo ya se encuentran en el sistema.";
                                return RedirectToAction("Index");
                            }
                        }

                        else
                        {
                            TempData["error"] = userDevices.UserValidation.ToString();
                            return RedirectToAction("Index");
                        }
                    }



                }
                else
                {
                    var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                    var cred = _db.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(import.DispositivoId));
                    var dispositivosEnBd = _db.UsersDevices.ToList();
                    var userDevices = await _wzmCore.UserManagement.ImportFromUbiquitiAsync(mikrotikCredential, dispositivosEnBd, dispositvo, _db.Dispositivos.ToList(), _wzmCore.LicenseManager.GetLicenseType(), cred, _wzmCore.Random.Next());

                    if (userDevices.UserValidation.Succeeded)
                    {

                        if (userDevices.Dispositivos.Count > 0)
                        {

                            int cantUsers = userDevices.Dispositivos.Count;
                            int tempAdded = 0;

                            foreach (var item in userDevices.Dispositivos)
                            {
                                await _db.AddAsync(item);
                                tempAdded++;
                                await _db.SaveChangesAsync();

                            }
                            TempData["success"] = $"Se han importado {tempAdded} dispositivos.";
                            return RedirectToAction("Index");


                        }
                        else
                        {
                            TempData["success"] = "Todos los dispositivos de este Dispositivo ya se encuentran en el sistema.";
                            return RedirectToAction("Index");
                        }
                    }

                    else
                    {
                        TempData["error"] = userDevices.UserValidation.ToString();
                        return RedirectToAction("Index");
                    }
                }
            }
            else
            {
                return NoContent();
            }
        }



        [HttpGet]
        public IActionResult ImportFromMikrotik()
        {
            ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo");
            ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients(), "Id", "UserName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ImportFromMikrotik(ImportDeviceViewModel deviceViewModel)
        {
            if (_db.UsersDevices.Any(c => c.Mac.Contains(deviceViewModel.Mac)))
            {
                ModelState.AddModelError("", "La mac del dispositivo introducida ya se encuentra en el sistema.");
            }

            var v = _db.ValidateMKCrendential();
            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction(nameof(Index));
            }


            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);


            if (ModelState.IsValid)
            {
                if (deviceViewModel.AdminDeviceId != null)
                {
                    if (_db.MikrotikCredentials.Any(c => c.DispositivoId.Equals(deviceViewModel.AdminDeviceId)))
                    {
                        if (_db.AdminDevicesRoles.Include(c => c.DeviceRole).Where(c => c.AdminDeviceId.Equals(deviceViewModel.AdminDeviceId)).Any(c => c.DeviceRole.Role == Roles.DHCP))
                        {
                            var adminDevice = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(deviceViewModel.AdminDeviceId));

                            if (_wzmCore.MikrotikManager.CanConnectToMikrotik(adminDevice))
                            {
                                var importUser = _wzmCore.UserManagement.DeviceManager.ImportDeviceFromMikrotik(mikrotikCredential, adminDevice, deviceViewModel.DeviceConecctionType, deviceViewModel.UserId, deviceViewModel.Mac);
                                if (importUser.Validation.Succeeded)
                                {
                                    importUser.Device.AdminDeviceId = deviceViewModel.AdminDeviceId;
                                    _db.Add(importUser.Device);
                                    await _db.SaveChangesAsync();
                                    TempData["success"] = "Dispositivo importado correctamente";
                                    return RedirectToAction(nameof(Index));
                                }
                                else
                                {
                                    TempData["error"] = importUser.Validation.ToString();
                                    return RedirectToAction(nameof(Index));
                                }
                            }
                            else
                            {
                                TempData["error"] = "No se puede establecer conexión con el dispositivo administrador.";
                                return RedirectToAction(nameof(Index));
                            }


                        }
                        else
                        {
                            var importedUser = _wzmCore.UserManagement.DeviceManager.ImportDeviceFromMikrotik(mikrotikCredential, deviceViewModel.DeviceConecctionType, deviceViewModel.UserId, deviceViewModel.Mac);

                            if (importedUser.Validation.Succeeded)
                            {
                                importedUser.Device.AdminDeviceId = deviceViewModel.AdminDeviceId;
                                _db.Add(importedUser.Device);
                                await _db.SaveChangesAsync();
                                TempData["success"] = "Dispositivo importado correctamente";
                                return RedirectToAction(nameof(Index));
                            }
                            else
                            {
                                TempData["error"] = importedUser.Validation.ToString();
                                return RedirectToAction(nameof(Index));
                            }
                        }
                    }
                    else
                    {
                        TempData["error"] = "El Dispositivo Administrador no posee credenciales";
                        ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo", deviceViewModel.AdminDeviceId);
                        ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients(), "Id", "UserName", deviceViewModel.UserId);
                        return View(deviceViewModel);
                    }

                }
                else
                {
                    var importedUser = _wzmCore.UserManagement.DeviceManager.ImportDeviceFromMikrotik(mikrotikCredential, deviceViewModel.DeviceConecctionType, deviceViewModel.Mac, deviceViewModel.UserId);

                    if (importedUser.Validation.Succeeded)
                    {
                        _db.Add(importedUser.Device);
                        await _db.SaveChangesAsync();
                        TempData["success"] = "Dispositivo importado correctamente";
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        TempData["error"] = importedUser.Validation.ToString();
                        return RedirectToAction(nameof(Index));
                    }
                }

            }
            else
            {
                ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo", deviceViewModel.AdminDeviceId);
                ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients(), "Id", "UserName", deviceViewModel.UserId);
                return View(deviceViewModel);
            }


        }

        [HttpGet]
        public IActionResult ImportFromMikrotikByIp()
        {
            ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo");
            ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients(), "Id", "UserName");
            return View("ImportFromMikrotikIP");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ImportFromMikrotikByIp(ImportDevice_IP_ViewModel deviceViewModel)
        {
            if (_db.UsersDevices.Any(c => c.Mac.Contains(deviceViewModel.IP)))
            {
                ModelState.AddModelError("", "El IP del dispositivo introducida ya se encuentra en el sistema.");
            }

            var v = _db.ValidateMKCrendential();
            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction(nameof(Index));
            }


            var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);


            if (ModelState.IsValid)
            {
                if (deviceViewModel.AdminDeviceId != null)
                {
                    if (_db.MikrotikCredentials.Any(c => c.DispositivoId.Equals(deviceViewModel.AdminDeviceId)))
                    {
                        if (_db.AdminDevicesRoles.Include(c => c.DeviceRole).Where(c => c.AdminDeviceId.Equals(deviceViewModel.AdminDeviceId)).Any(c => c.DeviceRole.Role == Roles.DHCP))
                        {
                            var adminDevice = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(deviceViewModel.AdminDeviceId));

                            if (_wzmCore.MikrotikManager.CanConnectToMikrotik(adminDevice))
                            {
                                var importUser = _wzmCore.UserManagement.DeviceManager.ImportDeviceFromMikrotik(mikrotikCredential, adminDevice, deviceViewModel.DeviceConecctionType, deviceViewModel.UserId, null,deviceViewModel.IP);
                                if (importUser.Validation.Succeeded)
                                {
                                    importUser.Device.AdminDeviceId = deviceViewModel.AdminDeviceId;
                                    _db.Add(importUser.Device);
                                    await _db.SaveChangesAsync();
                                    TempData["success"] = "Dispositivo importado correctamente";
                                    return RedirectToAction(nameof(Index));
                                }
                                else
                                {
                                    TempData["error"] = importUser.Validation.ToString();
                                    return RedirectToAction(nameof(Index));
                                }
                            }
                            else
                            {
                                TempData["error"] = "No se puede establecer conexión con el dispositivo administrador.";
                                return RedirectToAction(nameof(Index));
                            }


                        }
                        else
                        {
                            var importedUser = _wzmCore.UserManagement.DeviceManager.ImportDeviceFromMikrotik(mikrotikCredential, deviceViewModel.DeviceConecctionType, deviceViewModel.UserId, null, deviceViewModel.IP);

                            if (importedUser.Validation.Succeeded)
                            {
                                importedUser.Device.AdminDeviceId = deviceViewModel.AdminDeviceId;
                                _db.Add(importedUser.Device);
                                await _db.SaveChangesAsync();
                                TempData["success"] = "Dispositivo importado correctamente";
                                return RedirectToAction(nameof(Index));
                            }
                            else
                            {
                                TempData["error"] = importedUser.Validation.ToString();
                                return RedirectToAction(nameof(Index));
                            }
                        }
                    }
                    else
                    {
                        TempData["error"] = "El Dispositivo Administrador no posee credenciales";
                        ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo", deviceViewModel.AdminDeviceId);
                        ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients(), "Id", "UserName", deviceViewModel.UserId);
                        return View(deviceViewModel);
                    }

                }
                else
                {
                    var importedUser = _wzmCore.UserManagement.DeviceManager.ImportDeviceFromMikrotik(mikrotikCredential, deviceViewModel.DeviceConecctionType, deviceViewModel.UserId,null, deviceViewModel.IP);

                    if (importedUser.Validation.Succeeded)
                    {
                        _db.Add(importedUser.Device);
                        await _db.SaveChangesAsync();
                        TempData["success"] = "Dispositivo importado correctamente";
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        TempData["error"] = importedUser.Validation.ToString();
                        return RedirectToAction(nameof(Index));
                    }
                }

            }
            else
            {
                ViewData["AdminDevice"] = new SelectList(_db.AdminDevices.Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik), "Id", "GetDispositivo", deviceViewModel.AdminDeviceId);
                ViewData["Usuarios"] = new SelectList(_db.GetApplicationClients(), "Id", "UserName", deviceViewModel.UserId);
                return View(deviceViewModel);
            }


        }

        [HttpGet]
        public async Task<IActionResult> ChangeDeviceIp(int id)
        {

            var dispositivo = await _db.UsersDevices.FindAsync(id);
            if (dispositivo == null)
            {
                return NotFound();
            }
            var v = _db.ValidateMKCrendential();

            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction(nameof(Index));
            }

            var mk = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
            using (var connection = ConnectionFactory.OpenConnection(mk.Dispositivo.Firmware.GetApiVersion(), mk.Dispositivo.Ip, mk.User, mk.Password))
            {
                ViewData["DhcpServer"] = new SelectList(connection.LoadAll<IpDhcpServer>(), "Name", "Name");
                return View("Device", new DeviceViewModel() { Id = dispositivo.Id, UserId = dispositivo.ApplicationUserId, Ip = dispositivo.Ip, TipoIp = dispositivo.TipoIp });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeDeviceIp([Bind("UserId,TipoIp,DhcpServer,Ip")] DeviceViewModel clientIpViewModel)
        {
            var u = await _db.UsersDevices.FindAsync(clientIpViewModel.Id);
            MikrotikCredentials mikrotikCredentials = new MikrotikCredentials();
            if (clientIpViewModel.TipoIp == IpTypeEnum.Fija)
            {
                if (clientIpViewModel.Ip == null)
                {
                    ModelState.AddModelError("", "El campo Ip es obligatorio.");
                }
                if (!RemoteHelper.ValidateIpAddress(clientIpViewModel.Ip, null, _db, _wzmCore))
                {
                    ModelState.AddModelError("", "No se ha podido validar el Ip introducido.");
                }

            }
            var validator = CredentialValidator.ValidateMKCrendential(_db);

            if (!validator.Validate)
            {
                TempData["error"] = validator.Message;
                return RedirectToAction(nameof(Index));
            }
            else
            {
                mikrotikCredentials = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
            }


            if (clientIpViewModel.TipoIp == IpTypeEnum.Automatica)
            {

                if (clientIpViewModel.DhcpServer == null)
                {
                    ModelState.AddModelError("", "El campo Dhcp Server es obligatorio.");
                }
            }
            if (ModelState.IsValid)
            {

                var userOperation = _wzmCore.UserManagement.DeviceManager.UpdateDeviceIp(u, clientIpViewModel, mikrotikCredentials);

                if (userOperation.Validation.Succeeded)
                {
                    _db.Update(userOperation.Device);
                    await _db.SaveChangesAsync();
                    TempData["success"] = "La operación se ha realizado correctamente.";
                    return RedirectToAction(nameof(Index));

                }
                else
                {
                    TempData["error"] = userOperation.Validation.ToString();
                    return RedirectToAction(nameof(Index));
                }
            }
            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                ViewData["DhcpServer"] = new SelectList(connection.LoadAll<IpDhcpServer>(), "Name", "Name", clientIpViewModel.DhcpServer);
                return View(clientIpViewModel);
            }

        }

        /// <summary>
        /// Cambia el dispositivo del usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ChangeClientDevice(int id)
        {

            var device = await _db.UsersDevices.FindAsync(id);
            if (device == null)
            {
                return NotFound();
            }



            return View(new ChangeUserDevice() { Id = device.Id, UserId = device.ApplicationUserId, OldMac = device.Mac });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeClientDevice([Bind("UserId,OldMac,NewMac")] ChangeUserDevice changedDevice)
        {

            var device = _db.UsersDevices.Include(c => c.ApplicationUser).ThenInclude(c => c.Dispositivo).FirstOrDefault(c => c.Id.Equals(changedDevice.Id));

            var m = _db.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);

            var uc = _db.UbiquitiCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.DispositivoId == device.ApplicationUser.Dispositivo.Id);

            if (device.TipoIp == IpTypeEnum.Automatica)
            {

                if (await _wzmCore.UserManagement.ChangeClientDeviceDynamicIpAsync(changedDevice, device, device.ApplicationUser.Dispositivo, m, uc, _wzmCore.Random.Next()))
                {
                    device.Mac = changedDevice.NewMac;
                    _db.Update(device);
                    await _db.SaveChangesAsync();
                    TempData["success"] = "El dispositivo se ha cambiado correctamente";
                    return RedirectToAction(nameof(Index));
                }
                TempData["error"] = "No se ha podido cambiar el dispositivo del cliente";
                return RedirectToAction(nameof(Index));


            }
            //else
            //{
            //    if (user.UserConnectionType == UserConecctionType.Wifi)
            //    {
            //        //// Not working yet
            //        //if (_wzmCore.UserManagement.ChangeClientDeviceStaticIpAsync(changeUserDevice.OldMac, true, changeUserDevice.NewMac, m, uc))
            //        //{
            //        await _userManager.UpdateAsync(user);
            //        TempData["success"] = "El dispositivo del cliente se ha cambiado correctamente";
            //        return RedirectToAction(nameof(Index));
            //        //}
            //        //TempData["error"] = "No se ha podido cambiar el dispositivo del cliente";
            //        //return RedirectToAction(nameof(Index));
            //    }
            //    else
            //    {
            //        //if (await _wzmCore.UserManagement.ChangeClientDeviceStaticIpAsync(changeUserDevice.OldMac, false, changeUserDevice.NewMac, m, uc))
            //        //{
            //        await _userManager.UpdateAsync(user);
            //        TempData["success"] = "El dispositivo del cliente se ha cambiado correctamente";
            //        return RedirectToAction(nameof(Index));
            //        //}
            //        //TempData["error"] = "No se ha podido cambiar el dispositivo del cliente";
            //        //return RedirectToAction(nameof(Index));
            //    }
            //}
            return RedirectToAction(nameof(Index));
        }


        [HttpGet]
        public async Task<IActionResult> ChangePortal(int id)
        {
            var d = await _db.UsersDevices.FindAsync(id);

            if (d.ApplicationUserId == null)
            {
                TempData["error"] = "El dispositivo seleccionado, no posee ningún usuario, por favor, asígnele uno.";
                return RedirectToAction(nameof(Index));
            }

            var v = _db.ValidateMKCrendential();
            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction(nameof(Index));
            }

            var c = _db.MikrotikCredentials.Include(e => e.Dispositivo).First(e => e.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["UserName"] = d.Comment;
                ViewData["PortalActual"] = d.PortalDeOrigen;
                ViewData["UserId"] = id;
                ViewData["Salida"] = new SelectList(routes.Where(e => e.MarkRoute != d.PortalDeOrigen), "MarkRoute", "MarkRoute");

                return View(d);
            }
        }

        [HttpPost, ActionName("ChangePortal")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePortal(int id, string salida)
        {
            try
            {
                var c = _db.MikrotikCredentials.Include(e => e.Dispositivo).First(e => e.Selected == true);

                var d = await _db.UsersDevices.FindAsync(id);

                var userResult = _wzmCore.UserManagement.ChangeDeviceToAnotherPortal(d, c, salida);

                if (userResult.Succeeded)
                {
                    d.PortalDeOrigen = salida;

                    _db.Update(d);
                    _db.SaveChanges();

                    TempData["success"] = "El dispositivo ha sido cambiado de portal correctamente";
                    return RedirectToAction(nameof(Index));

                }
                else
                {
                    TempData["error"] = userResult.Errors;
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception e)
            {
                TempData["error"] = e.Message;
                return RedirectToAction(nameof(Index));
            }


        }

        [HttpGet]
        public IActionResult CantidadUsuarios(string id)
        {
            var applicationUsers = _db.UsersDevices.Where(c => c.PortalDeOrigen.Contains(id));

            ViewData["CantidadUsuarios"] = $"El portal {id} contiene {applicationUsers.Count()} usuarios.";

            return PartialView("_CantidadUsuariosPortal", applicationUsers);
        }

        /// <summary>
        /// Obtiene los usuarios existentes en un dispositivo
        /// </summary>
        /// <param name="dispositivoId"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetDevicesByUser(string userId)
        {
            List<UserDevice> dispositivos = _db.UsersDevices.Where(c => c.ApplicationUserId.Equals(userId)).ToList();

            return PartialView("_DevicesPartial", dispositivos);
        }
    }
}
