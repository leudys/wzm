﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects.Queue;
using wzm.Extensions;
using wzm.Validations;
using wzmCore.Interfaces;
using wzmCore.Mikrotik.Configuration;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    
    public class ConfConexPriorityController : Controller
    {
        ApplicationDbContext _db;

        private readonly IWzmCore _wzmCore;

        public ConfConexPriorityController(ApplicationDbContext db, IWzmCore wzmCore)
        {
            _db = db;
            _wzmCore = wzmCore;
        }

        // GET: ConfConexPriority
        public IActionResult Index()
        {
           
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }


            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);


            var qTree = _wzmCore.MikrotikManager.QueueTree.GetAllQueueTree(c);

            var qTrees = new List<QueueTree>();

            var parentsUpload = new List<QueueTree>();
            var parentsDownload = new List<QueueTree>();
            var parents = new List<QueueTree>();

            //Obtengo las interfaces
            var interfaces = _wzmCore.MikrotikManager.Interface.GetInterfaces(c);

            // Obtengo los parents de las interfaces
            foreach (var item in interfaces)
            {
                if (qTree.Any(d => d.Parent.Equals(item.Name)))
                {
                    parentsUpload.AddRange(qTree.Where(d => d.Parent.Equals(item.Name) && d.Queue.Equals("Subida")));
                    parentsDownload.AddRange(qTree.Where(d => d.Parent.Equals(item.Name) && d.Queue.Equals("Bajada")));
                }
            }

            foreach (var item in parentsUpload)
            {

                qTrees.AddRange(qTree.Where(d => d.Parent.Equals(item.Name)));
            }

            foreach (var item in parentsDownload)
            {
                qTrees.AddRange(qTree.Where(d => d.Parent.Equals(item.Name)));
            }

            if (parentsDownload.Count == parentsUpload.Count)
            {
                for (int i = 0; i < parentsDownload.Count; i++)
                {
                    parents.Add(parentsDownload[i]);
                    parents.Add(parentsUpload[i]);

                }
            }



            ViewData["Parents"] = parents;
            ViewData["QTrees"] = qTrees;

            return View();

        }

        public ActionResult Edit(string id)
        {
            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);
            var qTree = _wzmCore.MikrotikManager.QueueTree.GetAllQueueTree(c);

            if (!qTree.Any(d => d.Id.Equals(id)))
            {
                return NotFound();
            }
            else
            {
                if (ModelState.IsValid)
                {

                }
            }
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind("id,MaxLimit,Priority,MaxLimit")] QueueTree queueTree)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(string id)
        {
            try
            {
                new QoS().DeleteQoSfromPortal(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected), id.Split(' ')[0]);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                ViewData["error"] = e.Message;
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        public IActionResult ConfAutomatica()
        {
            var c = _db.MikrotikCredentials.Include(s => s.Dispositivo).FirstOrDefault(d => d.Selected);

            using (var connection = ConnectionFactory.OpenConnection(TikConnectionType.Api, c.Dispositivo.Ip, c.User, c.Password))
            {
                try
                {
                    _wzmCore.MikrotikManager.Firewall.Layer7.PriorityLayer7(c);
                    _wzmCore.MikrotikManager.QueueType.FirstConfig(c);
                    _wzmCore.MikrotikManager.Firewall.Mangle.PriorityMangle(c);
                    _wzmCore.MikrotikManager.QueueTree.FirstConfig(c);

                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                    return RedirectToAction(nameof(Index));
                }
                TempData["success"] = "La operación se ha realizado correctamente";
                return RedirectToAction(nameof(Index));
            }


        }
    }
}

