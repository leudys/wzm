﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using tik4net.Objects.Ip.DhcpServer;
using wzm.Extensions;
using wzm.Validations;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DhcpServerLeasesController : Controller
    {

        ApplicationDbContext _db;
        ILogger<DhcpServerLeasesController> _loger;
        private readonly IWzmCore _wzmCore;

        public DhcpServerLeasesController(ApplicationDbContext db, IWzmCore wzmCore,ILogger<DhcpServerLeasesController> logger)
        {
            _db = db;

            _wzmCore = wzmCore;

            _loger = logger;
        }

        // GET: DhcpServerLeases
        public IActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }

            IEnumerable<DhcpServerLease> d = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(_db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true)).ToList();

            return View(d.OrderBy(c => c.Status == "bound").ToList());


        }




        // GET: DhcpServerLeases/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null || id == string.Empty)
            {
                return NotFound();
            }
            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);

            var l = _wzmCore.MikrotikManager.Ip.DhcpServer.GetLeaseById(c, id);
            return View(l);
        }

        // POST: DhcpServerLeases/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, [Bind("Address,LeaseTime,AlwaysBroadcast,MacAddress,Id,SrcMacAddress,ClientId,BlockAccess")]DhcpServerLease lease)
        {
            try
            {
                var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);
                var df = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(c);
                var vb = df.FirstOrDefault(h => h.Id == id);
                vb.Address = lease.Address;
                //if (lease.LeaseTime.Seconds == 0 & lease.LeaseTime.Minutes == 0 && lease.LeaseTime.Hours == 0)
                //{
                //    vb.LeaseTime = new TimeSpan(00, 00, 00);
                //}
                //else
                //{
                vb.LeaseTime = lease.LeaseTime;
                //}

                vb.AlwaysBroadcast = lease.AlwaysBroadcast;
                vb.MacAddress = lease.MacAddress;
                vb.SrcMacAddress = lease.SrcMacAddress;
                vb.BlockAccess = lease.BlockAccess;
                vb.ClientId = lease.ClientId;
                using (var connection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
                {

                    connection.Save(vb);

                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception)
            {
                return View(lease);
            }
        }


        public IActionResult SaveToDatabase()
        {
            //var mikrotikCredential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected == true);

            //using (ITikConnection connection = ConnectionFactory.OpenConnection(TikConnectionType.Api, mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            //{
            //    IEnumerable<DhcpServerLease> d = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(connection).ToList();
            //    List<IpPool> ipPool = _wzmCore.MikrotikManager.Ip.Pool.GetIpPools(connection);
            //    List<IpDhcpServer> ipDhcpServers = (List<IpDhcpServer>)connection.LoadAll<IpDhcpServer>();
            //    DeleteFromDatabse();

            //    foreach (var item in ipPool)
            //    {
            //        _db.Add(item);
            //        _db.SaveChanges();
            //    }

            //    foreach (var item in ipDhcpServers)
            //    {
            //        _db.IpDhcpServers.Add(item);
            //        _db.SaveChanges();
            //    }

            //    foreach (var item in d)
            //    {
            //        _db.DhcpServerLease.Add(item);
            //        _db.SaveChanges();
            //    }
            //}


            return RedirectToAction(nameof(Index));
        }

        private void DeleteFromDatabse()
        {
            //var sl = _db.DhcpServerLease.ToList();
            //foreach (var item in sl)
            //{
            //    _db.Remove(item);
            //    _db.SaveChanges();
            //}

            //var dhcpservers = _db.IpDhcpServers.ToList();
            //foreach (var item in dhcpservers)
            //{
            //    _db.Remove(item);
            //    _db.SaveChanges();
            //}

            //var IpPools = _db.IpPools.ToList();
            //foreach (var item in IpPools)
            //{
            //    _db.Remove(item);
            //    _db.SaveChanges();
            //}
        }



        public IActionResult RestoreToDevice()
        {
            var cr = _db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true);

            using (ITikConnection connection = ConnectionFactory.OpenConnection(cr.Dispositivo.Firmware.GetApiVersion(), cr.Dispositivo.Ip, cr.User, cr.Password))
            {
                List<DhcpServerLease> d = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(cr).ToList();
                //var dhcpNetwork = _wzmCore.MikrotikManager.Ip.DhcpServer.GetDhcpServerNetworks(connection);
                DeleteFromDevice(d, cr);
                ImportToDevice(cr);
                d = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(cr).ToList();
            }


            return RedirectToAction(nameof(Index));
        }

        private void ImportToDevice(MikrotikCredentials cr)
        {
            //var l = _db.DhcpServerLease.ToList();
            //foreach (var item in l)
            //{
            //    try
            //    {
            //        _wzmCore.MikrotikManager.Ip.DhcpServer.AddLease(cr, item);
            //    }
            //    catch (Exception e)
            //    {
            //        _loger.LogCritical(e, "Error importando DHCP Server Lease");       
            //    }

            //}
        }

        /// <summary>
        /// Elimina los Lease del DhcpServer
        /// </summary>
        /// <param name="d"></param>
        /// <param name="cr"></param>
        private void DeleteFromDevice(List<DhcpServerLease> d, MikrotikCredentials cr)
        {
            if (d.Count > 0)
            {
                foreach (var item in d)
                {
                    _wzmCore.MikrotikManager.Ip.DhcpServer.DeleteLease(cr, item);
                }
            }
        }
    }
}