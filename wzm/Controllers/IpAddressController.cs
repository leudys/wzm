﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net.Objects.Ip;
using wzm.Extensions;
using wzm.Validations;
using wzm.ViewModels;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class IpAddressController : Controller
    {

        private bool _addressIsRunning { get; set; }

        private readonly IWzmCore _wzmCore;
        ApplicationDbContext _db;

        public IpAddressController(IWzmCore mikrotikManager, ApplicationDbContext db)
        {
            _wzmCore = mikrotikManager;
            _db = db;
        }
        // GET: IpAddress
        public async Task<IActionResult> Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }
            List<IpAddress> address = new List<IpAddress>();

            await _wzmCore.MikrotikManager.Ip.Address.RunCommand(() => _addressIsRunning, async () =>
            {
                await Task.Run(() =>
                {
                    address = _wzmCore.MikrotikManager.Ip.Address.GetIpAddress(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true));
                });

            });


            return View(address);
        }

        // GET: IpAddress/Create
        public ActionResult Create()
        {
            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);

            var interfaces = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWirelessViewModel(c);
            interfaces.OrderBy(d => d.Id);
            ViewData["Interfaces"] = new SelectList(interfaces, "Id", "Name");
            return View();


        }

        // POST: IpAddress/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Ip", "Interface")]IpAddressViewModel interfaces)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    IpAddress address = new IpAddress
                    {
                        Address = interfaces.Ip,
                        Interface = interfaces.Interface,
                    };
                    _wzmCore.MikrotikManager.Ip.Address.AddAddress(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true), address);
                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return View();
                }
            }
            else
            {
                var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);
                var inter = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWirelessViewModel(c);
                inter.OrderBy(d => d.Id);
                ViewData["Interfaces"] = new SelectList(inter, "Id", "Name", interfaces.Interface);
                return View(interfaces);



            }

        }

        // GET: IpAddress/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: IpAddress/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: IpAddress/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: IpAddress/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}