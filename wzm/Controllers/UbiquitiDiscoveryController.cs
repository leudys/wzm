﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Models;
using wzmData.Enum;
using wzmCore.Services;
using wzmCore.Services.System;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UbiquitiDiscoveryController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly ScanJob _scanJob;
        public UbiquitiDiscoveryController(ApplicationDbContext context, ScanJob scanJob)
        {

            _context = context;

            _scanJob = scanJob;
        }

        // GET: UbiquitiDiscovery
        public IActionResult Index()
        {

            return View();
        }


        public async Task<IActionResult> Import(string id)
        {
            var device = _scanJob.Devices.FirstOrDefault(c => c.FormatedMacAddress == id);

            var dispostivos = _context.Dispositivos.Where(c => c.Fabricante == DeviceTypeEnum.Ubiquiti);

            if (dispostivos.Any(c => c.Ip.Equals(device.IpAddress)))
            {
                TempData["error"] = $"Ya existe un dispositivo con el ip {device.IpAddress}";
                return RedirectToAction(nameof(Index));
            }
            if (dispostivos.Any(c => c.Mac.Equals(device.FormatedMacAddress)))
            {
                TempData["error"] = $"Ya existe un dispositivo con la mac {device.FormatedMacAddress}";
                return RedirectToAction(nameof(Index));
            }


            if (device.WirelessModeDescription.Equals("Station"))
            {
                var station = new StationDevice()
                {
                    Firmware = device.Firmware,
                    SSID = device.SSID,
                    Hostname = device.Hostname,
                    Ip = device.IpAddress,
                    Mac = device.FormatedMacAddress,
                    Platform = device.LongPlatform,
                    WirelessMode = WirelessMode.Station,
                };
                _context.Add(station);
                await _context.SaveChangesAsync();
                TempData["success"] = "Dispositivo añadido correctamente";
                return RedirectToAction(nameof(Index));

            }
            if (device.WirelessModeDescription == "AP")
            {
                var ap = new ApDevice()
                {
                    Firmware = device.Firmware,
                    SSID = device.SSID,
                    Hostname = device.Hostname,
                    Ip = device.IpAddress,
                    Mac = device.FormatedMacAddress,
                    Platform = device.LongPlatform,
                    WirelessMode = WirelessMode.Ap,
                };
                _context.Add(ap);
                await _context.SaveChangesAsync();
                TempData["success"] = "Dispositivo añadido correctamente";
                return RedirectToAction(nameof(Index));
            }
            TempData["error"] = "No se ha importado el dispositivo, porque su modo de operación en la Wireless no es compatible con WZM";
            return RedirectToAction(nameof(Index));


        }

        public async Task<IActionResult> GetDevices()
        {

            var s = _scanJob.Devices;
            if (s != null)
            {
                var savedDevices = await _context.Dispositivos.ToListAsync();

                if (savedDevices.Count > 0)
                {
                    foreach (var item in s)
                    {
                        if (savedDevices.Any(c => c.Mac.Equals(item.FormatedMacAddress)))
                        {
                            item.Imported = true;
                        }
                        else
                        {
                            item.Imported = false;
                        }
                    }
                    return View("_DevicesPartial", s.Where(c => !c.Imported));
                }
                else
                {
                    List<Dispositivo> dispositivos = new List<Dispositivo>();

                    //foreach (var item in s)
                    //{
                    //    dispositivos.Add(new Dispositivo() { Firmware = item.Firmware, Hostname = item.Hostname, Mac = item.FormatedMacAddress, Platform = item.Platform, SSID = item.SSID });
                    //}
                    return View("_DevicesPartial", s);
                }

            }

            return Ok();
        }
    }
}