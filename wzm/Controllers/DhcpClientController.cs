﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects.Ip;
using wzm.Extensions;
using wzm.Validations;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DhcpClientController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUsers> _userManager;

        private readonly IWzmCore _wzmCore;

        public DhcpClientController(ApplicationDbContext db, IWzmCore wzmCore, UserManager<ApplicationUsers> userManager)
        {
            _db = db;
            _wzmCore = wzmCore;
            _userManager = userManager;
        }
        // GET: DhcpClient
        public IActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }
            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                List<IpDhcpClient> dhcpClients = _wzmCore.MikrotikManager.Ip.DhcpClient.GetDhcpClients(conection);
                return View(dhcpClients);
            };
        }



        // GET: DhcpClient/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DhcpClient/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult Disconnect(string id)
        {
            try
            {
                if (id != string.Empty)
                {
                    _wzmCore.MikrotikManager.Ip.DhcpClient.Release(id, string.Empty, _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true), _userManager.GetUserName(User));
                    TempData["success"] = "La operación se ha realizado correctamente.";
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    TempData["error"] = "El id no puede estar vacío.";
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }

        }

        [HttpGet]
        public IActionResult Renew(string id)
        {
            try
            {
                if (id != string.Empty)
                {
                    TempData["success"] = "La operación se ha realizado correctamente.";
                    _wzmCore.MikrotikManager.Ip.DhcpClient.Renew(id, string.Empty, _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true));
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    TempData["error"] = "El id no puede estar vacío.";
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }

        }

        [HttpGet]
        public IActionResult DisconnectAll()
        {
            try
            {
                _wzmCore.MikrotikManager.Ip.DhcpClient.ReleaseAll(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true));
                TempData["success"] = "La operación se ha realizado correctamente.";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {

                TempData["success"] = e.Message;
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        public IActionResult IpClient()
        {

            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected == true);
            List<IpDhcpClient> dhcpClients = new List<IpDhcpClient>();
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                dhcpClients = _wzmCore.MikrotikManager.Ip.DhcpClient.GetDhcpClients(conection);
            };

            return PartialView("_PartialIndex", dhcpClients);
        }
    }
}