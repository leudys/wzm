﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using wzmData.Models;

namespace wzm.Controllers
{
    public class ExcentoPagosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExcentoPagosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ExcentoPagos
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ExcentoPagos.Include(e => e.User);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ExcentoPagos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var excentoPago = await _context.ExcentoPagos
                .Include(e => e.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (excentoPago == null)
            {
                return NotFound();
            }

            return View(excentoPago);
        }

        // GET: ExcentoPagos/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Usuarios, "Id", "Id");
            return View();
        }

        // POST: ExcentoPagos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Mes,UserId")] ExcentoPago excentoPago)
        {
            if (ModelState.IsValid)
            {
                _context.Add(excentoPago);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Usuarios, "Id", "Id", excentoPago.UserId);
            return View(excentoPago);
        }

        // GET: ExcentoPagos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var excentoPago = await _context.ExcentoPagos.FindAsync(id);
            if (excentoPago == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Usuarios, "Id", "Id", excentoPago.UserId);
            return View(excentoPago);
        }

        // POST: ExcentoPagos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Mes,UserId")] ExcentoPago excentoPago)
        {
            if (id != excentoPago.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(excentoPago);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExcentoPagoExists(excentoPago.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Usuarios, "Id", "Id", excentoPago.UserId);
            return View(excentoPago);
        }

        // GET: ExcentoPagos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var excentoPago = await _context.ExcentoPagos
                .Include(e => e.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (excentoPago == null)
            {
                return NotFound();
            }

            return View(excentoPago);
        }

        // POST: ExcentoPagos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var excentoPago = await _context.ExcentoPagos.FindAsync(id);
            _context.ExcentoPagos.Remove(excentoPago);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExcentoPagoExists(int id)
        {
            return _context.ExcentoPagos.Any(e => e.Id == id);
        }
    }
}
