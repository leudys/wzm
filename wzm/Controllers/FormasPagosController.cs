﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using wzmData.Models;

namespace wzm.Controllers
{
    public class FormasPagosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FormasPagosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: FormasPagos
        public async Task<IActionResult> Index()
        {
            return View(await _context.FormasDePagos.ToListAsync());
        }

        // GET: FormasPagos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formasDePago = await _context.FormasDePagos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formasDePago == null)
            {
                return NotFound();
            }

            return View(formasDePago);
        }

        // GET: FormasPagos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FormasPagos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Moneda,Clasificador,NumTelefono,CtaBancaria,Direccion")] FormasDePago formasDePago)
        {
            if (formasDePago.Clasificador == wzmData.Enum.UserPayClaissifier.Correo)
            {
                if (formasDePago.Direccion == null)
                {
                    ModelState.AddModelError("", $"El campo Dirección es obligatorio ");
                }
            }
            if (formasDePago.Clasificador == wzmData.Enum.UserPayClaissifier.Transferencia_Bancaria)
            {
                if (formasDePago.CtaBancaria == null)
                {
                    ModelState.AddModelError("", $"El campo Cuenta Bancaria es obligatorio ");
                }
            }

            if (formasDePago.Clasificador == wzmData.Enum.UserPayClaissifier.Transferencia_Saldo)
            {
                if (formasDePago.NumTelefono == null)
                {
                    ModelState.AddModelError("", $"El campo Número de Teléfono es obligatorio ");
                }
            }


            if (ModelState.IsValid)
            {
                _context.Add(formasDePago);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(formasDePago);
        }

        // GET: FormasPagos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formasDePago = await _context.FormasDePagos.FindAsync(id);
            if (formasDePago == null)
            {
                return NotFound();
            }
            return View(formasDePago);
        }

        // POST: FormasPagos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] FormasDePago formasDePago)
        {
            if (id != formasDePago.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(formasDePago);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FormasDePagoExists(formasDePago.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(formasDePago);
        }

        // GET: FormasPagos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var formasDePago = await _context.FormasDePagos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (formasDePago == null)
            {
                return NotFound();
            }

            return View(formasDePago);
        }

        // POST: FormasPagos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var formasDePago = await _context.FormasDePagos.FindAsync(id);
            _context.FormasDePagos.Remove(formasDePago);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FormasDePagoExists(int id)
        {
            return _context.FormasDePagos.Any(e => e.Id == id);
        }
    }
}
