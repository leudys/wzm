﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using tik4net.Objects.Ip.DhcpServer;
using wzm.Extensions;
using wzm.Validations;
using wzmCore.Interfaces;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DhcpNetworkController : Controller
    {
        ApplicationDbContext _db;

        private readonly IWzmCore _wzmCore;

        public DhcpNetworkController(ApplicationDbContext db, IWzmCore wzmCore)
        {
            _db = db;

            _wzmCore = wzmCore;
        }

        // GET: DhcpNetwork
        public IActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }

            IEnumerable<DhcpServerNetwork> d = _wzmCore.MikrotikManager.Ip.DhcpServer.GetDhcpServerNetworks(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(e => e.Selected == true));
            return View(d.OrderBy(c => c.Address));

        }

        // GET: DhcpNetwork/Details/5
        public IActionResult Details(int id)
        {
            return View();
        }

        // GET: DhcpNetwork/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DhcpNetwork/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: DhcpNetwork/Edit/5
        public IActionResult Edit(int id)
        {
            return View();
        }

        // POST: DhcpNetwork/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: DhcpNetwork/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: DhcpNetwork/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}