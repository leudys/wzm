﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using wzm.Services;
using wzmData.Enum;
using wzmData.Models;
using wzmCore.Services;
using wzm.Extensions;
using wzmCore.Data;
using wzmCore.Services.System;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using wzmCore.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using wzmCore.Extensions;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GamerController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;
        public GamerController(ApplicationDbContext context, IWzmCore wzmCore)
        {
            _context = context;
            _wzmCore = wzmCore;
        }

        public async Task<IActionResult> Index()
        {
            if (!_context.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _context.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }

            var dispositivos = await _context.DeviceToNationalNetworks.Include(c => c.UserDevice).ToListAsync();

            return View(dispositivos);
        }

        [HttpGet]
        public IActionResult Create()
        {


            ViewData["Dispositivos"] = new SelectList(_context.GetGamer(), "Id", "Comment");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PublicIp,AddressList,UserDeviceId,AdminDevice")] DeviceToNationalNetwork device)
        {
            if (ModelState.IsValid)
            {
                var userDevice = _context.UsersDevices.Include(c => c.AdminDevice).FirstOrDefault(c => c.Id.Equals(device.UserDeviceId));

                if (userDevice.AdminDeviceId != null)
                {
                    if (_context.AdminDevicesRoles.Include(c => c.DeviceRole).Where(c => c.AdminDeviceId.Equals(userDevice.AdminDeviceId)).Any(c => c.DeviceRole.Role == Roles.FIREWALL_JUEGOS))
                    {
                        if (!_context.MikrotikCredentials.Any(c => c.DispositivoId.Equals(userDevice.AdminDeviceId)))
                        {
                            TempData["error"] = "El dispositivo Administrador seleccionado no posee credenciales.";
                            return RedirectToAction(nameof(Index));
                        }
                        else
                        {
                            var cm = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId.Equals(userDevice.AdminDeviceId));


                            device.UserDevice = _context.UsersDevices.Find(device.UserDeviceId);
                            var deviceOperation = _wzmCore.UserManagement.DeviceManager.ToNationalNetwork(device, cm);

                            if (deviceOperation.Validation.Succeeded)
                            {
                                _context.Add(device);
                                userDevice.NationalNetwork = true;
                                await _context.SaveChangesAsync();
                                TempData["success"] = $"Se le ha dado acceso a la Red Nacional al dispositivo {userDevice.Comment} correctamente.";
                                return RedirectToAction(nameof(Index));
                            }
                            else
                            {
                                TempData["error"] = deviceOperation.Validation.Errors.ToString();
                                return RedirectToAction(nameof(Index));
                            }


                        }

                    }
                    else
                    {
                        TempData["error"] = $"El dispositivo {userDevice.AdminDevice.GetDispositivo} no posee rol de Firewall de Juegos";
                        return RedirectToAction(nameof(Index));

                    }


                }
                else
                {
                    TempData["error"] = $"El dispositivo seleccionado no posee un dispositivo administrador.";
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["Dispositivos"] = new SelectList(_context.GetGamer(), "Id", "Comment", device.UserDeviceId);

            return View(device);
        }

        public async Task<IActionResult> Disable(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var services = await _context.ApplicationServices.FindAsync(id);
            if (services == null)
            {
                return NotFound();
            }
            return View(services);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Disable(int id, [Bind("Id,State,Disabled,ServiceType")] ApplicationServices service)
        {
            if (id != service.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                return RedirectToAction(nameof(Index));



            }
            return View(service);
        }


        // GET: SReconnections/Edit/5
        public async Task<IActionResult> Enable(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var services = await _context.ApplicationServices.FindAsync(id);

            if (services is MorososService)
            {
                services = services as MorososService;

                return View("EnableMorosos", services);
            }

            if (services == null)
            {
                return NotFound();
            }

            return View(services);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Enable(int id, [Bind("Id,State,Disabled,ServiceType")] ApplicationServices service)
        {
            if (id != service.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                return RedirectToAction(nameof(Index));

            }
            return View(service);
        }

    }
}
