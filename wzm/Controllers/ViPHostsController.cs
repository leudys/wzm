﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using wzm.Services;
using wzm.Validations;
using wzmData.Enum;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Services;
using wzmCore.Services.System;
using tik4net;
using wzmCore.Extensions;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ViPHostsController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly HostedServices _services;
        private readonly IWzmCore _wzmCore;
        private readonly UserManager<ApplicationUsers> _userManager;

        public ViPHostsController(ApplicationDbContext context, IHostedService services, IWzmCore wzmCore, UserManager<ApplicationUsers> userManager)
        {
            _context = context;
            _services = services as HostedServices;
            _wzmCore = wzmCore;
            _userManager = userManager;
        }

        // GET: ViPHosts
        public async Task<IActionResult> Index()
        {
            if (!_context.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _context.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }


            var applicationDbContext = _context.HostsServices./*Include(c => c.VpnServer).*/Include(c => c.Device).Include(c => c.Device);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ViPHosts/Create
        public IActionResult Create()
        {

            var usuarios = _userManager.Users.ToList();

            ViewData["Dispositivos"] = new SelectList(_context.UsersDevices, "Id", "Comment");
            //ViewData["Vpn"] = new SelectList(_context.VpnServerNacional.Where(c => c.PersistentConnection), "Id", "Host");
            return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DeviceId,Vip,Limit,Balanceo,Disabled,NoPortal,Vpn,VpnServerId")] HostsServices viPHostsServices)
        {
            //if (!viPHostsServices.Balanceo && !viPHostsServices.Vip)
            //{
            //    ModelState.AddModelError("", "El dispositivo ha de tener al menos un servicio.");
            //    ViewData["Dispositivos"] = new SelectList(_context.UsersDevices, "Id", "Comment");
            //    return View(viPHostsServices);
            //}


            // Ni idea para que hago esto.
            var vipService = _context.ApplicationServices.FirstOrDefault(c => c.ServiceType == ServiceType.Vip);


            viPHostsServices.ApplicationServicesId = vipService.Id;
            var device = await _context.UsersDevices.FindAsync(viPHostsServices.DeviceId);
            if (device.Ip == null)
            {
                ModelState.AddModelError("", "El dispositivo seleccionado no contiene una Ip.");
                ViewData["Dispositivos"] = new SelectList(_context.UsersDevices, "Id", "Comment");
                return View(viPHostsServices);
            }
            if (device.Ip == string.Empty)
            {
                ModelState.AddModelError("", "El dispositivo seleccionado no contiene una Ip.");
                ViewData["Dispositivos"] = new SelectList(_context.UsersDevices, "Id", "Comment");
                return View(viPHostsServices);
            }


            if (ModelState.IsValid)
            {
                if (viPHostsServices.NoPortal)
                {
                    device.PortalDeOrigen = "NoPortalAccess";
                    _wzmCore.UserManagement.ReturnToOriginalPortal(device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                    _context.Update(device);
                    _context.SaveChanges();
                }

                viPHostsServices.Disabled = false;
                _context.Add(viPHostsServices);
                await _context.SaveChangesAsync();
                TempData["success"] = "Dispositivo añadido correctamente";
                return RedirectToAction(nameof(Index));
            }
            //ViewData["Vpn"] = new SelectList(_context.VpnServerNacional.Where(c => c.PersistentConnection), "Id", "Host", viPHostsServices.VpnServerId);
            ViewData["Dispositivos"] = new SelectList(_context.UsersDevices, "Id", "Comment", viPHostsServices.DeviceId);
            return View(viPHostsServices);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viPHostsServices = await _context.HostsServices.Include(d => d.Device).FirstAsync(d => d.Id == id);
            if (viPHostsServices == null)
            {
                return NotFound();
            }
            //ViewData["Vpn"] = new SelectList(_context.VpnServerNacional.Where(c => c.PersistentConnection), "Id", "Host", viPHostsServices.VpnServerId);
            return View(viPHostsServices);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DeviceId,Vip,Balanceo,HasInternet,Limit,PortalActual,Disabled,ApplicationServicesId,NoPortal,Vpn,VpnServerId")] HostsServices viPHostsServices)
        {
            //if (!viPHostsServices.Balanceo && !viPHostsServices.Vip)
            //{
            //    ModelState.AddModelError("", "El dispositivo ha de tener al menos un servicio (Balanceo ó VIP).");

            //}

            if (id != viPHostsServices.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var host = _context.HostsServices.Include(c => c.Device).FirstOrDefault(c => c.Id == id);

                    if (host.NoPortal != viPHostsServices.NoPortal)
                    {
                        var device = await _context.UsersDevices.FindAsync(host.DeviceId);
                        if (host.NoPortal && !viPHostsServices.NoPortal)
                        {

                            var cm = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                            using (var connection = ConnectionFactory.OpenConnection(cm.Dispositivo.Firmware.GetApiVersion(), cm.Dispositivo.Ip, cm.User, cm.Password))
                            {
                                var interfaces = _wzmCore.MikrotikManager.Interface.Wireless.GetOnlyStationPseudobridge(connection);
                                device.PortalDeOrigen = _wzmCore.MikrotikManager.Firewall.AddressList.SetAddressList(device, interfaces, connection);
                            }
                            host.NoPortal = viPHostsServices.NoPortal;
                            _context.Update(device);
                            _context.Update(host);
                            _context.SaveChanges();

                        }
                        else
                        {
                            if (!device.PortalDeOrigen.Equals("NoPortalAccess"))
                            {
                                device.PortalDeOrigen = "NoPortalAccess";
                                _wzmCore.UserManagement.ReturnToOriginalPortal(device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                                host.NoPortal = viPHostsServices.NoPortal;
                                _context.Update(device);
                                _context.Update(host);
                                _context.SaveChanges();
                            }
                        }
                    }

                    if (host.Balanceo != viPHostsServices.Balanceo)
                    {
                        if (host.Balanceo && !viPHostsServices.Balanceo)
                        {
                            var service = _context.ApplicationServices.Where(c => c.ServiceType == ServiceType.BalanceadorPCC || c.ServiceType == ServiceType.BalanceadorNTH && c.State == ServicesStatus.Running).FirstOrDefault();
                            if (service != null)
                            {
                                await _services.DeleteJob(service.ServiceType, new CancellationToken());
                            }

                            host.HasInternet = false;
                            host.IsBalanced = false;
                            host.Balanceo = false;
                            host.PortalActual = host.Device.PortalDeOrigen;

                            _context.HostsServices.Update(host);
                            await _context.SaveChangesAsync();
                            _wzmCore.UserManagement.ReturnToOriginalPortal(host.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                            if (service != null)
                            {
                                switch (service.ServiceType)
                                {

                                    case ServiceType.BalanceadorNTH:
                                        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoNTHJob)), new CancellationToken());
                                        break;
                                    case ServiceType.BalanceadorPCC:
                                        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoPCCJob)), new CancellationToken());
                                        break;
                                }
                            }

                        }
                        else
                        {
                            host.Balanceo = viPHostsServices.Balanceo;
                            _context.Update(host);
                            await _context.SaveChangesAsync();
                        }
                    }

                    if (host.Vip != viPHostsServices.Vip)
                    {
                        if (host.Vip && !viPHostsServices.Vip)
                        {
                            if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.Vip && c.State == ServicesStatus.Running))
                            {
                                await _services.DeleteJob(ServiceType.Vip, new CancellationToken());
                            }
                            host.HasInternet = false;
                            host.IsBalanced = false;
                            host.Vip = false;
                            host.PortalActual = host.Device.PortalDeOrigen;
                            _context.HostsServices.Update(host);
                            await _context.SaveChangesAsync();
                            _wzmCore.UserManagement.ReturnToOriginalPortal(host.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                            if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.Vip && c.State == ServicesStatus.Running))
                                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(VipJob)), new CancellationToken());
                        }
                        else
                        {
                            host.Vip = viPHostsServices.Vip;
                            _context.Update(host);
                            await _context.SaveChangesAsync();
                        }

                    }

                    if (host.Limit != viPHostsServices.Limit)
                    {
                        if (host.Limit && !viPHostsServices.Limit)
                        {
                            if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.SimpleQueueLimit && c.State == ServicesStatus.Running))
                                await _services.DeleteJob(ServiceType.SimpleQueueLimit, new CancellationToken());
                            host.Limit = false;
                            _context.HostsServices.Update(host);
                            await _context.SaveChangesAsync();
                            _wzmCore.UserManagement.RemoveSimpleQueue(host.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                            if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.SimpleQueueLimit && c.State == ServicesStatus.Running))
                                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(SimpleQueueJob)), new CancellationToken());
                        }
                        else
                        {
                            host.Limit = viPHostsServices.Limit;
                            _context.Update(host);
                            await _context.SaveChangesAsync();
                        }

                    }
                    if (viPHostsServices.Vpn)
                    {
                        host.Vpn = viPHostsServices.Vpn;
                        //host.VpnServerId = viPHostsServices.VpnServerId;
                        _context.Update(host);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        host.Vpn = viPHostsServices.Vpn;
                        //host.VpnServerId = null;
                        _context.Update(host);
                        await _context.SaveChangesAsync();
                    }

                    TempData["success"] = "Dispositivo Actualizado Correctamente";
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                    return RedirectToAction(nameof(Index));
                }

            }
            //ViewData["Vpn"] = new SelectList(_context.VpnServerNacional.Where(c => c.PersistentConnection), "Id", "Host", viPHostsServices.VpnServerId);
            viPHostsServices.Device = await _context.UsersDevices.FindAsync(viPHostsServices.DeviceId);
            return View(viPHostsServices);
        }

        [HttpPost]
        public async Task<IActionResult> Disable(int id)
        {
            try
            {
                var viPHostsServices = _context.HostsServices.Include(c => c.Device).FirstOrDefault(c => c.Id == id);

                //var balanceoService = _context.ApplicationServices.Where(c => c.ServiceType == ServiceType.BalanceadorPCC || c.ServiceType == ServiceType.BalanceadorNTH && c.State == ServicesStatus.Running).FirstOrDefault();

                //if (viPHostsServices.Balanceo)
                //{
                //    if (balanceoService != null)
                //    {
                //        await _services.DeleteJob(balanceoService.ServiceType, new CancellationToken());
                //    }

                //}
                //if (viPHostsServices.Vip)
                //{
                //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.Vip && c.State == ServicesStatus.Running))
                //    {
                //        await _services.DeleteJob(ServiceType.Vip, new CancellationToken());
                //    }
                //}
                //if (viPHostsServices.Limit)
                //{
                //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.SimpleQueueLimit && c.State == ServicesStatus.Running))
                //    {
                //        await _services.DeleteJob(ServiceType.SimpleQueueLimit, new CancellationToken());
                //    }
                //    _wzmCore.UserManagement.RemoveSimpleQueue(viPHostsServices.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                //}

                viPHostsServices.Disabled = true;
                viPHostsServices.HasInternet = false;
                viPHostsServices.IsBalanced = false;
                _context.HostsServices.Update(viPHostsServices);
                viPHostsServices.PortalActual = viPHostsServices.Device.PortalDeOrigen;
                await _context.SaveChangesAsync();
                _wzmCore.UserManagement.ReturnToOriginalPortal(viPHostsServices.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));

                //if (viPHostsServices.Balanceo)
                //{
                //    var service = _context.ApplicationServices.Where(c => c.ServiceType == ServiceType.BalanceadorPCC || c.ServiceType == ServiceType.BalanceadorNTH && c.State == ServicesStatus.Running).FirstOrDefault();

                //    if (service != null)
                //    {
                //        switch (service.ServiceType)
                //        {

                //            case ServiceType.BalanceadorNTH:
                //                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoNTHJob)), new CancellationToken());
                //                break;
                //            case ServiceType.BalanceadorPCC:
                //                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoPCCJob)), new CancellationToken());
                //                break;
                //        }
                //    }

                //}
                //if (viPHostsServices.Vip)
                //{
                //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.Vip && c.State == ServicesStatus.Running))
                //    {
                //        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(VipJob)), new CancellationToken());
                //    }
                //}
                //if (viPHostsServices.Limit)
                //{
                //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.SimpleQueueLimit && c.State == ServicesStatus.Running))
                //        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(SimpleQueueJob)), new CancellationToken());

                //}

                TempData["success"] = "Dispositivo Deshabilitado Correctamente";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {

                TempData["error"] = e.Message;
                return RedirectToAction(nameof(Index));
            }

        }

        public async Task<IActionResult> Enable(int id)
        {
            try
            {
                var viPHostsServices = await _context.HostsServices.FindAsync(id);

                viPHostsServices.Disabled = false;
                _context.HostsServices.Update(viPHostsServices);
                await _context.SaveChangesAsync();
                TempData["success"] = "Usuario Habilitado Correctamente";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {

                TempData["error"] = e.Message;
                return RedirectToAction(nameof(Index));
            }

        }

        public async Task<IActionResult> Delete(int id)
        {

            var viPHostsServices = _context.HostsServices.Include(c => c.Device).FirstOrDefault(c => c.Id == id);
            //var balanceoService = _context.ApplicationServices.Where(c => c.ServiceType == ServiceType.BalanceadorPCC || c.ServiceType == ServiceType.BalanceadorNTH && c.State == ServicesStatus.Running).FirstOrDefault();
            //if (viPHostsServices.Balanceo)
            //{
            //    if (balanceoService != null)
            //    {
            //        await _services.DeleteJob(balanceoService.ServiceType, new CancellationToken());
            //    }

            //}
            //if (viPHostsServices.Vip)
            //{
            //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.Vip && c.State == ServicesStatus.Running))
            //    {
            //        await _services.DeleteJob(ServiceType.Vip, new CancellationToken());
            //    }
            //}
            //if (viPHostsServices.Limit)
            //{
            //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.SimpleQueueLimit && c.State == ServicesStatus.Running))
            //    {
            //        await _services.DeleteJob(ServiceType.SimpleQueueLimit, new CancellationToken());
            //    }
            //    _wzmCore.UserManagement.RemoveSimpleQueue(viPHostsServices.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
            //}
            try
            {
                _context.HostsServices.Remove(viPHostsServices);
                await _context.SaveChangesAsync();
                _wzmCore.UserManagement.ReturnToOriginalPortal(viPHostsServices.Device, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return RedirectToAction(nameof(Index));
            }
          

            //if (viPHostsServices.Balanceo)
            //{
            //    var service = _context.ApplicationServices.Where(c => c.ServiceType == ServiceType.BalanceadorPCC || c.ServiceType == ServiceType.BalanceadorNTH && c.State == ServicesStatus.Running).FirstOrDefault();

            //    if (service != null)
            //    {
            //        switch (service.ServiceType)
            //        {

            //            case ServiceType.BalanceadorNTH:
            //                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoNTHJob)), new CancellationToken());
            //                break;
            //            case ServiceType.BalanceadorPCC:
            //                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoPCCJob)), new CancellationToken());
            //                break;
            //        }
            //    }

            //}
            //if (viPHostsServices.Vip)
            //{
            //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.Vip && c.State == ServicesStatus.Running))
            //    {
            //        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(VipJob)), new CancellationToken());
            //    }
            //}
            //if (viPHostsServices.Limit)
            //{
            //    if (_context.ApplicationServices.Any(c => c.ServiceType == ServiceType.SimpleQueueLimit && c.State == ServicesStatus.Running))
            //        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(SimpleQueueJob)), new CancellationToken());

            //}

            
        }

        private bool ViPHostsServicesExists(int id)
        {
            return _context.HostsServices.Any(e => e.Id == id);
        }
    }
}
