﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Enum;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmData.Ssh;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UbiquitiCredentialsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;

        public UbiquitiCredentialsController(ApplicationDbContext context, IWzmCore wzmCore)
        {
            _context = context;
            _wzmCore = wzmCore;
        }

        // GET: DeviceCredentials
        public async Task<IActionResult> Index()
        {
            return View(await _context.UbiquitiCredentials.Include(c => c.Dispositivo).ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> LoginUbiquiti()
        {
            var dispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Ubiquiti).ToListAsync();
            var tempDispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Ubiquiti).ToListAsync();


            foreach (var item in dispositivos)
            {
                if (_context.UbiquitiCredentials.Any(c => c.DispositivoId == item.Id))
                {
                    tempDispositivos.Remove(item);
                }
            }

            ViewData["Dispositivos"] = new SelectList(tempDispositivos, "Id", "Ip");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginUbiquiti(UbiquitiCredentials credential)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Login login = new Login()
                    {
                        Ip = _context.Dispositivos.Find(credential.DispositivoId).Ip,
                        User = credential.User,
                        Password = credential.Password,
                        Port = 22
                    };
                    await _wzmCore.UbiquitiManager.SshConnection.TrySaveCredentials(login);
                    _context.Add(credential);
                    _context.SaveChanges();

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);

                }
            }

            var dispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Ubiquiti).ToListAsync();
            var tempDispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Ubiquiti).ToListAsync();


            foreach (var item in dispositivos)
            {
                if (_context.UbiquitiCredentials.Any(c => c.DispositivoId == item.Id))
                {
                    tempDispositivos.Remove(item);
                }
            }

            ViewData["Dispositivos"] = new SelectList(tempDispositivos, "Id", "Ip");
            return View(credential);

        }



        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deviceCredentials = _context.DeviceCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Id == id) as UbiquitiCredentials;
            if (deviceCredentials == null)
            {
                return NotFound();
            }

            return View(deviceCredentials);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,User,Password,DispositivoId,Puerto")] UbiquitiCredentials credential)
        {
            if (id != credential.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Login login = new Login()
                    {
                        Ip = credential.Dispositivo.Ip,
                        User = credential.User,
                        Password = credential.Password,
                        Port = 22
                    };
                    await _wzmCore.UbiquitiManager.SshConnection.TrySaveCredentials(login);
                    _context.Update(credential);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    if (!DeviceCredentialsExists(credential.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("", e.Message);
                        return View(credential);
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(credential);
        }



        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deviceCredentials = await _context.DeviceCredentials.FindAsync(id);
            _context.DeviceCredentials.Remove(deviceCredentials);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeviceCredentialsExists(int id)
        {
            return _context.DeviceCredentials.Any(e => e.Id == id);
        }
    }
}
