﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using wzm.Utils;
using wzm.ViewModels;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmData.Enum;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DispositivosTpLinkController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUsers> _userManager;

        private readonly IWzmCore _wzmCore;

        public DispositivosTpLinkController(ApplicationDbContext context, UserManager<ApplicationUsers> userManager, IWzmCore wzmCore)
        {
            _context = context;
            _userManager = userManager;

            _wzmCore = wzmCore;
        }

        [HttpGet]
        public IActionResult Index()
        {

            ViewData["Ap"] = _context.Dispositivos.Include(d => d.DeviceCredential).Where(c => c is ApDevice && c.Fabricante == DeviceTypeEnum.TPLink).ToList();

            ViewData["Station"] = _context.Dispositivos.Include(d => d.DeviceCredential).Where(c => c is StationDevice && c.Fabricante == DeviceTypeEnum.TPLink).ToList();


            return View();
        }

        [HttpGet]
        public IActionResult CreateStation()
        {

            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo");
            return View(new StationDevice());// Le paso un station device para poder coger el valor de publico
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateStation([Bind("Id,Mac,Hostname,ApDeviceId,Platform,Firmware,SSID,Ip,Ubicacion,WirelessSecurity,Publico,MacApPublico,SSID")] StationDevice dispositivo)
        {
            if (dispositivo.Publico)
            {
                if (dispositivo.MacApPublico == null)
                {
                    ModelState.AddModelError("", "El campo MAC del AP Público es oblogatorio.");
                }

                if (dispositivo.SSID == null)
                {
                    ModelState.AddModelError("", "El campo SSID es oblogatorio.");
                }
            }
            else
            {
                dispositivo.SSID = _context.Dispositivos.Find(dispositivo.ApDeviceId).SSID;
            }

            if (ModelState.IsValid)
            {
                dispositivo.Fabricante = DeviceTypeEnum.TPLink;
                dispositivo.WirelessMode = WirelessMode.Station;

                _context.Add(dispositivo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo", dispositivo.ApDeviceId);
            return View(dispositivo);
        }

        [HttpGet]
        public IActionResult CreateAp()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAp([Bind("Id, Mac, Hostname, Platform, Firmware, SSID, Ip, Ubicacion, WirelessSecurity")]ApDevice apDevice)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    apDevice.Fabricante = DeviceTypeEnum.TPLink;
                    apDevice.WirelessMode = WirelessMode.Ap;
                    _context.Add(apDevice);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

                return View(apDevice);
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        public async Task<IActionResult> EditAp(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispositivo = await _context.Dispositivos.FindAsync(id) as ApDevice;
            if (dispositivo == null)
            {
                return NotFound();
            }

            return View(dispositivo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAp(int id, [Bind("Id,Mac,Hostname,Platform,Firmware,SSID,Ip,WirelessMode,Ubicacion, WirelessSecurity")] ApDevice dispositivo)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dispositivo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DispositivoExists(dispositivo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View(dispositivo);
        }

        [HttpGet]
        public async Task<IActionResult> EditStation(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispositivo = await _context.Dispositivos.FindAsync(id) as StationDevice;
            if (dispositivo == null)
            {
                return NotFound();
            }
            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo", dispositivo.ApDeviceId);
            return View(dispositivo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditStation(int id, [Bind("Id,Mac,Hostname,Platform,Firmware,ApDeviceId,SSID,Ip,Ubicacion,WirelessMode,Publico,MacApPublico,DeviceTypeEnum,WirelessSecurity")] StationDevice dispositivo)
        {
            if (id != dispositivo.Id)
            {
                return NotFound();
            }
            if (dispositivo.Publico)
            {
                if (dispositivo.MacApPublico == null)
                {
                    ModelState.AddModelError("", "El campo MAC del AP Público es oblogatorio.");
                }

                if (dispositivo.SSID == null)
                {
                    ModelState.AddModelError("", "El campo SSID es oblogatorio.");
                }
            }
            else
            {
                dispositivo.SSID = _context.Dispositivos.Find(dispositivo.ApDeviceId).SSID;
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dispositivo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DispositivoExists(dispositivo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo", dispositivo.ApDeviceId);
            return View(dispositivo);
        }



        public IActionResult ShowAp(string id)
        {
            if (int.Parse(id) == 1)
            {
                ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo");

                return PartialView("_ApPartial");
            }

            return PartialView("_EmptyPartial");
        }



        [HttpGet]
        public async Task<IActionResult> ChangeToStation(int id)
        {
            var dispositivo = await _context.Dispositivos.FindAsync(id);
            var ap = _context.Dispositivos.Where(c => c is ApDevice);
            if (dispositivo == null)
            {
                return NotFound();
            }

            ViewData["Ap"] = new SelectList(ap.ToList(), "Id", "GetDispositivo");
            return View(new ToStationViewModel() { DeviceId = dispositivo.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeToStation(int DeviceId, [Bind("DeviceId,ApId,Mac,SSID,Publico")] ToStationViewModel toStation)
        {
            if (toStation.Publico)
            {
                if (toStation.SSID == null)
                {
                    ModelState.AddModelError("", "El campo SSID es obligatorio.");
                }
                if (toStation.Mac == null)
                {
                    ModelState.AddModelError("", "El campo MAC es obligatorio.");
                }
            }

            if (ModelState.IsValid)
            {
                var dispositivo = await _context.Dispositivos.FindAsync(DeviceId);

                if (toStation.Publico)
                {
                    await DeviceStatus.ChangeToStationAsync(toStation.Mac, toStation.SSID, dispositivo.Id, _context);

                }
                else
                {
                    var ap = await _context.Dispositivos.FindAsync(toStation.ApId) as ApDevice;

                    if (dispositivo != null && ap != null)
                    {
                        await DeviceStatus.ChangeToStationAsync(ap, dispositivo.Id, _context);

                    }
                }

                return RedirectToAction(nameof(Index));

            }
            var aps = _context.Dispositivos.Where(c => c is ApDevice);
            ViewData["Ap"] = new SelectList(aps.ToList(), "Id", "GetDispositivo", toStation.ApId);
            return View(toStation);


        }

        [HttpGet]
        public async Task<IActionResult> ChangeToAp(int id)
        {
            var dispositivo = await _context.Dispositivos.FindAsync(id);

            var ap = await _context.Dispositivos.FindAsync(id);

            if (dispositivo != null && ap != null)
            {
                await DeviceStatus.ChangeToApAsync(dispositivo.Id, _context);
                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispositivo = await _context.Dispositivos
                .Include(d => d.DeviceCredential)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dispositivo == null)
            {
                return NotFound();
            }

            return View(dispositivo);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dispositivo = await _context.Dispositivos.FindAsync(id);


            //foreach (var item in _userManager.Users)
            //{
            //    if (item is ApplicationClients)
            //    {
            //        var user = item as ApplicationClients;
            //        if ((user as ApplicationClients).DispositivoId == id)
            //        {
            //            await _userManager.DeleteAsync(user);
            //        }
            //    }
            //}

            _context.Dispositivos.Remove(dispositivo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }



        private bool DispositivoExists(int id)
        {
            return _context.Dispositivos.Any(e => e.Id == id);
        }
    }
}
