﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmData.Enum;
using wzm.Extensions;
using wzmCore.Extensions;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PagosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUsers> _userManager;
        private readonly IWzmCore _wzmCore;
        public PagosController(ApplicationDbContext context, UserManager<ApplicationUsers> userManager, IWzmCore wzmCore)
        {
            _context = context;
            _userManager = userManager;
            _wzmCore = wzmCore;
        }

        // GET: Pagos
        public async Task<IActionResult> Index()
        {
            var meses = CustomCalendar.Calendar.GetMonth();
            ViewData["Meses"] = new SelectList(meses, "Id", "Name", DateTime.Now.Month);

            ViewData["Clasificadores"] = new SelectList(_context.ClasificadorPagos.Where(c => c.PaymentType != PaymentType.Gratis).GroupBy(c => c.DiaPago).Select(c => c.Key).ToList());
            return View(await _context.GetUsuariosPorPagarAsync(_userManager));
        }

        public IActionResult GetPagos(int mes)
        {

            var applicationDbContext = _context.Pagos.Include(p => p.Usuario).Where(c => c.FechaPago.Month == mes && c.FechaPago.Year == DateTime.Now.Year).ToList();
            return PartialView("_PagosPartial", applicationDbContext);
        }

        public IActionResult GetUsers(int clasificador)
        {
            var clients = _context.GetApplicationClients();

            return PartialView("_UsuariosPartial", clients.Where(c => c.ClasificadorPago != null && c.ClasificadorPago.PaymentType != PaymentType.Gratis && c.ClasificadorPago.DiaPago == clasificador));

        }


        public async Task<IActionResult> Create()
        {

            var usuariosPagar = await _context.GetUsuariosPorPagarAsync(_userManager);

            ViewData["UsuarioId"] = new SelectList(usuariosPagar, "Id", "UserName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FechaPago,UsuarioId")] Pago pago)
        {
            var user = _context.GetApplicationClients().FirstOrDefault(c => c.Id.Equals(pago.UsuarioId));
            if (_context.Pagos.Any(c => c.FechaPago.Month == pago.FechaPago.Month && c.FechaPago.Year == DateTime.Now.Year && c.UsuarioId == pago.UsuarioId))
            {

                ModelState.AddModelError("", $"El usuario {user.UserName} ya pagó el mes {pago.FechaPago.Month}.");
            }

            if (ModelState.IsValid)
            {
                if (!_context.UsersDevices.Any(c => c.ApplicationUserId.Equals(user.Id)))
                {
                    TempData["error"] = "El usuario seleccionado no posee ningún dispositivo asociado.";
                    return RedirectToAction(nameof(Index));
                }

                if (user.UserPaymentState == UserPaymentState.Atrasado)
                {
                    user.UserDevices = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(user.Id)).ToList();
                    user.UserPaymentState = UserPaymentState.AlDia;
                    user.Estado = StatusEnum.Habilitado;
                    _wzmCore.UserManagement.ReturnToOriginalPortal(user, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                    //_wzmCore.UserManagement.EnableUserInternet(user, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                }
                pago.Pagado = user.ClasificadorPago.Importe;
                _context.Add(pago);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UsuarioId"] = new SelectList(_userManager.Users, "Id", "UserName", pago.UsuarioId);
            return View(pago);
        }

        public async Task<IActionResult> InsertPayment(string id)
        {
            var user = _context.GetApplicationClients().FirstOrDefault(c => c.Id.Equals(id));
            
            if (_context.Pagos.Any(c => c.FechaPago.Month == DateTime.Now.Month && c.FechaPago.Year == DateTime.Now.Year && c.UsuarioId.Equals(id)))
            {
                TempData["error"] = $"El usuario { user.UserName} ya pagó el mes { DateTime.Now.Month}.";
                return RedirectToAction(nameof(Index));
            }
            if (user.UserPaymentState == UserPaymentState.Atrasado)
            {
                user.UserDevices = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(user.Id)).ToList();
                user.UserPaymentState = UserPaymentState.AlDia;
                user.Estado = StatusEnum.Habilitado;
                _wzmCore.UserManagement.ReturnToOriginalPortal(user, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
                //_wzmCore.UserManagement.EnableUserInternet(user, _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected));
            }
            var pago = new Pago()
            {
                FechaPago = DateTime.Now,
                UsuarioId = id,
                Pagado = user.ClasificadorPago.Importe
            };

            _context.Add(pago);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pago = await _context.Pagos.FindAsync(id);
            if (pago == null)
            {
                return NotFound();
            }
            ViewData["UsuarioId"] = new SelectList(_userManager.Users.ToList(), "Id", "Nick", pago.UsuarioId);
            return View(pago);
        }



        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pago = await _context.Pagos
                .Include(p => p.Usuario)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pago == null)
            {
                return NotFound();
            }

            return View(pago);
        }


        public async Task<IActionResult> DeleteConfirmed(int id)
        {

            var pago = await _context.Pagos.FindAsync(id);
            var user = await _userManager.FindByIdAsync(pago.UsuarioId) as ApplicationUsers;
            user.UserPaymentState = UserPaymentState.Atrasado;
            _context.Pagos.Remove(pago);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PagoExists(int id)
        {
            return _context.Pagos.Any(e => e.Id == id);
        }
    }
}
