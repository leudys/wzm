﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using wzmData.Models;

namespace wzm.Controllers
{
    public class VpnInternetInstanceController : Controller
    {
        private readonly ApplicationDbContext _context;

        public VpnInternetInstanceController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: VpnInternetInstance
        public async Task<IActionResult> Index()
        {
            return View(await _context.VpnServerInternet.ToListAsync());
        }

        // GET: VpnInternetInstance/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vpnServerInternet = await _context.VpnServerInternet
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vpnServerInternet == null)
            {
                return NotFound();
            }

            return View(vpnServerInternet);
        }

        // GET: VpnInternetInstance/Create
        public IActionResult Create()
        {
            ViewData["Nacional"] = new SelectList(_context.VpnServerNacional, "Id", "Host");
            return View();
        }

        // POST: VpnInternetInstance/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,User,Password,DisconnectOnPingFailed,Host,AddressList,VpnServerNacionalId")] VpnServerInternet vpnServerInternet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vpnServerInternet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Nacional"] = new SelectList(_context.VpnServerNacional, "Id", "Host", vpnServerInternet.VpnServerNacional.Host);
            return View(vpnServerInternet);
        }

        // GET: VpnInternetInstance/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vpnServerInternet = await _context.VpnServerInternet.FindAsync(id);
            if (vpnServerInternet == null)
            {
                return NotFound();
            }
            return View(vpnServerInternet);
        }

        // POST: VpnInternetInstance/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DisconnectOnPingFailed,User,Password,Host")] VpnServerInternet vpnServerInternet)
        {
            if (id != vpnServerInternet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vpnServerInternet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VpnServerInternetExists(vpnServerInternet.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vpnServerInternet);
        }

        // GET: VpnInternetInstance/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vpnServerInternet = await _context.VpnServerInternet
                .FirstOrDefaultAsync(m => m.Id == id);
            if (vpnServerInternet == null)
            {
                return NotFound();
            }

            return View(vpnServerInternet);
        }

        // POST: VpnInternetInstance/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vpnServerInternet = await _context.VpnServerInternet.FindAsync(id);
            _context.VpnServerInternet.Remove(vpnServerInternet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VpnServerInternetExists(int id)
        {
            return _context.VpnServerInternet.Any(e => e.Id == id);
        }
    }
}
