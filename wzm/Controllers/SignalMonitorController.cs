﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Data.Mikrotik;
using wzmCore.Services.Helpers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SignalMonitorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private DeviceSignalMonitor _deviceSignalMonitor;
        public SignalMonitorController(DeviceSignalMonitor deviceSignalMonitor, ApplicationDbContext context)
        {
            _deviceSignalMonitor = deviceSignalMonitor;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DeviceSignals.Include(d => d.Dispositivo);
            ViewData["Signal"] = _deviceSignalMonitor.DeviceStatus;
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DeviceSignals/Create
        public IActionResult Create()
        {
            ViewData["DispositivoId"] = new SelectList(_context.Dispositivos, "Id", "Ip");
            return View();
        }

        // POST: DeviceSignals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,MinimalSignal,MaximumSignal,DispositivoId,Enabled")] DeviceSignal deviceSignal)
        {
            if (ModelState.IsValid)
            {
                deviceSignal.Enabled = true;
                _context.Add(deviceSignal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DispositivoId"] = new SelectList(_context.Dispositivos, "Id", "Ip", deviceSignal.DispositivoId);
            return View(deviceSignal);
        }

        // GET: DeviceSignals/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deviceSignal = await _context.DeviceSignals.FindAsync(id);
            if (deviceSignal == null)
            {
                return NotFound();
            }
            ViewData["DispositivoId"] = new SelectList(_context.Dispositivos, "Id", "Discriminator", deviceSignal.DispositivoId);
            return View(deviceSignal);
        }

        // POST: DeviceSignals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,MinimalSignal,MaximumSignal,DispositivoId,Enabled")] DeviceSignal deviceSignal)
        {
            if (id != deviceSignal.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deviceSignal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeviceSignalExists(deviceSignal.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DispositivoId"] = new SelectList(_context.Dispositivos, "Id", "Discriminator", deviceSignal.DispositivoId);
            return View(deviceSignal);
        }

        // GET: DeviceSignals/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deviceSignal = await _context.DeviceSignals
                .Include(d => d.Dispositivo)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deviceSignal == null)
            {
                return NotFound();
            }

            return View(deviceSignal);
        }

        // POST: DeviceSignals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deviceSignal = await _context.DeviceSignals.FindAsync(id);
            _context.DeviceSignals.Remove(deviceSignal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeviceSignalExists(int id)
        {
            return _context.DeviceSignals.Any(e => e.Id == id);
        }

        // GET: RegistrationTable
        //public IActionResult Index()
        //{

        //    return View(_deviceSignalMonitor.DeviceStatus);
        //}

        [HttpGet]
        public IActionResult AjaxHandler()
        {

            return PartialView("_MonitorPartial", _deviceSignalMonitor.DeviceStatus);

        }
    }
}