﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.System;
using wzmData.Enum;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmCore.Services;
using wzm.Extensions;
using wzmData.ViewModels;
using System;
using wzmCore.Extensions;
using wzmCore.Services.System;
using wzmCore.Data.Mikrotik;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace wzm.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IHttpContextAccessor _accessor;
        private ApplicationDbContext _db;
        private List<string> _ip = new List<string>();
        public IWzmCore _wzmCore;
        IHostedService _service;
        private readonly UserManager<ApplicationUsers> _userManager;
        private readonly ILogger<HomeController> _logger;
        private readonly ActiveDevices _activeDevices;
        private EtecsaInternetPortalsJob _etecsaInternet;
        public HomeController(ApplicationDbContext db, IHttpContextAccessor accessor, EtecsaInternetPortalsJob etecsaInternet, IWzmCore wzmCore, IHostedService service, UserManager<ApplicationUsers> user, ILogger<HomeController> logger, ActiveDevices activeDevices)
        {
            _service = service;
            _db = db;
            _accessor = accessor;
            _ip = new List<string>();
            _wzmCore = wzmCore;
            _userManager = user;
            _logger = logger;
            _activeDevices = activeDevices;
            _etecsaInternet = etecsaInternet;

        }

        //[LicenseAttribute(validated)]
        public async Task<IActionResult> Index()
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                if (User.IsInRole("Admin"))
                {


                    var pagos = _db.Pagos.Where(c => c.FechaPago.Month == DateTime.Now.Month && c.FechaPago.Year.Equals(DateTime.Now.Year)).ToList();

                    var pagoAnual = _db.Pagos.Where(c => c.FechaPago.Year == DateTime.Now.Year).ToList();
                    decimal total = 0;
                    decimal totalAnual = 0;
                    foreach (var item in pagos)
                    {
                        try
                        {
                            total += (decimal)item.Pagado;
                        }
                        catch (Exception e)
                        {

                            throw;
                        }

                    }

                    foreach (var item in pagoAnual)
                    {
                        try
                        {
                            totalAnual += (decimal)item.Pagado;
                        }
                        catch (Exception)
                        {

                            throw;
                        }

                    }
                    List<ApplicationUsers> clientes = new List<ApplicationUsers>();
                    List<ApplicationUsers> sinClasificadorPago = new List<ApplicationUsers>();
                    var usuarios = _userManager.Users;
                    foreach (var item in usuarios)
                    {
                        if (item.ClasificadorPagoId != null)
                        {
                            item.ClasificadorPago = _db.ClasificadorPagos.FirstOrDefault(c => c.Id == item.ClasificadorPagoId);
                            clientes.Add(item);
                        }
                        else
                        {
                            sinClasificadorPago.Add(item);
                        }
                    }

                    var clientesDePago = clientes.Where(c => c.ClasificadorPago.PaymentType != PaymentType.Gratis).ToList();
                    var clientesGratis = clientes.Where(c => c.ClasificadorPago.PaymentType == PaymentType.Gratis);

                    int usuariosAPagar = clientesDePago.Count() - pagos.Count();
                    decimal porCobrarHabilitados = (decimal)clientesDePago.Where(c => c.Estado == StatusEnum.Habilitado).Sum(c => c.ClasificadorPago.Importe);
                    decimal porCobrarDeshabilitados = (decimal)clientesDePago.Where(c => c.Estado == StatusEnum.Deshabilitado).Sum(c => c.ClasificadorPago.Importe);
                    int blockedUser = 0;
                    foreach (var item in usuarios)
                    {
                        if (await _userManager.IsLockedOutAsync(item))
                        {
                            blockedUser += 1;
                        }
                    }

                    var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
                    SystemResource systemResource = new SystemResource();

                    if (credential != null)
                    {
                        try
                        {
                            var dhcpLeases = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(credential);
                            int usuariosConectados = dhcpLeases.Where(c => c.Status == "bound").Count();
                            int usuariosInactivos = dhcpLeases.Count() - usuariosConectados;

                            ViewData["UsuariosConectados"] = usuariosConectados;
                            ViewData["UsuariosInactivos"] = usuariosInactivos;

                            using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                            {
                                systemResource = connection.LoadSingle<SystemResource>();

                            }
                        }
                        catch (Exception)
                        {
                            ViewData["UsuariosConectados"] = 0;
                            ViewData["UsuariosInactivos"] = 0;
                        }

                    }
                    else
                    {
                        ViewData["UsuariosConectados"] = 0;
                        ViewData["UsuariosInactivos"] = 0;
                    }



                    var dispositivos = _db.Dispositivos.Count();


                    ViewData["SinClasificador"] = sinClasificadorPago.Count();
                    ViewData["Gratis"] = clientesGratis.Count();
                    ViewData["Dispositivos"] = dispositivos;
                    ViewData["Activos"] = _activeDevices.Devices.Where(c => c.Active).Count();
                    ViewData["Inactivos"] = _activeDevices.Devices.Where(c => !c.Active).Count();
                    ViewData["DispositivosActivos"] = dispositivos;
                    ViewData["CantUsuarios"] = clientes.Count() + sinClasificadorPago.Count();
                    ViewData["UsuaraiosBloqueados"] = blockedUser;
                    ViewData["UsuaraiosActivos"] = usuarios.Count() - blockedUser;
                    ViewData["Pagos"] = pagos.Count();
                    ViewData["PagosAnuales"] = pagoAnual.Count();
                    ViewData["Total"] = total;
                    ViewData["TotalAnual"] = $"{totalAnual} en {_db.Pagos.Where(c => c.FechaPago.Year.Equals(DateTime.Now.Year)).GroupBy(c => c.FechaPago.Month).Count()} meses";
                    ViewData["UsuariosPagar"] = usuariosAPagar;
                    ViewData["PorCobrarH"] = porCobrarHabilitados - total;
                    ViewData["PorCobrarD"] = porCobrarDeshabilitados;
                    ViewData["ClientDevices"] = _db.ClientServices.Count();
                    try
                    {
                        ViewData["Portales"] = _wzmCore.MikrotikManager.PortalNauta.GetPortalesAsync(credential).Result.Count();
                    }
                    catch (Exception)
                    {

                        ViewData["Portales"] = 0;
                    }

                    ViewData["Morosos"] = _db.GetApplicationClients().Where(c => c.UserPaymentState == UserPaymentState.Atrasado).Count();
                    return View("IndexAdmin", systemResource);
                }
                if (User.IsInRole("Sub_Admin"))
                {
                    var user = User.Identity.Name;

                    var mk = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true);
                    var connection = ConnectionFactory.OpenConnection(mk.Dispositivo.Firmware.GetApiVersion(), mk.Dispositivo.Ip, mk.User, mk.Password);
                    List<WirelessInterfaceModel> portales = _wzmCore.MikrotikManager.PortalNauta.GetPortalesbyUser(connection, user);
                    connection.Dispose();
                    return View("IndexSub_Admin", portales);
                }
                else
                {
                    var usuario = await _userManager.GetUserAsync(User) as ApplicationUsers;

                    if (usuario.UserPaymentState == UserPaymentState.Atrasado)
                    {
                        return View("Atrasado");
                    }
                    else
                    {

                        ClientInfoViewModel clientInfo = new ClientInfoViewModel();
                        if (!_db.ValidateMKCrendential().Validate)
                        {
                            TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                            return Redirect("MikrotikCredentials");
                        }
                        MikrotikCredentials credential = new MikrotikCredentials();
                        if (_db.AdminDevicesRoles.Any(c => c.DeviceRoleId.Equals(3)))
                        {
                            int id = _db.AdminDevicesRoles.FirstOrDefault(c => c.DeviceRoleId.Equals(3)).AdminDeviceId;


                            credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.DispositivoId.Equals(id));
                            
                        }
                        else
                        {
                            credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
                        }

                        
                        if (_db.UsersDevices.Any(c => c.ApplicationUserId.Equals(usuario.Id)))
                        {
                            var dispositivos = _db.UsersDevices.Where(c => c.ApplicationUserId.Equals(usuario.Id)).ToList();
                            clientInfo = _wzmCore.UserManagement.FillClientInfo(false, _accessor.GetClientIp(), string.Empty, credential);
                            //if (dispositivos.Any(c => c.Ip.Equals(_accessor.GetClientIp())))
                            //{
                            //    clientInfo = _wzmCore.UserManagement.FillClientInfo(false, _accessor.GetClientIp(), string.Empty, credential);

                            //    return View(clientInfo);
                            //}
                            return View(clientInfo);
                            //return NotFound();
                        }



                        return NotFound();
                    }

                }
            }

            else
            {
                return RedirectToAction("Index", "License");
            }


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Desconectar(string id)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                try
                {
                    string prefix = "wlan";

                    for (int i = 0; i < id.Length; i++)
                    {
                        if (char.IsNumber(id[i]))
                        {
                            prefix += id[i];
                        }
                    }

                    var mk = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true);


                    using (var connection = ConnectionFactory.OpenConnection(mk.Dispositivo.Firmware.GetApiVersion(), mk.Dispositivo.Ip, mk.User, mk.Password))
                    {
                        var wireless = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWireless(connection).FirstOrDefault(c => c.Name.Equals(prefix));

                        //var newMac = _wzmCore.MikrotikManager.MACChanger.GetRandomWifiMacAddress();

                        string UserName = _userManager.GetUserName(User);

                        _wzmCore.MikrotikManager.Ip.DhcpClient.Release(string.Empty, wireless.Name, mk, UserName);

                        if (_etecsaInternet.Portales.Any(c => c.Portal.Equals(prefix)))
                        {
                            _etecsaInternet.Portales.Remove(_etecsaInternet.Portales.FirstOrDefault(c => c.Portal.Equals(prefix)));

                        }


                        _logger.LogWarning($"{UserName}: ha cerrado sesión en {wireless.Name}");
                        //wireless.MacAddress = newMac;
                    }

                    TempData["success"] = "Su cuenta ha sido desconectada correctamente.";
                    return RedirectToAction("Index", "Home");
                }
                catch (System.Exception e)
                {

                    TempData["error"] = e.Message;

                    return RedirectToAction("Index", "Home");
                }

            }

            return RedirectToAction("Index", "License");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult InternetInfo(string wlan, string ip)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                if (wlan != null)
                {
                    if (wlan != string.Empty)
                    {
                        string prefix = "wlan";

                        for (int i = 0; i < wlan.Length; i++)
                        {
                            if (char.IsNumber(wlan[i]))
                            {
                                prefix += wlan[i];
                            }
                        }

                        MikrotikCredentials credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
                        ClientInfoViewModel clientInfo = _wzmCore.UserManagement.FillClientInfo(true, ip, prefix.ToString(), credential);
                        return PartialView("_IndexPartial", clientInfo);
                    }
                }
                return PartialView("_IndexPartial", new ClientInfoViewModel());
            }

            return RedirectToAction("Index", "License");
        }


        [HttpGet]
        public IActionResult SystemResources()
        {

            SystemResource sr = new SystemResource();

            var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
            if (credential != null)
            {
                using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                {
                    sr = connection.LoadSingle<SystemResource>();

                }
            }
            return PartialView("_ResourcesPartial", sr);
        }

        [HttpGet]
        public IActionResult GetActiveDevices()
        {

            ViewData["Dispositivos"] = _db.Dispositivos.Count();
            ViewData["Activos"] = _activeDevices.Devices.Where(c => c.Active).Count();
            ViewData["Inactivos"] = _activeDevices.Devices.Where(c => !c.Active).Count();
            return PartialView("_ActiveDevicesPartial");
        }
    }



}


