﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using wzm.ViewModels;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Data.Mikrotik;
using wzmCore.Extensions;
using wzmCore.Services.System;

namespace wzm.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class PortalNautaController : Controller
    {

        #region Private Memebers 
        private readonly IWzmCore _wzmCore;
        private ApplicationDbContext _db;
        private UserManager<ApplicationUsers> _userManager;
        #endregion

        private EtecsaInternetPortalsJob _etecsaInternet;
        public PortalNautaController(ApplicationDbContext db, EtecsaInternetPortalsJob etecsaInternet, IWzmCore wzmCore, UserManager<ApplicationUsers> userManager)
        {
            _db = db;
            _wzmCore = wzmCore;
            _userManager = userManager;
            _etecsaInternet = etecsaInternet;
        }
        [Authorize(Roles = "Admin")]
        // GET: Index
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                if (!_db.ValidateMKCrendential().Validate)
                {
                    TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                    return Redirect("MikrotikCredentials");
                }
                //List<PortalesEtecsa> portales = new List<PortalesEtecsa>();
                List<WirelessInterfaceModel> portales = new List<WirelessInterfaceModel>();
                var mk = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true);

                if (_wzmCore.MikrotikManager.GetLicenseLevel(mk) >= 4)
                {
                    //var watch = Stopwatch.StartNew();
                    try
                    {
                       portales = await _wzmCore.MikrotikManager.PortalNauta.GetPortalesAsync(mk);

                        //var portalesInternet = _db.PortalNauta.ToList();

                        //foreach (var item in portalesInternet)
                        //{
                        //    portales.Find(c => c.Wireless.Equals(item.Portal)).Internet = true;
                        //}

                        //watch.Stop();
                        //ViewData["ElapsedTime"] = watch.ElapsedMilliseconds;
                    }
                    catch (Exception e)
                    {
                        //watch.Stop();
                        TempData["error"] = e.Message;
                    }

                    using (ITikConnection conection = ConnectionFactory.OpenConnection(mk.Dispositivo.Firmware.GetApiVersion(), mk.Dispositivo.Ip, mk.User, mk.Password))
                    {
                        var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                        ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute");

                    };

                    return View(portales.OrderBy(c => c.Id));
                }
                else
                {
                    TempData["error"] = "Su dispositivo no consta con Licencia Level 4 ó superior. Por favor adquiera una";
                    return View(portales);
                }
            }
            return RedirectToAction("Index", "License");

        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Create()
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                //if (_wzmCore.GetLicenseLevel())
                //{

                //}
                return View(new CantidadViewModel { Ip = _wzmCore.MikrotikManager.Ip.DhcpClient.GetGateWay(_db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true)), QoS = true });
            }
            return RedirectToAction("Index", "License");

        }

        [Authorize(Roles = "Admin")]
        // POST: Wireless/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Cantidad,Ip,QoS")] CantidadViewModel cantidadViewModel)
        {

            if (_wzmCore.LicenseManager.Validated)
            {
                if (ModelState.IsValid)

                    try
                    {
                        _wzmCore.MikrotikManager.PortalNauta.AddNautaPortal(cantidadViewModel.Cantidad, cantidadViewModel.Ip, cantidadViewModel.QoS, _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true));

                        return RedirectToAction(nameof(Index));
                    }
                    catch (Exception e)
                    {
                        TempData["error"] = e.Message;

                        return RedirectToAction("Index");
                    }

                return View(cantidadViewModel);
            }
            return RedirectToAction("Index", "License");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult Delete(string id)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                PortalesEtecsa portalNautaViewModel = new PortalesEtecsa
                {
                    Interfaz = id,
                };
                return View(portalNautaViewModel);
            }
            return RedirectToAction("Index", "License");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(string wireless)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                try
                {
                    var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true);
                    using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                    {
                        var w = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWireless(connection).First(c => c.Name == wireless);
                        var r = _wzmCore.MikrotikManager.Ip.Routes.GetRoutes(connection).First(c => c.Gateway.Contains(w.Name));
                        _wzmCore.MikrotikManager.PortalNauta.DeletePortal(wireless, connection, w, r);
                    }

                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                }
                TempData["success"] = $"El portal {wireless} ha sido eliminado correctamente";
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction("Index", "License");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult DeleteByRange()
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
                var w = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWirelessViewModel(credential);
                var p = w.OrderBy(c => c.Id).ToList();
                ViewData["DesdeId"] = new SelectList(p, "Name", "Name");
                ViewData["HastaId"] = new SelectList(p, "Name", "Name");
                return View();
            }
            return RedirectToAction("Index", "License");
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteByRange([Bind("Desde,Hasta")] DeleteRangeViewModel deleteRangeViewModel)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                if (deleteRangeViewModel == null)
                {
                    return NotFound();
                }
                if (int.Parse(deleteRangeViewModel.Desde.Replace("wlan", "")) >= int.Parse(deleteRangeViewModel.Hasta.Replace("wlan", "")))
                {
                    ModelState.AddModelError("", "asdasdad");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        await _wzmCore.MikrotikManager.PortalNauta.DeleteByRange(deleteRangeViewModel.Desde, deleteRangeViewModel.Hasta, _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true));
                        return RedirectToAction(nameof(Index));
                    }
                    catch
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
                var credential = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);

                var portales = _wzmCore.MikrotikManager.Interface.Wireless.GetAllWirelessViewModel(credential);
                var p = portales.OrderBy(c => c.Id).ToList();
                ViewData["DesdeId"] = new SelectList(p, "Name", "Name", deleteRangeViewModel.Desde);
                ViewData["HastaId"] = new SelectList(p, "Name", "Name", deleteRangeViewModel.Hasta);
                return View();

            }
            return RedirectToAction("Index", "License");


        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult Deshabilitar(string id)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                if (id == string.Empty)
                {
                    return NotFound();

                }
                PortalesEtecsa portalNautaViewModel = new PortalesEtecsa
                {
                    Interfaz = id,
                };
                return View(portalNautaViewModel);
            }
            return RedirectToAction("Index", "License");
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeshabilitarPost(string wireless)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                if (wireless == string.Empty)
                {
                    return NotFound();

                }
                PortalesEtecsa portalNautaViewModel = new PortalesEtecsa
                {
                    Interfaz = wireless,
                };
                try
                {
                    await _wzmCore.MikrotikManager.PortalNauta.Disable(wireless, _db.MikrotikCredentials.FirstOrDefault(d => d.Selected == true));
                    return RedirectToAction(nameof(Index));
                }
                catch
                {

                    return View(portalNautaViewModel);
                }
            }
            return RedirectToAction("Index", "License");

        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> GetTablePartial()
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                var db = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
                if (_wzmCore.MikrotikManager.GetLicenseLevel(db) >= 4)
                {
                    //var watch = Stopwatch.StartNew();

                    var portales = await _wzmCore.MikrotikManager.PortalNauta.GetPortalesAsync(db);
                    portales.OrderBy(c => c.Id);
                    

                    foreach (var item in _etecsaInternet.Portales)
                    {
                        if (portales.Any(c => c.Name.Equals(item.Portal)))
                        {
                            portales.Find(c => c.Name.Equals(item.Portal)).Internet = true;
                        }

                        
                    }

                    //watch.Stop();
                    //ViewData["ElapsedTime"] = watch.ElapsedMilliseconds;
                    return PartialView("_TablePartial", portales);
                }
                return PartialView("_TablePartial", new List<PortalesEtecsa>());
            }
            return RedirectToAction("Index", "License");
        }

        [Authorize(Roles = "Admin,Sub_Admin")]
        [HttpGet]
        public IActionResult GetTableByUserPartial()
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                var mk = _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected);
                if (_wzmCore.MikrotikManager.GetLicenseLevel(mk) >= 4)
                {
                    //var watch = Stopwatch.StartNew();
                    var connection = ConnectionFactory.OpenConnection(mk.Dispositivo.Firmware.GetApiVersion(), mk.Dispositivo.Ip, mk.User, mk.Password);
                    var portales = _wzmCore.MikrotikManager.PortalNauta.GetPortalesbyUser(connection,User.Identity.Name);
                    portales.OrderBy(c => c.Id);

                    foreach (var item in _etecsaInternet.Portales)
                    {
                        if (portales.Any(c => c.Name.Equals(item.Portal)))
                        {
                            portales.Find(c => c.Name.Equals(item.Portal)).Internet = true;
                        }
                    }

                    //watch.Stop();
                    //ViewData["ElapsedTime"] = watch.ElapsedMilliseconds;
                    return PartialView("_TablePartial", portales);
                }
                return PartialView("_TablePartial", new List<PortalesEtecsa>());
            }
            return RedirectToAction("Index", "License");
        }

        [Authorize(Roles = "Admin,Sub_Admin")]
        [HttpGet]
        public IActionResult Desconectar(string id)
        {
            if (_wzmCore.LicenseManager.Validated)
            {
                try
                {
                    _wzmCore.MikrotikManager.Ip.DhcpClient.Release(string.Empty, id, _db.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(d => d.Selected == true), _userManager.GetUserName(User));
                    if (_etecsaInternet.Portales.Any(c => c.Portal.Equals(id)))
                    {
                        _etecsaInternet.Portales.Remove(_etecsaInternet.Portales.FirstOrDefault(c => c.Portal.Equals(id)));
                       
                    }
                    TempData["success"] = "La conexión a Internet se ha desconectado correctamente";

                    if (User.IsInRole("Sub_Admin"))
                    {
                        return RedirectToAction("Index","Home");
                    }

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                    return RedirectToAction(nameof(Index));
                }
            }
            return RedirectToAction("Index", "License");

        }

        public IActionResult GetJson()
        {
            //var db = _db.MikrotikCredentials.FirstOrDefault(d => d.Selected == true);
            //List<PortalNautaViewModel> portales = new List<PortalNautaViewModel>();
            //dynamic response = new
            //{
            //    Data = portales,
            //    Draw = "1",
            //    RecordsFiltered = portales.Count(),
            //    RecordsTotal = portales.Count()
            //};
            //await _wzmCore.MikrotikManager.PortalNauta.RunCommand(() => _wzmCore.MikrotikManager.PortalNauta.Working, async () =>
            //{
            //    //await Task.Delay(1000);
            //    await Task.Run(() =>
            //    {
            //        try
            //        {
            //            portales = _wzmCore.MikrotikManager.PortalNauta.GetPortales(portales, _db.MikrotikCredentials.FirstOrDefault(d => d.Selected == true));
            //            response = new
            //            {
            //                Data = portales,
            //                Draw = "1",
            //                RecordsFiltered = portales.Count(),
            //                RecordsTotal = portales.Count()
            //            };

            //        }
            //        catch (Exception e)
            //        {
            //            TempData["error"] = e.Message;
            //        }

            //    });

            //});

            //dynamic response = new
            //{
            //    Data = portales,
            //    Draw = "1",
            //    RecordsFiltered = portales.Count(),
            //    RecordsTotal = portales.Count()
            //};

            //try
            //{
            //    portales = _wzmCore.MikrotikManager.PortalNauta.GetPortales(db);
            //    if (portales != null)
            //    {
            //        response = new
            //        {
            //            Data = portales,
            //            Draw = "1",
            //            RecordsFiltered = portales.Count(),
            //            RecordsTotal = portales.Count()
            //        };
            //    }

            //}
            //catch (Exception e)
            //{
            //    //TempData["error"] = e.Message;
            //}

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetClientsPortal(string portal)
        {
            if (_wzmCore.LicenseManager.Validated)
            {

                var applicationUsers = _db.UsersDevices.Where(c => c.PortalDeOrigen.Equals(portal)).ToList();

                return PartialView("_UsersPartial", applicationUsers);
            }
            return RedirectToAction("Index", "License");
        }
    }
}