﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using wzm.Utils;
using wzm.ViewModels;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmCore.Utils;
using wzmCore.ViewModels;
using wzmData.Enum;
using System.Collections.Generic;
using wzmCore.Extensions;
using wzmCore.Services.System;
using SshCore;
using UbiquitiCore.Discovery;
using wzmData.Ubiquiti.UbiquitiProperties;
using wzmData.Ubiquiti;
using WzmCommon.Files;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DispositivosUbiquitiController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUsers> _userManager;
        private readonly ScanJob _scanJob;
        private readonly IWzmCore _wzmCore;

        public DispositivosUbiquitiController(ApplicationDbContext context, UserManager<ApplicationUsers> userManager, IWzmCore wzmCore, ScanJob scanJob)
        {
            _context = context;
            _userManager = userManager;
            _scanJob = scanJob;
            _wzmCore = wzmCore;
        }

        [HttpGet]
        public IActionResult IndexStation()
        {

            return View(_context.Dispositivos.Include(d => d.DeviceCredential).Where(c => c is StationDevice && c.Fabricante == DeviceTypeEnum.Ubiquiti).ToList());
        }

        public IActionResult IndexAp()
        {
            
            return View(_context.Dispositivos.Include(d => d.DeviceCredential).Where(c => c is ApDevice && c.Fabricante == DeviceTypeEnum.Ubiquiti).ToList());

        }

        [HttpGet]
        public IActionResult CreateStation()
        {
            ViewData["Platforms"] = new SelectList(new Device().DevicePlatforms(), "Description", "Description");
            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo");
            return View(new StationDevice());// Le paso un station device para poder coger el valor de publico
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateStation([Bind("Id,Mac,Hostname,ApDeviceId,Platform,Firmware,SSID,Ip,Ubicacion,WirelessSecurity,Publico,MacApPublico")] StationDevice dispositivo)
        {
            if (dispositivo.Publico)
            {
                if (dispositivo.MacApPublico == null)
                {
                    ModelState.AddModelError("", "El campo MAC del AP Público es oblogatorio.");
                }

                if (dispositivo.SSID == null)
                {
                    ModelState.AddModelError("", "El campo SSID es oblogatorio.");
                }
                dispositivo.ApDeviceId = null;
            }
            else
            {
                dispositivo.SSID = _context.Dispositivos.Find(dispositivo.ApDeviceId).SSID;
            }
            dispositivo.Fabricante = DeviceTypeEnum.Ubiquiti;
            dispositivo.WirelessMode = WirelessMode.Station;
            if (ModelState.IsValid)
            {


                _context.Add(dispositivo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(IndexStation));
            }
            ViewData["Platforms"] = new SelectList(new Device().DevicePlatforms(), "Description", "Description", dispositivo.Platform);
            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo", dispositivo.ApDeviceId);
            return View(dispositivo);
        }

        [HttpGet]
        public async Task<IActionResult> EditStation(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispositivo = await _context.Dispositivos.FindAsync(id) as StationDevice;
            if (dispositivo == null)
            {
                return NotFound();
            }
            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo", dispositivo.ApDeviceId);
            return View(dispositivo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditStation(int id, [Bind("Id,Mac,Hostname,Platform,Firmware,ApDeviceId,SSID,Ip,Ubicacion,WirelessMode,MacApPublico,Publico,DeviceTypeEnum,WirelessSecurity")] StationDevice dispositivo)
        {
            if (id != dispositivo.Id)
            {
                return NotFound();
            }
            if (dispositivo.Publico)
            {
                if (dispositivo.MacApPublico == null)
                {
                    ModelState.AddModelError("", "El campo MAC del AP Público es oblogatorio.");
                }

                if (dispositivo.SSID == null)
                {
                    ModelState.AddModelError("", "El campo SSID es oblogatorio.");
                }
            }
            else
            {
                dispositivo.SSID = _context.Dispositivos.Find(dispositivo.ApDeviceId).SSID;
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dispositivo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DispositivoExists(dispositivo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(IndexStation));
            }
            ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo", dispositivo.ApDeviceId);
            return View(dispositivo);
        }

        [HttpGet]
        public IActionResult CreateAp()
        {
            ViewData["Platforms"] = new SelectList(new Device().DevicePlatforms(), "Description", "Description");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAp([Bind("Id, Mac, Hostname, Platform, Firmware, SSID, Ip, Ubicacion, WirelessSecurity")] ApDevice apDevice)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    apDevice.Fabricante = DeviceTypeEnum.Ubiquiti;
                    apDevice.WirelessMode = WirelessMode.Ap;
                    _context.Add(apDevice);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(IndexAp));
                }
                ViewData["Platforms"] = new SelectList(new Device().DevicePlatforms(), "Description", "Description", apDevice.Platform);
                return View(apDevice);
            }
            catch (Exception)
            {

                return RedirectToAction(nameof(IndexAp));
            }
        }

        [HttpGet]
        public async Task<IActionResult> EditAp(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispositivo = await _context.Dispositivos.FindAsync(id) as ApDevice;
            if (dispositivo == null)
            {
                return NotFound();
            }

            return View(dispositivo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAp(int id, [Bind("Id,Mac,Hostname,Platform,Firmware,SSID,Ip,WirelessMode,Ubicacion, WirelessSecurity")] ApDevice dispositivo)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dispositivo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DispositivoExists(dispositivo.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(IndexAp));
            }

            return View(dispositivo);
        }

        /// <summary>
        /// Actualiza las estaciones que estén conectado al AP o que estén en la lista ACL
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> UpdateStations(int id)
        {
            if (DispositivoExists(id))
            {
                var ap = _context.Dispositivos.Find(id);

                if (_context.DeviceCredentials.Any(c => c.DispositivoId == id))
                {
                    var credential = _context.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId == id);

                    if (await _wzmCore.UbiquitiManager.CanConnectToUbiquitiDevice(credential))
                    {
                        var stations = await _wzmCore.UbiquitiManager.UbiquitiACLManager.GetDeviceUsersAsync(credential, "acl");

                        var conectedDevices = (List<ApInfo>)_wzmCore.UbiquitiManager.SshConnection.GetJson(CommandType.ApInformation);

                        var dispositivosBd = _context.StationDevices;

                        foreach (var item in stations)
                        {
                            if (dispositivosBd.Any(c => c.Mac.Equals(item.Mac)))
                            {
                                var dispositivoEnBd = dispositivosBd.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                                if (!dispositivoEnBd.ApDeviceId.Equals(id))
                                {
                                    dispositivoEnBd.ApDeviceId = id;

                                }

                                if (conectedDevices.Any(c => c.Mac.Equals(item.Mac)))
                                {
                                    var conectedDevice = conectedDevices.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                                    if (!dispositivoEnBd.Ip.Equals(conectedDevice.Lastip))
                                    {
                                        dispositivoEnBd.Ip = conectedDevice.Lastip;

                                    }
                                    if (!dispositivoEnBd.Hostname.Equals(conectedDevice.Remote.Hostname))
                                    {
                                        dispositivoEnBd.Hostname = conectedDevice.Remote.Hostname;

                                    }
                                    if (!dispositivoEnBd.Platform.Equals(conectedDevice.Remote.Platform))
                                    {
                                        dispositivoEnBd.Platform = conectedDevice.Remote.Platform;

                                    }
                                }

                                _context.Update(dispositivoEnBd);
                                _context.SaveChanges();
                                                            
                            }

                            else
                            {
                                if (_context.UsersDevices.Any(c => c.Mac.Equals(item.Mac)))
                                {
                                    var userDevice = _context.UsersDevices.Include(c => c.ApplicationUser).FirstOrDefault(c => c.Mac.Equals(item.Mac));

                                    if (conectedDevices.Any(c => c.Mac.Equals(item.Mac)))
                                    {
                                        var conectedDevice = conectedDevices.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                                        if (!userDevice.Ip.Equals(conectedDevice.Lastip))
                                        {
                                            userDevice.Ip = conectedDevice.Lastip;
                                            _context.Update(userDevice);
                                            _context.SaveChanges();
                                        }
                                        
                                    }

                                   var  user= await _userManager.FindByIdAsync(userDevice.ApplicationUserId);

                                    if (user.DispositivoId != id)
                                    {
                                        user.DispositivoId = id;

                                       await _userManager.UpdateAsync(user);
                                                                                                                      
                                    }

                                    
                                }

                                

                                else
                                {

                                }
                            }

                            if (conectedDevices.Any(c => c.Mac.Equals(item.Mac)))
                            {
                                //if (_context.UsersDevices.Any())
                                //{

                                //}
                                //var conectedDevice = conectedDevices.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                            }
                        }


                    }
                    else
                    {
                        TempData["error"] = "No se pudo establecer conexión con el dispositivo";
                        return RedirectToAction(nameof(IndexAp));
                    }
                }
                else
                {
                    TempData["error"] = "El dispositivo seleccionado no posee credenciales";
                    return RedirectToAction(nameof(IndexAp));
                }

            }
            else
            {
                return NotFound();
            }

            

            return RedirectToAction(nameof(IndexAp));
        }

        [HttpGet]
        public async Task<IActionResult> UpdateAll(WirelessMode id)
        {

            id = WirelessMode.Station;

            var dispositivos = _context.Dispositivos.Where(c => c.WirelessMode == id && c.Fabricante == DeviceTypeEnum.Ubiquiti).ToList();

            foreach (var item in dispositivos)
            {

                if (await _wzmCore.UbiquitiManager.CanConnectToUbiquitiDevice(_context.DeviceCredentials.Include(c => c.Dispositivo).ToList(), _scanJob.Devices, item, _context))
                {
                    await DeviceChanges(item);
                }

            }
            return RedirectToAction(nameof(IndexAp));
        }

        /// <summary>
        /// Realiza los cmabios en el sistema del dispositivo.
        /// </summary>
        /// <param name="dispositivo"></param>
        /// <returns></returns>
        private async Task DeviceChanges(Dispositivo dispositivo)
        {
            var infoActual = (UbiquitiObject)_wzmCore.UbiquitiManager.SshConnection.GetJson(CommandType.UbiquitiObject);

            WirelessMode deviceWirelessMode = _wzmCore.UbiquitiManager.UbiquitiWirelessManager.GetWirelessMode(infoActual);

            if (deviceWirelessMode != dispositivo.WirelessMode)
            {
                switch (deviceWirelessMode)
                {
                    case WirelessMode.Station:
                        var apInfo = (ApInfo)_wzmCore.UbiquitiManager.SshConnection.GetJson(CommandType.ApInformation);
                        if (_context.Dispositivos.Any(c => c.Mac.Equals(apInfo.Mac)))
                        {
                            await DeviceStatus.ChangeToStationAsync(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap && c.Mac.Equals(apInfo.Mac)) as ApDevice, dispositivo.Id, _context);
                        }
                        break;
                    case WirelessMode.Ap:
                        break;
                    case WirelessMode.Desconocido:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (deviceWirelessMode)
                {
                    case WirelessMode.Station:
                        var apInfo = (List<ApInfo>)_wzmCore.UbiquitiManager.SshConnection.GetJson(CommandType.ApInformation);
                        var station = _context.StationDevices.Include(c => c.ApDevice).FirstOrDefault(c => c.Id == dispositivo.Id);
                        DeviceStatus.UpdateStationDevice(station, infoActual, _scanJob.Devices, apInfo[0], _context);
                        break;
                    case WirelessMode.Ap:
                        break;
                    case WirelessMode.Desconocido:
                        break;
                    default:
                        break;
                }
            }
        }


        public IActionResult ShowAp(string id)
        {
            if (int.Parse(id) == 1)
            {
                ViewData["Ap"] = new SelectList(_context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap), "Id", "GetDispositivo");

                return PartialView("_ApPartial");
            }

            return PartialView("_EmptyPartial");
        }



        [HttpGet]
        public async Task<IActionResult> ChangeToStation(int id)
        {
            var dispositivo = await _context.Dispositivos.FindAsync(id);
            var ap = _context.Dispositivos.Where(c => c is ApDevice);
            if (dispositivo == null)
            {
                return NotFound();
            }

            ViewData["Ap"] = new SelectList(ap.ToList(), "Id", "GetDispositivo");
            return View(new ToStationViewModel() { DeviceId = dispositivo.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeToStation(int DeviceId, [Bind("DeviceId,ApId,Mac,SSID,Publico")] ToStationViewModel toStation)
        {
            if (toStation.Publico)
            {
                if (toStation.SSID == null)
                {
                    ModelState.AddModelError("", "El campo SSID es obligatorio.");
                }
                if (toStation.Mac == null)
                {
                    ModelState.AddModelError("", "El campo MAC es obligatorio.");
                }
            }

            if (ModelState.IsValid)
            {
                var dispositivo = await _context.Dispositivos.FindAsync(DeviceId);

                if (toStation.Publico)
                {
                    await DeviceStatus.ChangeToStationAsync(toStation.Mac, toStation.SSID, dispositivo.Id, _context);

                }
                else
                {
                    var ap = await _context.Dispositivos.FindAsync(toStation.ApId) as ApDevice;

                    if (dispositivo != null && ap != null)
                    {
                        await DeviceStatus.ChangeToStationAsync(ap, dispositivo.Id, _context);

                    }
                }

                return RedirectToAction(nameof(IndexStation));

            }
            var aps = _context.Dispositivos.Where(c => c is ApDevice);
            ViewData["Ap"] = new SelectList(aps.ToList(), "Id", "GetDispositivo", toStation.ApId);
            return View(toStation);


        }

        [HttpGet]
        public async Task<IActionResult> ChangeToAp(int id)
        {
            var dispositivo = await _context.Dispositivos.FindAsync(id);

            var ap = await _context.Dispositivos.FindAsync(id);

            if (dispositivo != null && ap != null)
            {
                await DeviceStatus.ChangeToApAsync(dispositivo.Id, _context);
                return RedirectToAction(nameof(IndexAp));
            }

            return NotFound();
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dispositivo = await _context.Dispositivos
                .Include(d => d.DeviceCredential)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dispositivo == null)
            {
                return NotFound();
            }

            return View(dispositivo);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dispositivo = await _context.Dispositivos.FindAsync(id);

            var users = _context.GetApplicationClients().Where(c => c.DispositivoId.Equals(id));

            foreach (var item in users)
            {
                if (item.DispositivoId.Equals(id))
                {
                    await _userManager.DeleteAsync(item);
                }
            }


            _context.Dispositivos.Remove(dispositivo);
            await _context.SaveChangesAsync();

            switch (dispositivo.WirelessMode)
            {
                case WirelessMode.Station:
                    return RedirectToAction(nameof(IndexStation));
                case WirelessMode.Ap:
                    return RedirectToAction(nameof(IndexAp));
                case WirelessMode.Desconocido:
                    break;
                
            }
            return RedirectToAction(nameof(IndexStation));

        }

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteApConfirmed(int id)
        //{
        //    var dispositivo = await _context.Dispositivos.FindAsync(id);

        //    var users = _context.GetApplicationClients();

        //    foreach (var item in users)
        //    {
        //        if (item.DispositivoId.Equals(id))
        //        {
        //            await _userManager.DeleteAsync(item);
        //        }
        //    }


        //    _context.Dispositivos.Remove(dispositivo);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(IndexAp));
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeDeviceWirelessMode(ChangeWirelessModeViewModel wireless)
        {
            switch (wireless.WirelessMode)
            {
                case WirelessMode.Station:
                    if (wireless.OtherDevice == null)
                    {
                        ModelState.AddModelError("", "El campo Otro Dispositivo es obligatorio.");
                        return View(wireless);
                    }
                    var deviceToBeChanged = _context.Dispositivos.Find(wireless.DispositivoId);
                    var masterDeVice = _context.Dispositivos.Find((int)wireless.DispositivoId);

                    if (masterDeVice.WirelessMode == WirelessMode.Station)
                    {
                        ModelState.AddModelError("", "El dispositivo seleccionado esta en Modo Estación.");
                        return View(wireless);
                    }

                    switch (deviceToBeChanged.Fabricante)
                    {
                        case DeviceTypeEnum.Ubiquiti:
                            switch (masterDeVice.Fabricante)
                            {


                                case DeviceTypeEnum.Ubiquiti:

                                    var uc1 = _context.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId == wireless.DispositivoId);
                                    var uc2 = _context.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId == wireless.DispositivoId);

                                    if (uc1 == null || uc2 == null)
                                    {
                                        ModelState.AddModelError("", "Los dispositivos no contienen credenciales.");
                                        return View(wireless);
                                    }

                                    await _wzmCore.UbiquitiManager.UbiquitiWirelessManager.SetWirelessToStationMode(uc1, uc2, FileName.ConstructFileName(uc1.Dispositivo.Mac, _wzmCore.Random.Next()));
                                    break;
                                case DeviceTypeEnum.Mikrotik:
                                    break;

                                default:
                                    break;
                            }
                            break;
                        case DeviceTypeEnum.Mikrotik:
                            break;

                        default:
                            break;
                    }
                    break;
                case WirelessMode.Ap:
                    var deviceToBeChanged1 = _context.Dispositivos.Find(wireless.DispositivoId);
                    switch (deviceToBeChanged1.Fabricante)
                    {
                        case DeviceTypeEnum.Ubiquiti:
                            var uc1 = _context.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId == wireless.DispositivoId);
                            if (uc1 == null)
                            {
                                ModelState.AddModelError("", "Los dispositivos no contienen credenciales.");
                                return View(wireless);
                            }
                            await _wzmCore.UbiquitiManager.UbiquitiWirelessManager.SetWirelessToApMode(uc1, FileName.ConstructFileName(uc1.Dispositivo.Mac, _wzmCore.Random.Next()));
                            break;
                        case DeviceTypeEnum.Mikrotik:
                            break;
                        case DeviceTypeEnum.TPLink:
                            break;
                        case DeviceTypeEnum.Otros:
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
            return View();
        }

        private bool DispositivoExists(int id)
        {
            return _context.Dispositivos.Any(e => e.Id == id);
        }
    }
}
