﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Interfaces;
using wzmData.Models;
using wzm.Extensions;
using wzmCore.Data.Mikrotik;
using wzmCore.Extensions;
using tik4net.Objects;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FirewallAddressController : Controller
    {

        ApplicationDbContext _db;

        private readonly IWzmCore _wzmCore;

        public FirewallAddressController(ApplicationDbContext db, IWzmCore wzmCore)
        {
            _db = db;

            _wzmCore = wzmCore;
        }

        // GET: MkAddresList
        public ActionResult Index()
        {
            if (!_db.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _db.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }

            IEnumerable<FirewallAddressList> addressList = _wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(_db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true));
            return View(addressList.OrderBy(c => c.List));


        }

        // GET: MkAddresList/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MkAddresList/Create
        public IActionResult AddAddressList()
        {
            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute");
                return View();
            };
        }

        // POST: MkAddresList/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddAddressList(string id, [Bind("Address,List,MarkRoute,Usuario,Disabled")] FirewallAddressViewModel firewallAddressViewModel)
        {
            var mikrotikCredential = _db.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);
            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {
                var addressList = _wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(connection);
                if (addressList.Any(d => d.Address.Equals(firewallAddressViewModel.Address)))
                {
                    ModelState.AddModelError(firewallAddressViewModel.Address, $"El IP: {firewallAddressViewModel.Address} introducido ya se encuentra en el AddressList del dispositivo.");
                }
                if (addressList.Any(d => d.Comment.Equals(firewallAddressViewModel.Usuario)))
                {
                    ModelState.AddModelError(firewallAddressViewModel.Usuario, $"El Usuario: {firewallAddressViewModel.Usuario} introducido ya se encuentra en el AddressList del dispositivo.");
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    firewallAddressViewModel.Id = id;
                    _wzmCore.MikrotikManager.Firewall.AddressList.CreateAddressList(firewallAddressViewModel, mikrotikCredential);
                    TempData["success"] = "La operación se ha realizado exitosamente";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["success"] = e.Message;
                    return View("Index");
                }

            }

            using (ITikConnection conection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["Salida"] = new SelectList(routes, "MarkRoute", "MarkRoute", firewallAddressViewModel.List);
                return View(firewallAddressViewModel);
            };


        }

        // GET: MkAddresList/Edit/5
        [HttpGet]
        public ActionResult Edit(string id)
        {
            var c = _db.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {

                List<FirewallAddressList> address = (List<FirewallAddressList>)_wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(conection);
                FirewallAddressList firewallAddressList = address.Find(d => d.Id == id);
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                var sd = firewallAddressList.List;
                ViewData["List"] = new SelectList(routes, "MarkRoute", "MarkRoute", sd);
                FirewallAddressViewModel f = new FirewallAddressViewModel()
                {
                    Id = firewallAddressList.Id,
                    Address = firewallAddressList.Address,
                    Usuario = firewallAddressList.Comment,
                    List = firewallAddressList.List,
                    Disabled = firewallAddressList.Disabled
                };
                return View(f);
            };


        }

        // POST: MkAddresList/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, [Bind("Address,List,MarkRoute,Usuario,Timeout")] FirewallAddressViewModel firewallAddressList)
        {
            try
            {
                firewallAddressList.Id = id;
                _wzmCore.MikrotikManager.Firewall.AddressList.UpdateAddressList(firewallAddressList, _db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true));
                TempData["success"] = "La operación se ha realizado exitosamente";

                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["success"] = e.Message;
                return View("Index");
            }

        }

        // GET: MkAddresList/Delete/5
        public ActionResult Delete(string id)
        {
            List<FirewallAddressList> address = (List<FirewallAddressList>)_wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(_db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true));
            FirewallAddressList firewallAddressList = address.Find(c => c.Id == id);
            return View(firewallAddressList);
        }

        // POST: MkAddresList/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id, IFormCollection collection)
        {
            List<FirewallAddressList> address = (List<FirewallAddressList>)_wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(_db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true));
            FirewallAddressList firewallAddressList = address.Find(c => c.Id == id);
            try
            {

                _wzmCore.MikrotikManager.Firewall.AddressList.DeleteAddressList(firewallAddressList, _db.MikrotikCredentials.First(c => c.Selected == true));
                TempData["success"] = "La operación se ha realizado exitosamente";
                
            }
            catch (Exception e)
            {
                TempData["error"] = e.Message;

            }

            return RedirectToAction(nameof(Index));

        }

        public IActionResult DisableOrEnable(string id, [Bind("Address,List,MarkRoute,Comment,Timeout,Disabled")] FirewallAddressViewModel firewallAddressList)
        {
            try
            {
                _wzmCore.MikrotikManager.Firewall.AddressList.DisableOrEnable(id, _db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true));
                TempData["success"] = "La operación se ha realizado exitosamente";
            }
            catch (Exception e)
            {
                TempData["error"] = e.Message;

            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult SaveToDatabase()
        {
            IEnumerable<FirewallAddressList> d = _wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(_db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true)).ToList();
            DeleteFromDatabse();
            foreach (var item in d)
            {
                _db.Add(item);
                _db.SaveChanges();
            }
            return View(nameof(Index), d);
        }

        private void DeleteFromDatabse()
        {
            //var sl = _db.FirewallAddressLists.ToList();
            //foreach (var item in sl)
            //{
            //    _db.Remove(item);
            //    _db.SaveChanges();
            //}
        }

        public IActionResult RestoreToDevice()
        {
            var cr = _db.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true);

            using (ITikConnection connection = ConnectionFactory.OpenConnection(cr.Dispositivo.Firmware.GetApiVersion(), cr.Dispositivo.Ip, cr.User, cr.Password))
            {
                var error = string.Empty;
                foreach (var item in _db.UsersDevices)
                {
                    if (item.PortalDeOrigen.Equals(string.Empty) || item.PortalDeOrigen == null || item.Ip.Equals(string.Empty) || item.Ip == null)
                    {
                        error = error + "," + item.Comment;
                    }
                    else
                    {
                        var f = new FirewallAddressList()
                        {
                            Address = item.Ip,
                            List = item.PortalDeOrigen,
                            Comment = item.Comment,
                        };
                        try
                        {
                            var addressList = connection.LoadAll<FirewallAddressList>().ToList();
                            if (addressList.Any(c => c.Address.Equals(item.Ip)))
                            {
                                var ips = addressList.Where(c => c.Address.Equals(item.Ip));

                                if (!ips.Any(c => c.Address.Equals(item.PortalDeOrigen)))
                                {
                                    connection.Save(f);
                                }
                            }
                            else
                            {
                                connection.Save(f);
                            }

                            
                        }
                        catch (Exception e)
                        {
                            TempData["error"] = $"Se importado satisfactoriamente el address list de algunos dispositivos. Error importando el dispositivo {item.Comment} ";
                            return RedirectToAction(nameof(Index));

                        }
                    }
                    

                }
                if (error!= string.Empty)
                {
                    TempData["error"] = $"Los dispositivos {error} no se han podido importar.";
                }
                return RedirectToAction(nameof(Index));
                //List<FirewallAddressList> d = _wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(connection).ToList();
                //DeleteFromDevice(d, connection);
                //ImportToDevice(connection);
                //d = _wzmCore.MikrotikManager.Firewall.AddressList.GetAddressList(connection).ToList();
                //return View(nameof(Index), d);
            }


        }

        private void ImportToDevice(ITikConnection connection)
        {

            // var d = _db.Dispositivos.ToList();
            //var df = _db.FirewallAddressLists.Count();
            // var l = _db.FirewallAddressLists.ToList();
            // foreach (var item in l)
            // {
            //     _wzmCore.MikrotikManager.Firewall.AddressList.Add(item, connection);
            // }
        }

        /// <summary>
        /// Elimina los Lease del DhcpServer
        /// </summary>
        /// <param name="d"></param>
        /// <param name="cr"></param>
        private void DeleteFromDevice(List<FirewallAddressList> d, ITikConnection connection)
        {
            if (d.Count > 0)
            {
                foreach (var item in d)
                {
                    _wzmCore.MikrotikManager.Firewall.AddressList.DeleteAddressList(item, connection);
                }
            }
        }


    }
}