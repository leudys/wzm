﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using wzm.Services;
using wzm.ViewModels;
using wzmCore.Interfaces;
using wzmCore.Services;
using wzmCore.Utils;
using wzmCore.WzmLicense;
using wzmData.Enum;
using wzmData.Models;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class LicenseController : Controller
    {

        private readonly IWzmCore _wzmCore;
        private readonly HostedServices _hostedService;
        private readonly UserManager<ApplicationUsers> _userManager;
        public LicenseController(IWzmCore wzmCore,UserManager<ApplicationUsers> userManager, IConfiguration configuration, IHostedService services)
        {
            _hostedService = services as HostedServices;
            _wzmCore = wzmCore;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            ViewData["ActivationCode"] = _wzmCore.LicenseManager.ExistActivationCode();

            var license = _wzmCore.LicenseManager.ShowLicense();
            return View(license);

        }

        [HttpGet]
        public IActionResult ActivationCode()
        {
            return View("ActivationCode");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ActivationCode([Bind("ClientName,Email,LicenseType")]ActivationCode activationCode)
        {
            

            if (ModelState.IsValid)
            {                               
                //_wzmCore.LicenseManager.SaveActivationCode(activationCode);
                return RedirectToAction(nameof(Index));
            }
            return View(activationCode);
        }

        [HttpGet]
        public IActionResult ImportLicense()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ImportLicense(FileUploadViewModel fileUpload)
        {
            await _hostedService.DeleteJob(ServiceType.LicenseChecker, new CancellationToken());
            Thread.Sleep(4000);
            var licenseValidator = await FileHelper.ProcessFromFile(fileUpload.UploadLicense);
            if (licenseValidator != string.Empty)
            {
                ModelState.AddModelError("", licenseValidator);
                return View(fileUpload);
            }
            else
            {


                await _wzmCore.LicenseManager.Import(fileUpload.UploadLicense);

                await _hostedService.CreateJob(ServicesGenerator.GetServiceModel(typeof(LicenseChecker)), new CancellationToken());

                return RedirectToAction(nameof(Index));
            }

        }

        [HttpGet]
        public async Task<IActionResult> DownloadActivationCode()
        {
            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot", "activacion.ac");

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".ac", "text/plain"},
                //{".txt", "text/plain"}
                //{".pdf", "application/pdf"},
                //{".doc", "application/vnd.ms-word"},
                //{".docx", "application/vnd.ms-word"},
                //{".xls", "application/vnd.ms-excel"},
                //{".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},
                //{".png", "image/png"},
                //{".jpg", "image/jpeg"},
                //{".jpeg", "image/jpeg"},
                //{".gif", "image/gif"},
                //{".csv", "text/csv"}
            };
        }

    }
}