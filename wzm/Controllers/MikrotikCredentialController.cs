﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Enum;
using wzmCore.Interfaces;
using wzmData.Models;


namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MikrotikCredentialsController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IWzmCore _wzmCore;

        public MikrotikCredentialsController(ApplicationDbContext context, IWzmCore wzmCore)
        {
            _context = context;
            _wzmCore = wzmCore;
        }

        // GET: MikrotikCredentials
        public async Task<IActionResult> Index()
        {
            return View(await _context.MikrotikCredentials.Include(c => c.Dispositivo).ToListAsync());
        }

        // GET: MikrotikCredentials/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var MikrotikCredentials = await _context.MikrotikCredentials
                .FirstOrDefaultAsync(m => m.Id == id);
            if (MikrotikCredentials == null)
            {
                return NotFound();
            }

            return View(MikrotikCredentials);
        }

        // GET: MikrotikCredentials/Create
        public async Task<IActionResult> LoginMikrotik()
        {
            var dispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik).ToListAsync();
            var tempDispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik).ToListAsync();


            foreach (var item in dispositivos)
            {
                if (_context.MikrotikCredentials.Any(c => c.DispositivoId == item.Id))
                {
                    tempDispositivos.Remove(item);
                }
            }

            ViewData["Dispositivos"] = new SelectList(tempDispositivos, "Id", "Ip");
            return View();
        }

        // POST: MikrotikCredentials/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginMikrotik([Bind("Id,User,Password,Ip,DispositivoId,Selected")] MikrotikCredentials credential)
        {

            if (ModelState.IsValid)
            {

                try
                {
                    credential.Dispositivo = _context.Dispositivos.Find(credential.DispositivoId);
                    _wzmCore.MikrotikManager.TrySaveCredential(credential);
                    _context.Add(credential);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);

                }

            }
            var dispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik).ToListAsync();
            var tempDispositivos = await _context.Dispositivos.Include(c => c.DeviceCredential).Where(c => c.Fabricante == DeviceTypeEnum.Mikrotik).ToListAsync();


            foreach (var item in dispositivos)
            {
                if (_context.MikrotikCredentials.Any(c => c.DispositivoId == item.Id))
                {
                    tempDispositivos.Remove(item);
                }
            }

            ViewData["Dispositivos"] = new SelectList(tempDispositivos, "Id", "Ip");
            return View(credential);
        }

        // GET: MikrotikCredentials/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var MikrotikCredentials = await _context.MikrotikCredentials.FindAsync(id);
            if (MikrotikCredentials == null)
            {
                return NotFound();
            }
            return View(MikrotikCredentials);
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,User,Password,Ip,DispositivoId,Selected")] MikrotikCredentials credential)
        {
            if (id != credential.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    credential.Dispositivo = _context.Dispositivos.Find(credential.DispositivoId);
                    _wzmCore.MikrotikManager.TrySaveCredential(credential);
                    _context.Update(credential);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    if (!MikrotikCredentialsExists(credential.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        ModelState.AddModelError("", e.Message);
                        return View(credential);
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(credential);
        }


       
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var MikrotikCredentials = await _context.MikrotikCredentials.FindAsync(id);
            _context.MikrotikCredentials.Remove(MikrotikCredentials);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MikrotikCredentialsExists(int id)
        {
            return _context.MikrotikCredentials.Any(e => e.Id == id);
        }
    }
}
