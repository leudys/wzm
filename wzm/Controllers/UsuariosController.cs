﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using wzm.ViewModels;
using wzmCore.Enum;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmData.Enum;
using wzm.Extensions;
using wzmCore.Services;
using wzmCore.Extensions;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsuariosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;
        private readonly ActiveDevices _activeDevices;
        private readonly UserManager<ApplicationUsers> _userManager;
        private readonly RoleManager<IdentityRole> _rolesManager;

        public object GetDispositivo { get; private set; }

        public UsuariosController(ApplicationDbContext context, ActiveDevices activeDevices, IWzmCore wzmCore, UserManager<ApplicationUsers> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _wzmCore = wzmCore;
            _activeDevices = activeDevices;
            _userManager = userManager;
            _rolesManager = roleManager;
            _activeDevices = activeDevices;

        }

        // GET: Usuarios
        public IActionResult Index()
        {
            try
            {
                if (_wzmCore.LicenseManager.Validated)
                {
                    var v = CredentialValidator.ValidateMKCrendential(_context);
                    if (!v.Validate)
                    {
                        TempData["errorCredenciales"] = v.Message;

                        return Redirect("MikrotikCredentials");
                    }
                    var dispositivos = _context.Dispositivos;

                    List<ApplicationUsers> clients = _context.GetApplicationClients();
                    List<ApplicationUsers> clientsHabilitados = clients.Where(c => c.UserPaymentState == UserPaymentState.AlDia && c.Estado == StatusEnum.Habilitado).ToList();
                    if (_wzmCore.LicenseManager.GetLicenseType() == LicenseType.Trial)
                    {
                        if (clients.Count() >= 20)
                        {
                            ViewData["QuantityExceded"] = true;
                        }
                        else
                        {
                            ViewData["QuantityExceded"] = false;
                        }
                    }

                    ViewData["Dispositivos"] = new SelectList(dispositivos, "Id", "GetDispositivo");
                    return View(clientsHabilitados);
                }
                else
                {
                    return RedirectToAction("Index", "License");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorTittle = "Usuarios";
                ViewBag.Error = ex.Message;
                return View("Error");
                
            }

        }

        /// <summary>
        /// Obtiene todos los Usuarios que se encuentren con el pago atrasado
        /// </summary>
        /// <returns></returns>
        public IActionResult Morosos()
        {
            var clients = _context.GetApplicationClients().Where(c => c.UserPaymentState == UserPaymentState.Atrasado);

            return View(clients.Where(c => c.UserPaymentState == UserPaymentState.Atrasado).ToList());
        }

        public async Task<IActionResult> Disabled()
        {
            var dispositivos = _context.Dispositivos;

            var clients = _context.GetApplicationClients();
            var deshabilitados = new List<ApplicationUsers>();
            foreach (var item in clients)
            {

                if (await _userManager.IsLockedOutAsync(item) || item.Estado == StatusEnum.Deshabilitado)
                {
                    deshabilitados.Add(item);
                }
            }


            ViewData["Dispositivos"] = new SelectList(dispositivos, "Id", "GetDispositivo");
            return View(deshabilitados);
        }


        /// <summary>
        /// Obtiene los usuarios existentes en un dispositivo
        /// </summary>
        /// <param name="dispositivoId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetUserByDevice(int dispositivoId)
        {
            List<ApplicationUsers> clients = _context.GetApplicationClients().Where(c => c.DispositivoId == dispositivoId).ToList();
            var clientsInDevice = new List<ApplicationUsers>();
            foreach (var item in clients)
            {
                if (!await _userManager.IsLockedOutAsync(item) && item.UserPaymentState == UserPaymentState.AlDia)
                {
                    item.ClasificadorPago = _context.ClasificadorPagos.Find(item.ClasificadorPagoId);
                    clientsInDevice.Add(item);
                }
            }

            return PartialView("_UsersPartial", clientsInDevice);
        }

        [HttpGet]
        public async Task<IActionResult> GetDisabledUserByDevice(int dispositivoId)
        {
            List<ApplicationUsers> clients = _context.GetApplicationClients().Where(c => c.DispositivoId == dispositivoId).ToList();
            var deshabilitados = new List<ApplicationUsers>();
            foreach (var item in clients)
            {

                if (await _userManager.IsLockedOutAsync(item) || item.Estado == StatusEnum.Deshabilitado)
                {

                    deshabilitados.Add(item);
                }
            }

            return PartialView("_DisabledPartial", deshabilitados);
        }

        [HttpGet]
        public async Task<IActionResult> ChangeClientToAnotherDevice(string id)
        {

            if (id == null)
                return NotFound();

            ApplicationUsers usuario = await _userManager.FindByIdAsync(id) as ApplicationUsers;
            if (usuario == null)
                return NotFound();
            var dispositivo = _context.Dispositivos.Find(usuario.DispositivoId);
            List<Dispositivo> dispositivos = _context.Dispositivos.Where(c => c.WirelessMode == WirelessMode.Ap || c.WirelessMode == WirelessMode.Station && !c.Mac.Equals(dispositivo.Mac)).ToList();
            ViewData["UserName"] = usuario.UserName;
            ViewData["UserId"] = usuario.Id;
            ViewData["Dispositivo"] = dispositivo.GetDispositivo;
            ViewData["Dispositivos"] = new SelectList(dispositivos, "Id", "GetDispositivo");
            return View("ChangeToAnotherDevice");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeClientToAnotherDevice(string id, string newDispositivoId)
        {

            if (newDispositivoId != null)
            {
                try
                {
                    ApplicationUsers user = await _userManager.FindByIdAsync(id) as ApplicationUsers;
                    Dispositivo dispositivoOrigen = _context.Dispositivos.Include(c => c.DeviceCredential).FirstOrDefault(c => c.Id == user.DispositivoId);

                    Dispositivo dispositivoDestino = _context.Dispositivos.Include(c => c.DeviceCredential).FirstOrDefault(c => c.Id == int.Parse(newDispositivoId));

                    var devices = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(user.Id)).ToList();

                    if (devices.Count > 0)
                    {
                        var validation = await _wzmCore.UserManagement.DeviceManager.ChangeUserDeviceToAnotherDevice(devices, dispositivoOrigen, dispositivoDestino, _wzmCore.Random.Next());

                        if (validation.Succeeded)
                        {
                            user.DispositivoId = dispositivoDestino.Id;
                            await _userManager.UpdateAsync(user);
                            TempData["success"] = $"El usuario ha sido movido al dispositivo {dispositivoDestino.GetDispositivo} correctamente.";
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["error"] = validation.ToString();
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        user.DispositivoId = dispositivoDestino.Id;
                        await _userManager.UpdateAsync(user);
                        TempData["success"] = $"El usuario ha sido movido al dispositivo {dispositivoDestino.GetDispositivo} correctamente.";
                        return RedirectToAction("Index");
                    }

                }
                catch (Exception)
                {
                    TempData["error"] = "No se ha podido cambiar el dispositivo del cliente";
                    return RedirectToAction("Index");
                }
            }
            TempData["error"] = (object)"No se ha podido cambiar el dispositivo del usuario";
            return RedirectToAction("Index");
        }



        // GET: Usuarios/Create
        public IActionResult Create()
        {
            var v = _context.ValidateMKCrendential();

            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction(nameof(Index));
            }

            var c = _context.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                var credenciales = _context.DeviceCredentials;

                var dispositivos = new List<Dispositivo>();
                var dispositivosBd = _context.Dispositivos.Where(d => d.WirelessMode != WirelessMode.Station);
                foreach (var item in credenciales)
                {
                    if (_context.Dispositivos.Any(f => f.Id == item.DispositivoId))
                    {
                        dispositivos.Add(_context.Dispositivos.Find(item.DispositivoId));
                    }
                }

                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["ClasificadorPagoId"] = new SelectList(_context.ClasificadorPagos, "Id", "GetPago");
                ViewData["DispositivoId"] = new SelectList(_context.Dispositivos, "Id", "GetDispositivo");
                ViewData["Servicios"] = new SelectList(_context.Services, "Id", "UserServiceType");
                return View();

            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ClienteDesde,UserName,HotspotUser,ConfirmPassword,Password,Estado,UserPaymentState,DispositivoId,PagoAdelantado,Servicios,ClasificadorPagoId")] UsuarioViewModel usuario)
        {

            if (ModelState.IsValid)
            {

                usuario.Estado = StatusEnum.Habilitado;

                var applicationClients = new ApplicationUsers()
                {

                    Estado = usuario.Estado,
                    ClasificadorPagoId = usuario.ClasificadorPagoId,
                    ClienteDesde = usuario.ClienteDesde,
                    DispositivoId = usuario.DispositivoId,
                    UserName = usuario.UserName,
                    UserPaymentState = usuario.UserPaymentState,

                };

                var userResult = await _userManager.CreateAsync(applicationClients, usuario.Password);

                if (userResult.Succeeded)
                {
                    try
                    {

                        var roleResult = await _userManager.AddToRoleAsync(applicationClients, "User");

                        if (roleResult.Succeeded)
                        {
                            foreach (var item in usuario.Servicios)
                            {
                                _context.Add(new ClientServices() { ServiceId = item, UserId = applicationClients.Id });
                            }
                            await _context.SaveChangesAsync();
                            TempData["success"] = "Usuario agregado correctamente.";
                        }
                        else
                        {
                            TempData["error"] = roleResult.ToString();
                            return RedirectToAction(nameof(Index));
                        }


                        return RedirectToAction(nameof(Index));
                    }
                    catch (Exception e)
                    {

                        TempData["error"] = e.Message;
                        return RedirectToAction(nameof(Index));
                    }

                }
                TempData["error"] = userResult.ToString();
                return RedirectToAction(nameof(Index));

            }
            var c = _context.MikrotikCredentials.Include(d => d.Dispositivo).First(d => d.Selected == true);
            using (ITikConnection conection = ConnectionFactory.OpenConnection(c.Dispositivo.Firmware.GetApiVersion(), c.Dispositivo.Ip, c.User, c.Password))
            {
                var routes = _wzmCore.MikrotikManager.Ip.Routes.GetMarkRoutes(conection);
                ViewData["ClasificadorPagoId"] = new SelectList(_context.ClasificadorPagos, "Id", "GetPago", usuario.ClasificadorPagoId);
                ViewData["DispositivoId"] = new SelectList(_context.Dispositivos, "Id", "GetDispositivo", usuario.DispositivoId);
                ViewData["Servicios"] = new SelectList(_context.Services, "Id", "UserServiceType", usuario.Servicios);
                return View(usuario);

            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuario = await _userManager.FindByIdAsync(id) as ApplicationUsers;
            if (usuario == null)
            {
                return NotFound();
            }

            var servicios = _context.ClientServices.Where(d => d.UserId.Equals(usuario.Id)).Select(d => d.ServiceId).ToList();

            var u = new UserEditViewModel()
            {
                Id = usuario.Id,
                UserName = usuario.UserName,
                ClasificadorPagoId = usuario.ClasificadorPagoId,
                Fecha = usuario.ClienteDesde,
                HotspotUser=usuario.HotSpotUser,
                Servicios = servicios
            };

            ViewData["ClasificadorPagoId"] = new SelectList(_context.ClasificadorPagos, "Id", "GetPago", usuario.ClasificadorPagoId);
            ViewData["Servicios"] = new SelectList(_context.Services, "Id", "UserServiceType", u.Servicios);
            return View(u);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,ClienteDesde,HotspotUser,UserName,ClasificadorPagoId,PagoAdelantado,Servicios,Fecha")] UserEditViewModel usuario)
        {
            if (id != usuario.Id)
            {
                return NotFound();
            }


            var u = await _userManager.FindByIdAsync(usuario.Id) as ApplicationUsers;

            if (ModelState.IsValid)
            {
                if (u.ClasificadorPagoId != usuario.ClasificadorPagoId)
                {
                    u.ClasificadorPagoId = usuario.ClasificadorPagoId;
                }
                if (u.ClienteDesde != usuario.Fecha)
                {
                    u.ClienteDesde = usuario.Fecha;
                }
                if (!u.UserName.Equals(usuario.UserName))
                {
                    u.UserName = usuario.UserName;

                }
                u.HotSpotUser = usuario.HotspotUser;
                await _userManager.UpdateAsync(u);

                var servicios = _context.ClientServices.Where(d => d.UserId == usuario.Id);

                foreach (var item in servicios)
                {
                    _context.Remove(item);
                }

                foreach (var item in usuario.Servicios)
                {
                    _context.Add(new ClientServices() { ServiceId = item, UserId = usuario.Id });
                }
                await _context.SaveChangesAsync();
                TempData["success"] = "Usuario actualizado correctamente.";
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClasificadorPagoId"] = new SelectList(_context.ClasificadorPagos, "Id", "GetPago", usuario.ClasificadorPagoId);
            ViewData["Servicios"] = new SelectList(_context.Services, "Id", "UserServiceType", usuario.Servicios);
            return View(usuario);
        }


        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarios = await _userManager.Users
                //.Include(u => u.ClasificadorPago)
                //.Include(u => u.Dispositivo).ThenInclude(u => u.DeviceCredential)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarios == null)
            {
                return NotFound();
            }



            return View(usuarios);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var usuario = await _userManager.FindByIdAsync(id) as ApplicationUsers;

            var v = _context.ValidateMKCrendential();

            if (!v.Validate)
            {
                TempData["error"] = v.Message;
                return RedirectToAction(nameof(Index));

            }

            try
            {
                if (_context.UsersDevices.Any(c => c.ApplicationUserId.Equals(usuario.Id)))
                {
                    var devices = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(usuario.Id));
                    if (usuario.DispositivoId != null)
                    {
                        if (devices.Count() > 0)
                        {
                            if (devices.Any(c => c.DeviceConnectionType == DeviceConecctionType.Wifi))
                            {
                                var dispositivo = _context.Dispositivos.FirstOrDefault(c => c.Id.Equals(usuario.DispositivoId));

                                if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti && dispositivo.WirelessMode == WirelessMode.Ap && (dispositivo.WirelessSecurity == WirelessSecurity.ACL || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WPA))
                                {
                                    foreach (var item in devices)
                                    {
                                        if (item.DeviceConnectionType == DeviceConecctionType.Wifi)
                                        {
                                            if (_context.UbiquitiCredentials.Any(c => c.DispositivoId.Equals(dispositivo.Id)))
                                            {
                                                if (!await _wzmCore.UserManagement.DeviceManager.DeleteDeviceFromUbiquiti(item.Mac, _context.UbiquitiCredentials.FirstOrDefault(c => c.DispositivoId.Equals(dispositivo.Id)), _wzmCore.Random.Next()))
                                                {
                                                    TempData["error"] = $"El dispositivo {item.Comment} no se pudo eliminar.";
                                                    //return RedirectToAction(nameof(Index));
                                                }

                                            }
                                            else
                                            {
                                                TempData["error"] = $"El dispositivo {item.Comment} no se puede eliminar porque el dispositivo {dispositivo.GetDispositivo} no posee credenciales.";
                                                //return RedirectToAction(nameof(Index));
                                            }

                                        }
                                    }


                                }
                            }


                        }

                    }
                    var mk = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                    foreach (var item in devices)
                    {
                        _wzmCore.UserManagement.DeviceManager.DeleteDeviceFromMikrotik(item.Mac, item.Ip, mk);
                        foreach (var service in _context.HostsServices.Where(c => c.DeviceId.Equals(usuario.Id)))
                        {
                            _context.HostsServices.Remove(service);
                        }

                    }

                }

                foreach (var item in _context.Pagos.Where(c => c.UsuarioId.Equals(usuario.Id)))
                {
                    _context.Pagos.Remove(item);
                }


                await _userManager.DeleteAsync(usuario);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                TempData["error"] = e.Message;
                return RedirectToAction(nameof(Index));
            }

        }

        //public IActionResult CantidadUsuarios(string id)
        //{
        //    var applicationUsers = _context.Users.Where(c => c.PortalDeOrigen.Contains(id));

        //    ViewData["CantidadUsuarios"] = $"El portal {id} contiene {applicationUsers.Count()} usuarios.";

        //    return PartialView("_CantidadUsuariosPortal", applicationUsers);
        //}

        public async Task<IActionResult> Details(string id)
        {
            if (!UsuariosExists(id))
            {
                return NotFound();
            }
            var usuario = await _userManager.FindByIdAsync(id);

            if (usuario == null)
            {
                return NotFound();
            }
            var servicios = _context.ClientServices.Where(d => d.UserId.Equals(usuario.Id)).Select(d => d.ServiceId).ToList();



            var u = new UsuarioViewModel()
            {
                Id = usuario.Id,
                UserName = usuario.UserName,
                ClasificadorPagoId = usuario.ClasificadorPagoId,
                ClienteDesde = usuario.ClienteDesde,

                Servicios = servicios,

            };

            if (usuario.DispositivoId.HasValue)
            {
                ViewData["DispositivoId"] = _context.Dispositivos.Find((int)usuario.DispositivoId).GetDispositivo;
            }

            ViewData["Dispositivos"] = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(usuario.Id));
            ViewData["ClasificadorPagoId"] = _context.ClasificadorPagos.FirstOrDefault(c => c.Id.Equals(usuario.ClasificadorPagoId)).GetPago;
            ViewData["Servicios"] = new SelectList(_context.Services, "Id", "UserServiceType", u.Servicios);
            return View(u);
        }

        [HttpGet]
        public async Task<IActionResult> BloquearUsuario(string id)
        {
            var user = _context.GetApplicationClients().FirstOrDefault(c => c.Id.Equals(id));

            var result = await _userManager.SetLockoutEnabledAsync(user, true);

            await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow.AddDays(36500));
            if (result.Succeeded && await _userManager.IsLockedOutAsync(user))
            {
                var uc = _context.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId == user.DispositivoId);
                var mc = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                var devices = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(user.Id)).ToList();

                if (devices.Count() > 0 && user.DispositivoId != null)
                {
                    await _wzmCore.UserManagement.DeviceManager.DisableDeviceAsync(devices, user.Dispositivo, uc, mc, _wzmCore.Random.Next());
                }



                user.Estado = StatusEnum.Deshabilitado;
                await _userManager.UpdateAsync(user);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> DesbloquearUsuario(string id)
        {
            var user = _context.GetApplicationClients().FirstOrDefault(c => c.Id.Equals(id));

            var result = await _userManager.SetLockoutEnabledAsync(user, false);
            if (result.Succeeded)
            {
                var access = await _userManager.ResetAccessFailedCountAsync(user);
                if (access.Succeeded)
                {

                    var uc = _context.UbiquitiCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.DispositivoId == user.DispositivoId);
                    var mc = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);

                    var devices = _context.UsersDevices.Where(c => c.ApplicationUserId.Equals(user.Id)).ToList();
                    if (devices.Count() > 0 && user.DispositivoId != null)
                    {
                        await _wzmCore.UserManagement.DeviceManager.EnableDeviceAsync(devices, user.Dispositivo, uc, mc, _wzmCore.Random.Next());
                    }

                    user.Estado = StatusEnum.Habilitado;
                    await _userManager.UpdateAsync(user);
                }
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> ResetPassword(string id)
        {
            ApplicationUsers user = await _userManager.FindByIdAsync(id);
            ResetPassowrdViewModel viewmodel = new ResetPassowrdViewModel() { UserId = user.Id };
            ViewBag.UserName = user.UserName;
            return View(viewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPassowrdViewModel viewModel)
        {

            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _userManager.FindByIdAsync(viewModel.UserId);
            if (user == null)
            {
                return NotFound($"No se puede obtener el usuario con ID '{_userManager.GetUserId(User)}'.");
            }

            await _userManager.RemovePasswordAsync(user);
            var addPasswordResult = await _userManager.AddPasswordAsync(user, viewModel.NewPassword);


            if (!addPasswordResult.Succeeded)
            {
                foreach (var error in addPasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return View();
            }

            TempData["success"] = "La operación se ha realizado correctamente";
            return RedirectToAction("Clients");

        }

        [HttpGet]
        public IActionResult FreeService()
        {
            var clients = _context.GetApplicationClients().Where(c => c.ClasificadorPagoId != null);
            var withFreService = new List<ApplicationUsers>();
            foreach (var item in clients)
            {


                item.ClasificadorPago = _context.ClasificadorPagos.FirstOrDefault(c => c.Id == item.ClasificadorPagoId);
                withFreService.Add(item);

            }

            return View(withFreService.Where(c => c.ClasificadorPago.PaymentType == PaymentType.Gratis));
        }

        [HttpGet]
        public IActionResult SinClasificador()
        {
            var clients = _context.GetApplicationClients();
            return View(clients.Where(c => c.ClasificadorPagoId == null));
        }


        private bool UsuariosExists(string id)
        {
            return _userManager.Users.Any(e => e.Id == id);
        }
    }
}
