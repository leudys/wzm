﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wzm.Controllers
{
    public class JcController : Controller
    {
        // GET: JcController
        public ActionResult Index()
        {
            return View();
        }

        // GET: JcController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: JcController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JcController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: JcController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: JcController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: JcController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: JcController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
