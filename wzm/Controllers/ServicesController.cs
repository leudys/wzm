﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using wzm.Services;
using wzmData.Enum;
using wzmData.Models;
using wzmCore.Services;
using wzm.Extensions;
using wzmCore.Data;
using wzmCore.Services.System;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ServicesController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly HostedServices _services;
        private readonly BalanceoNTHJob _balanceoNTHJob;
        private readonly BalanceoPCCJob _balanceoPCCJob;
        //private readonly VpnHelper _vpnHelper;
        public ServicesController(ApplicationDbContext context, /*VpnHelper vpnHelper,*/ IHostedService services, BalanceoNTHJob balanceoNTHJob, BalanceoPCCJob balanceoPCCJob)
        {
            _context = context;
            //_vpnHelper = vpnHelper;
            _services = services as HostedServices;
            _balanceoNTHJob = balanceoNTHJob;
            _balanceoPCCJob = balanceoPCCJob;
        }

        public async Task<IActionResult> Index()
        {
            if (!_context.ValidateMKCrendential().Validate)
            {
                TempData["errorCredenciales"] = _context.ValidateMKCrendential().Message;

                return Redirect("MikrotikCredentials");
            }
            List<ApplicationServices> services = await _context.ApplicationServices.ToListAsync();

            return View(services);
        }

        public async Task<IActionResult> Disable(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var services = await _context.ApplicationServices.FindAsync(id);
            if (services == null)
            {
                return NotFound();
            }
            return View(services);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Disable(int id, [Bind("Id,State,Disabled,ServiceType")] ApplicationServices service)
        {
            if (id != service.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var existentR = _context.ApplicationServices.FirstOrDefault(c => c.Id == service.Id);


                    existentR.State = ServicesStatus.Stopped;

                    switch (service.ServiceType)
                    {
                        case ServiceType.ReconexionAutomatica:
                            await _services.DeleteJob(ServiceType.ReconexionAutomatica, new CancellationToken());
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                        case ServiceType.AntiClon:
                            await _services.DeleteJob(ServiceType.AntiClon, new CancellationToken());
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                        case ServiceType.Vip:
                            await _services.DeleteJob(ServiceType.Vip, new CancellationToken());
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                        //case ServiceType.Vpn:

                        //    await _services.DeleteJob(ServiceType.Vpn, new CancellationToken());
                        //    await _context.SaveChangesAsync();
                        //    Thread.Sleep(10000);
                        //    _vpnHelper.Stop();
                        //    return RedirectToAction(nameof(Index));

                        case ServiceType.SimpleQueueLimit:

                            await _services.DeleteJob(ServiceType.SimpleQueueLimit, new CancellationToken());

                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                        case ServiceType.BalanceadorNTH:
                            await _services.DeleteJob(ServiceType.BalanceadorNTH, new CancellationToken());
                            Thread.Sleep(10000);
                            _services.RemoveBalanceo("wzm_balanceo_NTH");
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                        case ServiceType.BalanceadorPCC:
                            await _services.DeleteJob(ServiceType.BalanceadorPCC, new CancellationToken());
                            Thread.Sleep(10000);
                            _services.RemoveBalanceo("wzm_balanceo_PCC");

                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                        case ServiceType.Morosos:
                            await _services.DeleteJob(ServiceType.Morosos, new CancellationToken());
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));

                    }


                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SReconnectionExists(service.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

            }
            return View(service);
        }

        // GET: SReconnections/Edit/5
        public async Task<IActionResult> Enable(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var services = await _context.ApplicationServices.FindAsync(id);

            if (services is MorososService)
            {
                services = services as MorososService;

                return View("EnableMorosos", services);
            }
            //if (services is VpnService)
            //{
            //    services = services as VpnService;

            //    return View("EnableVpn", services);
            //}

            if (services == null)
            {
                return NotFound();
            }

            return View(services);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Enable(int id, [Bind("Id,State,Disabled,ServiceType")] ApplicationServices service)
        {
            if (id != service.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var existentR = _context.ApplicationServices.FirstOrDefault(c => c.Id == service.Id);

                    switch (service.ServiceType)
                    {
                        case ServiceType.ReconexionAutomatica:
                            await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(ReconnectionByPingJob)), new CancellationToken());
                            await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(ReconnectionByDefaultJob)), new CancellationToken());
                            existentR.State = ServicesStatus.Running;
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            TempData["success"] = "El servicio Reconexión Automática se ha iniciado.";
                            return RedirectToAction(nameof(Index));

                        case ServiceType.Vip:
                            await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(VipJob)), new CancellationToken());
                            existentR.State = ServicesStatus.Running;
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            TempData["success"] = "El servicio Localizar Portales con Internet se ha iniciado.";
                            return RedirectToAction(nameof(Index));

                        case ServiceType.NautaConnector:
                            await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(NautaConnectorJob)), new CancellationToken());
                            existentR.State = ServicesStatus.Running;
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            TempData["success"] = "El servicio Conector de Cuentas se ha iniciado.";
                            return RedirectToAction(nameof(Index));

                        case ServiceType.SimpleQueueLimit:
                            await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(SimpleQueueJob)), new CancellationToken());
                            existentR.State = ServicesStatus.Running;
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            TempData["success"] = $"El servicio Control de Anchdo de Banda se ha iniciado.";
                            return RedirectToAction(nameof(Index));

                        case ServiceType.AntiClon:
                            await _services.CreateJob(ServicesGenerator.GetAll().FirstOrDefault(c => c.ServiceType == typeof(AntiClonJob)), new CancellationToken());
                            existentR.State = ServicesStatus.Running;
                            _context.Update(existentR);
                            await _context.SaveChangesAsync();
                            TempData["success"] = "El servicio Anti Clon se ha iniciado.";
                            return RedirectToAction(nameof(Index));

                        case ServiceType.BalanceadorNTH:

                            if (!_services.IsJobRunning(new Quartz.JobKey("wzmBalanceoNTHJob")))
                            {
                                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoNTHJob)), new CancellationToken());
                                existentR.State = ServicesStatus.Running;
                                _context.Update(existentR);
                                await _context.SaveChangesAsync();
                                TempData["success"] = "El servicio Balanceador NTH se ha iniciado.";
                                return RedirectToAction(nameof(Index));
                            }
                            else
                            {
                                TempData["error"] = "El servicio Balanceador PCC está corriendo. WZM permite que se esté corriendo solamente un balanceador";
                                return RedirectToAction(nameof(Index));
                            }


                        case ServiceType.BalanceadorPCC:
                            if (!_services.IsJobRunning(new Quartz.JobKey("wzmBalanceoNTHJob")))
                            {
                                await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(BalanceoPCCJob)), new CancellationToken());
                                existentR.State = ServicesStatus.Running;
                                _context.Update(existentR);
                                await _context.SaveChangesAsync();
                                TempData["success"] = "El servicio Balanceador PCC se ha iniciado.";
                                return RedirectToAction(nameof(Index));
                            }
                            else
                            {
                                TempData["error"] = "El servicio Balanceador NTH está corriendo. WZM permite que se esté corriendo solamente un balanceador";
                                return RedirectToAction(nameof(Index));
                            }





                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SReconnectionExists(service.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

            }
            return View(service);
        }

        public async Task<ActionResult> EnableMorosos(int id, [Bind("Id,State,Disabled,ServiceType,RedirecTo,Horario")] MorososService service)
        {
            if (ModelState.IsValid)
            {
                var diasPagos = _context.ClasificadorPagos.Where(c => c.PaymentType != PaymentType.Gratis).GroupBy(c => c.DiaPago).Select(c => c.Key).ToList();

                if (diasPagos.Count() != 0)
                {
                    string dias = string.Empty;
                    foreach (var dia in diasPagos)
                    {
                        if (dia != 0)
                        {
                            if (!dias.Equals(dia))
                            {
                                dias += dia + ",";
                            }

                        }

                    }
                    dias = dias.Remove(dias.Length - 1);
                    service.State = ServicesStatus.Running;
                    _context.Update(service);
                    await _context.SaveChangesAsync();
                    await _services.CreateJob(new ServiceModel() { ServiceType = typeof(MorososJob), CronExpression = $"{service.Horario.Seconds} {service.Horario.Minutes} {service.Horario.Hours} {dias} * ? *" }, new CancellationToken());

                }
                return RedirectToAction(nameof(Index));
            }
            return View("EnableMorosos", service);
        }

        //public async Task<ActionResult> EnableVpn(int id, [Bind("Id,State,Disabled,ServiceType,Persistent,IncludeUsers")] VpnService service)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        service.State = ServicesStatus.Running;
        //        _context.Update(service);
        //        await _context.SaveChangesAsync();
        //        await _services.CreateJob(ServicesGenerator.GetServiceModel(typeof(VpnHelper)), new CancellationToken());
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View("EnableMorosos", service);
        //}

        private bool SReconnectionExists(int id)
        {
            return _context.ApplicationServices.Any(e => e.Id == id);
        }
    }
}
