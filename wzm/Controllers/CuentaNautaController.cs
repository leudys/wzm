﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using wzmData.Models;

namespace wzm.Controllers
{
    public class CuentaNautaController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CuentaNautaController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CuentaNauta
        public async Task<IActionResult> Index()
        {
            return View(await _context.CuentaNautas.Include(c => c.Portal).ToListAsync());
        }

        // GET: CuentaNauta/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cuentaNauta = await _context.CuentaNautas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cuentaNauta == null)
            {
                return NotFound();
            }

            return View(cuentaNauta);
        }

        // GET: CuentaNauta/Create
        public IActionResult Create()
        {
            ViewBag.Portales = new SelectList(_context.Portales, "Id", "Interfaz");
            return View();
        }

        // POST: CuentaNauta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,User,Password,PortalId")] CuentaNauta cuentaNauta)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cuentaNauta);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Portales = new SelectList(_context.Portales, "Id", "Interfaz", cuentaNauta.PortalId);
            return View(cuentaNauta);
        }

        // GET: CuentaNauta/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cuentaNauta = await _context.CuentaNautas.FindAsync(id);
            if (cuentaNauta == null)
            {
                return NotFound();
            }
            ViewBag.Portales = new SelectList(_context.Portales, "Id", "Interfaz", cuentaNauta.PortalId);
            return View(cuentaNauta);
        }

        // POST: CuentaNauta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,User,Password,PortalId")] CuentaNauta cuentaNauta)
        {
            if (id != cuentaNauta.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cuentaNauta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CuentaNautaExists(cuentaNauta.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Portales = new SelectList(_context.Portales, "Id", "Interfaz", cuentaNauta.PortalId);
            return View(cuentaNauta);
        }

        // GET: CuentaNauta/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cuentaNauta = await _context.CuentaNautas
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cuentaNauta == null)
            {
                return NotFound();
            }

            return View(cuentaNauta);
        }

        // POST: CuentaNauta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cuentaNauta = await _context.CuentaNautas.FindAsync(id);
            _context.CuentaNautas.Remove(cuentaNauta);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CuentaNautaExists(int id)
        {
            return _context.CuentaNautas.Any(e => e.Id == id);
        }
    }
}
