﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net;
using tik4net;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.ViewModels;
using wzmCore.Extensions;

namespace wzm.Controllers
{
    public class RemoteController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly IWzmCore _wzmCore;
        

        public RemoteController(ApplicationDbContext context, IWzmCore wzmCore)
        {
            _context = context;
            _wzmCore = wzmCore;
            
        }

        public JsonResult DeviceHasCredential(int DispositivoId)
        {
            //if (!_context.DeviceCredentials.Any(c => c.DispositivoId == int.Parse(dispositivoId)))
            //{
            //    return Json(data: $"El dispositivo seleccionado no contiene credenciales.");
            //}
            //else
            //{
            //    return Json(true);
            //}
            return Json(true);
        }

        /// <summary>
        /// Compara la nueva mac con la acutal que posee el usuario, y despues valida dicha mac
        /// </summary>
        /// <param name="newmac"></param>
        /// <param name="oldmac"></param>
        /// <returns></returns>
        public JsonResult CompareMac(string newmac, string oldmac)
        {
            if (newmac.Equals(oldmac))
            {
                return Json(data: "El campo MAC Actual y Nueva MAC no pueden ser iguales");
            }
            else
            {
                return IsMacValid(newmac);
            }
        }

        /// <summary>
        /// Valida la IP del usuario
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public JsonResult ValidateIp(string ip, string id)
        {
            return ValidateIpAddress(ip, id);
        }

        private JsonResult ValidateIpAddress(string ip, string id)
        {
            try
            {
                IPAddress iPAddress;
                IPAddress.TryParse(ip, out iPAddress);

                if (iPAddress == null)
                {
                    return Json(data: $"{ip} no es un IP válido.");
                }

                if (ip.Split('.').Length >= 4)
                {
                    if (id != null)
                    {
                        if (_context.UsersDevices.Any(c => c.Ip.Equals(ip) && !c.Id.Equals(id)))
                        {
                            return Json(data: $"El Ip {ip} ya está siendo usado por el usuario {_context.UsersDevices.FirstOrDefault(c => c.Mac.Equals(ip))}.");
                        }

                        var mikrotikCredential = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                        using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                        {
                            if (_wzmCore.MikrotikManager.Ip.DhcpServer.IsStaticIpInDhcpRange(ip, connection))
                            {
                                return Json(data: $"El Ip {ip} se encuentra en el rango del DHCP Server, no puedes asignar este Ip, puesto que daría conflictos.");
                            }

                        }
                    }
                    else
                    {
                        if (_context.UsersDevices.Any(c => c.Ip.Equals(ip)))
                        {
                            return Json(data: $"El Ip {ip} ya está siendo usado por el usuario {_context.UsersDevices.FirstOrDefault(c => c.Mac.Equals(ip))}.");
                        }



                        var mikrotikCredential = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                        using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                        {
                            if (_wzmCore.MikrotikManager.Ip.DhcpServer.IsStaticIpInDhcpRange(ip, connection))
                            {
                                return Json(data: $"El Ip {ip} se encuentra en el rango del DHCP Server, no puedes asignar este Ip, puesto que daría conflictos.");
                            }

                            var leases = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(connection);
                            var arp = _wzmCore.MikrotikManager.Ip.IpArp.GetAllArp(connection);

                            if (arp.Any(c => c.Address.Equals(ip) && c.Interface.Equals("ether1")) || leases.Any(c => c.Address.Equals(ip)))
                            {
                                return Json(data: $"El Ip {ip} ya está siendo usada por un usuario.");

                            }
                        }
                    }


                }

                return Json(true);
            }
            catch (Exception)
            {
                return Json(data: $"{ip} no es un IP válido.");
            }
        }

        public JsonResult Cantidad(string cantidad)
        {
            try
            {
                int c = int.Parse(cantidad);
                if (c < 1)
                {
                    return Json(data: $"La cantidad de Portales no puede ser inferior a 1.");
                }
                if (_wzmCore.LicenseManager.GetLicenseType() == wzmCore.Enum.LicenseType.Trial)
                {
                    if (c > 5)
                    {
                        return Json(data: $"La cantidad de Portales no puede exceder a 5, cuando su licencia es de Prueba.");
                    }
                    var credential = _context.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);

                    using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                    {
                        var wireless = new Wireless().GetAllWireless(connection).Where(d => d.InterfaceType.Equals("virtual") && d.Ssid.Equals("WIFI_ETECSA"));

                        if (wireless.Count() + c > 5)
                        {
                            if (wireless.Count() - 5 < 0)
                            {
                                return Json(data: $"Puedes introducir solamente {-1 * (wireless.Count() - 5)} portales, cuando su licencia es de Prueba.");
                            }

                            return Json(data: $"Puedes introducir solamente {wireless.Count() - 5} portales, cuando su licencia es de Prueba");
                        }
                    }
                }

                else
                {
                    if (c > 30)
                    {
                        return Json(data: $"La cantidad de Portales no puede exceder a 30.");
                    }

                    var credential = _context.MikrotikCredentials.Include(d => d.Dispositivo).FirstOrDefault(d => d.Selected);

                    using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                    {
                        var wireless = new Wireless().GetAllWireless(connection).Where(d => d.InterfaceType.Equals("virtual") && d.Ssid.Equals("WIFI_ETECSA"));

                        if (wireless.Count() + c > 30)
                        {
                            if (wireless.Count() - 30 < 0)
                            {
                                return Json(data: $"Puedes introducir solamente {-1 * (wireless.Count() - 30)} portales.");
                            }

                            return Json(data: $"Puedes introducir solamente {wireless.Count() - 30} portales.");
                        }
                    }
                }

                
                
                return Json(true);
            }
            catch (Exception)
            {
                return Json(data: $"{cantidad} no es un número válido.");
            }
        }

        /// <summary>
        /// Valida el Usuario
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public JsonResult ValidateUserName(string UserName)
        {
            if (_context.Users.Any(c => c.UserName.Equals(UserName)))
            {
                return Json(data: $"El nombre de usuario {UserName} ya se encuentra en el sistema.");
            }
            return Json(true);
        }

        /// <summary>
        /// Valida la MAC del usuario
        /// </summary>
        /// <param name="mac"></param>
        /// <returns></returns>
        public JsonResult IsMacValid(string mac)
        {
            for (int i = 0; i < mac.Length; i++)
            {
                char x = mac[i];

                if (char.IsWhiteSpace(x))
                {
                    return Json(data: $"El campo MAC no permite espacios vacíos.");
                }

                if (char.IsLower(x))
                {
                    return Json(data: $"El campo MAC no permite caracteres en minúsculas.");
                }
            }

            if (mac.Split(':').Length == 6)
            {


                if (_context.UsersDevices.Any(c => c.Mac.Equals(mac)))
                {
                    return Json(data: $"La MAC {mac} ya está siendo usada por el usuario {_context.UsersDevices.FirstOrDefault(c => c.Mac.Equals(mac))}.");
                }

                //var mikrotikCredential = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                //using (var connection = ConnectionFactory.OpenConnection(TikConnectionType.Api, mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                //{
                //    var leases = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(connection);
                //    var arp = _wzmCore.MikrotikManager.Ip.IpArp.GetAllArp(connection);

                //    if (arp.Any(c => c.MacAddress.Equals(mac)) || leases.Any(c => c.MacAddress.Equals(mac)))
                //    {
                //        return Json(data: $"La MAC {mac} ya está siendo usada por un usuario.");

                //    }

                //}


            }
            return Json(true);


        }

        public JsonResult IsDeviceMacValid(string mac)
        {
            for (int i = 0; i < mac.Length; i++)
            {
                char x = mac[i];

                if (char.IsWhiteSpace(x))
                {
                    return Json(data: $"El campo MAC no permite espacios vacíos.");
                }

                if (char.IsLower(x))
                {
                    return Json(data: $"El campo MAC no permite caracteres en minúsculas.");
                }
            }

            if (mac.Split(':').Length == 6)
            {


                if (_context.UsersDevices.Any(c => c.Mac.Equals(mac)))
                {
                    return Json(data: $"La MAC {mac} ya está siendo usada por el usuario {_context.UsersDevices.FirstOrDefault(c => c.Mac.Equals(mac)).Comment}.");
                }

                var mikrotikCredential = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                {
                    var leases = _wzmCore.MikrotikManager.Ip.DhcpServer.GetServerLeases(connection);
                    var arp = _wzmCore.MikrotikManager.Ip.IpArp.GetAllArp(connection);

                    if (arp.Any(c => c.MacAddress.Equals(mac)) || leases.Any(c => c.MacAddress.Equals(mac)))
                    {
                        return Json(data: $"La MAC {mac} ya está siendo usada por un usuario.");

                    }

                }
                if (_context.Dispositivos.Any(c => c.Mac.Equals(mac)))
                {
                    return Json(data: $"La MAC {mac} ya está siendo utilizada por un dispositivo.");
                }


            }
            return Json(true);


        }

        public JsonResult CheckDiaPago(string diapago)
        {
            try
            {
                if (int.Parse(diapago) < 0 || int.Parse(diapago) == 0)
                {
                    return Json(data: "El Día del Pago ha de ser mayor que 0.");
                }
                if (int.Parse(diapago) > 31)
                {
                    return Json(data: "Ningún mes tiene más de 31 días.");
                }
            }
            catch (Exception)
            {
                return Json(data: "El campo Día del Pago no se encuentra en el formato correcto.");
            }

            return Json(true);
        }

        public JsonResult CheckImporte(string importe)
        {
            try
            {
                if (decimal.Parse(importe) < 0 || decimal.Parse(importe) == 0)
                {
                    return Json(data: "El Importe ha de ser mayor que 0.");
                }

            }
            catch (Exception)
            {

                return Json(data: "El campo Importe no se encuentra en el formato correcto.");
            }

            return Json(true);
        }

        public JsonResult ValidateAddressListIp(string ip)
        {
            try
            {
                IPAddress iPAddress;
                IPAddress.TryParse(ip, out iPAddress);

                var mikrotikCredential = _context.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                {

                }

                return Json(true);
            }
            catch (System.Exception)
            {
                return Json(data: $"{ip} no es un IP válido.");
            }
        }
    }
}
