﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Models;
using wzmData.Enum;

namespace wzm.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ClasificadorPagoesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ClasificadorPagoesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ClasificadorPagoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.ClasificadorPagos.ToListAsync());
        }

        // GET: ClasificadorPagoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ClasificadorPagoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DiaPago,PaymentType,Importe")] ClasificadorPago clasificadorPago)
        {
            if (clasificadorPago.PaymentType != PaymentType.Gratis)
            {
                if (clasificadorPago.Importe == null)
                {
                    ModelState.AddModelError("", "El campo Importe es obligatorio.");

                }
                if (clasificadorPago.DiaPago == null)
                {
                    ModelState.AddModelError("", "El campo Día del Pago es obligatorio.");
                }

                if (clasificadorPago.Importe != null && clasificadorPago.DiaPago != null)
                {
                    if (_context.ClasificadorPagos.Any(c => c.PaymentType == clasificadorPago.PaymentType && c.DiaPago == clasificadorPago.DiaPago && c.Importe == clasificadorPago.Importe))
                    {
                        ModelState.AddModelError("", "El Clasifiacor de Pago introducido ya se encuentra en el sistema.");
                    }

                }

            }
            else
            {
                if (_context.ClasificadorPagos.Any(c => c.PaymentType == PaymentType.Gratis))
                {
                    ModelState.AddModelError("", "El Clasifiacor de Pago introducido ya se encuentra en el sistema.");
                }
            }

            if (ModelState.IsValid)
            {
                _context.Add(clasificadorPago);
                await _context.SaveChangesAsync();
                TempData["success"] = $"La operación se ha realizado correctamente";
                return RedirectToAction(nameof(Index));
            }

            ViewData["Payment"] = clasificadorPago.PaymentType;
            return View(clasificadorPago);
        }

        // GET: ClasificadorPagoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clasificadorPago = await _context.ClasificadorPagos.FindAsync(id);
            if (clasificadorPago == null)
            {
                return NotFound();
            }
            ViewData["Payment"] = clasificadorPago.PaymentType;
            return View(clasificadorPago);
        }

        // POST: ClasificadorPagoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DiaPago,PaymentType,Importe")] ClasificadorPago clasificadorPago)
        {
            if (id != clasificadorPago.Id)
            {
                return NotFound();
            }

            if (clasificadorPago.PaymentType != PaymentType.Gratis)
            {
                if (clasificadorPago.Importe == null)
                {
                    ModelState.AddModelError("", "El campo Importe es obligatorio.");

                }
                if (clasificadorPago.DiaPago == null)
                {
                    ModelState.AddModelError("", "El campo Día del Pago es obligatorio.");
                }

                if (clasificadorPago.Importe != null && clasificadorPago.DiaPago != null)
                {
                    if (_context.ClasificadorPagos.Any(c => c.PaymentType == clasificadorPago.PaymentType && c.DiaPago == clasificadorPago.DiaPago && c.Importe == clasificadorPago.Importe && c.Id != clasificadorPago.Id))
                    {
                        ModelState.AddModelError("", "El Clasifiacor de Pago introducido ya se encuentra en el sistema.");
                    }

                }

            }
            else
            {
                if (_context.ClasificadorPagos.Any(c => c.PaymentType == PaymentType.Gratis))
                {
                    ModelState.AddModelError("", "El Clasifiacor de Pago introducido ya se encuentra en el sistema.");
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(clasificadorPago);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClasificadorPagoExists(clasificadorPago.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Payment"] = clasificadorPago.PaymentType;
            return View(clasificadorPago);
        }


        // POST: ClasificadorPagoes/Delete/5
        //[HttpPost, ActionName("Delete")]       
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            
            var clasificadorPago = await _context.ClasificadorPagos.FindAsync(id);

            try
            {

                _context.ClasificadorPagos.Remove(clasificadorPago);
                await _context.SaveChangesAsync();
                TempData["success"] = $"La operación se ha realizado correctamente";
                return RedirectToAction(nameof(Index));
            }
            catch (System.Exception)
            {

                TempData["error"] = $"El clasificador {clasificadorPago.GetPago} no se ha podido eliminar porque está siendo usado por uno o varios usuarios";
                return RedirectToAction(nameof(Index));
            }

        }

        private bool ClasificadorPagoExists(int id)
        {
            return _context.ClasificadorPagos.Any(e => e.Id == id);
        }
    }
}
