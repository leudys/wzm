﻿using System.Linq;
using wzmData.Models;
using wzm.ViewModels.Validations;
using wzmCore.Enum;
using wzm.Validations;

namespace wzm.Extensions
{
    public static class CredentialValidator
    {

        public static ValidationsViewModel ValidateMKCrendential(this ApplicationDbContext dbContext)
        {
            if (dbContext.MikrotikCredentials == null)
            {
                return new ValidationsViewModel { Message = GenerateMessageValidator.Generate(MessageEnum.Null), Validate = false };
            }
            if (dbContext.MikrotikCredentials.ToList().Count == 0)
            {
                return new ValidationsViewModel { Message = GenerateMessageValidator.Generate(MessageEnum.NotCredentialFound), Validate = false };
            }
            if (!dbContext.MikrotikCredentials.Any(c => c.Selected))
            {
                return new ValidationsViewModel { Message = GenerateMessageValidator.Generate(MessageEnum.NotSelected), Validate = false };
            }
            return new ValidationsViewModel { Message = "", Validate = true };
        }

    }
}
