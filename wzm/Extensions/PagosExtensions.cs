﻿using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmData.Enum;
using wzmData.Models;

namespace wzm.Extensions
{
    public static class PagosExtensions
    {
        public static async Task<List<ApplicationUsers>> GetUsuariosPorPagarAsync(this ApplicationDbContext context, UserManager<ApplicationUsers> userManager)
        {
            var payedUsers = context.Pagos.Include(c => c.Usuario).Where(c => c.FechaPago.Month == DateTime.Now.Month && c.FechaPago.Year.Equals(DateTime.Now.Year));


            var usuarios = new List<ApplicationUsers>();

            var usuariosBd = userManager.Users;

            foreach (var item in usuariosBd)
            {
                if (item is ApplicationUsers)
                {

                    if (!payedUsers.Any(c => c.UsuarioId.Contains(item.Id)))
                    {
                        if (!await userManager.IsLockedOutAsync(item))
                        {
                            var client = item as ApplicationUsers;

                            if (client.ClasificadorPagoId != null)
                            {
                                if (context.ClasificadorPagos.FirstOrDefault(c => c.Id == client.ClasificadorPagoId).PaymentType != PaymentType.Gratis)
                                {
                                    usuarios.Add(item as ApplicationUsers);
                                }
                            }
                        }


                    }
                }


            }
            return usuarios;

        }
    }
}
