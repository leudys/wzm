﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System;
using System.IO;
using wzm.Services;
using wzm.Services.Helpers;
using wzmCore;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmCore.Services;
using wzmCore.Services.System;
using wzmCore.Services.Helpers;

namespace wzm
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(
 Configuration.GetConnectionString("Sqlite"), b => b.MigrationsAssembly("wzmData")));

            services.AddIdentity<ApplicationUsers, IdentityRole>()
              .AddEntityFrameworkStores<ApplicationDbContext>()
                            .AddDefaultTokenProviders();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddRazorPagesOptions(options =>
            {
                options.AllowAreas = true;
                options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Manage");
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/AccessDenied");
            });



            services.ConfigureApplicationCookie(options =>
     {
         // Cookie settings
         options.Cookie.HttpOnly = true;
         options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

         options.LoginPath = "/Identity/Account/Login";
         options.AccessDeniedPath = "/Identity/Account/AccessDenied";
         options.SlidingExpiration = true;
     });

            /*
                        services.Configure<IdentityOptions>(options =>
                        {
                            options.Password.RequireDigit = true;
                            options.Password.RequiredLength = 8;
                            options.Password.RequireNonAlphanumeric = false;
                            options.Password.RequireUppercase = true;
                            options.Password.RequireLowercase = false;
                            // Lockout settings
                            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                            options.Lockout.MaxFailedAccessAttempts = 10;
                            // Cookie settings


                            // User settings
                            options.User.RequireUniqueEmail = true;
                        });
            */
            AddAppSingletons(services);



            services.AddSingleton<IFileProvider>(
            new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));
        }

        private void AddAppSingletons(IServiceCollection services)
        {
            services.AddSingleton<IWzmCore, WzmCore>();
            services.AddSingleton<IJobFactory, SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IJobFactory, JobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<EtecsaInternetPortalsJob>();
            services.AddSingleton<ActiveDevices>();
            services.AddSingleton<MorososJob>();
            services.AddSingleton<ScanJob>();
            services.AddSingleton<ServerToInternet>();
            services.AddSingleton<DeviceSignalMonitor>();
            services.AddSingleton<TracerRouteJob>();
            services.AddSingleton<VipJob>();
            services.AddSingleton<ReconnectionByPingJob>();
            services.AddSingleton<ReconnectionByDefaultJob>();
            services.AddSingleton<BalanceoNTHJob>();
            services.AddSingleton<BalanceoPCCJob>();
            services.AddSingleton<SimpleQueueJob>();
            services.AddSingleton<AntiClonJob>();
            services.AddSingleton<ReconnectionByDefaultDisconnectedJob>();
            services.AddSingleton<NautaConnectorJob>();
            //services.AddSingleton<LicenseChecker>();
            services.AddSingleton<ResetTimeJob>();
            services.AddSingleton<IHostedService, HostedServices>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, UserManager<ApplicationUsers> userManager, RoleManager<IdentityRole> roleManager)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseStatusCodePagesWithReExecute("/StatusCode/{0}");
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });

            Seed.SeedUserAsync(userManager, roleManager);
        }
    }
}
