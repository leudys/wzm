﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using wzmData.Models;

/// <summary>  
/// Summary description for IPaddress  
/// </summary>  
public static class IPaddress
{
    
    [DllImport("Iphlpapi.dll")]
    private static extern int SendARP(Int32 dest, Int32 host, ref Int64 mac, ref Int32 length);

    [DllImport("Ws2_32.dll")]
    private static extern Int32 inet_addr(string ip);

    internal static string GetClientMAC(string strClientIP)
    {
        string mac_dest = "";
        try
        {
            Int32 ldest = inet_addr(strClientIP);
            Int32 lhost = inet_addr("");
            Int64 macinfo = new Int64();
            Int32 len = 6;
            int res = SendARP(ldest, 0, ref macinfo, ref len);
            string mac_src = macinfo.ToString("X");

            while (mac_src.Length < 12)
            {
                mac_src = mac_src.Insert(0, "0");
            }

            for (int i = 0; i < 11; i++)
            {
                if (0 == (i % 2))
                {
                    if (i == 10)
                    {
                        mac_dest = mac_dest.Insert(0, mac_src.Substring(i, 2));
                    }
                    else
                    {
                        mac_dest = ":" + mac_dest.Insert(0, mac_src.Substring(i, 2));
                    }
                }
            }
        }
        catch (Exception err)
        {
            throw new Exception("Lỗi " + err.Message);
        }
        return mac_dest;
    }

    internal static string GetClientIp(this IHttpContextAccessor accessor)
    {
        return accessor.HttpContext.Connection.RemoteIpAddress.ToString();
    }

    internal static string GetClientIp(List<string> ip, MikrotikCredentials credential)
    {
        string selectedIp = string.Empty;
        foreach (var item in ip)
        {
            string[] clientIp = item.Split('.');
            string[] routerIp = credential.Dispositivo.Ip.Split('.');
            if (int.Parse(clientIp[0]) == int.Parse(routerIp[0]) && int.Parse(clientIp[1]) == int.Parse(routerIp[1]) && int.Parse(clientIp[2]) == int.Parse(routerIp[2]))
            {
                selectedIp = item;
                break;
            }
        }
        return selectedIp;

    }

    // Retirve The Local IP Address  
    internal static List<string> GetLocalIPAddress()
    {
        List<string> ipadd = new List<string>();
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                ipadd.Add(ip.ToString());
            }
        }
        return ipadd;
    }

}