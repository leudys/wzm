﻿var gettingData = false;

function InitOverviewDataTable() {
    
    oOverviewTable = $('#example').dataTable({

        "serverside": true,
        "ajax": {
            "url": "PortalNauta/GetJson"

        },
        "columns": [
            { "data": "wireless" },
            { "data": "dhcpClient" },
            { "data": "nat" },
            { "data": "mangle" },
            { "data": "routes" },
            { "data": "internet" }
        ],
        "ordering": true,
        "paging": true,
        "pagingType": "full_numbers",
        "pageLenght": 10
    });

}

function RefreshTable(tableId, urlData) {
    if (!gettingData) {
        gettingData = true;
        $.getJSON(urlData, null, function (json) {
            table = $(tableId).dataTable();
            oSettings = table.fnSettings();

            table.fnClearTable(this);

            for (var i = 0; i < json.data.length; i++) {
                table.oApi._fnAddData(oSettings, json.data[i]);
            }

            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            table.fnDraw();
            gettingData = false;
        });
    }

}

function AutoReload() {
    if (!gettingData) {
        RefreshTable('#example', '/PortalNauta/GetJson');
        setTimeout(function () { AutoReload(); }, 1000);
    }

}

$(document).ready(function () {
    InitOverviewDataTable();
    //setTimeout(function () { AutoReload(); }, 1000);
});