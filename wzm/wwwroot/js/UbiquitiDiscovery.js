﻿var reload_timeout = 0;
function reloadStatus() {
    $.ajax({
        url: '/RegistrationTable/AjaxHandler',
        cache: false,
        dataType: 'html',
        
        success: refreshStatus,
        error: handleError,
        complete: function () {
            if (reload_timeout) {
                clearTimeout(reload_timeout)
            }
            reload_timeout = setTimeout(reloadStatus, 2000)
        }
    });
    return false
}

function refreshStatus(c, e, b) {
    if (c == null) {
        return handleError(b, e, null)
    }
    $('#tableId').html(c);
    
}

function handleError(b, c, a) {
    if (b && b.status != 200 && b.status != 0) {
        window.location.reload()
    }
}
$(document).ready(function () {
    reloadStatus();
});