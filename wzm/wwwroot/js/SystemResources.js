﻿var reload_timeout = 0;
function reloadResources() {
    $.ajax({
        url: '/Home/SystemResources',
        cache: false,
        dataType: 'html',
        
        success: refreshResources,
        error: handleError,
        complete: function () {
            if (reload_timeout) {
                clearTimeout(reload_timeout)
            }
            reload_timeout = setTimeout(reloadResources, 2000)
        }
    });
    return false
}

function refreshResources(c, e, b) {
    if (c == null) {
        return handleError(b, e, null)
    }
    $('#systemResources').html(c);
    
}

function handleError(b, c, a) {
    //if (b && b.status != 200 && b.status != 0) {
    //    window.location.reload()
    //}
}
$(document).ready(function () {
    reloadResources();
});