﻿var gettingData = false;
function InitOverviewDataTable() {
    oOverviewTable = $('#registrationTable').dataTable({

        "serverside": true,
        "ajax": {
            "url": "RegistrationTable/AjaxHandler"

        },
        "columns": [
            { "data": "interface" },
            { "data": "macAddress" },
            { "data": "signalStrengthCh0" },
            { "data": "txRate" },
            { "data": "rxRate" },
            { "data": "txCcq" }
        ],
        "ordering": true,
        "paging": true,
        "pagingType": "full_numbers",
        "pageLenght": 10
    });
}

function RefreshTable(tableId, urlData) {
    if (!gettingData) {
        gettingData = true;
        $.getJSON(urlData, null, function (json) {
            table = $(tableId).dataTable();
            oSettings = table.fnSettings();

            table.fnClearTable(this);

            for (var i = 0; i < json.data.length; i++) {
                table.oApi._fnAddData(oSettings, json.data[i]);
            }

            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            table.fnDraw();
        });
        gettingData = false;
    }
    else {
        alert("ups");
    }

}

function AutoReload() {
    RefreshTable('#registrationTable', '/RegistrationTable/AjaxHandler');
    setTimeout(function () { AutoReload(); }, 1000);
}

$(document).ready(function () {
    InitOverviewDataTable();
    setTimeout(function () { AutoReload(); }, 1000);
});