﻿$(document).ready(function () {

    var url;
    var controller;
    var target;
    var id;


    //Delete Action
    $(".delete").on('click', (e) => {
        e.preventDefault();

        target = e.target;
        id = $(target).data('id');
        controller = $(target).data('controller');
        var action = $(target).data('action');
        var headerMessage = $(target).data('header-message');
        var bodyMessage = $(target).data('content-message');
        redirectUrl = $(target).data('redirect-url');

        url = "/" + controller + "/" + action + "?Id=" + id;
        $("#headerContent").text(headerMessage);
        $("#bodyContent").text(bodyMessage);

        $("#deleteModalId").modal({ blurring: true }).modal('setting', 'closable', false).modal('setting', 'transition', "vertical flip").modal('show');
    });



    $(".disable").on('click', (e) => {
        e.preventDefault();

        target = e.target;
        id = $(target).data('id');
        controller = $(target).data('controller');
        var action = $(target).data('action');
        var headerMessage = $(target).data('header-message');
        var  bodyMessage = $(target).data('content-message');
        redirectUrl = $(target).data('redirect-url');

        url = "/" + controller + "/" + action + "?Id=" + id;
        $("#headerContent_disable").text(headerMessage);
        $("#bodyContent_disable").text(bodyMessage);
        $("#disableModalId").modal({ blurring: true }).modal('setting', 'closable', false).modal('setting', 'transition', "vertical flip").modal('show');
    });

    $("#confirm-delete").click(function () {

        $.ajax({

            url: url,
            method: 'post',
            cache: false,

            data: { 'id': id },
            success: function () {
                window.location.href = controller;
            },
        });

    });


    $("#confirm-disable").click(function () {

        $.ajax({

            url: url,
            method: 'post',
            cache: false,

            data: { 'id': id },
            success: function () {
                window.location.href = controller;
            },
        });

    });


});
