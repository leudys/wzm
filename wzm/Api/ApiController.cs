﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using wzm.Helper;
using wzm.ViewModels;
using wzmData.Models;

namespace wzm.Api
{
    [Route("api/login")]
    [AllowAnonymous]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private SignInManager<ApplicationUsers> _signInManager;

        private ILogger<LoginController> _logger;

        private UserManager<ApplicationUsers> _userManager;

        private AppSettings _appSettings;

        public LoginController(SignInManager<ApplicationUsers> signInManager, UserManager<ApplicationUsers> userManager, ILogger<LoginController> logger, IOptions<AppSettings> appSettings)
        {
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _logger = logger;
            _userManager = userManager;
        }


        [AllowAnonymous]
       
        [HttpPost]
        public async Task<IActionResult> Login([FromHeader]UserLoginViewModel userDto)
        {
            var result = await _signInManager.PasswordSignInAsync(userDto.UserName, userDto.Password, userDto.RememberMe, true);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in.");
                var user = await _userManager.FindByNameAsync(userDto.UserName);
                var roles = await _userManager.GetRolesAsync(user);

                return Ok(new
                {
                    //UserId = user.Id,
                    UserName = user.UserName,
                    //Roles = roles,

                });

            }

            if (result.IsLockedOut)
            {
                _logger.LogWarning("User account locked out.");
                return Ok("El usuario está bloqueado");
            }
            else
            {
                return Ok("Intento de Log In Fallido");
            }
        }
        [AllowAnonymous]
        [Route("api/logout")]
        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return Ok();
        }
    }
}
