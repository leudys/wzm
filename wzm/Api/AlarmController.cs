﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using wzmCore.Interfaces;
using wzmCore.Services;
using wzmCore.Services.System;
using wzmCore.ViewModels;
using wzmData.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace wzm.Api
{
    [Route("api/alarm")]
    [ApiController]
    [AllowAnonymous]
    //[Authorize(Roles = "Admin")]
    public class AlarmController : ControllerBase
    {
        private ApplicationDbContext _db;
        private ActiveDevices _activeDevices;
       

        public AlarmController(ApplicationDbContext applicationDbContext, ActiveDevices activeDevices)
        {
             _db = applicationDbContext;
            _activeDevices = activeDevices;
            //_vpnService = vpnService;
        }

        // GET: api/<AlarmController>
        [HttpGet]
        public List<ActiveDevicesViewModel> Get()
        {

            //var sf = new List<ActiveDevicesViewModel>()
            //{
            //    new ActiveDevicesViewModel()
            //    {
            //        Active=true,
            //        Mac=string.Empty,
            //    }
            //};

            return _activeDevices.Devices;
            //return new string[] { "value1", "value2" };
        }

        // GET api/<AlarmController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<AlarmController>
        [HttpPost("{value}")]
        public void SetAlarm([FromHeader] string value)
        {
            var device = _db.Dispositivos.FirstOrDefault(c => c.Mac.Equals(value));
            if (!device.Alarm)
            {
                device.Alarm = true;

                _db.Update(device);
                _db.SaveChanges();
            }
        }

             

        // PUT api/<AlarmController>/5
        [HttpPut("{id}")]
        [Consumes("application/json")]
        public void Put(string id, [FromBody] DeviceViewModel value)
        {
            try
            {
                var device = _db.Dispositivos.FirstOrDefault(c => c.Mac.Equals(value.Mac));

                if (device.Alarm != value.Alarm)
                {
                    device.Alarm = value.Alarm;
                    _db.Update(device);
                    _db.SaveChanges();
                }
            }
            catch (Exception e)
            {

               
            }

            
        }

        // DELETE api/<AlarmController>/5
        [HttpDelete("{id}")]
        public void Delete([FromHeader] string value)
        {
            var device = _db.Dispositivos.FirstOrDefault(c => c.Mac.Equals(value));
            if (device.Alarm)
            {
                device.Alarm = false;

                _db.Update(device);
                _db.SaveChanges();
            }
        }
    }

    public class DeviceViewModel
    {
        public string Mac { get; set; }

        public bool Alarm { get; set; }
    }
}
