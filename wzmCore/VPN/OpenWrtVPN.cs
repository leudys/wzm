﻿//using SshCore;
//using System;
//using System.Threading;
//using wzmData.Models;

//namespace wzmCore.VPN
//{
//    public static class OpenWrtVPN
//    {
//        public static void StartVPN(SshConnection connection)
//        {
//            connection.RunCommand("/etc/init.d/openvpn start");

//        }

//        public static void StartVPN(SshConnection connection, TimeSpan timeSpan)
//        {
//            connection.RunCommand("/etc/init.d/openvpn start");
//            Thread.Sleep(timeSpan);
//        }

//        public static void StopVPN(SshConnection connection)
//        {
//            connection.RunCommand("/etc/init.d/openvpn stop");
//        }

//        public static void StopVPN(SshConnection connection, TimeSpan timeSpan)
//        {
//            connection.RunCommand("/etc/init.d/openvpn stop");
//            Thread.Sleep(timeSpan);
//        }

//        public static bool IsVpnUp(VpnServer vpn)
//        {
//            using (SshConnection sshConnection = new SshConnection(vpn))
//            {
//                sshConnection.Initialize();

//                if (sshConnection.RunCommand("ps | grep openvpn | grep -v grep") != string.Empty)
//                {
//                    return true;
//                }
//                return false;
//            }
//        }

//        public static bool IsVpnUp(SshConnection sshConnection)
//        {
//            if (sshConnection.RunCommand("ps | grep openvpn | grep -v grep") != string.Empty)
//            {
//                return true;
//            }
//            return false;
//        }

//        public static string LoadVPNLog(SshConnection connection)
//        {
//            return connection.RunCommand("logread | grep daemon.notice");
//        }
//    }
//}
