﻿//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Logging;
//using Quartz;
//using SshCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using tik4net;
//using tik4net.Objects;
//using tik4net.Objects.Ip.Firewall;
//using tik4net.Objects.Ip.Hotspot;
//using wzmCore.Base;
//using wzmCore.Interfaces;
//using wzmCore.Mikrotik.Portales;
//using wzmCore.Services.Helpers;
//using wzmData.Models;

//namespace wzmCore.Services.System
//{
//    [DisallowConcurrentExecution]
//    public class VpnHelper : ApplicationService
//    {
//        private MkLoginNauta _mkNauta;
//        private List<SshConnection> _sshConnection;
//        private ITikConnection _connection;
//        private List<PingVPNTimer> _pingVPNTimers;
//        private bool makePing;

//        public VpnHelper(IWzmCore wzmCore, ILogger<object> logger, EtecsaInternetPortalsJob portales, IServiceProvider serviceScope) : base(wzmCore, logger, serviceScope, portales)
//        {
//            // Este puede ser el problema que me esta causando que el servicio se pare
//            _connection = ConnectionFactory.OpenConnection(TikConnectionType.Api_v2, "192.168.10.1", "admin", "admin.901224");
//            _mkNauta = new MkLoginNauta();
//            _sshConnection = new List<SshConnection>();

//            _pingVPNTimers = new List<PingVPNTimer>();
                       
//        }

//        public override async Task Execute(IJobExecutionContext context)
//        {
//            try
//            {

//                await base.Execute(context);
//                RemovePortalException(wzmData.Enum.ServiceType.Vip);

//                //var hosts = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => c.Vpn).ToList();

//                // Si los portales con internet cae por debajo de dos y hay algun usuario logueado en el hotspot, pongo la cuenta nacional y levanto las vpn

//                // Hasta el momento tengo 2 cuentas nacionales por lo que seria 2 VPN puestas que son 4mbps

//                var activos = _connection.LoadAll<HotspotActive>().ToList();

//                var firewallAddress = _connection.LoadAll<FirewallAddressList>().Where(c => c.List.Contains("vpn")).ToList();
//                //Usuarios que tienen conexión persistente
//                var premium = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => c.Vpn).ToList();
//                DisableInactiveUsers(activos);

//                var activosPersistente = await Persistent(premium, activos, firewallAddress);


//                // Si no hay portales con internet y hay usuarios conectados pongo la cuenta
//                if (PortalesEtecsa.Count() == 0 && activos.Count() >= 1)
//                {
//                    await ConectarUnaCuenta(ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).FirstOrDefault(c => !c.Desabled && !c.PersistentConnection), firewallAddress, activos);

//                    // Dos VPN hasta el momento, pongo las 2 VPN cuando tenga mas de cuatro usuarios conectados.
//                    // Si existen tres usuarios, solo pondré una VPN
//                    //if (activos.Count >= 4)
//                    //{
//                    //    Minimo por VPN son 4 usuarios
//                    //    Si de casualidad hay 4 usuarios conectados, pongo una sola cuenta
//                    //    if (activos.Count == 4)
//                    //    {
//                    //        await ConectarUnaCuenta(ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).FirstOrDefault(c => !c.Desabled && !c.PersistentConnection), firewallAddress, activos);
//                    //    }
//                    //    else
//                    //    {
//                    //        Conecto todas las VPN
//                    //        var vpnN = ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).Where(c => !c.Desabled && !c.PersistentConnection).ToList();

//                    //        Cuentas Nacionales que se conectaron, no las VPN
//                    //        List<int> connected = new List<int>();
//                    //        foreach (var item in vpnN)
//                    //        {
//                    //            if (!_mkNauta.LoggedIn(Connection, item))
//                    //            {
//                    //                if (DoLogin(item))
//                    //                {
//                    //                    connected.Add(item.Id);
//                    //                    CreateTimerInstance(item);
//                    //                }


//                    //            }
//                    //            else
//                    //            {
//                    //                connected.Add(item.Id);
//                    //                CreateTimerInstance(item);
//                    //            }
//                    //        }

//                    //        if (connected.Count != 0)
//                    //        {
//                    //            int vpnCount = 0;
//                    //            for (int i = 0; i < activos.Count(); i++)
//                    //            {
//                    //                if (vpnCount >= connected.Count())
//                    //                {
//                    //                    vpnCount = 0;
//                    //                }

//                    //                if (vpnN[vpnCount].AsInternet)
//                    //                {
//                    //                    if (!string.IsNullOrEmpty(vpnN[vpnCount].AddressList))
//                    //                    {
//                    //                        EnableAddress(firewallAddress, activos.ElementAt(i).Address, vpnN[vpnCount].AddressList);
//                    //                    }
//                    //                    else
//                    //                    {
//                    //                        LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, "Las cuentas nacionales no se han podido conectar.");
//                    //                    }
//                    //                }
//                    //                else
//                    //                {
//                    //                    var vpnI = ApplicationDbContext.VpnServerInternet.FirstOrDefault(c => c.VpnServerNacionalId.Equals(vpnN[vpnCount].Id));

//                    //                    if (!string.IsNullOrEmpty(vpnN[vpnCount].AddressList))
//                    //                    {
//                    //                        EnableAddress(firewallAddress, activos.ElementAt(i).Address, vpnI.AddressList);
//                    //                        await CreateTimerInstance(vpnN[vpnCount], vpnI);
//                    //                    }

//                    //                    else
//                    //                    {
//                    //                        LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, "Las cuentas nacionales no se han podido conectar.");
//                    //                    }
//                    //                }

//                    //                vpnCount++;

//                    //            }
//                    //        }
//                    //        else
//                    //        {
//                    //            LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, "Las cuentas nacionales no se han podido conectar.");
//                    //        }

//                    //    }
//                    //}
//                    //else
//                    //{
//                    //    //Levanto una sola VPN
//                    //    await ConectarUnaCuenta(ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).FirstOrDefault(c => !c.Desabled && !c.PersistentConnection), firewallAddress, activos);
//                    //}

//                    return;
//                }
//                else
//                {
//                    DisconnectNauta(ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).FirstOrDefault(c => c.Host.Equals("192.168.10.11")));
//                    foreach (var item in activos)
//                    {
//                        DesableAddress(firewallAddress, item.Address);
//                    }
//                }
               
//            }
//            catch (Exception e)
//            {
//                LoggerHelper.Log(Logger, Helpers.LogLevel.Error, e.Message);
//            }
//        }

//        private async Task<List<HotspotActive>> Persistent(List<HostsServices> premium, List<HotspotActive> activos, List<FirewallAddressList> firewallAddress)
//        {
//            // Elimino los usuarios que tengan conexion persistente de los activos
//            var activosPersistente = new List<HotspotActive>();
//            var service = ApplicationDbContext.VpnService.FirstOrDefault();
//            if (service.Persistent)
//            {
//                foreach (var item in premium)
//                {
//                    if (activos.Any(c => c.Address.Equals(item.Device.Ip)))
//                    {
//                        var active = activos.FirstOrDefault(c => c.Address.Equals(item.Device.Ip));
//                        activosPersistente.Add(active);
//                        activos.Remove(active);
//                    }
//                }

//                if (service.IncludeUsers)
//                {
//                    //En caso de que existan pocos usuarios en la conexion persistente digamos 2
//                    //y hayan mas usuarios que no sean persistentes pues muevo esos usaurios para la conexion persistente
//                    //hasta un máximo de 10
//                    if (activosPersistente.Count() <= 5)
//                    {
//                        if (activosPersistente.Count() + activos.Count() >= 5)
//                        {
//                            int count = 0;
//                            while (activosPersistente.Count() <= 5)
//                            {
//                                if (activos.Count == 0)
//                                {
//                                    break;
//                                }

//                                try
//                                {
//                                    if (count >= activos.Count)
//                                    {
//                                        count = 0;
//                                    }

//                                    if (!activosPersistente.Any(c => c.Address.Equals(activos[count].Address)))
//                                    {
//                                        activosPersistente.Add(activos[count]);
//                                        var f = activos[count];
//                                        activos.Remove(f);
//                                        count++;
//                                    }
//                                }
//                                catch (Exception)
//                                {
//                                    break;
//                                }
//                            }
//                        }


//                    }
//                }

//                if (activosPersistente.Count() != 0)
//                {
//                    await ConectarUnaCuenta(ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).FirstOrDefault(c => c.Host.Equals("192.168.10.13")), firewallAddress, activosPersistente);
//                }
//                else
//                {
//                    DisconnectNauta(ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).FirstOrDefault(c => c.Host.Equals("192.168.10.13")));
//                }


//            }

//            return activosPersistente;
//        }
                
//        private void DesconectarTodo(List<FirewallAddressList> firewallAddress)
//        {
//            if (firewallAddress.Any(c => !c.Disabled))
//            {
//                foreach (var a in firewallAddress.Where(c => !c.Disabled))
//                {
//                    if (!a.Disabled)
//                    {
//                        LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Warning, $"Deshabilitando acceso a la VPN al dispositivo {a.Comment}.");
//                        DesableAddress(firewallAddress, a.Address, a.List);
//                    }

//                }
//            }

//            foreach (var item in ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Where(c => !c.Desabled).ToList())
//            {
//                if (_mkNauta.LoggedIn(Connection, item))
//                {
//                    DisconnectNauta(item);
//                }
//                var cta = ApplicationDbContext.CuentaNautas.FirstOrDefault(c => c.Id.Equals(item.CuentaNautaId));
//                if (cta.Connected)
//                {
//                    cta.Connected = false;
//                    ApplicationDbContext.Update(cta);
//                    ApplicationDbContext.SaveChanges();
//                }
//            }
//        }

//        private async Task ConectarUnaCuenta(VpnServerNacional vpn, List<FirewallAddressList> firewallAddress, List<HotspotActive> activos)
//        {
//            if (vpn != null)
//            {
//                if (vpn.AsInternet)
//                {
//                    if (!string.IsNullOrEmpty(vpn.AddressList))
//                    {
//                        if (vpn != null)
//                        {
//                            bool connected = false;

//                            if (!_mkNauta.LoggedIn(Connection, vpn, Logger))
//                            {
//                                connected = DoLogin(vpn);
//                            }
//                            else
//                            {
//                                if (ApplicationDbContext.CuentaNautas.Any(c => c.Id.Equals(vpn.CuentaNautaId)))
//                                {
//                                    var cta = ApplicationDbContext.CuentaNautas.FirstOrDefault(c => c.Id.Equals(vpn.CuentaNautaId));
//                                    if (!cta.Connected)
//                                    {
//                                        cta.Connected = true;
//                                        ApplicationDbContext.Update(cta);
//                                        await ApplicationDbContext.SaveChangesAsync();
//                                    }
//                                }
//                                connected = true;
//                            }
//                            if (connected)
//                            {
//                                //CreateTimerInstance(vpn);

//                                for (int i = 0; i < activos.Count(); i++)
//                                {
//                                    if (firewallAddress.Any(c => c.Address.Equals(activos.ElementAt(i).Address) && c.Disabled))
//                                    {
//                                        EnableAddress(firewallAddress, activos.ElementAt(i).Address, vpn.AddressList);

//                                    }
//                                    else if (!firewallAddress.Any(c => c.Address.Equals(activos.ElementAt(i).Address)))
//                                    {
//                                        EnableAddress(firewallAddress, activos.ElementAt(i).Address, vpn.AddressList);
//                                    }
//                                }
//                                ////////////////////////////////////////////////////////////
//                                ///// Dejo un usuario con la conexion normal
//                                //DesconectarVPNRestante(item, ApplicationDbContext.VpnServerInternet.FirstOrDefault(c => c.VpnServerNacionalId.Equals(item.Id)));
//                                //break;
//                            }

//                        }
//                        else
//                        {
//                            //Log ERROR!!!!!!!!!!
//                        }
//                    }
//                    else
//                    {
//                        LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, $"El servidor VPN: {vpn.Host} no posee un address list");
//                    }

//                }
//                else
//                {

//                }
//            }
//        }
       
//        //private async Task CreateTimerInstance(VpnServerNacional vpnServerNacional, VpnServerInternet vpnServerInternet)
//        //{
//        //    if (!_pingVPNTimers.Any(c => c.GetHostNacional.Equals(vpnServerNacional.Host)))
//        //    {
//        //        var timer = new PingVPNTimer(vpnServerNacional, Logger,vpnServerNacional.DisconnectOnPingFailed);
//        //        await timer.Add(vpnServerInternet);
//        //        _pingVPNTimers.Add(timer);

//        //    }
//        //    else
//        //    {
//        //        var timer = _pingVPNTimers.FirstOrDefault(c => c.GetHostNacional.Equals(vpnServerNacional.Host));
//        //        await timer.Add(vpnServerInternet);
//        //    }
//        //}
//        //private void CreateTimerInstance(VpnServerNacional vpnServerNacional)
//        //{
//        //    if (!_pingVPNTimers.Any(c => c.GetHostNacional.Equals(vpnServerNacional.Host)))
//        //    {
//        //        var timer = new PingVPNTimer(vpnServerNacional, Logger,vpnServerNacional.DisconnectOnPingFailed);
//        //        _pingVPNTimers.Add(timer);
//        //    }
//        //    else
//        //    {
//        //        _pingVPNTimers.FirstOrDefault(c => c.GetHostNacional.Equals(vpnServerNacional.Host)).Start();
//        //    }
//        //}


//        private void DesconectarVPNRestante(VpnServerNacional vpnConectada, List<FirewallAddressList> firewallAddressList)
//        {
//            var vpn = ApplicationDbContext.VpnServerNacional.Where(c => !c.Host.Equals(vpnConectada.Host) && !c.PersistentConnection);
//            if (vpn.Count() != 0)
//            {
//                foreach (var item in vpn)
//                {
//                    DisconnectNauta(item);
//                    var cuenta = ApplicationDbContext.CuentaNautas.Find(item.CuentaNautaId);
//                    cuenta.Connected = false;
//                    ApplicationDbContext.Update(cuenta);
//                    ApplicationDbContext.SaveChanges();

//                    if (string.IsNullOrEmpty(item.AddressList))
//                    {
//                        DesableAddress(firewallAddressList.Where(c => c.List.Equals(item.AddressList)).ToList());

//                    }

//                }

//            }

//        }

//        private void DisableInactiveUsers(List<HotspotActive> activos)
//        {
//            List<FirewallAddressList> adress = new List<FirewallAddressList>();
//            foreach (var item in activos)
//            {
//                adress.AddRange(_connection.LoadList<FirewallAddressList>(_connection.CreateParameter("address", item.Address)));
                                
//            }
//        }
//        private void DisconnectNauta(VpnServerNacional vpnServer)
//        {
//            //DeleteTimerInstance(vpnServer);
//            //var vpn = ApplicationDbContext.VpnServerNacional.Include(c => c.Portal).Include(c => c.CuentaNauta).Where(c => !c.Desabled).ToList();
//            //foreach (var item in vpn)
//            //{
//            //    if (_mkNauta.LoggedIn(Connection, item))
//            //    {
//            //        _mkNauta.DisconnectAccount(Connection, Logger, item.Portal.Interfaz, item.CuentaNauta.User);

//            //    }
//            //}
//            if (_mkNauta.LoggedIn(Connection, vpnServer))
//            {
//                _mkNauta.DisconnectAccount(Connection, Logger, vpnServer.Portal.Interfaz, vpnServer.CuentaNauta.User);

//            }


//        }

//        //private void DeleteTimerInstance(VpnServerNacional vpnServer)
//        //{
//        //    if (_pingVPNTimers.Any(c => c.GetHostNacional.Equals(vpnServer.Host)))
//        //    {
//        //        var timer = _pingVPNTimers.FirstOrDefault(c => c.GetHostNacional.Equals(vpnServer.Host));
//        //        timer.Stop();
//        //        _pingVPNTimers.Remove(timer);
//        //        LoggerHelper.Log(_connection, Helpers.LogLevel.Error, $"Eliminando instancia del Timer: {vpnServer.Host}");
//        //    }
//        //}
               

//        public void Stop()
//        {

//            DesconectarTodo(_connection.LoadAll<FirewallAddressList>().Where(c => c.List.Contains("vpn")).ToList());
//        }

//        #region Cuando el usuario se loguea al HotSpot
//        /// <summary>
//        /// Conecta la cuenta Nauta, levanta y conecta las VPN, añade el IP del Usuario al Address List
//        /// </summary>
//        /// <param name="ip"></param>
//        /// <returns></returns>
//        public void OnUserLogIn(string ip)
//        {
//            var activos = _connection.LoadAll<HotspotActive>().Count();

//            // Buscar la VPN a la cual el usuario pertenece (Implementar)

//            // Garantizo 800K a los usuarios
//            if ((PortalesEtecsa.Count() == 0) && activos >= 1 || (PortalesEtecsa.Count() == 1 && activos >= 2)) // O si no le puedo garantizar los 800k a los usuarios por la red, por la VPN seria 1mbps por usuario
//            {
//                // Habilito la VPN para el usuario
//                //EnableAddress(ip, "vpn");
//            }

//        }
               
//        private bool DoLogin(VpnServerNacional nacional)
//        {
//            try
//            {
//                LoggerHelper.Log(_connection, Helpers.LogLevel.Warning, $"Conectando cuenta {nacional.CuentaNauta.User} en el portal {nacional.Portal.Interfaz}.");
//                if (_mkNauta.Login(Connection, _connection, nacional, "10.197.0.1"))
//                {
//                    var cuenta = ApplicationDbContext.CuentaNautas.Find(nacional.CuentaNauta.Id);
//                    cuenta.Connected = true;
//                    ApplicationDbContext.Update(cuenta);
//                    ApplicationDbContext.SaveChanges();
//                    LoggerHelper.Log(_connection, Helpers.LogLevel.Warning, $"Cuenta {nacional.CuentaNauta.User} conectada.");

//                    return true;
//                }
//                else
//                {
//                    var cuenta = ApplicationDbContext.CuentaNautas.Find(nacional.CuentaNauta.Id);
//                    cuenta.Connected = false;
//                    ApplicationDbContext.Update(cuenta);
//                    ApplicationDbContext.SaveChanges();
//                    return false;
//                }

//            }
//            catch (Exception e)
//            {
//                var cuenta = ApplicationDbContext.CuentaNautas.Find(nacional.CuentaNauta.Id);
//                cuenta.Connected = false;
//                ApplicationDbContext.Update(cuenta);
//                ApplicationDbContext.SaveChanges();
//                LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, e.Message);
//                return false;
//            }

//        }

//        private void EnableAddress(List<FirewallAddressList> firewallAddress, string ip, string list)
//        {
//            var user = ApplicationDbContext.UsersDevices.FirstOrDefault(c => c.Ip.Equals(ip));

//            if (firewallAddress.Any(c => c.Address.Equals(ip) && c.List.Contains("vpn")))
//            {
//                var addr = firewallAddress.FirstOrDefault(c => c.Address.Equals(ip) && c.List.Contains("vpn"));

//                if (addr.Disabled || !addr.List.Equals(list))
//                {
//                    if (addr.Disabled)
//                    {
//                        addr.Disabled = false;
//                    }
//                    if (!addr.List.Equals(list))
//                    {
//                        addr.List = list;
//                    }
//                    if (!addr.Comment.Equals(user.Comment))
//                    {
//                        addr.Comment = user.Comment;
//                    }
//                    try
//                    {
//                        _connection.Save(addr);
//                    }
//                    catch (Exception e)
//                    {
//                        LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, $"Error modificando addres list del dispostivo:{addr.Address},error:{e.Message}");
//                    }
//                }

//            }
//            else
//            {
//                if (user != null)
//                {
//                    FirewallAddressList addressList = new FirewallAddressList()
//                    {
//                        Address = ip,
//                        List = list,
//                        Comment = user.Comment
//                    };
//                    _connection.Save(addressList);
//                }


//            }
//            //if (address.Any(c => c.Address.Contains(ip) && c.List.Contains(list) && !c.List.Equals(list)))
//            //{
//            //    var addr = address.FirstOrDefault(c => c.Address.Contains(ip) && !c.List.Equals(list));
//            //    if (!addr.Disabled)
//            //    {
//            //        addr.Disabled = true;

//            //        try
//            //        {
//            //            _connection.Save(addr);
//            //        }
//            //        catch (Exception)
//            //        {

//            //            //
//            //        }
//            //    }
//            //}

//        }
//        private void DesableAddress(List<FirewallAddressList> firewallAddress, string ip)
//        {
//            if (firewallAddress.Any(c => c.Address.Contains(ip) && c.List.Contains("vpn")))
//            {
//                var addr = firewallAddress.FirstOrDefault(c => c.Address.Contains(ip) && c.List.Contains("vpn"));

//                if (!addr.Disabled)
//                {
//                    addr.Disabled = true;

//                    try
//                    {
//                        _connection.Save(addr);
//                    }
//                    catch (Exception)
//                    {

//                        //
//                    }
//                }

//            }
//        }
//        private void DesableAddress(List<FirewallAddressList> firewallAddress)
//        {
//            foreach (var item in firewallAddress)
//            {
//                if (!item.Disabled)
//                {
//                    item.Disabled = true;
//                    _connection.Save(item);
//                }
//            }


//        }
//        private void DesableAddress(string ip, string list)
//        {

//            var address = _connection.LoadAll<FirewallAddressList>();

//            if (address.Any(c => c.Address.Contains(ip) && c.List.Equals(list)))
//            {
//                var addr = address.FirstOrDefault(c => c.Address.Contains(ip) && c.List.Equals(list));

//                if (!addr.Disabled)
//                {
//                    addr.Disabled = true;

//                    try
//                    {
//                        _connection.Save(addr);
//                    }
//                    catch (Exception)
//                    {

//                        //
//                    }
//                }
//            }
//        }

//        private void DesableAddress(List<FirewallAddressList> address, string ip, string list)
//        {
//            if (address.Any(c => c.Address.Contains(ip) && c.List.Equals(list)))
//            {
//                var addr = address.FirstOrDefault(c => c.Address.Contains(ip) && c.List.Equals(list));

//                if (!addr.Disabled)
//                {
//                    addr.Disabled = true;

//                    try
//                    {
//                        _connection.Save(addr);
//                    }
//                    catch (Exception)
//                    {

//                        //
//                    }
//                }
//            }
//        }

//        public void OnUserLogOut(string ip)
//        {
//            //var activeUsers = _connection.LoadAll<HotspotActive>();

//            //if (activeUsers.Count() == 0)
//            //{
//            //    if (_mkNauta.LoggedIn(Connection, VpnModel.VpnServerNacional))
//            //    {
//            //        _mkNauta.DisconnectAccount(Connection, "wlan26");

//            //    }
//            //    StopVPN(VpnModel.VpnServerNacional);
//            //    StopVPN(VpnModel.VpnServerInternet);

//            //}

//            DesableAddress(ip, "vpn");

//        }

//        private void CreateVpnInstance()
//        {
//            try
//            {
//                var serviceScopeFactory = ServiceProvider.GetRequiredService<IServiceScopeFactory>();
//                var scope = serviceScopeFactory.CreateScope();
//                ApplicationDbContext = new ApplicationDbContext(scope.ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
//                //if (_sshConnection.Any())
//                //{
//                foreach (var item in ApplicationDbContext.VpnServerNacional.Where(c => !c.Desabled).ToList())
//                {
//                    if (!_sshConnection.Any(c => c.Info.Host.Equals(item.Host)))
//                    {
//                        if (item.AsInternet)
//                        {
//                            _sshConnection.Add(new SshConnection(item));
//                            _sshConnection.Last().Initialize();
//                        }
//                        else
//                        {
//                            _sshConnection.Add(new SshConnection(item));
//                            _sshConnection.Last().Initialize();
//                            if (ApplicationDbContext.VpnServerInternet.Any(c => c.VpnServerNacionalId.Equals(item.Id)))
//                            {
//                                _sshConnection.Add(new SshConnection(ApplicationDbContext.VpnServerInternet.FirstOrDefault(c => c.VpnServerNacionalId.Equals(item.Id))));
//                                _sshConnection.Last().Initialize();
//                            }

//                        }


//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                Logger.LogCritical($"No se ha podido crear las instancias de OpenWRT: {e.Message}");
//            }

//        }

//        //private void StartVPN(List<VpnServer> vpns)
//        //{

//        //    foreach (var item in _sshConnection)
//        //    {
//        //        OpenWrtVPN.StartVPN(item, TimeSpan.FromSeconds(20));

//        //    }
//        //}

//        //public void StopVPN()
//        //{
//        //    foreach (var item in _sshConnection)
//        //    {
//        //        OpenWrtVPN.StopVPN(item);
//        //    }
//        //}

//        //private void StartVPN(VpnServer vpn)
//        //{
//        //    var ssh = _sshConnection.FirstOrDefault(c => c.Info.Host.Equals(vpn.Host));

//        //    if (ssh != null)
//        //    {
//        //        if (!ssh.IsConnected)
//        //        {
//        //            _sshConnection.FirstOrDefault(c => c.Info.Host.Equals(vpn.Host)).Initialize();
//        //        }

//        //        if (IsVpnUp(ssh))
//        //        {
//        //            LoggerHelper.Log(_connection, Helpers.LogLevel.Info, $"Parando VPN {vpn.Host}");
//        //            OpenWrtVPN.StopVPN(_sshConnection.FirstOrDefault(c => c.Info.Host.Equals(vpn.Host)));
//        //        }
//        //        LoggerHelper.Log(_connection, Helpers.LogLevel.Info, $"Levantando VPN {vpn.Host}");
//        //        OpenWrtVPN.StartVPN(ssh);
//        //    }
//        //    else
//        //    {
//        //        _sshConnection.Add(new SshConnection(vpn));
//        //        _sshConnection.Last().Initialize();
//        //        _sshConnection.Add(new SshConnection(ApplicationDbContext.VpnServerInternet.FirstOrDefault(c => c.VpnServerNacionalId.Equals(vpn.Id))));
//        //        _sshConnection.Last().Initialize();
//        //    }



//        //}

//        //public void StopVPN(VpnServer vpn)
//        //{
//        //    var ssh = _sshConnection.FirstOrDefault(c => c.Info.Host.Equals(vpn.Host));

//        //    if (!ssh.IsConnected)
//        //    {
//        //        _sshConnection.FirstOrDefault(c => c.Info.Host.Equals(vpn.Host)).Initialize();
//        //    }


//        //    if (IsVpnUp(ssh))
//        //    {
//        //        LoggerHelper.Log(_connection, Helpers.LogLevel.Warning, $"Parando VPN {vpn.Host}");
//        //        OpenWrtVPN.StopVPN(ssh);
//        //    }


//        //}

//        //private bool IsVpnUp(VpnServer vpn)
//        //{
//        //    using (SshConnection sshConnection = new SshConnection(vpn))
//        //    {
//        //        sshConnection.Initialize();

//        //        if (sshConnection.RunCommand("ps | grep openvpn | grep -v grep") != string.Empty)
//        //        {
//        //            //LoggerHelper.Log(_connection, Helpers.LogLevel.Info, $"Comprobando que el servidor VPN {vpn.Host} se encuentre levantado.");
//        //            return true;
//        //        }
//        //        return false;
//        //    }
//        //}

//        //private bool IsVpnUp(SshConnection sshConnection)
//        //{
//        //    if (sshConnection.RunCommand("ps | grep openvpn | grep -v grep") != string.Empty)
//        //    {
//        //        //LoggerHelper.Log(_connection, Helpers.LogLevel.Info, $"Comprobando que el servidor VPN {sshConnection.Info.Host} se encuentre levantado.");
//        //        return true;
//        //    }
//        //    return false;
//        //}

//        #endregion
//    }

//    public class VpnModel
//    {
//        public VpnServerNacional VpnServerNacional { get; private set; }
//        public VpnServerInternet VpnServerInternet { get; private set; }

//        public VpnModel()
//        {
//            VpnServerNacional = new VpnServerNacional()
//            {
//                CuentaNauta = new CuentaNauta() { Password = "@dminredesD18", User = "rosaine.morejon@nauta.co.cu" },
//                Host = "192.168.10.11",
//                User = "root",
//                Password = "qaz",
//                Portal = new PortalesEtecsa() { Interfaz = "wlan26", Route = "salida26" }
//            };
//            VpnServerInternet = new VpnServerInternet()
//            {
//                User = "root",
//                Password = "qaz",
//                Host = "192.168.10.12",

//            };
//        }
//    }

//    public class GatewayModel
//    {
//        public Result Result { get; set; }
//    }

//    public class Result
//    {
//        public string Host { get; set; }

//        public string Time { get; set; }
//    }

//}
