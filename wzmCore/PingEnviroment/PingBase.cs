﻿using System;
using System.Collections.Generic;
using System.Threading;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Tool;

namespace wzmCore.PingEnviroment
{
    /// <summary>
    /// Manda al Mikrotik a hacer ping.
    /// </summary>
    public class PingBase
    {
        public virtual string MaxCount { get; set; } = "5";

        public virtual string Host { get; set; }

        public virtual int TimeSleep { get; set; } = 5000;

        /// <summary>
        /// El dispotivo Mikrotik realiza un Ping hacia un IP determinado
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="routingTable"></param>
        /// <returns></returns>
        public virtual List<ToolPing> MakePing(ITikConnection connection, string routingTable)
        {
            List<ToolPing> responseList = new List<ToolPing>();
            Exception responseException = null;

            ITikCommand pingCommand = connection.LoadAsync<ToolPing>(
                ping => responseList.Add(ping), //read callback
                exception => responseException = exception, //exception callback
                connection.CreateParameter("address", Host),
                connection.CreateParameter("count", MaxCount),
                connection.CreateParameter("routing-table", routingTable),
                connection.CreateParameter("size", "50"));
            Thread.Sleep(TimeSleep);

            return responseList;
        }
    }
}
