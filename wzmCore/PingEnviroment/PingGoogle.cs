﻿namespace wzmCore.PingEnviroment
{
    public class PingGoogle : PingBase
    {
        public PingGoogle()
        {
            Host = "8.8.8.8";

            TimeSleep = 2000;

            MaxCount = "3";
        }

        public PingGoogle(string host)
        {
            Host = host;
        }

    }
}
