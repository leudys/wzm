﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace wzmCore.PingEnviroment
{
    public class HostPing
    {
        private string _host;
        List<bool> _pings;
        public HostPing(string host)
        {
            _host = host;
            _pings = new List<bool>();
        }


        /// <summary>
        /// Realiza el ping hacia un Host.
        /// </summary>
        /// <param name="withError">Determina si el ping ha sido llamado nuevamente cuando este dió error.</param>
        /// <returns></returns>
        public List<bool> PingHost(bool withError)
        {
            if (!withError)
            {
                if (_pings.Count > 0)
                {
                    _pings.Clear();
                }
            }

            var ping = new Ping();
            byte[] buffer = new byte[32];

            try
            {
                while (_pings.Count <= 10)
                {
                    var pingReply = ping.Send(_host, 500, buffer, new PingOptions(50, false));
                    if (pingReply != null && pingReply.Status == IPStatus.Success)
                    {

                        _pings.Add(true);
                        return _pings;
                    }
                    else
                    {
                        _pings.Add(false);

                    }
                }
                return _pings;

            }
            catch (Exception)
            {

                _pings.Add(false);
                PingHost(true);
            }
            return _pings;
        }

    }
}
