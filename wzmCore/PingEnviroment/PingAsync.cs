﻿using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace wzmCore.PingEnviroment
{
    public static class PingAsync
    {
        public static Task<PingReply> SendPingAsync(string address)
        {
            var tcs = new TaskCompletionSource<PingReply>();
            Ping ping = new Ping();
            ping.PingCompleted += (obj, sender) =>
            {
                tcs.SetResult(sender.Reply);
            };
            ping.SendAsync(address, new object());
            return tcs.Task;
        }
    }
}
