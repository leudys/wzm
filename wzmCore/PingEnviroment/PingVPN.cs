﻿//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;
//using SshCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Threading;
//using System.Threading.Tasks;
//using tik4net;
//using wzmCore.Services.Helpers;
//using wzmCore.Services.System;
//using wzmCore.VPN;
//using wzmData.Models;
//using LogLevel = wzmCore.Services.Helpers.LogLevel;

//namespace wzmCore.PingEnviroment
//{
//    public static class PingVPN
//    {
//        public static async Task<bool> IsVpnConnectedAsync(string host)
//        {
//            try
//            {
//                // url que hace ping a la gateway del VPN http://192.168.10.12/cgi-bin/vpnstatus?ping=gw

//                using (HttpClient client = new HttpClient())
//                {
//                    HttpResponseMessage response = await client.GetAsync($"http://{host}/cgi-bin/vpnstatus?ping=gw");
//                    response.EnsureSuccessStatusCode();
//                    string responseBody = await response.Content.ReadAsStringAsync();
//                    if (responseBody != string.Empty)
//                    {
//                        var result = JsonConvert.DeserializeObject<GatewayModel>(responseBody);

//                        if (result.Result.Time != null && result.Result.Time != string.Empty)
//                        {
//                            return true;
//                        }
//                        return false;
//                    }

//                    return false;
//                }

//            }
//            catch (Exception e)
//            {
//                return false;
//            }

//        }

//        public static async Task<bool> IsVpnConnectedAsync(string host, ITikConnection tikConnection)
//        {
//            try
//            {
//                // url que hace ping a la gateway del VPN http://192.168.10.12/cgi-bin/vpnstatus?ping=gw

//                using (HttpClient client = new HttpClient())
//                {
//                    HttpResponseMessage response = await client.GetAsync($"http://{host}/cgi-bin/vpnstatus?ping=gw");
//                    response.EnsureSuccessStatusCode();
//                    string responseBody = await response.Content.ReadAsStringAsync();
//                    if (responseBody != string.Empty)
//                    {
//                        var result = JsonConvert.DeserializeObject<GatewayModel>(responseBody);

//                        if (result.Result.Time != null && result.Result.Time != string.Empty)
//                        {
//                            return true;
//                        }
//                        return false;
//                    }

//                    return false;
//                }

//            }
//            catch (Exception e)
//            {
//                LoggerHelper.Log(tikConnection, LogLevel.Error, $"Error intentando comunicarse con el servidor:{host},{ e.Message}");
//                return false;
//            }

//        }

//        public static async Task<List<bool>> IsVpnConnectedAsync(List<string> host)
//        {
//            List<bool> ping = new List<bool>();
//            try
//            {
//                foreach (var item in host)
//                {
//                    using (HttpClient client = new HttpClient())
//                    {
//                        HttpResponseMessage response = await client.GetAsync($"http://{item}/cgi-bin/vpnstatus?ping=gw");
//                        response.EnsureSuccessStatusCode();
//                        string responseBody = await response.Content.ReadAsStringAsync();
//                        if (responseBody != string.Empty)
//                        {
//                            var result = JsonConvert.DeserializeObject<GatewayModel>(responseBody);

//                            if (result.Result.Time != null && result.Result.Time != string.Empty)
//                            {
//                                ping.Add(true);
//                            }
//                            ping.Add(false);
//                        }
//                        ping.Add(false);
//                    }
//                }
//                return ping;
//            }
//            catch (Exception)
//            {
//                ping.Add(false);
//                return ping;
//            }

//        }
//    }

//    public class PingVPNTimer
//    {
//        private Timer _pingVPN;
//        private VpnServerNacional _serverNacional;
//        private VpnServerInternet _serverInternet;
//        private List<SshConnection> _sshConnections;
//        private int _vpnNErrorCount;
//        private int _vpnIErrorCount;
//        //private IServiceProvider _service;
//        private ITikConnection _connection;
//        public bool Started { get; private set; }
//        private ILogger<object> _logger;
//        private bool _disconnect;
//        public PingVPNTimer(/*IServiceProvider iserviceScope*/)
//        {
//            _connection = ConnectionFactory.OpenConnection(TikConnectionType.Api_v2, "192.168.10.1", "admin", "admin.901224");
//            _sshConnections = new List<SshConnection>();
//            _vpnNErrorCount = 0;
//            _vpnIErrorCount = 0;
//            //_service = iserviceScope;
//        }

//        public PingVPNTimer(VpnServer vpn, ILogger<object> logger, bool disconnect)
//        {
//            _connection = ConnectionFactory.OpenConnection(TikConnectionType.Api_v2, "192.168.10.1", "admin", "admin.901224");
//            _sshConnections = new List<SshConnection>();
//            bool added = Task.Run(async () => await Add(vpn)).Result;
//            _vpnNErrorCount = 0;
//            _vpnIErrorCount = 0;
//            _logger = logger;
//            _disconnect = disconnect;
//            if (added)
//            {
//                Start();
//            }
//        }

//        public void Start()
//        {
//            _pingVPN = new Timer(CheckVpnPingAsync, new object(), 0, 10000);
//            Started = true;
//        }

//        public string GetHostNacional
//        {
//            get
//            {
//                if (_serverNacional != null)
//                {
//                    return _serverNacional.Host;
//                }
//                else
//                {
//                    return string.Empty;
//                }
//            }
//        }

//        public string GetHostInternet
//        {
//            get
//            {
//                if (_serverInternet != null)
//                {
//                    return _serverInternet.Host;
//                }
//                else
//                {
//                    return string.Empty;
//                }
//            }
//        }


//        public async Task<bool> Add(VpnServer vpnServer)
//        {
//            try
//            {
//                SshConnection sshConnection = new SshConnection(vpnServer);

//                if (await sshConnection.InitializeAsync())
//                {
//                    _sshConnections.Add(sshConnection);
//                    LoggerHelper.Log(_connection, LogLevel.Warning, $"Se ha añadido PING TIMER al servidor: {vpnServer.Host}");

//                    if (vpnServer is VpnServerNacional)
//                    {
//                        _serverNacional = vpnServer as VpnServerNacional;
//                    }
//                    if (vpnServer is VpnServerInternet)
//                    {
//                        _serverInternet = vpnServer as VpnServerInternet;
//                    }
//                    return true;
//                }
//                else
//                {
//                    LoggerHelper.Log(_connection, LogLevel.Error, $"No se ha podido añadir el servidor virutal:{vpnServer.Host}, error: No se ha podido realizar conexion con el servidor ");
//                    return false;
//                }
//            }
//            catch (Exception)
//            {
//                LoggerHelper.Log(_connection, LogLevel.Error, $"No se ha podido conectar el servidor virutal:{vpnServer.Host}");
//                return false;
//            }



//        }

//        bool working;
//        private async void CheckVpnPingAsync(object sd)
//        {
//            if (!working)
//            {
//                working = true;
//                try
//                {
//                    if (_serverNacional != null)
//                    {
//                        if (_sshConnections.Any(c => c.Info.Host.Equals(_serverNacional.Host)))
//                        {
//                            var connection = _sshConnections.FirstOrDefault(c => c.Info.Host.Equals(_serverNacional.Host));

//                            if (!connection.IsConnected)
//                            {
//                                connection.Initialize();
//                            }


//                          var ssd=  OpenWrtVPN.LoadVPNLog(connection);

//                            if (!OpenWrtVPN.IsVpnUp(connection))
//                            {
//                                OpenWrtVPN.StartVPN(connection);

//                                //while (OpenWrtVPN.LoadVPNLog(connection).Contains("Reloading Firewall rules"))
//                                //{
//                                //    LoggerHelper.Log(_logger, LogLevel.Info, OpenWrtVPN.LoadVPNLog(connection));
//                                //}

//                            }
//                            if (!await PingVPN.IsVpnConnectedAsync(connection.Info.Host, _connection))
//                            {
//                                LoggerHelper.Log(_logger, LogLevel.Warning, $"La VPN {connection.Info.Host}: Ha fallado el ping.");
//                                _vpnNErrorCount++;
//                            }
//                            else
//                            {

//                                LoggerHelper.Log(_logger, LogLevel.Warning, $"La VPN {connection.Info.Host}: Ha realizado el ping correctamente.");
//                                _vpnNErrorCount = 0;
//                            }
//                            if (_disconnect)
//                            {
//                                if (_vpnNErrorCount >= 5)
//                                {
//                                    if (!OpenWrtVPN.IsVpnUp(connection))
//                                    {
//                                        OpenWrtVPN.StartVPN(connection);

//                                    }
//                                    else
//                                    {
//                                        OpenWrtVPN.StopVPN(connection);
//                                        Thread.Sleep(TimeSpan.FromSeconds(2));
//                                        LoggerHelper.Log(_logger, LogLevel.Warning, $"La VPN {connection.Info.Host}: No se ha conectado en {_vpnNErrorCount} intentos, esperando 50 segundos para levantarla.");
//                                        OpenWrtVPN.StartVPN(connection);
//                                        Thread.Sleep(TimeSpan.FromSeconds(50));
//                                    }
//                                    //LoggerHelper.Log(_connection, LogLevel.Error, $"La VPN {connection.Info.Host}: No se ha conectado en {_vpnNErrorCount} intentos.");
//                                }
//                            }
                            

//                        }
//                    }
//                    if (_serverInternet != null)
//                    {
//                        if (_sshConnections.Any(c => c.Info.Host.Equals(_serverInternet.Host)))
//                        {
//                            var connection = _sshConnections.FirstOrDefault(c => c.Info.Host.Equals(_serverInternet.Host));


//                            if (!OpenWrtVPN.IsVpnUp(_serverInternet))
//                            {
//                                OpenWrtVPN.StartVPN(connection);
//                            }
//                            if (!await PingVPN.IsVpnConnectedAsync(connection.Info.Host, _connection))
//                            {
//                                LoggerHelper.Log(_logger, LogLevel.Warning, $"La VPN {connection.Info.Host}: Ha fallado el ping.");
//                                //_vpnConnectedN = false;
//                            }
//                            else
//                            {

//                                LoggerHelper.Log(_connection, LogLevel.Warning, $"La VPN {connection.Info.Host}: Ha realizado el ping correctamente.");
//                                //_vpnConnectedN = true;
//                            }
//                            if (_disconnect)
//                            {
//                                if (_vpnIErrorCount >= 5)
//                                {
//                                    if (!OpenWrtVPN.IsVpnUp(_serverNacional))
//                                    {
//                                        OpenWrtVPN.StartVPN(connection);

//                                    }
//                                    else
//                                    {
//                                        OpenWrtVPN.StopVPN(connection);
//                                        LoggerHelper.Log(_logger, LogLevel.Warning, $"La VPN {connection.Info.Host}: No se ha conectado en {_vpnIErrorCount} intentos, esperando 10 segundos para levantarla.");
//                                        Thread.Sleep(TimeSpan.FromSeconds(10));
//                                        OpenWrtVPN.StartVPN(connection);
//                                    }
//                                    LoggerHelper.Log(_logger, LogLevel.Error, $"La VPN {connection.Info.Host}: No se ha conectado en {_vpnIErrorCount} intentos.");
//                                }
//                            }
                           
//                        }
//                    }

//                }
//                catch (Exception e)
//                {
//                    LoggerHelper.Log(_logger, LogLevel.Error, $"Error conectando al servidor. Error: {e.Message}");
//                    //throw;
//                }
//                finally
//                {
//                    working = false;
//                }
//            }
//        }

//        public void Stop()
//        {
//            _pingVPN?.Dispose();
//            Started = false;

//            if (_serverNacional != null)
//            {
//                if ((_sshConnections.Any(c => c.Info.Host.Equals(_serverNacional.Host))))
//                {
//                    if (!OpenWrtVPN.IsVpnUp(_serverNacional))
//                    {
//                        var connection = _sshConnections.FirstOrDefault(c => c.Info.Host.Equals(_serverNacional.Host));
//                        OpenWrtVPN.StopVPN(connection);
//                    }
//                }
//            }

//            if (_serverInternet != null)
//            {
//                if ((_sshConnections.Any(c => c.Info.Host.Equals(_serverInternet.Host))))
//                {
//                    if (!OpenWrtVPN.IsVpnUp(_serverInternet))
//                    {
//                        var connection = _sshConnections.FirstOrDefault(c => c.Info.Host.Equals(_serverInternet.Host));
//                        OpenWrtVPN.StopVPN(connection);
//                    }
//                }
//            }
//        }
//    }
//}
