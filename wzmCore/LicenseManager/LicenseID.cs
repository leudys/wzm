﻿using Microsoft.Win32;
using System;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.System;
using wzmCore.Extensions;
using wzmCore.ViewModels;
using wzmCore.WzmLicense.Crypt;

namespace wzmCore.WzmLicense
{
    public static class LicenseID
    {
        private static string _activationCode = string.Empty;
        private static string _serialKey = string.Empty;

        internal static void MakeRegeditActivationCode()
        {
            Registry.CurrentUser.CreateSubKey(@"Freelancer\Activation");
        }

        internal static string ReadRegeditKey()
        {
            try
            {
                RegistryKey myKey = Registry.CurrentUser.OpenSubKey("Freelancer\\Activation", true);

                return myKey.GetValue("ActivationCode").ToString();
            }
            catch
            {
                SetRegeditKeyValue("");
                return null;
            }
        }

        internal static void SetRegeditKeyValue(string value)
        {
            var myKey = Registry.CurrentUser.OpenSubKey("Freelancer\\Activation", true);
            if (myKey != null) myKey.SetValue("ActivationCode", value, RegistryValueKind.String);
        }


        public static string GetLicenseId()
        {

            return GetLicenseProperties().ToString();

        }

        private static LicenseProperties GetLicenseProperties()
        {
            return new LicenseProperties()
            {
                BiosId = BiosId(),
                CpuId = CpuId(),
                MotherBoardId = BaseId(),
                // WindowsInstalationDate = WindowsInstalationDate()
            };
        }

        public static DateTime WindowsInstalationDate()
        {

            var key = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, Environment.MachineName, RegistryView.Registry64);

            key = key.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", false);
            if (key != null)
            {
                DateTime startDate = new DateTime(1970, 1, 1, 0, 0, 0);
                object objValue = key.GetValue("InstallDate");
                string stringValue = objValue.ToString();
                long regVal = Convert.ToInt64(stringValue);


                DateTime startDate1 = new DateTime(regVal);
                startDate.AddSeconds(regVal);

                return startDate;
            }
            return DateTime.Now;
        }

        public static string GetEncryptedLicenseId()
        {
            return ElectronicCodeBook.Encrypt(GetLicenseProperties().ToString(), true, SecretKeys.StringKey);

        }

        public static string GetEncryptedLicenseId(string ip, string user, string password,string routerOs)
        {
            string mikrotikId;
            using (ITikConnection connection = ConnectionFactory.OpenConnection(routerOs.GetApiVersion(), ip, user, password))
            {
                var mkLicense = connection.LoadSingle<License>();
                var router = connection.LoadSingle<Routerboard>();
                mikrotikId = $"{mkLicense.SoftwareId};{router.SerialNumber}";
            }

            return $"{mikrotikId}; {ElectronicCodeBook.Encrypt(GetLicenseProperties().ToString(), true, SecretKeys.StringKey)}";

        }

        private static string GetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            return GetHexString(sec.ComputeHash(bt));
        }

        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte b = bt[i];
                var n = (int)b;
                var n1 = n & 15;
                var n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char)(n2 - 10 + 'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char)(n1 - 10 + 'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
            }
            return s;
        }


        #region Original Device ID Getting Code

        private static string Identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            ManagementClass mc = new ManagementClass(wmiClass);
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (var o in moc)
            {
                var mo = (ManagementObject)o;
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
            return result;
        }

        private static string CpuId()
        {
            //Uses first CPU Identifier available in order of preference
            //Don't get all Identifiers, as it is very time consuming
            string retVal = Identifier("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = Identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = Identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = Identifier("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += Identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }
        //BIOS Identifier
        private static string BiosId()
        {
            return Identifier("Win32_BIOS", "Manufacturer")
            + Identifier("Win32_BIOS", "SMBIOSBIOSVersion")
            //+ Identifier("Win32_BIOS", "IdentificationCode")
            + Identifier("Win32_BIOS", "SerialNumber")
            + Identifier("Win32_BIOS", "ReleaseDate")
            + Identifier("Win32_BIOS", "Version");
        }

        //Motherboard ID
        private static string BaseId()
        {
            return //Identifier("Win32_BaseBoard", "Model")
             Identifier("Win32_BaseBoard", "Manufacturer")
            + Identifier("Win32_BaseBoard", "Name")
            + Identifier("Win32_BaseBoard", "SerialNumber");
        }

        #endregion
    }
}

