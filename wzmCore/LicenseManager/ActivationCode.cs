﻿using System;
using System.ComponentModel.DataAnnotations;
using wzmCore.Enum;

namespace wzmCore.WzmLicense
{
    public class ActivationCode
    {
        [Display(Name = "Nombre y Apellidos")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string ClientName { get; set; }

        [Display(Name = "Correo Electrónico")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string AppVersion { get; set; }

        public string LicenseId { get; set; }

        public DateTime StartDate { get; set; }

        [Display(Name = "Tipo de Licencia")]
        public LicenseType LicenseType { get; set; }

        public override string ToString()
        {
            return $"{ClientName};{Email};{AppVersion};{LicenseId};{StartDate.Date};{LicenseType}";
        }


    }
}
