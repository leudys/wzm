﻿using System;

namespace wzmCore.WzmLicense
{
    public class LicenseProperties
    {
        public string CpuId { get; set; }

        public string BiosId { get; set; }

        public string MotherBoardId { get; set; }

        public DateTime WindowsInstalationDate { get; set; }

        public DateTime StartDate { get; set; }

        public override string ToString()
        {
            return $"{CpuId};{BiosId};{MotherBoardId};{WindowsInstalationDate.ToLongTimeString()}";
        }

    }
}
