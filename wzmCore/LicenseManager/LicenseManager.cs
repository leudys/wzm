﻿using Microsoft.AspNetCore.Http;
using Standard.Licensing;
using Standard.Licensing.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WzmCommon.Files;
using wzmCore.Enum;
using wzmCore.Extensions;
using wzmCore.Utils;
using wzmCore.ViewModels;
using wzmCore.WzmLicense.Crypt;

namespace wzmCore.WzmLicense
{
    public class LicenseManager
    {
        private string _ActualLicenseId { get; set; }

        public bool Validated { get; private set; }

        public string StoredLicense { get; private set; }

        public bool Reset { get; private set; }

        public bool ServerHasInternet { get; private set; }

        public bool DeviceHasInternet { get; set; }

        private List<InvalidateReason> _reasons;

        public LicenseManager()
        {
            try
            {
                // Guarda la licencia en memoria
                StoredLicense = LoadLicenseFromFile();
                _reasons = new List<InvalidateReason>();
            }
            catch (Exception)
            {

            }

        }

        /// <summary>
        /// Obtiene el tipo de licencia, vital para los servicios poder levantar
        /// </summary>
        /// <param name="secretKeys"></param>
        /// <returns></returns>
        public Enum.LicenseType GetLicenseType()
        {
            return (Enum.LicenseType)StoredLicense.GetLicense().Type;

        }

        /// <summary>
        /// Obtiene el Id de la Licencia
        /// </summary>
        /// <returns></returns>
        public string GetLicenseId()
        {
            return StoredLicense.GetLicense().Id.ToString();
        }

        /// <summary>
        /// Obtiene el Email del cliente
        /// </summary>
        /// <returns></returns>
        public string GetLicenseEmail()
        {
            return StoredLicense.GetLicense().Customer.Email;
        }

        /// <summary>
        /// Valida la licencia guardada.
        /// </summary>
        /// <param name="runningTime"></param>
        /// <returns></returns>
        public bool Validate()
        {
            //if (_reasons.Count() > 0)
            //{
            //    return Validated = false;
            //}
            //try
            //{
            //    var license = StoredLicense.GetLicense();
            //    //Validacion por defecto
            //    var validationFailures = license.Validate()
            //    .ExpirationDate()
            //    .When(lic => lic.Type == Standard.Licensing.LicenseType.Trial)
            //    .And().Signature(SecretKeys.PublicKey).AssertValidLicense();

            //    if (validationFailures.Any())
            //    {
            //        return Validated = false;
            //    }

            //    // Este es un proceso costoso por lo que le hago una sola instancia.
            //    if (_ActualLicenseId == null)
            //    {
            //        _ActualLicenseId = LicenseID.GetEncryptedLicenseId();
            //    }


            //    // Si la fecha actual que tiene la computadora es menor que la fecha cuando se hizo el codigo de activacion, es que el usuario reinstaló windows.
            //    var startDate = DateTime.Parse(license.AdditionalAttributes.Get("StartDate"));
            //    if (DateTime.Compare(DateTime.Now, startDate) < 0)
            //    {
            //        return Validated = false;
            //    }

            //    if (!CompareLicensesId(ElectronicCodeBook.Decrypt(license.AdditionalAttributes.Get("LicenseId"), true, SecretKeys.StringKey).LicenseProperties(), ElectronicCodeBook.Decrypt(_ActualLicenseId, true, SecretKeys.StringKey).LicenseProperties()))
            //    {
            //        return Validated = false;
            //    }

            //    if (startDate > DateTime.Now)
            //    {
            //        return Validated = false;
            //    }



            //    return Validated = true;

            //}
            //catch (Exception)
            //{
            //    return Validated = false;
            //}
            return Validated = true;
        }

        /// <summary>
        /// Valida el tiempo que se encuentra corriendo la aplicacion.
        /// </summary>
        /// <param name="runningTime"></param>
        /// <param name="_lastDay"></param>
        /// <returns></returns>
        public bool ValidateRunningTime(TimeSpan runningTime, DateTime _lastDay)
        {

            var license = StoredLicense.GetLicense();
            var startDate = DateTime.Parse(license.AdditionalAttributes.Get("StartDate"));

            if (runningTime.Hours >= (license.Expiration - startDate).Days * 24)
            {
                return Validated = false;
            }

            if (_lastDay.Date > DateTime.Now.Date)
            {
                return Validated = false;
            }
            return true;
        }

        /// <summary>
        /// Comprueba si el ultimo día ha de ser cambiado.
        /// </summary>
        /// <param name="_lastDay"></param>
        /// <returns></returns>
        public bool ValidateWriteLastDay(DateTime _lastDay)
        {
            return _lastDay.Date <= DateTime.Now.Date;
        }

        /// <summary>
        /// Carga la Licencia.
        /// </summary>
        /// <returns></returns>
        private string LoadLicenseFromFile()
        {
            return FileHelper.FileDecrypt(FilePath.GetFilePath(Files.License), SecretKeys.FilesKey);

        }

        /// <summary>
        /// Invalida la licencia
        /// </summary>
        public void InvalidateLicense(InvalidateReason invalidateReason)
        {
            if (!_reasons.Any(c => c == invalidateReason))
            {
                _reasons.Add(invalidateReason);
            }

            switch (invalidateReason)
            {

                case InvalidateReason.NoInternetAccess:
                    break;
                case InvalidateReason.TimeChanged:
                    break;
                case InvalidateReason.Inmediatly:
                    break;
                default:
                    break;
            }

            Validated = false;
        }


        /// <summary>
        /// Comprueba la fecha de Instalación de Windows
        /// </summary>
        /// <returns></returns>
        public bool CheckWindowsInstalationDate()
        {
            var instalationDate = LicenseID.WindowsInstalationDate();

            var license = StoredLicense.GetLicense();
            var licenseId = ElectronicCodeBook.Decrypt(license.AdditionalAttributes.Get("LicenseId"), true, SecretKeys.StringKey);

            int sd = instalationDate.CompareTo(DateTime.Parse(licenseId.Split(";")[3]));
            return true;

        }

        /// <summary>
        /// Muestra la Licencia al Usuario
        /// </summary>
        /// <param name="fieleskey">Llave para desencriptar el archivo.</param>
        /// <param name="stringKey">Llave para desencriptar los strings.</param>
        /// <returns></returns>
        public LicenseModel ShowLicense()
        {
            LicenseModel licenseModel = new LicenseModel();

            try
            {
                var license = StoredLicense.GetLicense();

                licenseModel.ClientName = license.Customer.Name;
                licenseModel.Email = license.Customer.Email;
                licenseModel.Periodo = license.Expiration;

                licenseModel.LicenseType = (Enum.LicenseType)license.Type;
                return licenseModel;

            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Guarda el Código de Activación 
        /// </summary>
        /// <param name="licenseModel"></param>
        public void SaveActivationCode(ActivationCode activationCode, string ip, string user, string password,string routerOs)
        {
            activationCode.StartDate = DateTime.Now;
            activationCode.LicenseId = LicenseID.GetEncryptedLicenseId(ip, user, password, routerOs);

            if (!FilePath.ExistFile(Files.Activation))
            {
                //File.Create(ActivationCodePath);
                using (var streaWriter = new StreamWriter(FilePath.GetFilePath(Files.Activation)))
                {
                    streaWriter.Write(ElectronicCodeBook.Encrypt(activationCode.ToString(), true, SecretKeys.StringKey));
                }
            }
            // Aquí no he de comprobar nada, soloamente si el usuario genera un nuevo código de activación, pues que lo genere
            // esto no provoca ningún impacto en la aplicación. Hasta donde se.
            else
            {
                File.Delete(FilePath.GetFilePath(Files.Activation));

                using (var streaWriter = new StreamWriter(FilePath.GetFilePath(Files.Activation)))
                {
                    streaWriter.Write(ElectronicCodeBook.Encrypt(activationCode.ToString(), true, SecretKeys.StringKey));
                }
            }

        }

        /// <summary>
        /// Importa la Licencia
        /// </summary>
        /// <param name="license"></param>
        /// <returns></returns>
        public async Task Import(IFormFile file)
        {
            // Si existe una licencia.
            if (FilePath.ExistFile(Files.License))
            {
                // Creo una licencia temporal.
                using (FileStream fileStream = new FileStream(FilePath.GetFilePath(Files.TempLicense), mode: FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                //Desencripto la licencia.
                var decryptedLicenseTemp = ElectronicCodeBook.Decrypt(FileHelper.FileDecrypt(FilePath.GetFilePath(Files.TempLicense), SecretKeys.FilesKey), true, SecretKeys.StringKey);
                var templicense = License.Load(decryptedLicenseTemp);


                var license = StoredLicense.GetLicense();

                // Compruebo que la licencia existente no sea igual a la que se va a importar.
                if (templicense.Expiration != license.Expiration && CompareLicensesId(ElectronicCodeBook.Decrypt(license.AdditionalAttributes.Get("LicenseId"), true, SecretKeys.StringKey).LicenseProperties(), ElectronicCodeBook.Decrypt(templicense.AdditionalAttributes.Get("LicenseId"), true, SecretKeys.StringKey).LicenseProperties()))
                {
                    File.Delete(FilePath.GetFilePath(Files.TempLicense));
                    File.Delete(FilePath.GetFilePath(Files.License));

                    using (FileStream fileStream = new FileStream(FilePath.GetFilePath(Files.License), mode: FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    //Necesito borrar el tiempo que lleva la app corriendo.
                    if (FilePath.ExistFile(Files.Time))
                    {
                        File.Delete(FilePath.GetFilePath(Files.Time));

                        // Para hacer esto se ha comprobado que exista Internet en la PC.

                        var webserver = new HttpRequestResponse();
                        try
                        {
                            if (webserver.WebOnline)
                            {
                                if (webserver.LoginWeb())
                                {
                                    webserver.ResetRunningTime(GetLicenseId(), DateTime.Now);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }


                    }



                    StoredLicense = LoadLicenseFromFile();
                }


            }
            else
            {
                using (var stream = File.Create(FilePath.GetFilePath(Files.License)))
                {
                    await file.CopyToAsync(stream);
                }

                if (!Validate())
                {
                    File.Delete(FilePath.GetFilePath(Files.License));
                }
                else
                {
                    StoredLicense = LoadLicenseFromFile();
                }
            }


        }

        /// <summary>
        /// Comprueba que exista el código de activación
        /// </summary>
        /// <returns></returns>
        public bool ExistActivationCode()
        {
            return FilePath.ExistFile(Files.Activation);
        }

        /// <summary>
        /// Comprueba que las propiedades del localhost sean iguales.
        /// </summary>
        /// <param name="propiedadesPc"></param>
        /// <param name="propiedaddesLicencia"></param>
        /// <returns></returns>
        private bool CompareLicensesId(LicenseProperties propiedadesPc, LicenseProperties propiedadesLicencia)
        {
            if (propiedadesPc != null && propiedadesLicencia != null)
            {
                if (!propiedadesPc.BiosId.Equals(propiedadesLicencia.BiosId) || !propiedadesPc.CpuId.Equals(propiedadesLicencia.CpuId) || !propiedadesPc.MotherBoardId.Equals(propiedadesLicencia.MotherBoardId))
                {
                    // ! Devolver falso, esta comenatdo porque tenia que instalar el VS para poder cambiar el codigo de la licencia. No olvidar
                    //return false;
                    return true;
                }

                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
