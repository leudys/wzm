﻿
using System;
using System.ComponentModel.DataAnnotations;
using wzmCore.Enum;

namespace wzmCore.WzmLicense
{
    public class LicenseModel
    {
        [Display(Name = "Nombre y Apellidos")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string ClientName { get; set; }

        [Display(Name = "Correo Electrónico")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string AppVersion { get; set; }

        public string LicenseId { get; set; }

        [Display(Name ="Fecha de Expiración")]
        [DataType(DataType.DateTime)]
        public DateTime Periodo { get; set; }

        [Display(Name ="Tipo de Licencia")]
        public LicenseType LicenseType { get; set; }
    }
}
