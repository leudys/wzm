﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Tool;

namespace wzmCore.Utils
{
    internal static class MikrotikTracerRoute
    {
        public static List<ToolTraceroute> TracerRoute(ITikConnection connection, int wait, string routingTable, string count, string host)
        {
            List<ToolTraceroute> responseList = new List<ToolTraceroute>();
            Exception responseException = null;

            ITikCommand pingCommand = connection.LoadAsync<ToolTraceroute>(
                ping => responseList.Add(ping), //read callback
                exception => responseException = exception, //exception callback
                connection.CreateParameter("address", host),
                connection.CreateParameter("count", count),
                connection.CreateParameter("routing-table", routingTable),
                connection.CreateParameter("size", "56"));
            Thread.Sleep(wait);

            List<ToolTraceroute> result = new List<ToolTraceroute>();

            foreach (var item in responseList)
            {
                
                if (!item.Address.Equals(""))
                {
                    result.Add(item);
                }
            }

            return result;
        }
    }
}
