﻿//using System;
//using System.Diagnostics;
//using System.IO;
//using wzmCore.ViewModels;
//using wzmCore.WzmLicense.Crypt;

//namespace wzmCore.Utils
//{
//    public class LicenseFilesHelper
//    {
//        private readonly SendInfoToWeb sendInfoToWeb;

//        private DateTime _lastDay;

//        private TimeSpan timeRunning;

//        public DateTime GetLastDay
//        {
//            get
//            {
//                return _lastDay;
//            }
//        }

//        public TimeSpan GetTimeRunning
//        {
//            get
//            {
//                return timeRunning;
//            }
//        }

//        public void IncreaseTimeRunning(Stopwatch stopwatch)
//        {
//            timeRunning.Add(stopwatch.Elapsed);
//        }

//        public LicenseFilesHelper()
//        {
//            sendInfoToWeb = new SendInfoToWeb();
//        }

//        #region Local Files Section

//        /// <summary>
//        /// Intento coger el ultimo día que se encuentra en la PC local, si es que se encuentra.
//        /// Por defecto devuelve la fecha actual.
//        /// </summary>
//        private DateTime GetLocalStoredLastDayFile()
//        {
//            //Intento cargar el archivo desde la PC local.

//            // Si el archivo existe en la PC local
//            if (FilePath.ExistFile(Files.LastDay))
//            {
//                try
//                {
//                    // Intento cargar el ultimo día.
//                    using (var stream = new StreamReader(FilePath.GetFilePath(Files.LastDay)))
//                    {
//                        return DateTime.Parse(ElectronicCodeBook.Decrypt(stream.ReadToEnd(), true, SecretKeys.StringKey));
//                        // Como este método será llamado varias veces he de comprobar que el día almacenado sea mayor que el ultimo dia en el sistema.
//                        //if (_lastDay < localDay)
//                        //{
//                        //    _lastDay = localDay;
//                        //}

//                    }
//                }
//                catch (Exception)
//                {
//                    return DateTime.Now;
//                }

//            }

//            return DateTime.Now;
//            //else
//            //{
//            //    // Si no existe el archivo, creo uno con la fecha actual.
//            //    using (var stream = new StreamWriter(FilePath.LastDayPath))
//            //    {
//            //        stream.Write(ElectronicCodeBook.Encrypt(DateTime.Now.ToString(), true, SecretKeys.StringKey));

//            //        _lastDay = DateTime.Now;
//            //    }
//            //}
//        }

//        private DateTime GetRemoteStoredLastDayFile()
//        {

//        }

//        /// <summary>
//        /// Intento coger el tiempo de ejecucion de la app en la PC local.
//        /// </summary>
//        private void GetLocalRunnginTime()
//        {
//            if (FilePath.ExistFile(Files.Time))
//            {
//                try
//                {
//                    using (var stream = new StreamReader(FilePath.TimePath))
//                    {
//                        timeRunning = TimeSpan.Parse(ElectronicCodeBook.Decrypt(stream.ReadToEnd(), true, SecretKeys.StringKey));
//                    }
//                }
//                catch (Exception)
//                {

//                }

//            }

//            else
//            {

//                using (var stream = new StreamWriter(FilePath.TimePath))
//                {
//                    // Si el usuario elimino el archivo o reinstaló la pc, este codigo es inutil.
//                    timeRunning.Add(_stopWatch.Elapsed);
//                    stream.Write(ElectronicCodeBook.Encrypt(_timeRunning.ToString(), true, SecretKeys.StringKey));

//                }


//            }
//        }

//        #endregion



//        #region WebServerSection

//        /// <summary>
//        /// Comprueba el último día que existe en el Servidor
//        /// </summary>
//        public void CheckLastDayWithServer(string licenseId, string email)
//        {
//            // Si la Web se encuentra accesible
//            if (sendInfoToWeb.IsWebOnline())
//            {
//                // Si ya se ha logueado a la Web
//                if (sendInfoToWeb.LoggedIn)
//                {
//                    GetLocalLastDayFile();
//                    var lastDay = sendInfoToWeb.ExistLastDay(licenseId);

//                    if (lastDay.Exist)
//                    {
//                        if (_lastDay < lastDay.Value)
//                        {
//                            _lastDay = lastDay.Value;

//                            // Escribo los datos en la PC local.
//                            using (var stream = new StreamWriter(FilePath.LastDayPath))
//                            {
//                                stream.Write(ElectronicCodeBook.Encrypt(_lastDay.ToString(), true, SecretKeys.StringKey));
//                            }
//                        }
//                        if (_lastDay > lastDay.Value)
//                        {
//                            sendInfoToWeb.WriteLastDayToServer(licenseId, email, _lastDay);
//                        }
//                    }
//                    else
//                    {

//                        sendInfoToWeb.WriteLastDayToServer(licenseId, email, _lastDay);
//                    }
//                }

//                else
//                {
//                    // Intento loguearme a la Web
//                    if (sendInfoToWeb.LogIn())
//                    {
//                        var lastDay = sendInfoToWeb.ExistLastDay(licenseId);

//                        if (lastDay.Exist)
//                        {
//                            if (_lastDay < lastDay.Value)
//                            {
//                                _lastDay = lastDay.Value;
//                            }
//                        }
//                    }
//                    // Si falla el Login por cualquier motivo
//                    else
//                    {
//                        GetLocalLastDayFile();
//                    }
//                }

//            }
//            else
//            {
//                GetLocalLastDayFile();
//            }
//        }



//        /// <summary>
//        /// Realiza operaciones con el Server para determinar el tiempo que la app esté corriendo.
//        /// </summary>
//        private void CheckRunningTimeWithServer()
//        {
//            if (SendInfoToWeb.IsWebOnline())
//            {
//                if (SendInfoToWeb.LogIn())
//                {
//                    var runningTimeWeb = SendInfoToWeb.ExistRunnigTime(_wzmCore.LicenseManager.GetLicenseId());
//                    if (runningTimeWeb.Exist)
//                    {
//                        var time = TimeSpan.FromDays(runningTimeWeb.Value);
//                        if (_timeRunning < time)
//                        {
//                            _timeRunning = time;
//                        }
//                        else
//                        {
//                            SendInfoToWeb.WriteRunningTimeToServer(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _timeRunning.TotalDays);
//                        }

//                    }
//                    else
//                    {
//                        SendInfoToWeb.WriteRunningTimeToServer(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _timeRunning.TotalDays);
//                    }
//                }
//            }
//        }

//        #endregion
//    }
//}
