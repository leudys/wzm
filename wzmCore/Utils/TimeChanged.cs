﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using wzmCore.Interfaces;

namespace wzmCore.Utils
{
    public class TimeChanged
    {
        public TimeSpan CheckInterval { get; private set; } = TimeSpan.FromMinutes(1);
        public TimeSpan MaxAllowedDeviation { get; private set; } = TimeSpan.FromSeconds(1);

        private readonly ILogger<object> _logger;
        private CancellationToken _cancelationToken;
        private DateTimeOffset _beforeTimer;
        private readonly IWzmCore _wzmCore;

        public TimeChanged(IWzmCore wzmCore,ILogger<object> logger ,CancellationToken cancellationToken)
        {
            _cancelationToken = cancellationToken;
            _logger = logger;
            _wzmCore = wzmCore;
            _beforeTimer = DateTimeOffset.Now;
        }
              

        public void CheckTimeUpdate()
        {

            if (DateTimeOffset.Now.Day > _beforeTimer.Day)
            {
                _beforeTimer = DateTimeOffset.Now;
            }
                        
            DateTimeOffset afterTimer = DateTimeOffset.Now;

            if (DateTimeOffset.Now.Day < _beforeTimer.Day)
            {
                if (_wzmCore.LicenseManager.Validated)
                {
                    //_logger.LogCritical("Cambio de fecha detectada. Todos los servicios se han parado.");
                    _wzmCore.LicenseManager.InvalidateLicense(Enum.InvalidateReason.TimeChanged);
                    //_logger.LogCritical("La licencia se ha invalidado.");
                }
               
            }

            //if ((afterTimer.UtcDateTime - beforeTimer.UtcDateTime).Duration() > MaxAllowedDeviation + CheckInterval)
            //{
            //    i++;
            //}


        }

    }
}