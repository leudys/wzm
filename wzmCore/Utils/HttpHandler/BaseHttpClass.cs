﻿using System.IO;
using System.Net;
using System.Text;
using wzmCore.ViewModels;
using wzmCore.WzmLicense.Crypt;

namespace wzmCore.Utils
{

    internal class BaseHttpClass
    {
        /// <summary>
        /// Crea una petición Web
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="requestMethod"></param>
        /// <param name="cookieContainer"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        internal virtual HttpWebRequest CreateWebRequest(string uri, string requestMethod, CookieContainer cookieContainer, int timeout)
        {
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.KeepAlive = false;
            webrequest.CookieContainer = cookieContainer;
            webrequest.Method = requestMethod;
            webrequest.Timeout = timeout;
            webrequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            webrequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            webrequest.AllowAutoRedirect = false;
            return webrequest;
        }

        /// <summary>
        /// Crea una petición Web
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        internal virtual HttpWebRequest CreateWebRequest(string uri)
        {
            CookieContainer cookieContainer = new CookieContainer();
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.KeepAlive = false;
            webrequest.Method = "Get";
            webrequest.CookieContainer = cookieContainer;
            webrequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            webrequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            webrequest.AllowAutoRedirect = false;
            return webrequest;
        }


        /// <summary>
        /// Envía información hacia la Web
        /// </summary>
        /// <param name="reUri">URL de la accion</param>
        /// <param name="cookieContainer">Cookies</param>
        /// <param name="method">Método que se realizará</param>
        /// <param name="expectedParam">Parámetro que espera el servidor</param>
        /// <param name="valParam">Valor del Parámetro</param>
        /// <returns></returns>
        internal virtual HttpWebResponse Send(string reUri, CookieContainer cookieContainer, string method, string expectedParam, string valParam)
        {
            HttpWebRequest webrequest = CreateWebRequest(reUri, method, cookieContainer, 20000);
            BuildReqStream(ref webrequest, $"{expectedParam}={valParam}");
            var webresponse = (HttpWebResponse)webrequest.GetResponse();
            return webresponse;
        }

        /// <summary>
        /// Envia informacion hacia la Web con el contenido encriptado.
        /// </summary>
        /// <param name="reUri">URL de la acción</param>
        /// <param name="cookieContainer">Cookies</param>
        /// <param name="method">Método ej: Post,Get</param>
        /// <param name="expectedParam">Parámetro que espera el servidor</param>
        /// <param name="valParam">Valor del Parámetro</param>
        /// <returns></returns>
        internal virtual HttpWebResponse SendEncrypted(string reUri, CookieContainer cookieContainer, string method, string expectedParam, string valParam)
        {
            var encryptedParams = ElectronicCodeBook.Encrypt(valParam, true, SecretKeys.StringKey);
            HttpWebRequest webrequest = CreateWebRequest(reUri, method, cookieContainer, 20000);
            BuildReqStream(ref webrequest, $"{expectedParam}={encryptedParams}");
            var webresponse = (HttpWebResponse)webrequest.GetResponse();
            return webresponse;
        }

        private void BuildReqStream(ref HttpWebRequest webrequest, string loginParams)
        {

            try
            {
                byte[] bytes = Encoding.ASCII.GetBytes(loginParams);
                webrequest.ContentLength = bytes.Length;
                Stream oStreamOut = webrequest.GetRequestStream();
                oStreamOut.Write(bytes, 0, bytes.Length);
                oStreamOut.Close();

            }
            catch (WebException)
            {

            }

        }
    }

}
