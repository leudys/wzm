﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using wzmCore.ViewModels;

namespace wzmCore.Utils
{
    internal class HttpRequestResponse : BaseHttpClass
    {
        public bool WebOnline { get; private set; }

        public bool LoggedIn { get; private set; }

        public CookieContainer CookieContainer { get; private set; }

        public HttpRequestResponse()
        {
            CookieContainer = new CookieContainer();
        }

        /// <summary>
        /// Comprueba que la web esté activa
        /// </summary>
        /// <returns></returns>
        internal bool IsWebOnline()
        {
            try
            {
                HttpWebRequest webrequest = CreateWebRequest(HttpHelpers.Uri);
                var webresponse = (HttpWebResponse)webrequest.GetResponse();

                if (webresponse.Cookies.Count != 0)
                {
                    foreach (Cookie item in webresponse.Cookies)
                    {
                        CookieContainer.Add(item);
                    }
                }

                if ((webresponse.StatusCode == HttpStatusCode.Found) || (webresponse.StatusCode == HttpStatusCode.Redirect) || (webresponse.StatusCode == HttpStatusCode.Moved) || (webresponse.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    return WebOnline = false;
                }

                using (StreamReader streamReader = new StreamReader(webresponse.GetResponseStream()))
                {

                    string portal = streamReader.ReadToEnd();
                    webresponse.Dispose();
                    return WebOnline = portal.Contains("WZM");
                }
            }
            catch (Exception)
            {

                return WebOnline = false;
            }


        }

        /// <summary>
        /// Reinicia el tiempo que lleva corriendo la APP.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="now"></param>
        internal void ResetRunningTime(string v, DateTime now)
        {
            try
            {
                var response = SendEncrypted(HttpHelpers.ResetRunningTime, CookieContainer, "Post", "resetToken", $"{v};{now}");

                if ((response.StatusCode == HttpStatusCode.Found) || (response.StatusCode == HttpStatusCode.Redirect) || (response.StatusCode == HttpStatusCode.Moved) || (response.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    LoggedIn = false;
                    return;
                }

                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var runningtTime = JsonConvert.DeserializeObject<object>(streamReader.ReadToEnd());
                    response.Dispose();

                }
            }
            catch (WebException)
            {
            }

        }


        /// <summary>
        /// Se loguea a la WEB
        /// </summary>
        /// <returns></returns>
        internal bool LoginWeb()
        {
            try
            {
                var login = SendEncrypted(HttpHelpers.SecureLogin, CookieContainer, "Post", "encryptedCredentials", $"{HttpHelpers.UserName};{HttpHelpers.Password};{DateTime.Now.Second}");

                if ((login.StatusCode == HttpStatusCode.Found) || (login.StatusCode == HttpStatusCode.Redirect) || (login.StatusCode == HttpStatusCode.Moved) || (login.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    // Get redirected uri
                    string redirectedUri = login.Headers["Location"];
                    if (redirectedUri != string.Empty)
                    {
                        return LoggedIn = false;
                    }
                }
                else
                {
                    // ReSharper disable once AssignNullToNotNullAttribute
                    using (var streamReader = new StreamReader(login.GetResponseStream()))
                    {
                        var responseString = streamReader.ReadToEnd();
                        var response = JsonConvert.DeserializeObject<string>(responseString);
                        if (!response.Equals("ok"))
                        {
                            return LoggedIn = false;
                        }

                    }
                    login.Dispose();
                }
                return LoggedIn = true;

            }
            catch (Exception)
            {
                return LoggedIn = false;
            }


        }

        /// <summary>
        /// Comprueba que exista un tiempo de ejecucion en la Web
        /// </summary>
        /// <returns></returns>
        internal RunningTimeWeb ExistRunningTime(string licenseId)
        {
            try
            {

                var response = Send(HttpHelpers.RunningTimeUri, CookieContainer, "Post", "licenseId", licenseId);

                if ((response.StatusCode == HttpStatusCode.Found) ||
                (response.StatusCode == HttpStatusCode.Redirect) ||
                (response.StatusCode == HttpStatusCode.Moved) ||
                (response.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    LoggedIn = false;
                    return new RunningTimeWeb();
                }

                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var runningtTime = JsonConvert.DeserializeObject<RunningTimeWeb>(streamReader.ReadToEnd());
                    response.Dispose();
                    return runningtTime;
                }
            }
            catch (Exception)
            {

                return new RunningTimeWeb();
            }

        }

        /// <summary>
        /// Envía a la Web el tiempo que lleva la app corriendo.
        /// </summary>
        /// <param name="licenseId"></param>
        /// <param name="email"></param>
        /// <param name="totalDays"></param>
        internal void SaveRunningTime(string licenseId, string email, double totalDays)
        {
            try
            {
                var response = SendEncrypted(HttpHelpers.SaveRunningTimeUri, CookieContainer, "Post", "runningTime", $"{licenseId};{email};{totalDays};{DateTime.Now.Second}");

                if ((response.StatusCode == HttpStatusCode.Found) ||
                    (response.StatusCode == HttpStatusCode.Redirect) ||
                    (response.StatusCode == HttpStatusCode.Moved) ||
                    (response.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    LoggedIn = false;
                    return;
                }

                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var runningtTime = JsonConvert.DeserializeObject<object>(streamReader.ReadToEnd());
                    response.Dispose();

                }
            }
            catch (Exception)
            {

            }

        }


        /// <summary>
        /// Compureba que exista en la web el ultimo dia que se utilizo la app
        /// </summary>
        /// <param name="licenseId"></param>
        /// <returns></returns>
        internal LastDayWeb ExistLastDay(string licenseId)
        {
            try
            {

                var response = Send(HttpHelpers.LastDay, CookieContainer, "Post", "licenseId", licenseId);

                if ((response.StatusCode == HttpStatusCode.Found) ||
                (response.StatusCode == HttpStatusCode.Redirect) ||
                (response.StatusCode == HttpStatusCode.Moved) ||
                (response.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    LoggedIn = false;
                    return new LastDayWeb();
                }

                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var runningtTime = JsonConvert.DeserializeObject<LastDayWeb>(streamReader.ReadToEnd());
                    response.Dispose();
                    return runningtTime;
                }
            }
            catch (Exception)
            {

                return new LastDayWeb();
            }

        }

        /// <summary>
        /// Guarda el último día que se utilizó la APP.
        /// </summary>
        /// <param name="licenseId"></param>
        /// <param name="email"></param>
        /// <param name="lastDay"></param>
        internal void SaveLastDay(string licenseId, string email, DateTime lastDay)
        {
            try
            {
                var sd = lastDay.GetDateTimeFormats();
                var response = SendEncrypted(HttpHelpers.SaveLastDay, CookieContainer, "Post", "lastDay", $"{licenseId};{email};{lastDay.ToString("MM/dd/yyyy")};{DateTime.Now}");
                if ((response.StatusCode == HttpStatusCode.Found) ||
                    (response.StatusCode == HttpStatusCode.Redirect) ||
                    (response.StatusCode == HttpStatusCode.Moved) ||
                    (response.StatusCode == HttpStatusCode.MovedPermanently))
                {
                    LoggedIn = false;
                    return;
                }

                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var runningtTime = JsonConvert.DeserializeObject<object>(streamReader.ReadToEnd());
                    response.Dispose();

                }
            }
            catch (Exception)
            {

            }

        }

    }
}
