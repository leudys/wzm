﻿namespace wzmCore.Utils
{
    public static class HttpHelpers
    {
        public static string Uri = "http://www.wzm.somee.com";

        public static string localhostUri = "http://localhost:13378";

        public static string UserName = "leudys.nodarse@gmail.com";

        public static string Password = "Leudys.901224";

        public static string SecureLogin = $"{Uri}/Account/SecureLogin";

        public static string RunningTimeUri = $"{Uri}/RunningTime/Get";

        public static string ResetRunningTime = $"{Uri}/RunningTime/Reset";

        public static string SaveRunningTimeUri = $"{Uri}/RunningTime/Update";

        public static string LastDay = $"{Uri}/LastDay/Get";

        public static string SaveLastDay = $"{Uri}/LastDay/Update";
    }
}
