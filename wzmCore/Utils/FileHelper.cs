﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using wzmData.Models;

namespace wzmCore.Utils
{
    public class FileHelper
    {
        public static async Task<string> ProcessFromFile(IFormFile formFile)
        {
            if (formFile == null)
            {
                return $"Por favor seleccione una licencia.";
            }

            var fieldDisplayName = string.Empty;

            MemberInfo property = typeof(FileUpload).GetProperty(formFile.Name.Substring(formFile.Name.IndexOf(".") + 1));

            if (property != null)
            {
                var displayAttribute = property.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;
                if (displayAttribute != null)
                {
                    fieldDisplayName = $"{displayAttribute.Name} ";
                }
            }
            // Use Path.GetFileName to obtain the file name, which will
            // strip any path information passed as part of the
            // FileName property. HtmlEncode the result in case it must
            // be returned in an error message.
            var fileName = WebUtility.HtmlEncode(
            Path.GetFileName(formFile.FileName));
            if (formFile.ContentType.ToLower() != "application/octet-stream")
            {

                return $"El {fieldDisplayName}archivo ({fileName}) ha de ser un archivo de texto.";
            }
            // Check the file length and don't bother attempting to
            // read it if the file contains no content. This check
            // doesn't catch files that only have a BOM as their
            // content, so a content length check is made later after
            // reading the file's content to catch a file that only
            // contains a BOM.
            if (formFile.Length == 0)
            {
                return $"El {fieldDisplayName}archivo ({fileName}) se encuentra vacío.";
            }
            else if (formFile.Length > 1048576)
            {

                return $"El {fieldDisplayName}archivo ({fileName}) excede 1 MB.";
            }
            else
            {
                try
                {
                    string fileContents;
                    // The StreamReader is created to read files that are UTF-8 encoded.
                    // If uploads require some other encoding, provide the encoding in the// If uploads require some other encoding, provide the encoding in the
                    // using statement. To change to 32-bit encoding, change
                    // new UTF8Encoding(...) to new UTF32Encoding().
                    using (var reader = new StreamReader(formFile.OpenReadStream(), new UTF8Encoding(encoderShouldEmitUTF8Identifier: false)))
                    {
                        fileContents = await reader.ReadToEndAsync();
                        // Check the content length in case the file's only
                        // content was a BOM and the content is actually
                        // empty after removing the BOM.
                        if (fileContents.Length > 0)
                        {
                            return string.Empty;
                        }
                        else
                        {

                            return $"The {fieldDisplayName}file ({fileName}) is empty.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    return
                    $"The {fieldDisplayName}file ({fileName}) upload failed. " +
                    $"Please contact the Help Desk for support. Error: {ex.Message}";
                    // Log the exception
                }
            }

        }

        private static string InitialDate = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\application.xml";

        /// <summary>
        /// Decrypts an encrypted file with the FileEncrypt method through its path and the plain password.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="password"></param>
        public static string FileDecrypt(string inputFile, string password)
        {
            string decrypted = string.Empty;

            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[32];

            // Crypted File
            FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
            fsCrypt.Read(salt, 0, salt.Length);


            var AES = Aes.Create();

            //AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CBC;

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {
                    decrypted += Encoding.Default.GetString(buffer, 0, read);

                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {

                fsCrypt.Close();


            }
            return decrypted;
        }
                
        /// <summary>
        /// Decrypts an encrypted file with the FileEncrypt method through its path and the plain password.
        /// </summary>
        /// <param name="inputFile">The memory stremam</param>
        /// <param name="outputFile">Decrypted File</param>
        /// <param name="password"></param>
        public void FileDecrypt(MemoryStream inputFile, string outputFile, string password)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[32];

            // Crypted File
            //FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
            inputFile.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CFB;

            CryptoStream cs = new CryptoStream(inputFile, AES.CreateDecryptor(), CryptoStreamMode.Read);

            FileStream fsOut = new FileStream(outputFile, FileMode.Create);

            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {

                    fsOut.Write(buffer, 0, read);
                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {
                fsOut.Close();



                File.Delete(fsOut.ToString());
            }
        }
    }
}
