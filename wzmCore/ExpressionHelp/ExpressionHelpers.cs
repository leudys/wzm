﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace wzmCore.ExpressionHelp
{
    /// <summary>
    /// A helper for an expression
    /// </summary>
    public static class ExpressionHelpers
    {
        /// <summary>
        ///  Compiles an expression and gets the functions return value
        /// </summary>
        /// <typeparam name="T"> The type of return value</typeparam>
        /// <param name="lambda"> The expression to compile</param>
        /// <returns></returns>
        public static T GetPropertyValue<T>(this Expression<Func<T>> lambda)
        {
            return lambda.Compile().Invoke();
        }

        /// <summary>
        /// Set the underlying properties value to the given value from an expression that contains the property
        /// </summary>
        /// <typeparam name="T">The type of value to set</typeparam>
        /// <param name="lambda">The expression</param>
        public static void SetPropertyValue<T>(this Expression<Func<T>> lambda, T value)
        {
            //Convert a lambda ()=> somre.Proerty
            var expression = (lambda as LambdaExpression).Body as MemberExpression;
            //Get the property info so i can set it
            var propertyInfo = (PropertyInfo)expression.Member;
            var target = Expression.Lambda(expression.Expression).Compile().DynamicInvoke();

            // Set the value
            propertyInfo.SetValue(target, value);
        }
    }
}
