﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using wzmData.Models;

namespace wzmCore.Extensions
{
    public static class WlanExtensions
    {
        /// <summary>
        /// Obtiene los portales con internet de forma ordenada
        /// </summary>
        /// <param name="wlan"></param>
        /// <returns></returns>
        public static List<PortalesInternet> GetOrderedWlan(this List<PortalesInternet> wlan)
        {
            List<PortalesInternet> portales = new List<PortalesInternet>();
            if (wlan.Count > 0)
            {
                List<int> wlans = new List<int>();
               
                string wlanPrefix = string.Empty;
                for (int i = 0; i < wlan[0].Portal.Length; i++)
                {
                    if (char.IsLetter(wlan[0].Portal[i]))
                    {
                        wlanPrefix = wlanPrefix + wlan[0].Portal[i];
                    }
                }


                foreach (var item in wlan)
                {
                    string numWlan = string.Empty;
                    for (int i = 0; i < item.Portal.Length; i++)
                    {
                        if (char.IsDigit(item.Portal[i]))
                        {
                            numWlan = numWlan + item.Portal[i];
                        }

                    }
                    if (numWlan != string.Empty)
                    {
                        wlans.Add(int.Parse(numWlan));
                    }
                }

                wlans.Sort();

                foreach (var item in wlans)
                {
                    portales.Add(new PortalesInternet()
                    {
                        Portal = $"{wlanPrefix}{item}",
                        Route = wlan.FirstOrDefault(c => c.Portal.Equals($"{wlanPrefix}{item}")).Route,

                    });
                }
            }
            

            return portales;
        }

        public static string[] GetNumAndName(this string wlan)
        {
            string[] portal = new string[2];
            string nameWlan = string.Empty;
            string numWlan = string.Empty;
            for (int i = 0; i < wlan.Length; i++)
            {
                if (char.IsDigit(wlan[i]))
                {
                    numWlan = numWlan + wlan[i];

                }
                else
                {
                    nameWlan = nameWlan + wlan[i];
                }
            }

            portal[0] = nameWlan;
            portal[1] = numWlan;

            return portal;
        }

        public static int GetNum(this string wlan)
        {
            
           
            string numWlan = string.Empty;
            for (int i = 0; i < wlan.Length; i++)
            {
                if (char.IsDigit(wlan[i]))
                {
                    numWlan = numWlan + wlan[i];

                }
               
            }
                       

            return int.Parse(numWlan);
        }
    }
}
