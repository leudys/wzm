﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;
using wzmData.Models;

namespace wzmCore.Extensions
{
   public static class Disposed
    {
        public static bool IsDisposed(ApplicationDbContext applicationDbContext)
        {
            var result = true;

            var typeDbContext = typeof(DbContext);
            var typeInternalContext = typeDbContext.Assembly.GetType("System.Data.Entity.Internal.InternalContext");

            var fi_InternalContext = typeDbContext.GetField("_internalContext", BindingFlags.NonPublic | BindingFlags.Instance);
            var pi_IsDisposed = typeInternalContext.GetProperty("IsDisposed");

            var ic = fi_InternalContext.GetValue(applicationDbContext);

            if (ic != null)
            {
                result = (bool)pi_IsDisposed.GetValue(ic);
            }

            return result;
        } 
    }
}
