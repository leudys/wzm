﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Collections.Generic;
using System.Linq;
using wzmData.Enum;
using wzmData.Models;

namespace wzmCore.Extensions
{
    public static class Clients
    {
        public static List<ApplicationUsers> GetApplicationClients(this ApplicationDbContext applicationDbContext)
        {
            List<ApplicationUsers> clients = new List<ApplicationUsers>();
            foreach (var item in applicationDbContext.Users)
            {
                if (item is ApplicationUsers)
                {
                    var client = item as ApplicationUsers;

                    if (client.ClasificadorPagoId != null)
                    {
                        client.ClasificadorPago = applicationDbContext.ClasificadorPagos.Find(client.ClasificadorPagoId);
                    }
                    if (client.DispositivoId != null)
                    {
                        client.Dispositivo = applicationDbContext.Dispositivos.Find(client.DispositivoId);
                    }
                    if (applicationDbContext.UsersDevices.Any(c => c.ApplicationUserId.Equals(item.Id)))
                    {
                        foreach (var device in applicationDbContext.UsersDevices.Where(c => c.ApplicationUserId.Equals(item.Id)))
                        {
                            client.UserDevices.Add(device);
                        }
                        
                    }
                                        
                    clients.Add(client);


                }
            }
            return clients;
        }

        public static List<UserDevice> GetGamer(this ApplicationDbContext applicationDbContext)
        {
            var usuarios = applicationDbContext.ClientServices.Include(c => c.Service).Include(c => c.Usuarios).Where(c => c.Service.UserServiceType == UserServiceType.Games);

            List<UserDevice> dispositivos = new List<UserDevice>();

            foreach (var item in usuarios)
            {
                dispositivos.AddRange(applicationDbContext.UsersDevices.Where(c => c.ApplicationUserId.Equals(item.UserId)));
            }
            return dispositivos;
        }
    }
}
