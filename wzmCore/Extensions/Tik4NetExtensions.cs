﻿using System;
using System.Collections.Generic;
using System.Text;
using tik4net;

namespace wzmCore.Extensions
{
    public static class Tik4NetExtensions
    {
        public static TikConnectionType GetApiVersion(this string routerOsVersion)
        {
            try
            {
                string[] version = routerOsVersion.Split('.');

                if (int.Parse(version[0]) >= 6 && int.Parse(version[1]) >= 43)
                {
                    return TikConnectionType.Api_v2;
                }
                else
                {
                    return TikConnectionType.Api;
                }
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
           
        }
    }
}
