﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using System.Management;

namespace wzmCore.Extensions
{
    public static class List
    {
        public static string ToString(this List<object> list)
        {
            string l = string.Empty;
            int count = 0;
            foreach (var item in list)
            {
                if (list.Count() - count != 0)
                {
                    l = $"{l},{item}";
                }
            }

            return l;
        }

        internal static List<PropertyData> ToList(this PropertyDataCollection dataCollection)
        {
            List<PropertyData> list = new List<PropertyData>();

            foreach (var item in dataCollection)
            {
                list.Add(item);
            }

            return list;
        }

        internal static string GetNetwork(this string ip)
        {
            string[] value = ip.Split('.');
            string value1 = value[0] + ".";

            for (int i = 1; i <= 2; i++)
            {
                if (i != 2)
                {
                    value1 = value1 + value[i] + ".";
                }
                else
                {
                    value1 = value1 + value[i];
                }
            }
            return value1;
        }

        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}
