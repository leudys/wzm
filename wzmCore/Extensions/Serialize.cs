﻿using Standard.Licensing;
using System;
using wzmCore.ViewModels;
using wzmCore.WzmLicense;
using wzmCore.WzmLicense.Crypt;

namespace wzmCore.Extensions
{
    public static class Serialize
    {
        public static ActivationCode SerializeActivationCode(this string activationCode)
        {
            string[] activationString = activationCode.Split(';');

            return new ActivationCode() { AppVersion = "0", ClientName = activationString[0], Email = activationString[1], LicenseId = activationString[3], LicenseType = (Enum.LicenseType)int.Parse(activationString[5]), StartDate = DateTime.Parse(activationString[4]) };
        }

        public static LicenseProperties LicenseProperties(this string license)
        {
            string[] licenseString = license.Split(';');

            return new LicenseProperties()
            {
                BiosId = licenseString[1],
                CpuId = licenseString[0],
                MotherBoardId = licenseString[2],
                WindowsInstalationDate = DateTime.Parse(licenseString[3])
            };

        }

        public static License GetLicense(this string encryptedLicense)
        {
            return License.Load(ElectronicCodeBook.Decrypt(encryptedLicense, true, SecretKeys.StringKey));
        }
    }
}
