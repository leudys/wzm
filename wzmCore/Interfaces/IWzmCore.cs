﻿using System;
using System.Collections.Generic;
using wzmCore.Administration;
using wzmCore.WzmLicense;
using wzmData.Models;

namespace wzmCore.Interfaces
{
    public interface IWzmCore
    {
        MikrotikManager MikrotikManager { get; }

        UbiquitiManager UbiquitiManager { get; }

        UserManagement UserManagement { get; }

        Random Random { get; }

        LicenseManager LicenseManager { get; }

        List<HostsServices> Users { get; set; }

        List<PortalesInternet> Internet { get; set; }
               

    }
}
