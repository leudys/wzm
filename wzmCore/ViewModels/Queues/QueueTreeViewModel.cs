﻿namespace wzmCore.ViewModels.Queues
{
    public class QueueTreeViewModel
    {

        public string Name { get; set; }

        public bool Active { get; set; }

        public long Priority { get; set; }

        public string LimitAt { get; set; }

        public string MaxLimit { get; set; }

    }
}
