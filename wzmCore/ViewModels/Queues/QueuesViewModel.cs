﻿namespace wzmCore.ViewModels.Queues
{
    public class QueuesViewModel
    {
        public QueuesViewModel()
        {
            YouTube = new QueueTreeViewModel();
            Facebook = new QueueTreeViewModel();
            WhatsApp = new QueueTreeViewModel();
            Dns = new QueueTreeViewModel();
            Administracion = new QueueTreeViewModel();
            ICMP = new QueueTreeViewModel();
            Otros = new QueueTreeViewModel();

        }

        public string Interfaz { get; set; }

        public QueueTreeViewModel YouTube { get; set; }

        public QueueTreeViewModel Facebook { get; set; }

        public QueueTreeViewModel ICMP { get; set; }

        public QueueTreeViewModel Administracion { get; set; }

        public QueueTreeViewModel Otros { get; set; }

        public QueueTreeViewModel WhatsApp { get; set; }

        public QueueTreeViewModel Dns { get; set; }



        public bool Configured { get; set; }
    }
}
