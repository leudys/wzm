﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wzmCore.ViewModels
{
    public class ActiveDevicesViewModel
    {
        public bool Active { get; set; }

        public string Mac { get; set; }

        public string Name { get; set; }

        public string Ip { get; set; }

        public string Ubicacion { get; set; }

        public bool Alarm { get; set; }
    }
}
