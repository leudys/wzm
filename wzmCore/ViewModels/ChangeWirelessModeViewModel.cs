﻿using wzmData.Enum;

namespace wzmCore.ViewModels
{
    public class ChangeWirelessModeViewModel
    {
        public int DispositivoId { get; set; }

        public WirelessMode WirelessMode { get; set; }

        public int? OtherDevice { get; set; }
    }
}
