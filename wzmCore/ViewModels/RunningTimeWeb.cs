﻿using System;

namespace wzmCore.ViewModels
{
    public class RunningTimeWeb
    {
        public bool Exist { get; set; }

        public double Value { get; set; }
    }
}
