﻿using System;

namespace wzmCore.ViewModels
{
    public class LastDayWeb
    {
        public bool Exist { get; set; }

        public DateTime Value { get; set; }
    }
}
