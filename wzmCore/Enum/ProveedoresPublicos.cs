﻿using System.ComponentModel.DataAnnotations;

namespace wzmCore.Enum
{
    public enum ProveedoresPublicos
    {
        [Display(Name = "Joben Club")]
        Joven_Club,
        [Display(Name = "WIFI_ETECSA")]
        Wifi_Etecsa
    }
}
