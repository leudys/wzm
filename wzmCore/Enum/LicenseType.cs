﻿using System.ComponentModel.DataAnnotations;

namespace wzmCore.Enum
{
    /// <summary>
    /// Defines the type of a <see cref="License"/>
    /// </summary>
    public enum LicenseType
    {
        /// <summary>
        /// For trial or demo use
        /// </summary>
        [Display(Name = "Prueba")]
        Trial = 1,

        /// <summary>
        /// Standard license
        /// </summary>
        [Display(Name = "Estándar")]
        Standard = 2,


    }
}
