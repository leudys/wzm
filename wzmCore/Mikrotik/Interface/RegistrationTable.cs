﻿using System.Collections.Generic;
using tik4net.Objects;
using tik4net.Objects.Interface.Wireless;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using tik4net;

namespace wzmCore.Mikrotik
{
    public class RegistrationTable : Wireless
    {

        /// <summary>
        /// Obtiene la Tabla de Registro de la Wireless del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<WirelessRegistrationModel> GetRergistrationTable(MikrotikCredentials credential, List<WirelessRegistrationModel> wirelessRegistration)
        {
            Init(credential);
            List<WirelessRegistrationTable> list = (List<WirelessRegistrationTable>)Connection.LoadAll<WirelessRegistrationTable>();

            foreach (var item in list)
            {
                wirelessRegistration.Add(new WirelessRegistrationModel()
                {
                    Ccq = item.TxCcq,
                    Id = int.Parse(item.Interface.Replace("wlan", "")),
                    Interface = item.Interface,
                    MacAddress = item.MacAddress,
                    RxRate = item.RxRate,
                    SignalStrength = item.SignalStrength.Split('@')[0],
                    TxRate = item.TxRate
                });
            }
            Dispose();
            return wirelessRegistration;
        }

        public List<WirelessRegistrationModel> GetRergistrationTable(ITikConnection connection)
        {

            List<WirelessRegistrationTable> list = (List<WirelessRegistrationTable>)connection.LoadAll<WirelessRegistrationTable>();
            List<WirelessRegistrationModel> registration = new List<WirelessRegistrationModel>();
            foreach (var item in list)
            {
                registration.Add(new WirelessRegistrationModel()
                {
                    Ccq = item.TxCcq,
                    Id = int.Parse(item.Interface.Replace("wlan", "")),
                    Interface = item.Interface,
                    MacAddress = item.MacAddress,
                    RxRate = item.RxRate,
                    SignalStrength = item.SignalStrength.Split('@')[0],
                    TxRate = item.TxRate
                });
            }

            return registration;
        }
    }
}
