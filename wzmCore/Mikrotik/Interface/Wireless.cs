﻿using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.Data.Mikrotik;

namespace wzmCore.Mikrotik
{
    public class Wireless : MikrotikBaseConnection
    {

        public RegistrationTable RegistrationTable => new RegistrationTable();

        /// <summary>
        /// Obtiene una lista con todas las WLAN que existen en el Mikrotik.
        /// </summary>
        /// <param name="mikrotikCredentials"></param>
        /// <returns></returns>
        public List<InterfaceWireless> GetAllWireless(MikrotikCredentials mikrotikCredentials)
        {
            Init(mikrotikCredentials);
            var Wireless = (List<InterfaceWireless>)Connection.LoadAll<InterfaceWireless>();
            DisposeMkConnection();
            return Wireless;
        }

        /// <summary>
        /// Obtiene una lista con todas las WLAN que existen en el Mikrotik.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public List<InterfaceWireless> GetAllWireless(ITikConnection connection)
        {

            return (List<InterfaceWireless>)connection.LoadAll<InterfaceWireless>();
        }

        /// <summary>
        /// Obtiene una lista con todas las WLAN que se encuentren en Estación Pseodobridge y que no estén deshabilitadas y que su tipo sea virtual.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public List<InterfaceWireless> GetOnlyStationPseudobridge(ITikConnection connection)
        {

            return connection.LoadAll<InterfaceWireless>().Where(c => c.Mode == InterfaceWireless.WirelessMode.StationPseudobridge && c.InterfaceType.Equals("virtual") && !c.Disabled).ToList();
        }

        /// <summary>
        /// Agrega una Interface Wireless al Mikrotik.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="wlan"></param>
        public void AddWireless(ITikConnection connection, int wlan)
        {
            ITikCommand addWirelesss = connection.CreateCommand("/interface/wireless/add",
                connection.CreateParameter("name", "wlan" + wlan),
                connection.CreateParameter("master-interface", "wlan1"),
                connection.CreateParameter("mode", "station-pseudobridge"),
                connection.CreateParameter("ssid", "WIFI_ETECSA"),
                connection.CreateParameter("band", "5ghz-a/n/ac"),
                connection.CreateParameter("wireless-protocol", "802.11"),
                connection.CreateParameter("wds-cost-range", "0"),
                connection.CreateParameter("wds-default-cost", "0"),
                connection.CreateParameter("multicast-buffering", "disabled"),
                connection.CreateParameter("keepalive-frames", "disabled"),
                connection.CreateParameter("disabled", "no"),
                connection.CreateParameter("wps-mode", "disabled"));
            addWirelesss.ExecuteScalar();
        }

        /// <summary>
        /// Obtiene la WLAN donde se encuentra un IP determinado.
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string GetWlanGivenIp(string ip, MikrotikCredentials credential)
        {
            string wlan = string.Empty;
            try
            {

                using (ITikConnection mkConnection = ConnectionFactory.CreateConnection(TikConnectionType.Api))
                {
                    mkConnection.Open(credential.Dispositivo.Ip, credential.User, credential.Password);
                    var addressLists = mkConnection.LoadList<FirewallAddressList>();
                    var nat = mkConnection.LoadList<FirewallNat>();

                    if (addressLists.Any(c => c.Address == ip))
                    {
                        wlan = nat.First(c => c.SrcAddressList == addressLists.First(d => d.Address == ip).List).OutInterface;

                    }
                    else
                    {
                        foreach (var item in addressLists)
                        {
                            if (item.Address.Contains("-"))
                            {
                                string[] ipArray = item.Address.Split('-');
                                string[] firstIp = ipArray[0].Split('.');
                                string[] LastIp = ipArray[1].Split('.');

                                string[] ipCompare = ip.Split('.');
                                int numberCompare = int.Parse(ipCompare[ipCompare.Length - 1]);

                                if (numberCompare >= int.Parse(firstIp[firstIp.Length - 1]) && numberCompare <= int.Parse(LastIp[LastIp.Length - 1]))
                                {
                                    wlan = nat.First(c => c.SrcAddressList == item.List).OutInterface;

                                    break;
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }

            return wlan;
        }

        /// <summary>
        /// Obtiene la WLAN donde se encuentra un IP determinado.
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string GetPortalGivenIp(string ip, MikrotikCredentials credential)
        {
            string portal = string.Empty;
            try
            {

                using (ITikConnection mkConnection = ConnectionFactory.CreateConnection(TikConnectionType.Api))
                {
                    mkConnection.Open(credential.Dispositivo.Ip, credential.User, credential.Password);
                    var addressLists = mkConnection.LoadList<FirewallAddressList>();
                    var nat = mkConnection.LoadList<FirewallNat>();

                    if (addressLists.Any(c => c.Address.Equals(ip)))
                    {

                        portal = addressLists.FirstOrDefault(c => c.Address.Equals(ip)).List;
                    }
                    else
                    {
                        foreach (var item in addressLists)
                        {
                            if (item.Address.Contains("-"))
                            {
                                string[] ipArray = item.Address.Split('-');
                                string[] firstIp = ipArray[0].Split('.');
                                string[] LastIp = ipArray[1].Split('.');

                                string[] ipCompare = ip.Split('.');
                                int numberCompare = int.Parse(ipCompare[ipCompare.Length - 1]);

                                if (numberCompare >= int.Parse(firstIp[firstIp.Length - 1]) && numberCompare <= int.Parse(LastIp[LastIp.Length - 1]))
                                {

                                    portal = nat.First(c => c.SrcAddressList.Equals(item.List)).SrcAddressList;
                                    break;
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                return portal;
            }

            return portal;
        }

        /// <summary>
        /// Obtiene la WLAN donde se encuentra un IP determinado.
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string GetPortalGivenIp(string ip, ITikConnection tikConnection)
        {
            string portal = string.Empty;
            try
            {
                var addressLists = tikConnection.LoadList<FirewallAddressList>();
                var nat = tikConnection.LoadList<FirewallNat>();

                if (addressLists.Any(c => c.Address == ip))
                {

                    portal = nat.First(c => c.SrcAddressList == addressLists.First(d => d.Address == ip).List).SrcAddressList;
                }
                else
                {
                    foreach (var item in addressLists)
                    {
                        if (item.Address.Contains("-"))
                        {
                            string[] ipArray = item.Address.Split('-');
                            string[] firstIp = ipArray[0].Split('.');
                            string[] LastIp = ipArray[1].Split('.');

                            string[] ipCompare = ip.Split('.');
                            int numberCompare = int.Parse(ipCompare[ipCompare.Length - 1]);

                            if (numberCompare >= int.Parse(firstIp[firstIp.Length - 1]) && numberCompare <= int.Parse(LastIp[LastIp.Length - 1]))
                            {

                                portal = nat.First(c => c.SrcAddressList == item.List).SrcAddressList;
                                break;
                            }

                        }
                    }
                }

            }
            catch (Exception)
            {

                return portal;
            }
            return portal;

        }

        /// <summary>
        /// Obtiene todas las Interfaces Wireless del Mikrotik.
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<WirelessInterfaceModel> GetAllWirelessViewModel(MikrotikCredentials credential)
        {

            //Obtengo todas las wireless
            var wireless = GetAllWireless(credential);

            List<WirelessInterfaceModel> list = new List<WirelessInterfaceModel>();
            int i = 0;
            foreach (var item in wireless)
            {
                list.Add(new WirelessInterfaceModel()
                {
                    Id = i++,
                    Name = item.Name,
                    Disabled = item.Disabled,
                    FrequencyMode = item.FrequencyMode,
                    MacAddress = item.MacAddress,
                    MasterInterface = item.MasterInterface,
                    Mode = item.Mode,
                    Ssid = item.Ssid
                });
            }

            return list;
        }

        /// <summary>
        /// Obtiene una Interfaz Wireless dado su nombre
        /// </summary>
        /// <param name="name">Nombre de la Wireless</param>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <returns></returns>
        public WirelessInterfaceModel GetWirelessViewModel(string name, MikrotikCredentials credential)
        {
            Init(credential);
            var w = Connection.LoadList<InterfaceWireless>(Connection.CreateParameter("name", name)).SingleOrDefault();
            try
            {
                if (w != null)
                {
                    return new WirelessInterfaceModel()
                    {
                        Id = 1,
                        Name = w.Name,
                        Disabled = w.Disabled,
                        FrequencyMode = w.FrequencyMode,
                        MacAddress = w.MacAddress,
                        MasterInterface = w.MasterInterface,
                        Mode = w.Mode,
                        Ssid = w.Ssid,
                        Comment = w.Comment
                    };
                }
                return null;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Obtiene una Interfaz Wireless dado su nombre
        /// </summary>
        /// <param name="name">Nombre de la Wireless</param>
        /// <param name="connection">Conexion del Dispositivo</param>
        /// <returns></returns>
        public WirelessInterfaceModel GetWirelessViewModel(string name, ITikConnection connection)
        {

            var w = connection.LoadList<InterfaceWireless>(connection.CreateParameter("name", name)).SingleOrDefault();
            try
            {
                if (w != null)
                {
                    return new WirelessInterfaceModel()
                    {
                        Id = 1,
                        Name = w.Name,
                        Disabled = w.Disabled,
                        FrequencyMode = w.FrequencyMode,
                        MacAddress = w.MacAddress,
                        MasterInterface = w.MasterInterface,
                        Mode = w.Mode,
                        Ssid = w.Ssid

                    };
                }
                return null;
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        /// <summary>
        /// Deshabilita o Habilita una Interfaz Wirless en el Mikrotik.
        /// </summary>
        /// <param name="wlan"></param>
        /// <param name="enable"></param>
        public void DesableOrEnable(string wlan, bool op, MikrotikCredentials credential)
        {
            var wireless = GetAllWireless(credential).Where(c => c.Name == wlan).First();
            wireless.Disabled = op;
            Init(credential);
            Connection.Save(wireless);
            Dispose();

        }

    }
}
