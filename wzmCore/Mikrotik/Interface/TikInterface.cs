﻿using System.Collections.Generic;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface;
using wzmCore.Extensions;
using wzmData.Models;

namespace wzmCore.Mikrotik { 
    public class TikInterface
    {

        public Wireless Wireless => new Wireless();

        /// <summary>
        /// Obtiene todas las interfaces que posee el Mikrotik.
        /// </summary>
        /// <param name="crendential"></param>
        /// <returns></returns>
        public List<Interface> GetInterfaces(MikrotikCredentials credential)
        {
            using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                List<Interface> interfaces = (List<Interface>)connection.LoadAll<Interface>();

                return interfaces;
            }

            
        }
    }
}
