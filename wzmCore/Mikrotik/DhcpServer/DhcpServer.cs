﻿using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using tik4net.Objects.Ip.DhcpServer;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using wzmCore.Data;
using wzmCore.Extensions;
using tik4net.Objects.Ppp;

namespace wzmCore.Mikrotik
{
    /// <summary>
    /// Manage the DHCP Mikrotik Server
    /// </summary>
    public class DHCPServer : MikrotikBaseConnection
    {

        #region Leases 
        /// <summary>
        /// Optiene los DHCP Leases.
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<DhcpServerLease> GetServerLeases(MikrotikCredentials credential)
        {
            Init(credential);
            var dhcp = (List<DhcpServerLease>)Connection.LoadAll<DhcpServerLease>();
            Dispose();
            return dhcp;
        }


        /// <summary>
        /// Optiene los DHCP Leases.
        /// </summary>
        /// <param name="tikConnection"></param>
        /// <returns></returns>
        public List<DhcpServerLease> GetServerLeases(ITikConnection tikConnection)
        {

            var dhcp = (List<DhcpServerLease>)tikConnection.LoadAll<DhcpServerLease>();

            return dhcp;
        }

        /// <summary>
        /// Get one Lease by an Id.
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DhcpServerLease GetLeaseById(MikrotikCredentials credential, string id)
        {
            try
            {
                var d = GetServerLeases(credential).FirstOrDefault(c => c.Id == id);
                return d;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Deshabilita un Lease
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="lease"></param>
        public void DisableLease(ITikConnection connection, DhcpServerLease lease)
        {
            lease.Disabled = true;
            connection.Save(lease);
        }

        /// <summary>
        /// Habilita un lease
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="lease"></param>
        public void EnableLease(ITikConnection connection, DhcpServerLease lease)
        {
            lease.Disabled = false;
            connection.Save(lease);
        }
        #endregion

        #region DhcpServer
        /// <summary>
        /// Config the Mikrotik DHCP Server
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="parameters"></param>
        public void ConfigDhcpServer(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            var dhcpServer = new IpDhcpServer()
            {

                Name = "dhcp",
                AddArp = false,
                AddressPool = "dhcp",
                Disabled = false,
                Interface = "ether1",
                LeaseTime = "00:10:00"
            };
            Init(credential);
            Connection.Save(dhcpServer);
            var server = Connection.LoadAll<IpDhcpServer>().FirstOrDefault();
            if (server.Disabled)
            {
                server.Disabled = false;
                Connection.Save(server);
                Dispose();
            }
        }

        public void ConfigDhcpServer(ITikConnection connection, ConfigMikrotikModel parameters)
        {
            var dhcpServer = new IpDhcpServer()
            {

                Name = "WifiZoneManager",
                AddArp = false,
                AddressPool = "wzm",
                Disabled = false,
                Interface = "ether1",
                LeaseTime = "00:10:00",

            };

            connection.Save(dhcpServer);
            var server = connection.LoadAll<IpDhcpServer>().FirstOrDefault();
            if (server.Disabled)
            {
                server.Disabled = false;
                connection.Save(server);

            }
        }

        /// <summary>
        /// Elimina un Lease del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="lease"></param>
        public void DeleteLease(MikrotikCredentials credential, DhcpServerLease lease)
        {
            Init(credential);
            Connection.Delete(lease);
            Dispose();
        }

        /// <summary>
        /// Elimina un Lease del Mikrotik
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="lease"></param>
        public void DeleteLease(ITikConnection connection, DhcpServerLease lease)
        {

            connection.Delete(lease);

        }

        /// <summary>
        /// Añade un Lease al Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="lease"></param>
        public void AddLease(MikrotikCredentials credential, DhcpServerLease lease)
        {
            Init(credential);

            var ulease = new DhcpServerLease()
            {
                Address = lease.Address,
                MacAddress = lease.MacAddress,
                Comment = lease.Comment,
                Server = lease.Server
            };

            Connection.Save(ulease);
            Dispose();
        }



        /// <summary>
        /// Busca un ip disponible en el DHCP Server para asignarle al usuario y lo añade al AdressList
        /// </summary>
        /// <param name="mac">Mac del Usuario</param>
        /// <param name="dhcpServer">Dhcp Server encargado de dar el ip</param>
        /// <param name="nick">Nombre de Usuario</param>
        /// <param name="portal">Portal donde va a tener Internet</param>
        /// <param name="connection">Conexion hacia el Mikrotik</param>
        /// <returns></returns>
        public AssignedIp AssignIpAndAdressListToUser(string mac, string dhcpServer, string nick, string portal, ITikConnection connection)
        {
            string ipToAsign = GetUnsuedIp(connection, dhcpServer);
            if (ipToAsign != string.Empty)
            {
                var lease = new DhcpServerLease()
                {
                    Address = ipToAsign,
                    MacAddress = mac,
                    Comment = nick,
                    Server = dhcpServer

                };

                var addressList = new FirewallAddressList()
                {
                    Address = ipToAsign,
                    Comment = nick,
                    Disabled = false,
                    List = portal
                };
                var leases = connection.LoadAll<DhcpServerLease>();
                if (leases.Any(c => c.MacAddress.Equals(mac)))
                {
                    var userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(mac));

                    if (userLease.Disabled)
                    {
                        userLease.Disabled = false;
                    }
                    if (userLease.Dynamic)
                    {
                        userLease.MakeStatic(connection);
                        userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(mac));
                    }
                    if (userLease.BlockAccess)
                    {
                        userLease.BlockAccess = false;

                    }
                    userLease.Comment = nick;
                    addressList.Address = userLease.Address;
                    connection.Save(userLease);
                    connection.Save(addressList);
                    return new AssignedIp() { Assigned = true, Ip = userLease.Address };
                }
                else
                {
                    connection.Save(lease);
                    connection.Save(addressList);
                    return new AssignedIp() { Assigned = true, Ip = lease.Address };
                }




            }
            else return new AssignedIp();
        }

        /// <summary>
        /// Busca un IP disponible en el DHCP Server apra asignarle al usuario
        /// </summary>
        /// <param name="mac">MACa del usuario que se va a agregar.</param>
        /// <param name="dhcpServer">DHCP server al cual se va a agregar el usuario.</param>
        /// <param name="nick">Nombre del usuario.</param>
        /// <param name="connection">Conexión al Mikrotik</param>
        /// <returns></returns>
        public AssignedIp AssignIpToDevice(string mac, string dhcpServer, string nick, ITikConnection connection)
        {
            AssignedIp assignedIp = new AssignedIp();
            string ipToAsign = GetUnsuedIp(connection, dhcpServer);
            if (ipToAsign != string.Empty)
            {
                var lease = new DhcpServerLease()
                {
                    Address = ipToAsign,
                    MacAddress = mac,
                    Comment = nick,
                    Dynamic = false,
                };

                connection.Save(lease);
                assignedIp.Assigned = true;
                assignedIp.Ip = ipToAsign;
                return assignedIp;
            }
            return assignedIp;
        }

        /// <summary>
        /// Busca la existencia de algún ip disponible en el Server (Lease)
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public bool AvailableIp(ITikConnection connection)
        {
            var dhcpLease = connection.LoadAll<DhcpServerLease>();
            var pool = connection.LoadAll<IpPool>().FirstOrDefault();
            var ipRanges = GenerateIpRange(pool.Ranges.Split("-")[0], pool.Ranges.Split("-")[1]);
            foreach (var item in ipRanges)
            {
                if (!dhcpLease.Any(c => c.Address == item))
                {
                    return true;

                }

            }
            return false;
        }

        /// <summary>
        /// Comprueba que exista algún Ip disponible en el DHCP Server
        /// </summary>
        /// <param name="connection">Conexión al Mikrotik</param>
        /// <param name="dhcpServer">DHCP Server donde se buscará el Ip disponible</param>
        /// <returns></returns>
        public bool AvailableIp(ITikConnection connection, string dhcpServer)
        {
            var dhcpLease = connection.LoadAll<DhcpServerLease>();
            var pool = connection.LoadAll<IpPool>().FirstOrDefault(c => c.Name.Equals(dhcpServer));
            var ipRanges = GenerateIpRange(pool.Ranges.Split("-")[0], pool.Ranges.Split("-")[1]);
            foreach (var item in ipRanges)
            {
                if (!dhcpLease.Any(c => c.Address.Equals(item) && !c.Disabled && !c.Blocked))
                {
                    return true;

                }

            }
            return false;
        }

        /// <summary>
        /// Busca la existencia de algún ip disponible en el Server Lease
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public bool AvailableIp(MikrotikCredentials credential)
        {
            Init(credential);
            var dhcpLease = Connection.LoadAll<DhcpServerLease>();
            var pool = Connection.LoadAll<IpPool>().FirstOrDefault();
            var ipRanges = GenerateIpRange(pool.Ranges.Split("-")[0], pool.Ranges.Split("-")[1]);
            foreach (var item in ipRanges)
            {
                if (!dhcpLease.Any(c => c.Address == item))
                {
                    return true;

                }

            }
            return false;
        }


        /// <summary>
        /// Compureba que el IP no se encuentre en el rango del servidor DHCP
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="mikrotikCredentials"></param>
        /// <returns></returns>
        public bool IsStaticIpInDhcpRange(string ip, MikrotikCredentials mikrotikCredentials)
        {
            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                var pool = connection.LoadAll<IpPool>().FirstOrDefault();
                var ipRanges = GenerateIpRange(pool.Ranges.Split("-")[0], pool.Ranges.Split("-")[1]);
                if (ipRanges.Any(c => c.Equals(ip)))
                {
                    return true;
                }
                return false;
            }

        }

        /// <summary>
        /// Compureba que el IP no se encuentre en el rango del servidor DHCP
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public bool IsStaticIpInDhcpRange(string ip, ITikConnection connection)
        {

            var pool = connection.LoadAll<IpPool>().FirstOrDefault();
            if (pool != null)
            {
                var ipRanges = GenerateIpRange(pool.Ranges.Split("-")[0], pool.Ranges.Split("-")[1]);
                if (ipRanges.Any(c => c.Equals(ip)))
                {
                    return true;
                }
                return false;
            }


            return false;
        }



        /// <summary>
        /// Busca la existencia de algún ip disponible en el Server Lease y lo devuelve
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        private string GetUnsuedIp(ITikConnection connection, string dhcpServer)
        {
            // Obtengo los leases que no estén bloqueados ni deshabilitados y pertenezcan al DHCPServer introducido
            var dhcpLease = connection.LoadAll<DhcpServerLease>().Where(c => c.Server.Equals(dhcpServer));
            // Obtengo el DHCP Server
            var server = connection.LoadAll<IpDhcpServer>().FirstOrDefault(c => c.Name.Equals(dhcpServer));
            // Obtengo el Pool de ese DHCP Server
            var pool = connection.LoadAll<IpPool>().FirstOrDefault(c => c.Name.Equals(server.AddressPool));
            var ipRanges = GenerateIpRange(pool.Ranges.Split("-")[0], pool.Ranges.Split("-")[1]);
            foreach (var item in ipRanges)
            {
                if (!dhcpLease.Any(c => c.Address.Equals(item)))
                {
                    var lease = dhcpLease.FirstOrDefault(c => c.Address.Equals(item));
                    return item;
                }

            }
            return string.Empty;
        }

        private List<string> GenerateIpRange(string initIp, string endIp)
        {
            List<string> ips = new List<string>();

            var start = int.Parse(initIp.Split('.')[3]);
            var end = int.Parse(endIp.Split('.')[3]);
            string subIp = GetSubIp(initIp);
            for (int i = start; i <= end; i++)
            {
                ips.Add(subIp + i.ToString());
            }

            return ips;
        }

        private string GetSubIp(string ip)
        {
            string subIp = string.Empty;
            int point = 0;
            for (int i = 0; i <= ip.Length; i++)
            {
                if (ip[i] == '.')
                {
                    point++;

                    if (point == 3)
                    {
                        subIp += ip[i];
                        break;
                    }
                    subIp += ip[i];
                }
                else
                {
                    subIp += ip[i];
                }
            }

            return subIp;
        }

        /// <summary>
        /// Añade un Lease al Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="lease"></param>
        public void AddCustomLease(MikrotikCredentials credential, CustomDhcpServerLease lease)
        {
            Init(credential);

            Connection.Save(lease);
            Dispose();
        }

        /// <summary>
        /// Añade un Lease al Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="lease"></param>
        public void AddCustomLease(ITikConnection connection, CustomDhcpServerLease lease)
        {

            connection.Save(lease);

        }

        #endregion

        #region DHCP Network
        /// <summary>
        /// Get DHCP Server Network
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<DhcpServerNetwork> GetDhcpServerNetworks(MikrotikCredentials credential)
        {
            Init(credential);
            var d = (List<DhcpServerNetwork>)Connection.LoadAll<DhcpServerNetwork>();
            Dispose();
            return d;
        }

        /// <summary>
        /// Config DHCP Network for the first DHCP Server
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="parameters"></param>
        public void ConfigDhcpNetwork(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            var ipAddress = new Address().GetIpAddress(credential).FirstOrDefault(c => c.Interface == "ether1");
            var mask = ipAddress.Address.Split('/')[1];
            var network = ipAddress.Network;
            var dhcpNetwork = new DhcpServerNetwork()
            {
                Comment = "Creado por WZM",
                Address = network + "/" + mask,
                //DnsServer = parameters.DnsP + "," + parameters.DnsS,
                Gateway = credential.Dispositivo.Ip,
                Netmask = mask,
            };
            Init(credential);
            Connection.Save(dhcpNetwork);
            Dispose();
        }

        public void ConfigDhcpNetwork(ITikConnection connection, ConfigMikrotikModel parameters)
        {
            var ipAddress = new Address().GetIpAddress(connection).FirstOrDefault(c => c.Interface == "ether1");
            var mask = ipAddress.Address.Split('/')[1];
            var network = ipAddress.Network;
            var dhcpNetwork = new DhcpServerNetwork()
            {
                Comment = "Creado por WZM",
                Address = network + "/" + mask,
                //DnsServer = parameters.DnsP + "," + parameters.DnsS,
                Gateway = parameters.DhcpIp,
                Netmask = mask,

            };

            connection.Save(dhcpNetwork);

        }

        /// <summary>
        /// Cambia la Mac del Lease de un dispositivo en el DHCP Server
        /// </summary>
        /// <param name="oldMac">MAC antigua</param>
        /// <param name="newMac">MAC nueva</param>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <returns></returns>
        public bool ChangeLeaseMac(string oldMac, string newMac, MikrotikCredentials credential)
        {
            Init(credential);
            var leases = GetServerLeases(Connection);

            if (leases.Any(c => c.MacAddress.Equals(oldMac)))
            {
                var userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(oldMac));

                userLease.MacAddress = newMac;

                Connection.Save(userLease);

                return true;

            }
            else
            {
                if (leases.Any(c => c.MacAddress.Equals(newMac)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        /// <summary>
        /// Cambia la Mac del Lease de un dispositivo en el DHCP Server
        /// </summary>
        /// <param name="oldMac">MAC antigua</param>
        /// <param name="newMac">MAC nueva</param>
        /// <param name="connection"></param>
        /// <param name="deviceName"></param>
        /// <returns></returns>
        public bool ChangeLeaseMac(string oldMac, string newMac, ITikConnection connection, string deviceName)
        {

            var leases = GetServerLeases(connection);
            if (leases.Any(c => c.MacAddress.Equals(oldMac)))
            {
                var userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(oldMac));

                if (userLease.ClientId != null)
                {
                    userLease.ClientId = string.Empty;

                }

                userLease.MacAddress = newMac;

                connection.Save(userLease);

                return true;

            }
            // Si la MAC vieja del usuario no se encuentra en el dispositivo.
            else
            {
                // Compruebo que la MAC nueva no se encuentre en el dispositivo.
                if (!leases.Any(c => c.MacAddress.Equals(newMac)))
                {
                    // No lo he probado.
                    AddCustomLease(connection, new CustomDhcpServerLease() { Comment = deviceName, MacAddress = newMac, });
                }
            }
            return true;

        }

        #endregion


    }
}
