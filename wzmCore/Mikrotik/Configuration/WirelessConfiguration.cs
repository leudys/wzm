﻿using tik4net.Objects.Interface;
using wzmCore.Base;

namespace wzmCore.Mikrotik.Configuration
{
    class WirelessConfiguration : MikrotikBaseConnection
    {
        /// <summary>
        /// Get a Pre Configured Wireless to compare with others.
        /// </summary>
        /// <returns></returns>
        public InterfaceWireless GetPreConfWireless()
        {
            return new InterfaceWireless()
            {
                Ssid = "WIFI_ETECSA",
                AmpduPriorities = "0,1,2,3,4,5,6,7",
                Band = "5ghz-a/n",
                Frequency = "5520",
                FrequencyMode = "superchannel",
                Mode = InterfaceWireless.WirelessMode.Station,
                PreambleMode = InterfaceWireless.WirelessPreambleMode.Long,
                RadioName = "wzm",
                ScanList = "5150-5875",
                WirelessProtocol = InterfaceWireless.WirelessWirelessProtocol.Plain80211,
                Country = "debug",
                Disabled = false,

            };
        }
            

        ///// <summary>
        ///// Config the main Wireless of the mikrotik for the first use.
        ///// </summary>
        ///// <param name="credential"></param>
        ///// <param name="parameters"></param>
        //public void ConfigWireless(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        //{
        //    Init(credential);
        //    var v = new Wireless().GetAllWireless(Connection).FirstOrDefault(c => c.Name == "wlan1");
        //    var pre = GetPreConfWireless();
        //    v.Ssid = pre.Ssid;
        //    v.Comment = "Creado por WZM";
        //    v.AmpduPriorities = pre.AmpduPriorities;
        //    v.Band = pre.Band;
        //    v.Frequency = pre.Frequency;
        //    v.FrequencyMode = pre.FrequencyMode;
        //    v.Mode = pre.Mode;
        //    v.PreambleMode = pre.PreambleMode;
        //    v.RadioName = pre.RadioName;
        //    v.ScanList = pre.ScanList;
        //    v.WirelessProtocol = pre.WirelessProtocol;
        //    v.Country = pre.Country;
        //    v.Disabled = pre.Disabled;
            
        //    Connection.Save(v);
        //    Dispose();
        //}
    }
}
