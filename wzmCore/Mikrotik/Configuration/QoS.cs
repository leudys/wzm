﻿using tik4net;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik.Configuration
{
    public class QoS: MikrotikBaseConnection
    {
        public void FirstQoSCofing(ITikConnection connection)
        {
            new Layer7().PriorityLayer7(connection);
            new MkQueueType().FirstConfig(connection);
            new Mangle().PriorityMangle(connection);
            new MkQueueTree().FirstConfig(connection);
        }

        /// <summary>
        /// Aplica Calidad de Servicio a un Portal
        /// </summary>
        /// <param name="wlan">Wlan para aplicar el QoS</param>
        /// <param name="num">Número de la Wlan anterior</param>
        /// <param name="nameWlan">Prefijo de Wlan  º</param>
        /// <param name="connection">Conexion del Mikrotik</param>
        public void AddQosToPortal(string wlan, int num, string nameWlan, ITikConnection connection)
        {

            Mangle mangle = new Mangle();
            MkQueueTree mkQueueTree = new MkQueueTree();

            // Estos dos métodos chequean que los objetos no existan en el Mikrotik. Es importante llamarlos 
            // porque estos no pueden existir o el usuario los pudo haber eliminado.


            mkQueueTree.AddQueueTree(connection, wlan);

            mangle.PriorityMangle(connection, nameWlan, num, wlan);

        }

        /// <summary>
        /// Comprueba que el Layer7, QueueTree y QueueType existan
        /// </summary>
        /// <param name="connection"></param>
        public void CheckQoS(ITikConnection connection)
        {
            Layer7 layer7 = new Layer7();
            MkQueueType mkQueueType = new MkQueueType();
            layer7.PriorityLayer7(connection);

            mkQueueType.FirstConfig(connection);

        }

        /// <summary>
        /// Elimina el QoS de un Portal
        /// </summary>
        /// <param name="connection">Conexión del Mikrotik</param>
        /// <param name="wlan">Portal para eliminar el QoS</param>
        public void DeleteQoSfromPortal(ITikConnection connection, string wlan)
        {
            Mangle mangle = new Mangle();
            MkQueueTree mkQueueTree = new MkQueueTree();

            mangle.DeleteMangleInQos(connection,wlan);
            mkQueueTree.RemoveQueueTree(connection, wlan);
        }

        /// <summary>
        /// Elimina el QoS de un Portal
        /// </summary>
        /// <param name="mikrotikCredential">Credenciales del Mikrotik</param>
        /// <param name="wlan">Portal para eliminar el QoS</param>
        public void DeleteQoSfromPortal(MikrotikCredentials mikrotikCredential, string wlan)
        {
            Init(mikrotikCredential);
            Mangle mangle = new Mangle();
            MkQueueTree mkQueueTree = new MkQueueTree();

            mangle.DeleteMangleInQos(Connection, wlan);
            mkQueueTree.RemoveQueueTree(Connection, wlan);
        }

    }
}
