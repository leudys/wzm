﻿using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface;
using tik4net.Objects.Queue;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class MkQueueTree : MikrotikBaseConnection
    {
        

        #region Variables de Inicializacion
        // Prioridad
        private static int FacebookPriority = 4;
        private static int IcmpPriority = 1;
        private static int DnsPriority = 2;
        private static int ManagmentPriority = 2;
        private static int WhatsappPriority = 3;
        private static int YoutubePriority = 6;
        private static int OtherPriority = 8;

        //Ancho de Banda
        private static string Icmplimitat = "200k";
        private static string Icmpmaxlimit = "500k";
        private static string Dnslimitat = "200k";
        private static string Dnsmaxlimit = "500k";
        private static string Managmentlimitat = "200k";
        private static string Managmentmaxlimit = "500k";
        private static string Whatsapplimitat = "600k";
        private static string Whatsappmaxlimit = "2100k";
        private static string Facebooklimitat = "300k";
        private static string Facebookmaxlimit = "2100k";
        private static string Youtubelimitat = "300k";
        private static string Youtubemaxlimit = "2100k";
        private static string Otherlimitat = "300k";
        private static string Othermaxlimit = "2100k";
        private static string MaxLimit = "2100k";
        #endregion

        public List<QueueTree> GetAllQueueTree(MikrotikCredentials credential)
        {
            Init(credential);
            var qt = Connection.LoadAll<QueueTree>().ToList();
            Dispose();
            return qt;
        }

        public void Add(MikrotikCredentials credential, QueueTree queue)
        {
            Init(credential);
            Connection.Save(queue);
            Dispose();
        }

        public void Delete(MikrotikCredentials credential, QueueTree queue)
        {
            Init(credential);
            Connection.Delete(queue);
            Dispose();
        }

        public void Update(MikrotikCredentials credential, QueueTree queue)
        {

        }

        public string MasterInterface { get; set; } = "ether1";

        /// <summary>
        /// Crea los Queue Trees a todos los portales existentes
        /// </summary>
        /// <param name="credential">Credenciales del Mikrotik</param>
        public void FirstConfig(MikrotikCredentials credential)
        {
            Init(credential);

            if (MasterInterface != null)
            {
                if (MasterInterface != string.Empty)
                {

                    IEnumerable<InterfaceWireless> cantWlan = Connection.LoadAll<InterfaceWireless>();

                    if (cantWlan.Count() > 1)
                    {

                        List<QueueTree> queueTree = new List<QueueTree>();

                        FillQueueTree(Connection, queueTree, cantWlan);



                        foreach (var item in queueTree)
                        {
                            try
                            {
                                Connection.Save(item);
                            }
                            catch (System.Exception)
                            {
                            }

                        }
                        DisposeMkConnection();
                    }

                }
            }


        }

        /// <summary>
        /// Crea los Queue Trees a todos los portales existentes
        /// </summary>
        /// <param name="connection">Conexión al Mikrotik</param>
        public void FirstConfig(ITikConnection connection)
        {

            if (MasterInterface != null)
            {
                if (MasterInterface != string.Empty)
                {

                    IEnumerable<InterfaceWireless> cantWlan = connection.LoadAll<InterfaceWireless>();

                    if (cantWlan.Count() > 1)
                    {

                        List<QueueTree> queueTree = new List<QueueTree>();

                        FillQueueTree(connection, queueTree, cantWlan);



                        foreach (var item in queueTree)
                        {
                            try
                            {
                                connection.Save(item);
                            }
                            catch (Exception)
                            {
                            }

                        }

                    }

                }
            }


        }

        /// <summary>
        /// Añade un Queue Tree dado un portal
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <param name="wireless">Wireless que se le añadirá el Queue Tree</param>
        public void AddQueueTree(ITikConnection connection, InterfaceWireless wireless)
        {
            if (MasterInterface != null)
            {
                if (MasterInterface != string.Empty)
                {
                    List<QueueTree> queueTree = new List<QueueTree>();

                    FillQueueTree(connection, queueTree, wireless);



                    foreach (var item in queueTree)
                    {
                        try
                        {
                            connection.Save(item);
                        }
                        catch (Exception)
                        {
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Añade un Queue Tree dado un portal
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <param name="wireless">Nombre de la Wireless</param>
        public void AddQueueTree(ITikConnection connection, string wireless)
        {
            if (MasterInterface != null)
            {
                if (MasterInterface != string.Empty)
                {
                    List<QueueTree> queueTree = new List<QueueTree>();

                    FillQueueTree(connection, queueTree, wireless);



                    foreach (var item in queueTree)
                    {
                        try
                        {
                            connection.Save(item);
                                                      

                        }
                        catch (Exception)
                        {
                        }

                    }
                }
            }
        }

        /// <summary>
        /// Elimina un Queue Tree
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="wireless"></param>
        public void RemoveQueueTree(ITikConnection connection, InterfaceWireless wireless)
        {
            var queues = connection.LoadAll<QueueTree>().Where(c => c.Name.Contains(wireless.Name));

            foreach (var item in queues)
            {
                connection.Delete(item);
            }

            
        }

        /// <summary>
        /// Elimina un Queue Tree
        /// </summary>
        /// <param name="connection">Conexión al Mikrotik</param>
        /// <param name="wireless">WLAN que se eliminó</param>
        public void RemoveQueueTree(ITikConnection connection, string wireless)
        {
            var queues = connection.LoadAll<QueueTree>().Where(c => c.Name.Contains(wireless));

            foreach (var item in queues)
            {
                connection.Delete(item);
            }


        }

        /// <summary>
        /// Obtiene los Queue Trees de las Interfaces para ser añadidos al Mikrotik
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <param name="queueTree">Lista de Queue Trees para llenar</param>
        /// <param name="cantWlan">Lista de Wlans</param>
        private void FillQueueTree(ITikConnection connection, List<QueueTree> queueTree, IEnumerable<InterfaceWireless> cantWlan)
        {
            var usedWlan = cantWlan.Where(c => c.InterfaceType.Equals("virtual") && c.Ssid.Equals("WIFI_ETECSA")).ToList();

            for (int i = 0; i < usedWlan.Count(); i++)
            {

                queueTree.Add(new QueueTree()
                {
                    MaxLimit = MaxLimit,
                    Name = $"{usedWlan[i].Name} Downstream",
                    Parent = MasterInterface,
                    Queue = "Bajada"

                });
                queueTree.Add(new QueueTree()
                {
                    MaxLimit = MaxLimit,
                    Name = $"{usedWlan[i].Name} Upstream",
                    Parent = $"{usedWlan[i].Name}",
                    Queue = "Subida"

                });

                #region Dns
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - DNS - rx",
                    PacketMark = $"Dns-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = DnsPriority,
                    Queue = "Bajada",
                    LimitAt = Dnslimitat,
                    MaxLimit = Dnsmaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - DNS - tx",
                    PacketMark = $"Dns-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = DnsPriority,
                    Queue = "Subida",
                    LimitAt = Dnslimitat,
                    MaxLimit = Dnsmaxlimit,
                });
                #endregion

                #region Management
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Managment - rx",
                    PacketMark = $"Managment-fw-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = ManagmentPriority,
                    Queue = "Bajada",
                    LimitAt = Managmentlimitat,
                    MaxLimit = Managmentmaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Managment - tx",
                    PacketMark = $"Managment-fw-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = ManagmentPriority,
                    Queue = "Subida",
                    LimitAt = Managmentlimitat,
                    MaxLimit = Managmentmaxlimit,
                });
                #endregion

                #region ICMP
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - ICMP - rx",
                    PacketMark = $"ICMP-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = IcmpPriority,
                    Queue = "Bajada",
                    LimitAt = Icmplimitat,
                    MaxLimit = Icmpmaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - ICMP - tx",
                    PacketMark = $"ICMP-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = IcmpPriority,
                    Queue = "Subida",
                    LimitAt = Icmplimitat,
                    MaxLimit = Icmpmaxlimit,
                });
                #endregion

                #region Whatsapp
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Whatsapp - rx",
                    PacketMark = $"Whatsapp-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = WhatsappPriority,
                    Queue = "Bajada",
                    LimitAt = Whatsapplimitat,
                    MaxLimit = Whatsappmaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Whatsapp - tx",
                    PacketMark = $"Whatsapp-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = WhatsappPriority,
                    Queue = "Subida",
                    LimitAt = Whatsapplimitat,
                    MaxLimit = Whatsappmaxlimit,
                });
                #endregion

                #region Youtube
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Youtube - rx",
                    PacketMark = $"Youtube-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = YoutubePriority,
                    Queue = "Bajada",
                    LimitAt = Youtubelimitat,
                    MaxLimit = Youtubemaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Youtube - tx",
                    PacketMark = $"Youtube-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = YoutubePriority,
                    Queue = "Subida",
                    LimitAt = Youtubelimitat,
                    MaxLimit = Youtubemaxlimit,
                });
                #endregion

                #region Facebook
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Facebook - rx",
                    PacketMark = $"Facebook-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = FacebookPriority,
                    Queue = "Bajada",
                    LimitAt = Facebooklimitat,
                    MaxLimit = Facebookmaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Facebook - tx",
                    PacketMark = $"Facebook-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = FacebookPriority,
                    Queue = "Subida",
                    LimitAt = Facebooklimitat,
                    MaxLimit = Facebookmaxlimit,
                });
                #endregion

                #region Other
                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Other - rx",
                    PacketMark = $"Other-{usedWlan[i].Name}-Download",
                    Parent = $"{usedWlan[i].Name} Downstream",
                    Priority = OtherPriority,
                    Queue = "Bajada",
                    LimitAt = Otherlimitat,
                    MaxLimit = Othermaxlimit,
                });

                queueTree.Add(new QueueTree()
                {
                    Name = $"{usedWlan[i].Name} - Other - tx",
                    PacketMark = $"Other-{usedWlan[i].Name}-Upload",
                    Parent = $"{usedWlan[i].Name} Upstream",
                    Priority = OtherPriority,
                    Queue = "Subida",
                    LimitAt = Otherlimitat,
                    MaxLimit = Othermaxlimit,
                });
                #endregion
            }
        }

        /// <summary>
        /// Obtiene los Queue Trees de las Interfaces para ser añadidos al Mikrotik
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <param name="queueTree">Lista de Queue Trees para llenar</param>
        /// <param name="wlan">Interfaz Wireless</param>
        private void FillQueueTree(ITikConnection connection, List<QueueTree> queueTree, InterfaceWireless wlan)
        {
            queueTree.Add(new QueueTree()
            {
                MaxLimit = MaxLimit,
                Name = $"{wlan.Name} Downstream",
                Parent = MasterInterface,
                Queue = "Bajada"

            });
            queueTree.Add(new QueueTree()
            {
                MaxLimit = MaxLimit,
                Name = $"{wlan.Name} Upstream",
                Parent = $"{wlan.Name}",
                Queue = "Subida"

            });

            #region Dns
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - DNS - rx",
                PacketMark = $"Dns-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = DnsPriority,
                Queue = "Bajada",
                LimitAt = Dnslimitat,
                MaxLimit = Dnsmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - DNS - tx",
                PacketMark = $"Dns-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = DnsPriority,
                Queue = "Subida",
                LimitAt = Dnslimitat,
                MaxLimit = Dnsmaxlimit,
            });
            #endregion

            #region Management
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Managment - rx",
                PacketMark = $"Managment-fw-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = ManagmentPriority,
                Queue = "Bajada",
                LimitAt = Managmentlimitat,
                MaxLimit = Managmentmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Managment - tx",
                PacketMark = $"Managment-fw-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = ManagmentPriority,
                Queue = "Subida",
                LimitAt = Managmentlimitat,
                MaxLimit = Managmentmaxlimit,
            });
            #endregion

            #region ICMP
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - ICMP - rx",
                PacketMark = $"ICMP-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = IcmpPriority,
                Queue = "Bajada",
                LimitAt = Icmplimitat,
                MaxLimit = Icmpmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - ICMP - tx",
                PacketMark = $"ICMP-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = IcmpPriority,
                Queue = "Subida",
                LimitAt = Icmplimitat,
                MaxLimit = Icmpmaxlimit,
            });
            #endregion

            #region Whatsapp
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Whatsapp - rx",
                PacketMark = $"Whatsapp-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = WhatsappPriority,
                Queue = "Bajada",
                LimitAt = Whatsapplimitat,
                MaxLimit = Whatsappmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Whatsapp - tx",
                PacketMark = $"Whatsapp-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = WhatsappPriority,
                Queue = "Subida",
                LimitAt = Whatsapplimitat,
                MaxLimit = Whatsappmaxlimit,
            });
            #endregion

            #region Youtube
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Youtube - rx",
                PacketMark = $"Youtube-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = YoutubePriority,
                Queue = "Bajada",
                LimitAt = Youtubelimitat,
                MaxLimit = Youtubemaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Youtube - tx",
                PacketMark = $"Youtube-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = YoutubePriority,
                Queue = "Subida",
                LimitAt = Youtubelimitat,
                MaxLimit = Youtubemaxlimit,
            });
            #endregion

            #region Facebook
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Facebook - rx",
                PacketMark = $"Facebook-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = FacebookPriority,
                Queue = "Bajada",
                LimitAt = Facebooklimitat,
                MaxLimit = Facebookmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Facebook - tx",
                PacketMark = $"Facebook-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = FacebookPriority,
                Queue = "Subida",
                LimitAt = Facebooklimitat,
                MaxLimit = Facebookmaxlimit,
            });
            #endregion

            #region Other
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Other - rx",
                PacketMark = $"Other-{wlan.Name}-Download",
                Parent = $"{wlan.Name} Downstream",
                Priority = OtherPriority,
                Queue = "Bajada",
                LimitAt = Otherlimitat,
                MaxLimit = Othermaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan.Name} - Other - tx",
                PacketMark = $"Other-{wlan.Name}-Upload",
                Parent = $"{wlan.Name} Upstream",
                Priority = OtherPriority,
                Queue = "Subida",
                LimitAt = Otherlimitat,
                MaxLimit = Othermaxlimit,
            });
            #endregion

        }

        /// <summary>
        /// Obtiene los Queue Trees de las Interfaces para ser añadidos al Mikrotik
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <param name="queueTree">Lista de Queue Trees para llenar</param>
        /// <param name="wlan">Nombre de la Interfaz Wireless</param>
        private void FillQueueTree(ITikConnection connection, List<QueueTree> queueTree, string wlan)
        {
            queueTree.Add(new QueueTree()
            {
                MaxLimit = MaxLimit,
                Name = $"{wlan} Downstream",
                Parent = MasterInterface,
                Queue = "Bajada"

            });
            queueTree.Add(new QueueTree()
            {
                MaxLimit = MaxLimit,
                Name = $"{wlan} Upstream",
                Parent = $"{wlan}",
                Queue = "Subida"

            });

            #region Dns
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - DNS - rx",
                PacketMark = $"Dns-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = DnsPriority,
                Queue = "Bajada",
                LimitAt = Dnslimitat,
                MaxLimit = Dnsmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - DNS - tx",
                PacketMark = $"Dns-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = DnsPriority,
                Queue = "Subida",
                LimitAt = Dnslimitat,
                MaxLimit = Dnsmaxlimit,
            });
            #endregion

            #region Management
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Managment - rx",
                PacketMark = $"Managment-fw-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = ManagmentPriority,
                Queue = "Bajada",
                LimitAt = Managmentlimitat,
                MaxLimit = Managmentmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Managment - tx",
                PacketMark = $"Managment-fw-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = ManagmentPriority,
                Queue = "Subida",
                LimitAt = Managmentlimitat,
                MaxLimit = Managmentmaxlimit,
            });
            #endregion

            #region ICMP
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - ICMP - rx",
                PacketMark = $"ICMP-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = IcmpPriority,
                Queue = "Bajada",
                LimitAt = Icmplimitat,
                MaxLimit = Icmpmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - ICMP - tx",
                PacketMark = $"ICMP-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = IcmpPriority,
                Queue = "Subida",
                LimitAt = Icmplimitat,
                MaxLimit = Icmpmaxlimit,
            });
            #endregion

            #region Whatsapp
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Whatsapp - rx",
                PacketMark = $"Whatsapp-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = WhatsappPriority,
                Queue = "Bajada",
                LimitAt = Whatsapplimitat,
                MaxLimit = Whatsappmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Whatsapp - tx",
                PacketMark = $"Whatsapp-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = WhatsappPriority,
                Queue = "Subida",
                LimitAt = Whatsapplimitat,
                MaxLimit = Whatsappmaxlimit,
            });
            #endregion

            #region Youtube
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Youtube - rx",
                PacketMark = $"Youtube-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = YoutubePriority,
                Queue = "Bajada",
                LimitAt = Youtubelimitat,
                MaxLimit = Youtubemaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Youtube - tx",
                PacketMark = $"Youtube-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = YoutubePriority,
                Queue = "Subida",
                LimitAt = Youtubelimitat,
                MaxLimit = Youtubemaxlimit,
            });
            #endregion

            #region Facebook
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Facebook - rx",
                PacketMark = $"Facebook-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = FacebookPriority,
                Queue = "Bajada",
                LimitAt = Facebooklimitat,
                MaxLimit = Facebookmaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Facebook - tx",
                PacketMark = $"Facebook-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = FacebookPriority,
                Queue = "Subida",
                LimitAt = Facebooklimitat,
                MaxLimit = Facebookmaxlimit,
            });
            #endregion

            #region Other
            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Other - rx",
                PacketMark = $"Other-{wlan}-Download",
                Parent = $"{wlan} Downstream",
                Priority = OtherPriority,
                Queue = "Bajada",
                LimitAt = Otherlimitat,
                MaxLimit = Othermaxlimit,
            });

            queueTree.Add(new QueueTree()
            {
                Name = $"{wlan} - Other - tx",
                PacketMark = $"Other-{wlan}-Upload",
                Parent = $"{wlan} Upstream",
                Priority = OtherPriority,
                Queue = "Subida",
                LimitAt = Otherlimitat,
                MaxLimit = Othermaxlimit,
            });
            #endregion

        }

        private List<QueueTree> GetUploadQueueTree()
        {
            var list = new List<QueueTree>();

            list.Add(new QueueTree()
            {

                Name = "Subida",
                Parent = "ether1",
                Priority = 3,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "1 - PING_S",
                PacketMark = "ICMP",
                Parent = "Subida",
                Priority = 1,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "2 - DNS_S",
                PacketMark = "DNS",
                Parent = "Subida",
                Priority = 2,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "6 - YouTube_S",
                PacketMark = "YOUTUBE",
                Parent = "Subida",
                Priority = 6,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "5 - HTTP_S",
                PacketMark = "HTTP",
                Parent = "Subida",
                Priority = 5,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "4 - HTTPS_S",
                PacketMark = "HTTPS",
                Parent = "Subida",
                Priority = 4,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "3 - Whatsapp_S",
                PacketMark = "Whatsapp",
                Parent = "Subida",
                Priority = 1,
                Queue = "Upload"
            });

            list.Add(new QueueTree()
            {
                Name = "8 - OTROS_S",
                PacketMark = "OTROS",
                Parent = "Subida",

                Queue = "Upload"
            });

            return list;
        }

        private List<QueueTree> GetDownloadQueueTree()
        {
            var list = new List<QueueTree>();

            list.Add(new QueueTree()
            {
                Comment = "Descarga",
                Name = "Descarga",
                Parent = "global",
                Priority = 2,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "1 - PING_D",
                PacketMark = "ICMP",
                Parent = "Descarga",
                Priority = 1,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "2 - DNS_D",
                PacketMark = "DNS",
                Parent = "Descarga",
                Priority = 2,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "6 - YouTube_D",
                PacketMark = "YOUTUBE",
                Parent = "Descarga",
                Priority = 6,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "5 - HTTP_D",
                PacketMark = "HTTP",
                Parent = "Descarga",
                Priority = 5,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "4 - HTTPS_D",
                PacketMark = "HTTPS",
                Parent = "Descarga",
                Priority = 4,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "3 - Whatsapp_D",
                PacketMark = "Whatsapp",
                Parent = "Descarga",
                Priority = 1,
                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "8 - OTROS_D",
                PacketMark = "OTROS",
                Parent = "Descarga",

                Queue = "Download"
            });

            list.Add(new QueueTree()
            {
                Name = "7 - Facebook_D",
                PacketMark = "Facebook",
                Parent = "Descarga",
                Priority = 3,
                Queue = "Download"
            });

            return list;
        }

        private void Layer7()
        {

        }
    }
}
