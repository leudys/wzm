﻿using System.Collections.Generic;
using System.Linq;
using tik4net.Objects;
using tik4net.Objects.Queue;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.ViewModels;

namespace wzmCore.Mikrotik
{
    public class MkQueueSimple : MikrotikBaseConnection
    {
        public List<QueueSimple> GetAllQueueType(MikrotikCredentials credential)
        {
            Init(credential);
            var qt = Connection.LoadAll<QueueSimple>().ToList();
            Dispose();
            return qt;
        }

        public void Add(MikrotikCredentials credential, QueueSimple queue)
        {
            Init(credential);
            Connection.Save(queue);
            Dispose();
        }

        public void Delete(MikrotikCredentials credential, QueueSimple queue)
        {
            Init(credential);
            Connection.Delete(queue);
            Dispose();
        }

        public void Update(MikrotikCredentials credential, QueueSimple queue)
        {

        }
    }
}
