﻿using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Queue;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class MkQueueType : MikrotikBaseConnection
    {
        public List<QueueType> GetAllQueueType(MikrotikCredentials credential)
        {
            Init(credential);
            var qt = Connection.LoadAll<QueueType>().ToList();
            Dispose();
            return qt;
        }

        public void Add(MikrotikCredentials credential, QueueType queue)
        {
            Init(credential);
            Connection.Save(queue);
            Dispose();
        }

        public void Delete(MikrotikCredentials credential, QueueType queue)
        {
            Init(credential);
            Connection.Delete(queue);
            Dispose();
        }

        public void Update(MikrotikCredentials credential, QueueType queue)
        {

        }

        /// <summary>
        /// Añade el Queue Type para el QoS
        /// </summary>
        /// <param name="credential">Credencial del Mikrotik</param>
        public void FirstConfig(MikrotikCredentials credential)
        {
            Init(credential);

            if (!ExistsQueue(Connection))
            {
                var qtypeD = GetDownloadObject();
                var qtypeU = GetUploadObject();

                Connection.Save(qtypeD);
                Connection.Save(qtypeU);
                DisposeMkConnection();
            }

          
        }

        /// <summary>
        /// Añade el Queue Type para el QoS
        /// </summary>
        /// <param name="connection">Conexión establecida al Mikrotik</param>
        public void FirstConfig(ITikConnection connection)
        {
          

            if (!ExistsQueue(connection))
            {
                var qtypeD = GetDownloadObject();
                var qtypeU = GetUploadObject();
                connection.Save(qtypeD);
                connection.Save(qtypeU);
                
            }

        
        }


        private QueueType GetUploadObject()
        {
            return new QueueType()
            {
                Kind = "pcq",
                Name = "Subida",
                PcqClassifier = "dst-address",
                PcqTotalLimit = 2100
            };
        }

        private QueueType GetDownloadObject()
        {
            return new QueueType()
            {
                Kind = "pcq",
                Name = "Bajada",
                PcqClassifier = "dst-address",
                PcqTotalLimit = 2100
            };
        }

        public bool ExistsQueue(ITikConnection connection)
        {
            var queue = connection.LoadAll<QueueType>();

            if (queue.Any(c => c.Name.Equals("Subida")) || queue.Any(c => c.Name.Equals("Bajada")))
            {
                return true;
            }
            return false;
        }
    }
}
