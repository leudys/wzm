﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class Mangle : MikrotikBaseConnection
    {
        /// <summary>
        /// Agrega un Mangle al Mikrotik para que el Portal tenga salida a Etecsa 
        /// </summary>
        /// <param name="connection"></param>
        public void AddMangleForPortal(ITikConnection connection, int wlan)
        {
            ITikCommand addMangles = connection.CreateCommand("/ip/firewall/mangle/add",
              connection.CreateParameter("comment", "salida" + wlan),
              connection.CreateParameter("chain", "prerouting"),
              connection.CreateParameter("src-address-list", "salida" + wlan),
              connection.CreateParameter("action", "mark-routing"),
              connection.CreateParameter("new-routing-mark", "salida" + wlan));
            addMangles.ExecuteScalar();

            // Buscar posición del Mangle, no puede estar dejabajo del QOS, por lo
            // que tengo que buscar donde se encuentra y moverlo.

            if (wlan > 1)
            {
                var firewallMangles = connection.LoadAll<FirewallMangle>().ToList();

                int wlanAnterior = wlan - 1;

                var indexMangleCreado = firewallMangles.IndexOf(firewallMangles.FirstOrDefault(c => c.NewRoutingMark.Contains($"salida{wlan}")));

                var indexMangleAnterior = firewallMangles.IndexOf(firewallMangles.FirstOrDefault(c => c.NewRoutingMark.Contains($"salida{wlanAnterior}")));
                if (indexMangleAnterior != -1)
                {
                    // Si la diferencia de un idex del otro no es uno, significa que estos estan separados.
                    if (indexMangleCreado - indexMangleAnterior != 1)
                    {
                        // Busco el mangle que se encuentra después de {indexMangleAnterior}
                        var mangleDespuesDeAnterior = firewallMangles[++indexMangleAnterior];
                        var mangleAMover = firewallMangles[indexMangleCreado];

                        connection.Move(mangleAMover, mangleDespuesDeAnterior);
                    }
                }


            }



        }


        public void AddCustomMangle(MikrotikCredentials credential, FirewallMangle mangle)
        {
            Init(credential);
            Connection.Save(mangle);
            DisposeMkConnection();
        }

        public void AddCustomMangle(ITikConnection connection, FirewallMangle mangle)
        {
            connection.Save(mangle);

        }

        /// <summary>
        /// Obtiene los Mangles del Firewall del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<FirewallMangle> GetAllMangle(ITikConnection connection)
        {
            List<FirewallMangle> mangles = new List<FirewallMangle>();
            try
            {
                mangles = (List<FirewallMangle>)connection.LoadAll<FirewallMangle>();
            }
            catch (Exception)
            {

            }
            return mangles;


        }


        /// <summary>
        /// Obtiene un Mangle dado el Source Address List
        /// </summary>
        /// <param name="SrcAddressList"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public FirewallMangle GetMangle(string SrcAddressList, MikrotikCredentials credential)
        {
            Init(credential);
            try
            {
                var mangle = Connection.LoadList<FirewallMangle>(Connection.CreateParameter("src-address-list", SrcAddressList)).First();

                return mangle;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally { DisposeMkConnection(); }
        }

        /// <summary>
        /// Obtiene un Mangle dado el Source Address List
        /// </summary>
        /// <param name="SrcAddressList"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public FirewallMangle GetMangle(string SrcAddressList, ITikConnection connection)
        {

            try
            {
                var mangle = connection.LoadList<FirewallMangle>(connection.CreateParameter("src-address-list", SrcAddressList)).First();

                return mangle;
            }
            catch (Exception e)
            {
                throw e;

            }
        }

        /// <summary>
        /// Config the Mangle for Priority porpouse
        /// </summary>
        /// <param name="credential"></param>
        public void PriorityMangle(MikrotikCredentials credential)
        {
            Init(credential);
            var mangleList = GetMangleList();

            foreach (var item in mangleList)
            {
                Connection.Save(item);
            }
            DisposeMkConnection();
        }


        /// <summary>
        /// Agrega los Mangles necesarios para el QoS
        /// </summary>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <param name="wlan">Nombre de la Wireless</param>
        public void PriorityMangle(MikrotikCredentials credential, string wlan)
        {
            Init(credential);
            var mangleList = GetMangleList();

            foreach (var item in mangleList)
            {
                Connection.Save(item);
            }
            DisposeMkConnection();
        }

        /// <summary>
        /// Agrega los Mangles necesarios para el QoS, este método es llamando solamente cuando es la Primera Configuración.
        /// </summary>
        /// <param name="connection">Conexión del Mikrotik</param>
        public void PriorityMangle(ITikConnection connection)
        {


            var mangleList = GetMangleList(connection);

            foreach (var item in mangleList)
            {
                connection.Save(item);
            }

        }

        /// <summary>
        /// Agrega los Mangles necesarios para el QoS
        /// </summary>
        /// <param name="connection">Conexión del Mikrotik</param>
        /// <param name="nameWlan">Prefijo de la WLAN ej: wlan, portal</param>
        /// <param name="numWlan">Número de la WLAN ej: 5</param>
        /// <param name="wlan">Nombre completo de la WLAN</param>
        public void PriorityMangle(ITikConnection connection, string nameWlan, int numWlan, string wlan)
        {
            // Genero los Mangles para Crear.
            var mangleList = GetMangleList(connection, wlan);

            foreach (var item in mangleList)
            {
                connection.Save(item);
            }
            OrganizarPriorityMangle(connection, numWlan, nameWlan, mangleList);
        }

        /// <summary>
        /// Organiza los Mangles de la última WLAN creada para el QoS
        /// </summary>
        /// <param name="connection">Conexión del Mikrotik</param>
        /// <param name="numWlan">Último número de la Wlan ej: 5</param>
        /// <param name="nameWlan">Prefijo de la WLAN ej: wlan o portal</param>
        public void OrganizarPriorityMangle(ITikConnection connection, int numWlan, string nameWlan, List<FirewallMangle> mangleList)
        {
            if (numWlan > 1)
            {
                // Si este métedo fue llamado, y el dispositivo no tenia el QoS habilitado, 
                // la lista me vendrá muy grande. Y solamente quiero los que contengan InInterface o OutInterface
                //mangleList.RemoveAll(c => c.OutIterface != null && c.InIterface == null);
                //mangleList.RemoveAll(c => c.InIterface != null && c.OutIterface == null);


                string wlanAnterior = $"{nameWlan}{numWlan - 1}";



                foreach (var item in mangleList)
                {
                    var firewallMangles = connection.LoadAll<FirewallMangle>().ToList();

                    if (item.InIterface != null)
                    {
                        // Compruebo que exista un mangle cuya interfaz de salida sea la {wlanAnterior} y su marca de conexion sea {item.ConnectionMark}
                        // de no existir, significa que la {wlanAnterior} no contiene QoS, por lo que debo seguir buscando.
                        if (firewallMangles.Any(c => c.OutIterface.Equals(wlanAnterior) && c.ConnectionMark.Equals(item.ConnectionMark)))
                        {
                            int indexMangleActual = firewallMangles.IndexOf(firewallMangles.First(c => c.InIterface.Equals(item.InIterface) && c.ConnectionMark.Equals(item.ConnectionMark)));
                            int indexMangleAnterior = firewallMangles.IndexOf(firewallMangles.First(c => c.OutIterface.Equals(wlanAnterior) && c.ConnectionMark.Equals(item.ConnectionMark)));

                            if (indexMangleActual - indexMangleAnterior != 1)
                            {
                                int tempIndex = indexMangleAnterior + 1;
                                var ultimoMangle = firewallMangles[tempIndex];

                                connection.Move(firewallMangles[indexMangleActual], ultimoMangle);
                            }
                        }
                        else
                        {
                            // Si llega a cero significa que ninguna interfaz de las que existen poseen QoS, por lo que no debo hacer nada.
                            // Puesto que las reglas ya se agergaron.
                            int tempWlan = numWlan;
                            while (tempWlan > 0)
                            {
                                wlanAnterior = $"{nameWlan}{tempWlan - 1}";

                                if (firewallMangles.Any(c => c.OutIterface.Equals(wlanAnterior) && c.ConnectionMark.Equals(item.ConnectionMark)))
                                {
                                    int indexMangleActual = firewallMangles.IndexOf(firewallMangles.First(c => c.InIterface.Equals(item.InIterface) && c.ConnectionMark.Equals(item.ConnectionMark)));
                                    int indexMangleAnterior = firewallMangles.IndexOf(firewallMangles.First(c => c.OutIterface.Equals(wlanAnterior) && c.ConnectionMark.Equals(item.ConnectionMark)));

                                    if (indexMangleActual - indexMangleAnterior != 1)
                                    {
                                        int tempIndex = indexMangleAnterior + 1;
                                        var ultimoMangle = firewallMangles[tempIndex];

                                        connection.Move(firewallMangles[indexMangleActual], ultimoMangle);
                                        break;
                                    }
                                }
                                else
                                {
                                    --tempWlan;
                                }
                            }


                        }


                    }
                    if (item.OutIterface != null)
                    {
                        int indexMangleActual = firewallMangles.IndexOf(firewallMangles.First(c => c.OutIterface.Equals(item.OutIterface) && c.ConnectionMark.Equals(item.ConnectionMark)));
                        int indexMangleAnterior = firewallMangles.IndexOf(firewallMangles.First(c => c.InIterface.Equals(item.OutIterface) && c.ConnectionMark.Equals(item.ConnectionMark)));

                        if (indexMangleActual - indexMangleAnterior != 1)
                        {
                            int tempIndex = indexMangleAnterior + 1;
                            var ultimoMangle = firewallMangles[tempIndex];

                            connection.Move(firewallMangles[indexMangleActual], ultimoMangle);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get a pre configured List of Firewall Mangle .
        /// </summary>
        /// <returns></returns>
        private List<FirewallMangle> GetMangleList()
        {
            var list = new List<FirewallMangle>();

            IEnumerable<InterfaceWireless> cantWlan = Connection.LoadAll<InterfaceWireless>();

            List<FirewallMangle> firewallMangles = (List<FirewallMangle>)Connection.LoadAll<FirewallMangle>();

            #region Whatsapp/Messenger/IMO & VOIP

            if (!ExistsMangle("Whatsapp", "", "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061", null, "tcp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Whatsapp/Messenger/IMO & VOIP",
                    DstPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "tcp",
                });
            }


            if (!ExistsMangle("Whatsapp", "", "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478", null, "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    DstPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",

                });
            }

            if (!ExistsMangle("Whatsapp", "", null, "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478", "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",
                    SrcPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478",

                });
            }
            if (!ExistsMangle("Whatsapp", "", null, "!53", "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",
                    SrcPort = "!53",

                });
            }

            if (!firewallMangles.Any(c => c.DstAddressList.Equals("Mark - Whatsapp") && c.NewConnectionMark.Equals("Whatsapp")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    DstAddressList = "Mark - Whatsapp",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddressList.Equals("Mark - Whatsapp") && c.NewConnectionMark.Equals("Whatsapp")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",

                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    SrcAddressList = "Mark - Whatsapp",
                });
            }

            FillMarkPacket("Whatsapp", cantWlan, list, firewallMangles);


            #endregion

            #region Youtube

            if (!firewallMangles.Any(c => c.Layer7.Equals("L7-Youtube") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    Layer7 = "L7-Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true
                });
            }

            if (!firewallMangles.Any(c => c.DstAddress.Equals("190.92.112.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    DstAddress = "190.92.112.0/24",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp"
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("190.92.112.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "190.92.112.0/24"
                });
            }

            if (!firewallMangles.Any(c => c.DstAddress.Equals("172.217.3.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    DstAddress = "172.217.3.0/24",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",

                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("172.217.3.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "172.217.3.0/24"
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("172.217.2.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {

                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "172.217.2.0/24"
                });
            }

            FillMarkPacket("Youtube", cantWlan, list, firewallMangles);

            #endregion

            #region Facebook
            if (!firewallMangles.Any(c => c.Layer7.Equals("L7_Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    Layer7 = "L7_Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true,

                });
            }

            if (!firewallMangles.Any(c => c.DstAddressList.Equals("Mark - Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    DstAddressList = "Mark - Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddressList.Equals("Mark - Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true,
                    SrcAddressList = "Mark - Facebook"
                });
            }
            FillMarkPacket("Facebook", cantWlan, list, firewallMangles);

            #endregion

            #region DNS
            if (!firewallMangles.Any(c => c.DstPort.Equals("53") && c.NewConnectionMark.Equals("Dns") && c.Protocol.Equals("udp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - DNS",
                    DstPort = "53",
                    NewConnectionMark = "Dns",
                    Passthrough = true,
                    Protocol = "udp"
                });
            }
            FillMarkPacket("Dns", cantWlan, list, firewallMangles);

            #endregion

            #region Managment
            if (!firewallMangles.Any(c => c.DstPort.Equals("8291,8728,8729,22,23") && c.NewConnectionMark.Equals("Managment-fw") && c.Protocol.Equals("tcp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Managment (Forward)",
                    DstPort = "8291,8728,8729,22,23",
                    NewConnectionMark = "Managment-fw",
                    Passthrough = false,
                    Protocol = "tcp"
                });
            }

            FillMarkPacket("Managment-fw", cantWlan, list, firewallMangles);

            #endregion

            #region ICMP
            if (!firewallMangles.Any(c => c.NewConnectionMark.Equals("Icmp") && c.Protocol.Equals("icmp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - ICMP",
                    NewConnectionMark = "Icmp",
                    Passthrough = true,
                    Protocol = "icmp"
                });
            }
            FillMarkPacket("Icmp", cantWlan, list, firewallMangles);
            #endregion

            #region Other
            if (!firewallMangles.Any(c => c.NewConnectionMark.Equals("Other") && c.Comment.Equals("Mark - Other")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Other",
                    ConnectionMark = "no-mark",
                    NewConnectionMark = "Other",
                    Passthrough = true,

                });
            }
            FillMarkPacket("Other", cantWlan, list, firewallMangles);
            #endregion



            return list;
        }

        /// <summary>
        /// Obtiene los Mangles que contienen las Marcas de Conexiones y las Marcas de Paquetes para el QoS
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <returns></returns>
        private List<FirewallMangle> GetMangleList(ITikConnection connection)
        {
            var list = new List<FirewallMangle>();

            IEnumerable<InterfaceWireless> cantWlan = connection.LoadAll<InterfaceWireless>();

            List<FirewallMangle> firewallMangles = (List<FirewallMangle>)connection.LoadAll<FirewallMangle>();

            #region Whatsapp/Messenger/IMO & VOIP

            if (!ExistsMangle("Whatsapp", "", "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061", null, "tcp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Whatsapp/Messenger/IMO & VOIP",
                    DstPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "tcp",
                });
            }


            if (!ExistsMangle("Whatsapp", "", "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478", null, "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    DstPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",

                });
            }

            if (!ExistsMangle("Whatsapp", "", null, "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478", "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",
                    SrcPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478",

                });
            }
            if (!ExistsMangle("Whatsapp", "", null, "!53", "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",
                    SrcPort = "!53",

                });
            }

            if (!firewallMangles.Any(c => c.DstAddressList.Equals("Mark - Whatsapp") && c.NewConnectionMark.Equals("Whatsapp")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    DstAddressList = "Mark - Whatsapp",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddressList.Equals("Mark - Whatsapp") && c.NewConnectionMark.Equals("Whatsapp")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",

                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    SrcAddressList = "Mark - Whatsapp",
                });
            }

            FillMarkPacket("Whatsapp", cantWlan, list, firewallMangles);


            #endregion

            #region Youtube

            if (!firewallMangles.Any(c => c.Layer7.Equals("L7-Youtube") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    Layer7 = "L7-Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true
                });
            }

            if (!firewallMangles.Any(c => c.DstAddress.Equals("190.92.112.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    DstAddress = "190.92.112.0/24",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp"
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("190.92.112.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "190.92.112.0/24"
                });
            }

            if (!firewallMangles.Any(c => c.DstAddress.Equals("172.217.3.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    DstAddress = "172.217.3.0/24",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",

                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("172.217.3.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "172.217.3.0/24"
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("172.217.2.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {

                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "172.217.2.0/24"
                });
            }

            FillMarkPacket("Youtube", cantWlan, list, firewallMangles);

            #endregion

            #region Facebook
            if (!firewallMangles.Any(c => c.Layer7.Equals("L7_Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    Layer7 = "L7_Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true,

                });
            }

            if (!firewallMangles.Any(c => c.DstAddressList.Equals("Mark - Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    DstAddressList = "Mark - Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddressList.Equals("Mark - Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true,
                    SrcAddressList = "Mark - Facebook"
                });
            }
            FillMarkPacket("Facebook", cantWlan, list, firewallMangles);

            #endregion

            #region DNS
            if (!firewallMangles.Any(c => c.DstPort.Equals("53") && c.NewConnectionMark.Equals("Dns") && c.Protocol.Equals("udp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - DNS",
                    DstPort = "53",
                    NewConnectionMark = "Dns",
                    Passthrough = true,
                    Protocol = "udp"
                });
            }
            FillMarkPacket("Dns", cantWlan, list, firewallMangles);

            #endregion

            #region Managment
            if (!firewallMangles.Any(c => c.DstPort.Equals("8291,8728,8729,22,23") && c.NewConnectionMark.Equals("Managment-fw") && c.Protocol.Equals("tcp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Managment (Forward)",
                    DstPort = "8291,8728,8729,22,23",
                    NewConnectionMark = "Managment-fw",
                    Passthrough = false,
                    Protocol = "tcp"
                });
            }

            FillMarkPacket("Managment-fw", cantWlan, list, firewallMangles);

            #endregion

            #region ICMP
            if (!firewallMangles.Any(c => c.NewConnectionMark.Equals("Icmp") && c.Protocol.Equals("icmp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - ICMP",
                    NewConnectionMark = "Icmp",
                    Passthrough = true,
                    Protocol = "icmp"
                });
            }
            FillMarkPacket("Icmp", cantWlan, list, firewallMangles);
            #endregion

            #region Other
            if (!firewallMangles.Any(c => c.NewConnectionMark.Equals("Other") && c.Comment.Equals("Mark - Other")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Other",
                    ConnectionMark = "no-mark",
                    NewConnectionMark = "Other",
                    Passthrough = true,

                });
            }
            FillMarkPacket("Other", cantWlan, list, firewallMangles);
            #endregion



            return list;
        }

        /// <summary>
        /// Obtiene los Mangles que contienen los Mark Connection y Mark Packet para el QoS
        /// </summary>
        /// <param name="connection">Conexion del Mikrotik</param>
        /// <param name="wlan">Nombre de la Wireless</param>
        /// <returns></returns>
        private List<FirewallMangle> GetMangleList(ITikConnection connection, string wlan)
        {
            var list = new List<FirewallMangle>();

            List<FirewallMangle> firewallMangles = (List<FirewallMangle>)connection.LoadAll<FirewallMangle>();

            // Cada metodo valida que no exista el Mangle.

            #region Whatsapp/Messenger/IMO & VOIP

            // Estos métodos se quedan, puesto que validan que existan las Marcas de Conexiones

            if (!ExistsMangle("Whatsapp", "", "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061", null, "tcp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Whatsapp/Messenger/IMO & VOIP",
                    DstPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "tcp",
                });
            }


            if (!ExistsMangle("Whatsapp", "", "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478", null, "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    DstPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",

                });
            }

            if (!ExistsMangle("Whatsapp", "", null, "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478", "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",
                    SrcPort = "1167,1719,1720,8010,5222,5223,5228,5240,5243,5244,5248,5060-5061,3478",

                });
            }

            if (!ExistsMangle("Whatsapp", "", null, "!53", "udp", firewallMangles))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    Protocol = "udp",
                    SrcPort = "!53",

                });
            }

            if (!firewallMangles.Any(c => c.DstAddressList.Equals("Mark - Whatsapp") && c.NewConnectionMark.Equals("Whatsapp")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    DstAddressList = "Mark - Whatsapp",
                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddressList.Equals("Mark - Whatsapp") && c.NewConnectionMark.Equals("Whatsapp")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",

                    NewConnectionMark = "Whatsapp",
                    Passthrough = true,
                    SrcAddressList = "Mark - Whatsapp",
                });
            }


            FillMarkPacket("Whatsapp", wlan, list, firewallMangles);


            #endregion

            #region Youtube

            if (!firewallMangles.Any(c => c.Layer7.Equals("L7-Youtube") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    Layer7 = "L7-Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true
                });
            }

            if (!firewallMangles.Any(c => c.DstAddress.Equals("190.92.112.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    DstAddress = "190.92.112.0/24",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp"
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("190.92.112.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "190.92.112.0/24"
                });
            }

            if (!firewallMangles.Any(c => c.DstAddress.Equals("172.217.3.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    DstAddress = "172.217.3.0/24",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",

                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("172.217.3.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "172.217.3.0/24"
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddress.Equals("172.217.2.0/24") && c.Protocol.Equals("tcp") && c.NewConnectionMark.Equals("Youtube")))
            {

                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Youtube",
                    NewConnectionMark = "Youtube",
                    Passthrough = true,
                    Protocol = "tcp",
                    SrcAddress = "172.217.2.0/24"
                });
            }

            FillMarkPacket("Youtube", wlan, list, firewallMangles);

            #endregion

            #region Facebook
            if (!firewallMangles.Any(c => c.Layer7.Equals("L7_Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    Layer7 = "L7_Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true,

                });
            }

            if (!firewallMangles.Any(c => c.DstAddressList.Equals("Mark - Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    DstAddressList = "Mark - Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true
                });
            }

            if (!firewallMangles.Any(c => c.SrcAddressList.Equals("Mark - Facebook") && c.NewConnectionMark.Equals("Facebook")))
            {
                list.Add(new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Facebook",
                    NewConnectionMark = "Facebook",
                    Passthrough = true,
                    SrcAddressList = "Mark - Facebook"
                });
            }

            FillMarkPacket("Facebook", wlan, list, firewallMangles);

            #endregion

            #region DNS
            if (!firewallMangles.Any(c => c.DstPort.Equals("53") && c.NewConnectionMark.Equals("Dns") && c.Protocol.Equals("udp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - DNS",
                    DstPort = "53",
                    NewConnectionMark = "Dns",
                    Passthrough = true,
                    Protocol = "udp"
                });
            }

            FillMarkPacket("Dns", wlan, list, firewallMangles);

            #endregion

            #region Managment
            if (!firewallMangles.Any(c => c.DstPort.Equals("8291,8728,8729,22,23") && c.NewConnectionMark.Equals("Managment-fw") && c.Protocol.Equals("tcp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Managment (Forward)",
                    DstPort = "8291,8728,8729,22,23",
                    NewConnectionMark = "Managment-fw",
                    Passthrough = false,
                    Protocol = "tcp"
                });
            }

            FillMarkPacket("Managment-fw", wlan, list, firewallMangles);

            #endregion

            #region ICMP
            if (!firewallMangles.Any(c => c.NewConnectionMark.Equals("Icmp") && c.Protocol.Equals("icmp")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - ICMP",
                    NewConnectionMark = "Icmp",
                    Passthrough = true,
                    Protocol = "icmp"
                });
            }

            FillMarkPacket("Icmp", wlan, list, firewallMangles);
            #endregion

            #region Other
            if (!firewallMangles.Any(c => c.NewConnectionMark.Equals("Other") && c.Comment.Equals("Mark - Other")))
            {
                list.Add(new FirewallMangle()
                {

                    Action = FirewallMangle.ActionType.MarkConnection,
                    Chain = "prerouting",
                    Comment = "Mark - Other",
                    ConnectionMark = "no-mark",
                    NewConnectionMark = "Other",
                    Passthrough = true,

                });
            }

            FillMarkPacket("Other", wlan, list, firewallMangles);
            #endregion

            return list;
        }

        /// <summary>
        /// Genera una lista de Paquetes para añadir al Mangle
        /// </summary>
        /// <param name="packet">Paquete</param>
        /// <param name="cantWlan">Cantidad de Wlans</param>
        /// <param name="list">Lista para añadir los Mangles</param>
        /// <param name="firewallMangles">Lista de Mangles que se encuentran en el Firewall</param>
        private void FillMarkPacket(string packet, IEnumerable<InterfaceWireless> cantWlan, List<FirewallMangle> list, List<FirewallMangle> firewallMangles)
        {
            if (cantWlan.Count() > 1)
            {
                var usedWlan = cantWlan.Where(c => c.InterfaceType == "virtual").ToList();


                if (packet.Contains("Dns"))
                {
                    for (int i = 0; i < usedWlan.Count; i++)
                    {
                        //if (!ExistsMangle($"{packet}-{usedWlan[i].Name}-Upload", packet, usedWlan[i].Name, null, firewallMangles))
                        //{
                        //    list.Add(new FirewallMangle()
                        //    {
                        //        Action = FirewallMangle.ActionType.MarkPacket,
                        //        Chain = "prerouting",
                        //        ConnectionMark = packet,
                        //        InIterface = usedWlan[i].Name,
                        //        NewPacketMark = $"{packet}-{usedWlan[i].Name}-Upload",
                        //        Passthrough = false,

                        //    });
                        //}


                    }
                }

                if (packet.Contains("Icmp"))
                {
                    for (int i = 0; i < usedWlan.Count; i++)
                    {
                        if (!ExistsMangle($"{packet}-{usedWlan[i].Name}-Download", packet, usedWlan[i].Name, null, firewallMangles))
                        {
                            list.Add(new FirewallMangle()
                            {
                                Action = FirewallMangle.ActionType.MarkPacket,
                                Chain = "prerouting",
                                ConnectionMark = packet,
                                InIterface = usedWlan[i].Name,
                                NewPacketMark = $"{packet}-{usedWlan[i].Name}-Download",
                                Protocol = "icmp",
                                Passthrough = false,

                            });
                        }
                        if (!ExistsMangle($"{packet}-{usedWlan[i].Name}-Upload", packet, null, usedWlan[i].Name, firewallMangles))
                        {
                            list.Add(new FirewallMangle()
                            {

                                Action = FirewallMangle.ActionType.MarkPacket,
                                Chain = "postrouting",
                                ConnectionMark = packet,
                                OutIterface = usedWlan[i].Name,
                                NewPacketMark = $"{packet}-{usedWlan[i].Name}-Upload",
                                Protocol = "icmp",
                                Passthrough = false
                            });
                        }
                    }
                }

                else
                {
                    for (int i = 0; i < usedWlan.Count; i++)
                    {
                        if (!ExistsMangle($"{packet}-{usedWlan[i].Name}-Download", packet, usedWlan[i].Name, null, firewallMangles))
                        {
                            list.Add(new FirewallMangle()
                            {
                                Action = FirewallMangle.ActionType.MarkPacket,
                                Chain = "prerouting",
                                ConnectionMark = packet,
                                InIterface = usedWlan[i].Name,
                                NewPacketMark = $"{packet}-{usedWlan[i].Name}-Download",
                                Passthrough = false,

                            });
                        }
                        if (!ExistsMangle($"{packet}-{usedWlan[i].Name}-Upload", packet, null, usedWlan[i].Name, firewallMangles))
                        {
                            list.Add(new FirewallMangle()
                            {

                                Action = FirewallMangle.ActionType.MarkPacket,
                                Chain = "postrouting",
                                ConnectionMark = packet,
                                OutIterface = usedWlan[i].Name,
                                NewPacketMark = $"{packet}-{usedWlan[i].Name}-Upload",
                                Passthrough = false
                            });
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Genera una lista de Paquetes para añadir al Mangle
        /// </summary>
        /// <param name="packet">Paquete</param>
        /// <param name="wlan">Nombre de la Wlan</param>
        /// <param name="list">Lista para añadir los Mangles</param>
        /// <param name="firewallMangles">Lista de Mangles que se encuentran en el Firewall</param>
        private void FillMarkPacket(string packet, string wlan, List<FirewallMangle> list, List<FirewallMangle> firewallMangles)
        {


            if (packet.Contains("Icmp"))
            {

                if (!ExistsMangle($"{packet}-{wlan}-Download", packet, wlan, null, firewallMangles))
                {
                    list.Add(new FirewallMangle()
                    {
                        Action = FirewallMangle.ActionType.MarkPacket,
                        Chain = "prerouting",
                        ConnectionMark = packet,
                        InIterface = wlan,
                        NewPacketMark = $"{packet}-{wlan}-Download",
                        Protocol = "icmp",
                        Passthrough = false,

                    });
                }
                if (!ExistsMangle($"{packet}-{wlan}-Upload", packet, null, wlan, firewallMangles))
                {
                    list.Add(new FirewallMangle()
                    {

                        Action = FirewallMangle.ActionType.MarkPacket,
                        Chain = "postrouting",
                        ConnectionMark = packet,
                        OutIterface = wlan,
                        NewPacketMark = $"{packet}-{wlan}-Upload",
                        Protocol = "icmp",
                        Passthrough = false
                    });
                }

            }

            else
            {

                if (!ExistsMangle($"{packet}-{wlan}-Download", packet, wlan, null, firewallMangles))
                {
                    list.Add(new FirewallMangle()
                    {
                        Action = FirewallMangle.ActionType.MarkPacket,
                        Chain = "prerouting",
                        ConnectionMark = packet,
                        InIterface = wlan,
                        NewPacketMark = $"{packet}-{wlan}-Download",
                        Passthrough = false,

                    });
                }
                if (!ExistsMangle($"{packet}-{wlan}-Upload", packet, null, wlan, firewallMangles))
                {
                    list.Add(new FirewallMangle()
                    {

                        Action = FirewallMangle.ActionType.MarkPacket,
                        Chain = "postrouting",
                        ConnectionMark = packet,
                        OutIterface = wlan,
                        NewPacketMark = $"{packet}-{wlan}-Upload",
                        Passthrough = false
                    });
                }
            }



        }

        /// <summary>
        /// Elimina todos los Mangles que se encontraban asociadas a la Wireless que se eliminó.
        /// </summary>
        /// <param name="connection"></param>
        public void DeleteMangleInQos(ITikConnection connection, string wlan)
        {
            var firewallMangles = connection.LoadAll<FirewallMangle>().Where(c => c.InIterface.Equals(wlan) || c.OutIterface.Equals(wlan)).ToList();

            foreach (var item in firewallMangles)
            {
                connection.Delete(item);
            }
        }

        /// <summary>
        /// Elimina un Mangle del Mikrotik
        /// </summary>
        /// <param name="mangle"></param>
        /// <param name="credential"></param>
        public void Delete(FirewallMangle mangle, MikrotikCredentials credential)
        {
            Init(credential);

            Connection.Delete(mangle);
            DisposeMkConnection();
        }

        /// <summary>
        /// Comprueba que no exista el Mangle
        /// </summary>
        /// <param name="newPacketMark">Nueva Marca de Paquete</param>
        /// <param name="connectionMark">Marca de Conexion</param>
        /// <param name="inInterface">Interfaz de Entrada</param>
        /// /// <param name="outInterface">Interfaz de Salida</param>
        /// <param name="firewallMangles">Firewall Mangles</param>
        /// <returns></returns>
        private bool ExistsMangle(string newPacketMark, string connectionMark, string inInterface, string outInterface, List<FirewallMangle> firewallMangles)
        {
            if (outInterface != null)
            {
                if (firewallMangles.Any(c => c.NewPacketMark.Equals(newPacketMark) && c.ConnectionMark.Equals(connectionMark) && c.OutIterface.Equals(outInterface)))
                {
                    return true;
                }
            }
            else
            {
                if (firewallMangles.Any(c => c.NewPacketMark.Equals(newPacketMark) && c.ConnectionMark.Equals(connectionMark) && c.InIterface.Equals(inInterface)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Comprueba que no exista el Mangle
        /// </summary>
        /// <param name="NewConnectionMark">Marca de Conexión</param>
        /// <param name="comment">Comentario</param>
        /// <param name="dstPort">Puerto destino</param>
        /// <param name="srcPort">Puerto de Origen</param>
        /// <param name="protocol">Protocolo</param>
        /// <param name="firewallMangles"></param>
        /// <returns></returns>
        private bool ExistsMangle(string NewConnectionMark, string comment, string dstPort, string srcPort, string protocol, List<FirewallMangle> firewallMangles)
        {
            if (dstPort != null)
            {
                if (firewallMangles.Any(c => c.NewConnectionMark.Equals(NewConnectionMark) && c.DstPort.Equals(dstPort) && c.Protocol.Equals(protocol)))
                {
                    return true;
                }
            }
            else
            {
                if (firewallMangles.Any(c => c.NewConnectionMark.Equals(NewConnectionMark) && c.SrcPort.Equals(srcPort) && c.Protocol.Equals(protocol)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Elimina todos los Mnagles dado un comentario
        /// </summary>
        /// <param name="connection"></param>
        public void DeleteMangleMultiByComment(MikrotikCredentials credential, string comment)
        {
            Init(credential);
            var existingMangle = Connection.LoadList<FirewallMangle>(
                Connection.CreateParameter("comment", comment)).ToList();
            var listClonedBackup = existingMangle.CloneEntityList(); //creates clone of all entities in list

            existingMangle.Clear();

            //save differences into mikrotik  (existingAddressList=modified, listClonedBackup=unmodified)
            Connection.SaveListDifferences(existingMangle, listClonedBackup);
            DisposeMkConnection();
        }

        /// <summary>
        /// Elimina todos los Mnagles dado un comentario
        /// </summary>
        /// <param name="connection"></param>
        public void DeleteMangleMultiByComment(ITikConnection connection, string comment)
        {

            var existingMangle = connection.LoadList<FirewallMangle>(
                connection.CreateParameter("comment", comment)).ToList();
            var listClonedBackup = existingMangle.CloneEntityList(); //creates clone of all entities in list

            existingMangle.Clear();

            //save differences into mikrotik  (existingAddressList=modified, listClonedBackup=unmodified)
            connection.SaveListDifferences(existingMangle, listClonedBackup);

        }
    }
}
