﻿namespace wzmCore.Mikrotik
{

    public class Firewall
    {
        public Mangle Mangle => new Mangle();

        public AddressList AddressList => new AddressList();

        public Nat Nat => new Nat();

        public Layer7 Layer7 => new Layer7();

    }
}
