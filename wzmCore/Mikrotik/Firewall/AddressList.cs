﻿using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using tik4net.Objects.Interface;
using System;
using wzmCore.Extensions;

namespace wzmCore.Mikrotik
{
    public class AddressList : MikrotikBaseConnection
    {

        /// <summary>
        /// Elimina una Address List del Firewall del Mikrotik
        /// </summary>
        /// <param name="firewallAddressList"></param>
        /// <param name="credential"></param>
        public void DeleteAddressList(FirewallAddressList firewallAddressList, MikrotikCredentials credential)
        {
            Init(credential);
            var existingAddressList = Connection.LoadList<FirewallAddressList>(
           Connection.CreateParameter("list", firewallAddressList.List),
           Connection.CreateParameter("address", firewallAddressList.Address)).SingleOrDefault();
            if (existingAddressList != null)
                Connection.Delete(existingAddressList);

            Dispose();
        }

        public void DeleteAddressList(FirewallAddressList firewallAddressList, ITikConnection connection)
        {

            var existingAddressList = connection.LoadList<FirewallAddressList>(
           connection.CreateParameter("list", firewallAddressList.List),
           connection.CreateParameter("address", firewallAddressList.Address)).SingleOrDefault();
            if (existingAddressList != null)
                connection.Delete(existingAddressList);


        }

        public void Disable(FirewallAddressList firewallAddressList, ITikConnection connection)
        {
            firewallAddressList.Disabled = true;
            connection.Save(firewallAddressList);
        }

        public void Enable(FirewallAddressList firewallAddressList, ITikConnection connection)
        {
            firewallAddressList.Disabled = false;
            connection.Save(firewallAddressList);
        }

        /// <summary>
        /// Añade o actualiza una Address List al Mikrotik
        /// </summary>
        /// <param name="firewallAddressList"></param>
        /// <param name="credential"></param>
        public void CreateAddressList(FirewallAddressViewModel firewallAddressList, MikrotikCredentials credential)
        {

            Init(credential);

            //Create
            var newAddressList = new FirewallAddressList()
            {
                Address = firewallAddressList.Address,
                List = firewallAddressList.List,
                Comment = firewallAddressList.Usuario
            };
            Connection.Save(newAddressList);
            Dispose();

        }

        /// <summary>
        /// Añade o actualiza una Address List al Mikrotik
        /// </summary>
        /// <param name="firewallAddressList"></param>
        /// <param name="connection"></param>
        public void CreateAddressList(FirewallAddressViewModel firewallAddressList, ITikConnection connection)
        {
            //Create
            var newAddressList = new FirewallAddressList()
            {
                Address = firewallAddressList.Address,
                List = firewallAddressList.List,
                Comment = firewallAddressList.Usuario
            };
            Connection.Save(newAddressList);

        }

        /// <summary>
        /// Actualiza el Address List del Usuario.
        /// </summary>
        /// <param name="firewallAddressList">Address List ya modificado</param>
        /// <param name="credential">Credenciales del Mikrotik</param>
        public void UpdateAddressList(FirewallAddressViewModel firewallAddressList, MikrotikCredentials credential)
        {

            var a = GetAddressList(credential).First(c => c.Id.Equals(firewallAddressList.Id));
            a.List = firewallAddressList.List;
            a.Comment = firewallAddressList.Usuario;
            a.Address = firewallAddressList.Address;
            Init(credential);
            Connection.Save(a);
            Dispose();


        }

        /// <summary>
        /// /// Actualiza el Address List del Usuario.
        /// </summary>
        /// <param name="firewallAddressList">Address List ya modificado</param>
        /// <param name="connection">Conexión del Mikrotik</param>
        public void UpdateAddressList(FirewallAddressList firewallAddressList, ITikConnection connection)
        {
            Connection.Save(firewallAddressList);
        }

        public void UpdateAddressList(FirewallAddressList firewallAddressList, MikrotikCredentials mikrotikCredentials)
        {
            Init(mikrotikCredentials);
            Connection.Save(firewallAddressList);
            Dispose();
        }

        /// <summary>
        /// Actualiza el Address List del Usuario.
        /// </summary>
        /// <param name="firewallAddressList">Address List ya modificado</param>
        /// <param name="connection">Conexión del Mikrotik</param>
        public void UpdateAddressList(FirewallAddressViewModel firewallAddressList, ITikConnection connection)
        {

            var a = GetAddressList(connection).First(c => c.Id.Equals(firewallAddressList.Id));
            if (a != null)
            {
                if (!a.List.Equals(firewallAddressList.List))
                {
                    a.List = firewallAddressList.List;
                    connection.Save(a);
                }
                if (!a.Comment.Equals(firewallAddressList.Usuario))
                {
                    a.Comment = firewallAddressList.Usuario;
                    connection.Save(a);
                }
                if (a.Disabled != firewallAddressList.Disabled)
                {
                    a.Disabled = firewallAddressList.Disabled;
                    connection.Save(a);
                }
                if (!a.Address.Equals(firewallAddressList.Address))
                {
                    a.Address = firewallAddressList.Address;
                    connection.Save(a);
                }
            }

        }

        /// <summary>
        /// Actualiza el Address List del Usuario.
        /// </summary>
        /// <param name="comment">Comentario a Cambiar</param>
        /// <param name="ip">Ip a cambair</param>
        /// <param name="markRoute">Mark Route a Cambiar</param>
        /// <param name="userIp">Ip actual del Usuario para buscar el addressList</param>
        /// <param name="connection">Conexion del Mikrotik</param>
        public void UpdateAddressList(string comment, string ip, string markRoute, string userIp, ITikConnection connection)
        {
            var a = GetAddressList(connection).First(c => c.Address.Equals(userIp));

            if (a != null)
            {
                if (comment != null)
                {
                    a.Comment = comment;
                }

                if (ip != null)
                {
                    a.Address = ip;

                }

                if (markRoute != null)
                {
                    a.List = markRoute;
                }
            }
            connection.Save(a);
        }

        /// <summary>
        /// Añade un Address List al Mikrotik
        /// </summary>
        /// <param name="firewallAddressList"></param>
        /// <param name="credential"></param>
        public void Add(FirewallAddressList firewallAddressList, MikrotikCredentials credential)
        {
            var f = new FirewallAddressList()
            {
                Address = firewallAddressList.Address,
                Comment = firewallAddressList.Comment,
                Disabled = firewallAddressList.Disabled,
                List = firewallAddressList.List,
                Timeout = firewallAddressList.Timeout
            };

            Init(credential);
            Connection.Save(f);
            Dispose();
        }

        /// <summary>
        /// Añade un Address List al Mikrotik
        /// </summary>
        /// <param name="firewallAddressList"></param>
        /// <param name="credential"></param>
        public void Add(FirewallAddressList firewallAddressList, ITikConnection connection)
        {
            var f = new FirewallAddressList()
            {
                Address = firewallAddressList.Address,
                Comment = firewallAddressList.Comment,
                Disabled = firewallAddressList.Disabled,
                List = firewallAddressList.List,
                Timeout = firewallAddressList.Timeout
            };
            connection.Save(f);

        }

        /// <summary>
        /// Deshabilita o Habilita un address list.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="credential"></param>
        public void DisableOrEnable(string id, MikrotikCredentials credential)
        {
            Init(credential);
            var a = GetAddressList(credential).First(c => c.Id == id);

            if (a.Disabled)
            {
                a.Disabled = false;
            }
            else
            {
                a.Disabled = true;
            }

            Connection.Save(a);
            Dispose();
        }

        /// <summary>
        /// Obtiene todos los Address List del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public IEnumerable<FirewallAddressList> GetAddressList(MikrotikCredentials credential)
        {

            Init(credential);
            IEnumerable<FirewallAddressList> firewall = Connection.LoadAll<FirewallAddressList>();
            Dispose();
            return firewall;
        }

        /// <summary>
        /// Obtiene todos los Address List del Mikrotik
        /// </summary>
        /// <param name="connection"></param>
        /// <returns>IEnumerable<FirewallAddressList></returns>
        public IEnumerable<FirewallAddressList> GetAddressList(ITikConnection connection)
        {
            return connection.LoadAll<FirewallAddressList>();
        }

        /// <summary>
        /// Agrega un dispositivo al Address List seleccionando cualquier Wireless
        /// </summary>
        /// <param name="device"></param>
        /// <param name="connection"></param>     
        public string SetAddressList(UserDevice device, ITikConnection connection)
        {
            Random random = new Random();
            var wireless = connection.LoadAll<InterfaceWireless>().Where(c => c.InterfaceType.Equals("virtual")).ToList();
            var routes = new Routes().GetMarkRoutes(connection);
            int randomInt = random.Next(1, wireless.Count());
            var r = routes.Where(c => c.Gateway.Contains(wireless[randomInt].Name)).First();

            FirewallAddressList firewallAddressList = new FirewallAddressList()
            {
                Address = device.Ip,
                List = r.MarkRoute,
                Comment = device.Comment,
                Disabled = false,

            };
            connection.Save(firewallAddressList);
            return r.MarkRoute;

        }

        /// <summary>
        /// Agrega un dispositivo al Address List seleccionando cualquier Wireless
        /// </summary>
        /// <param name="device"></param>
        /// <param name="wireless"></param>
        /// <param name="routes"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public string SetAddressList(UserDevice device, List<InterfaceWireless> wireless, List<RouteViewModel> routes, ITikConnection connection)
        {
            Random random = new Random();

            int randomInt = random.Next(1, wireless.Count());
            var r = routes.Where(c => c.Gateway.Contains(wireless[randomInt].Name)).First();

            FirewallAddressList firewallAddressList = new FirewallAddressList()
            {
                Address = device.Ip,
                List = r.MarkRoute,
                Comment = device.Comment,
                Disabled = false,

            };
            connection.Save(firewallAddressList);
            return r.MarkRoute;

        }

        /// <summary>
        /// Agrega un dispositivo al Address List seleccionando de forma aleatoria un Wireless
        /// </summary>
        /// <param name="device"></param>
        /// <param name="wireless"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public string SetAddressList(UserDevice device, List<InterfaceWireless> wireless, ITikConnection connection)
        {
            var addressList = connection.LoadList<FirewallAddressList>(connection.CreateParameter("address", device.Ip)).ToList();
            Random random = new Random();
            int randomInt = random.Next(1, wireless.Count());
            var r = new Routes().GetMarkRoutes(connection).Where(c => c.Gateway.Contains(wireless[randomInt].Name)).First();
            if (addressList.Any())
            {
                var userAddresList = addressList.FirstOrDefault();
                userAddresList.List = r.MarkRoute;
                connection.Save(userAddresList);
            }
            else
            {
                FirewallAddressList firewallAddressList = new FirewallAddressList()
                {
                    Address = device.Ip,
                    List = r.MarkRoute,
                    Comment = device.Comment,
                    Disabled = false,

                };
                connection.Save(firewallAddressList);

            }
            return r.MarkRoute;
        }

        /// <summary>
        /// Agrega un dispositivo al Address List seleccionando cualquier Wireless
        /// </summary>
        /// <param name="device"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string SetAddressList(UserDevice device, MikrotikCredentials credential)
        {
            Random random = new Random();
            using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                var wireless = new Mikrotik.Wireless().GetAllWireless(connection);
                var routes = new Routes().GetMarkRoutes(connection);

                int randomInt = random.Next(1, wireless.Count());
                var r = routes.Where(c => c.Gateway.Contains(wireless[randomInt].Name)).First();

                FirewallAddressList firewallAddressList = new FirewallAddressList()
                {
                    Address = device.Ip,
                    List = r.MarkRoute,
                    Comment = device.Comment,
                    Disabled = false,

                };
                connection.Save(firewallAddressList);
                return r.MarkRoute;
            }

        }

        /// <summary>
        /// Crea o Cambia el Address List de un dispositivo
        /// </summary>
        /// <param name="device"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public string SetOrChangeAddressList(UserDevice device, ITikConnection connection)
        {
            var addressList = connection.LoadList<FirewallAddressList>(connection.CreateParameter("address", device.Ip)).ToList();

            if (addressList.Any())
            {
                var userAddresList = addressList.FirstOrDefault();

                if (!userAddresList.List.Equals(device.PortalDeOrigen))
                {
                    userAddresList.List = device.PortalDeOrigen;
                    connection.Save(userAddresList);
                }
                return device.PortalDeOrigen;
            }
            else
            {
                return SetAddressList(device, connection);
            }

        }
    }
}
