﻿using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmCore.Extensions;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class Nat : MikrotikBaseConnection
    {
        /// <summary>
        ///  Agrega un Nat al Mikrotik dada una Interfaz Wireless
        /// </summary>
        /// <param name="connection">Conexión al Mikrotik</param>
        /// <param name="wlan">Wireless para añadir su NAT</param>
        public void AddNat(ITikConnection connection, int wlan)
        {
            ITikCommand addNat = connection.CreateCommand("/ip/firewall/nat/add",
              connection.CreateParameter("action", "masquerade"),
              connection.CreateParameter("chain", "srcnat"),
              connection.CreateParameter("out-interface", "wlan" + wlan),
              connection.CreateParameter("routing-table", "salida" + wlan),
              connection.CreateParameter("src-address-list", "salida" + wlan));
            addNat.ExecuteScalar();
        }

        /// <summary>
        /// Agrega un NAT al Mikrotik
        /// </summary>
        /// <param name="nat">NAT a agregar</param>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <param name="connection"></param>
        public void AddCustomNat(FirewallNat nat, MikrotikCredentials credential, ITikConnection connection)
        {
            if (credential == null)
            {
                Connection = connection;
            }
            else
            {
                Init(credential);
            }

            Connection.Save(nat);
            Dispose();
        }

        public List<FirewallNat> GetAllNats(ITikConnection connection)
        {
            return (List<FirewallNat>)connection.LoadAll<FirewallNat>();
        }

        public List<FirewallNat> GetAllNats(MikrotikCredentials credential)
        {
            using ( var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                return (List<FirewallNat>)Connection.LoadAll<FirewallNat>();
            }                                
        }

        /// <summary>
        /// Elimina todos los Nat dado un comentario
        /// </summary>
        /// <param name="connection"></param>
        public void DeleteNatMultiByComment(MikrotikCredentials credential, string comment)
        {
            Init(credential);
            var existingNat = Connection.LoadList<FirewallNat>(
                Connection.CreateParameter("comment", comment)).ToList();
            var listClonedBackup = existingNat.CloneEntityList(); //creates clone of all entities in list

            existingNat.Clear();

            //save differences into mikrotik  (existingAddressList=modified, listClonedBackup=unmodified)
            Connection.SaveListDifferences(existingNat, listClonedBackup);
            Dispose();
        }

        /// <summary>
        /// Elimina todos los Nat dado un comentario
        /// </summary>
        /// <param name="connection"></param>
        public void DeleteNatMultiByComment(ITikConnection connection, string comment)
        {
            
            var existingNat = connection.LoadList<FirewallNat>(
                connection.CreateParameter("comment", comment)).ToList();
            var listClonedBackup = existingNat.CloneEntityList(); //creates clone of all entities in list

            existingNat.Clear();

            //save differences into mikrotik  (existingAddressList=modified, listClonedBackup=unmodified)
            connection.SaveListDifferences(existingNat, listClonedBackup);

        }

        /// <summary>
        /// Obtiene el Nat del Firewall que corresponda a la Interfaz de Salida del Mikrotik.
        /// </summary>
        /// <param name="outInterface"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public FirewallNat GetNat(string outInterface, MikrotikCredentials credential)
        {
            Init(credential);
            try
            {
                var nat = Connection.LoadList<FirewallNat>(Connection.CreateParameter("out-interface", outInterface)).First();
                return nat;
            }
            catch (Exception r)
            {
                throw r;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        /// Comprueba que exista el NAT para los Morosos.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="morososComment"></param>
        /// <returns></returns>
        private bool ExistMorososNat(ITikConnection connection, string morososComment)
        {
            int morosos = connection.LoadAll<FirewallNat>().Where(c => c.Comment.Equals(morososComment)).Count();

            if (morosos < 2)
            {
                return false;
            }
            return true;
        }

        //       /ip firewall nat
        //add action=redirect chain = dstnat comment=\
        //    "Manager - Suspension de clientes(TCP)" dst-port=!8291 in-interface=\
        //    bridge-local protocol = tcp src-address-list=morosos to-ports=999
        //add action = redirect chain=dstnat comment =\
        //    "Manager - Suspension de clientes(UDP)" dst-port=!8291,53 in-interface=\
        //    bridge-local protocol = udp src-address-list=morosos to-ports=999


        /// <summary>
        /// /// Agrega reglas al NAT para los morosos
        /// </summary>
        /// <param name="tikConnection"></param>
        /// <param name="morososComment"></param>
        public void AddMorososNat(ITikConnection connection, string morososComment)
        {
            if (!ExistMorososNat(connection, morososComment))
            {
                var addressList = new List<FirewallNat>();

                addressList.Add(new FirewallNat() { Action = "redirect", Chain = "dstnat", Comment = morososComment, DstPort = "!8291", InInterface = "ether1", Protocol = "tcp", SrcAddressList = morososComment, ToPorts = 999 });
                addressList.Add(new FirewallNat() { Action = "redirect", Chain = "dstnat", Comment = morososComment, DstPort = "!8291", InInterface = "ether1", Protocol = "udp", SrcAddressList = morososComment, ToPorts = 999 });

                //var a = connection.LoadAll<FirewallNat>().Where(c => !c.Disabled).First();

                // Mas adelante ponerlas reglas de primeras.

                foreach (var item in addressList)
                {
                    connection.Save(item);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outInterface"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public FirewallNat GetNat(string outInterface, ITikConnection connection)
        {

            try
            {
                var nat = connection.LoadList<FirewallNat>(connection.CreateParameter("out-interface", outInterface)).First();
                return nat;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
