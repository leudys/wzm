﻿using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class Layer7 : MikrotikBaseConnection
    {
        /// <summary>
        /// Añade el Layer7 para el QoS
        /// </summary>
        /// <param name="credential"></param>
        public void PriorityLayer7(MikrotikCredentials credential)
        {

            Init(credential);

            if (!ExistLayer7(Connection))
            {
                List<FirewallLayer7> list = new List<FirewallLayer7>() { new FirewallLayer7() { Name = "L7-Youtube", Regexp = "^..+\\.(youtube.com|googlevideo.com|akamaihd.net).*\\$" },
                new FirewallLayer7() { Name = "L7_Facebook", Regexp = "^..+\\.(facebook.com|facebook.net|fbcdn.com|fbsbx.com|fbcdn.net|fb.com|tfbnw.net).*\\$" }
            };

                foreach (var item in list)
                {
                    Connection.Save(item);
                }
            }
            DisposeMkConnection();
        }

        /// <summary>
        /// Añade el Layer7 para el QoS
        /// </summary>
        /// <param name="connection"></param>
        public void PriorityLayer7(ITikConnection connection)
        {
            if (!ExistLayer7(connection))
            {
                List<FirewallLayer7> list = new List<FirewallLayer7>() { new FirewallLayer7() { Name = "L7-Youtube", Regexp = "^..+\\.(youtube.com|googlevideo.com|akamaihd.net).*\\$" },
                new FirewallLayer7() { Name = "L7_Facebook", Regexp = "^..+\\.(facebook.com|facebook.net|fbcdn.com|fbsbx.com|fbcdn.net|fb.com|tfbnw.net).*\\$" }
            };

                foreach (var item in list)
                {
                    connection.Save(item);
                }
            }        
        }

        public bool ExistLayer7(ITikConnection connection)
        {
            var layer7 = connection.LoadAll<FirewallLayer7>();

            if (layer7.Any(c => c.Name.Equals("L7_Facebook")) || layer7.Any(c => c.Name.Equals("L7-Youtube")))
            {
                return true;
            }
            return false;
        }
    }
}
