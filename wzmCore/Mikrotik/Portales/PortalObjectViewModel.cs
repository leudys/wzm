﻿using System;
using System.Threading.Tasks;
using tik4net;
using wzmData.Models;
using wzmCore.Data.Mikrotik;

namespace wzmCore.Mikrotik
{

    public class PortalObjectViewModel
    {
        /// <summary>
        /// Obtiene todos los objetos que el Portal Nauta va a utilizar
        /// </summary>
        public async Task<MkObjectListViewModel> GetPortalNautaAsync(string w, MikrotikCredentials credential)
        {
            MkObjects portalObjects = new MkObjects();
            MkObjectListViewModel portalObjectListViewModel = await GetPortalObjectList(credential, new WzmObjectMk(), w);
            return portalObjectListViewModel;
        }

        public async Task<MkObjectListViewModel> GetPortalNautaAsync(string w, ITikConnection connection)
        {
            MkObjects portalObjects = new MkObjects();
            MkObjectListViewModel portalObjectListViewModel = await GetPortalObjectListAsync(connection, new WzmObjectMk(), w);
            return portalObjectListViewModel;
        }

        public MkObjectListViewModel GetPortalNauta(string w, ITikConnection connection)
        {
            MkObjects portalObjects = new MkObjects();
            MkObjectListViewModel portalObjectListViewModel = GetPortalObjectList(connection, new WzmObjectMk(), w);
            return portalObjectListViewModel;
        }

        /// <summary>
        /// Obtiene los objetos del Mikrotik que compone un portal.
        /// </summary>
        /// <see cref="MkObjectListViewModel"/>
        /// <param name="credential"></param>
        /// <param name="portalObjects"></param>
        /// <returns></returns>
        private async Task<MkObjectListViewModel> GetPortalObjectList(MikrotikCredentials credential, WzmObjectMk mk, string w)
        {
            try
            {
                MkObjectListViewModel mkList = await Task.Run(() => new MkObjectListViewModel()
                {
                    IpDhcpClient = mk.DhcpClient.GetDhcpClient(w, credential),
                    Nat = mk.Nat.GetNat(w, credential),
                    Wireless = mk.Wireless.GetWirelessViewModel(w, credential)
                });

                var route = mk.Routes.GetRoute(w, credential);
                mkList.Route = route;
                mkList.Mangle = mk.Mangle.GetMangle(route.MarkRoute, credential);

                return mkList;

            }
            catch (Exception r)
            {

                throw r;
            }
        }

        private async Task<MkObjectListViewModel> GetPortalObjectListAsync(ITikConnection connection, WzmObjectMk mk, string w)
        {
            if (connection.IsOpened)
            {
                try
                {
                    MkObjectListViewModel mkList = await Task.Run(() => new MkObjectListViewModel()
                    {
                        IpDhcpClient = mk.DhcpClient.GetDhcpClient(w, connection),
                        Nat = mk.Nat.GetNat(w, connection),
                        Wireless = mk.Wireless.GetWirelessViewModel(w, connection)
                    });

                    var route = mk.Routes.GetRoute(w, connection);
                    mkList.Route = route;
                    mkList.Mangle = mk.Mangle.GetMangle(route.MarkRoute, connection);

                    return mkList;

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                throw new NotImplementedException();
            }

        }

        private MkObjectListViewModel GetPortalObjectList(ITikConnection connection, WzmObjectMk mk, string w)
        {
            if (connection.IsOpened)
            {
                try
                {
                    MkObjectListViewModel mkList = new MkObjectListViewModel()
                    {
                        IpDhcpClient = mk.DhcpClient.GetDhcpClient(w, connection),
                        Nat = mk.Nat.GetNat(w, connection),
                        Wireless = mk.Wireless.GetWirelessViewModel(w, connection)
                    };

                    var route = mk.Routes.GetRoute(w, connection);
                    mkList.Route = route;
                    mkList.Mangle = mk.Mangle.GetMangle(route.MarkRoute, connection);

                    return mkList;

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                throw new NotImplementedException();
            }

        }
    }








}
