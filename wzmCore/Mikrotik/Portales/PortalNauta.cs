﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface;
using tik4net.Objects.Ip;
using wzmCore.Base;
using wzmCore.Mikrotik.Configuration;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using wzmCore.Services;
using wzmCore.Extensions;

namespace wzmCore.Mikrotik
{
    public class PortalNauta : MikrotikBaseConnection
    {

        public bool Working { get; set; }

        #region Private memebers
        private MikrotikCredentials _credential;
        private static PortalObjectViewModel _portalObjectViewModel => new PortalObjectViewModel();
        private InternetInformation _internetInfo => new InternetInformation();
        private List<PortalesEtecsa> _portales { get; set; }

        #endregion


        #region Add new Portals

        /// <summary>
        /// Añande X portales de Wifi_Etecsa al mikrotik
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="gateway"></param>
        /// <param name="credential"></param>
        public void AddNautaPortal(int cantidad, string gateway, bool qos, MikrotikCredentials credential)
        {
            Init(credential);
            WzmObjectMk wzmCore = new WzmObjectMk();
            // Obtener todas las wlan que hay en ese momento
            // para coger la ultima y comenzar a crear las wlans consecutivamente
            var wireless = wzmCore.Wireless.GetAllWirelessViewModel(credential);

            //Última vlan
            WirelessInterfaceModel interfaceWireless = wireless.Last();

            string[] portalNumName = interfaceWireless.Name.GetNumAndName();


            var QoS = new QoS();

            if (qos)
            {
                // Compruebo que el Layer7 y QueueType existan, si no existen, los creo.
                QoS.CheckQoS(Connection);

            }


            int count = int.Parse(portalNumName[1]);
            try
            {
                for (int i = 0; i < cantidad; i++)
                {
                    ++count;
                    CreatePortal(count, Connection, gateway, wzmCore);

                    if (qos)
                    {
                        // El número se incrementa porque ya la interfaz {num} se incremento en uno ya que se creó otra interfaz
                        QoS.AddQosToPortal($"{portalNumName[0]}{count}", count, portalNumName[0], Connection);
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Añade un portal de Wifi_Etecsa al Mikrotik
        /// </summary>
        /// <param name="wlan">Número de la últma Wlan incrementado en uno</param>
        /// <param name="num">Número de Wlan sin ser incrementado</param>
        /// <param name="nameWlan">Nombre de la Wlan</param>
        /// <param name="connection">Conexión al Mikrotik</param>
        /// <param name="gateway">Puerta de enlace</param>
        /// <param name="qos">Calidad de Servicio</param>
        /// <param name="wzmCore"></param>
        private void CreatePortal(int wlan, ITikConnection connection, string gateway, WzmObjectMk wzmCore)
        {
            // Analiza si el dhcp client de la wlan1 necesita cambiar el parámetro AddDefaultRoute
            IpDhcpClient dhcpClient = connection.LoadAll<IpDhcpClient>().First();
            if (dhcpClient.AddDefaultRoute != IpDhcpClient.AddDefaultRouteType.No)
            {
                dhcpClient.AddDefaultRoute = IpDhcpClient.AddDefaultRouteType.No;
                connection.Save(dhcpClient);
            }


            // Crea una WLAN               
            wzmCore.Wireless.AddWireless(connection, wlan);

            // Crea un DHCP Client
            wzmCore.DhcpClient.Add(connection, wlan);

            // Crea el Mangle
            wzmCore.Mangle.AddMangleForPortal(connection, wlan);

            // Crea la Ruta
            wzmCore.Routes.AddRoute(connection, gateway, wlan);

            //Crea el Nateo
            wzmCore.Nat.AddNat(connection, wlan);



        }

        #endregion

        #region GetPortals
        /// <summary>
        /// Obtiene todos los portales que existan en el Mikrotik.
        /// </summary>
        /// <param name="credential">Credencial del Mikrotik</param>
        /// <returns></returns>
        public async Task<List<PortalesEtecsa>> GetPortalesAsync(MikrotikCredentials credential, bool withInternet)
        {
            _credential = credential;
            if (_portales == null)
            {
                _portales = new List<PortalesEtecsa>();
            }

            List<Task<PortalesEtecsa>> tasks = new List<Task<PortalesEtecsa>>();

            if (withInternet)
            {
                _portales = await GetPortalsWithInterAync(_portales);
            }
            else
            {
                if (_portales.Count() > 0)
                {
                    _portales.Clear();
                }
                var w = await Task.Run(() => new Wireless().GetAllWirelessViewModel(credential));
                _portales = await GetPortalsWithNoInter(w, tasks);
            }

            return _portales;
        }

        /// <summary>
        /// Obtiene solamente las interfaces Wireless del dispositivo.
        /// </summary>
        /// <param name="credential">Credenciales del dispositivo.</param>
        /// <returns></returns>
        public async Task<List<WirelessInterfaceModel>> GetPortalesAsync(MikrotikCredentials credential)
        {
            return await Task.Run(() => new Wireless().GetAllWirelessViewModel(credential));
        }

        public List<WirelessInterfaceModel> GetPortalesbyUser(ITikConnection connection, string user)
        {            
            var nat = new Nat().GetAllNats(connection);
            var addresList = new AddressList().GetAddressList(connection).Where(c => c.Comment.Contains(user)).OrderBy(c => c.List);
            List<WirelessInterfaceModel> pe = new List<WirelessInterfaceModel>();
            int index = 1;

            foreach (var list in addresList)
            {
                try
                {
                    var n = nat.FirstOrDefault(c => c.SrcAddressList.Equals(list.List));
                    pe.Add(new WirelessInterfaceModel()
                    {
                        Id = index++,
                        Name = n.OutInterface
                    });
                }
                catch (Exception)
                {

                }              
            }
            return pe;
        }

        /// <summary>
        /// Obtiene una lista de todos los Portales incluyendo Internet
        /// </summary>
        /// <param name="wireless"></param>
        /// <returns></returns>
        private async Task<List<PortalesEtecsa>> GetPortalsWithInterAync(List<PortalesEtecsa> _portales)
        {
            List<Task<InterViewModel>> internetTasks = new List<Task<InterViewModel>>();

            try
            {
                Init(_credential);
                var routes = new Routes().GetMarkRoutes(Connection);
                Dispose();
                foreach (var item in _portales)
                {    //Obtengo todos los portales en paralelo
                    //tasks.Add(Task.Run(() => DoWork(_portalObjectViewModel, item)));
                    // Obtengo todas las salidas que tengan Internet, en pararelo puesto que este proceso demora
                    internetTasks.Add(Task.Run(() => GetInternetInfo(item.Interfaz, routes)));

                }
                // Espero a que ambas tareas termien
                //var port = await Task.WhenAll(tasks);
                var inter = await Task.WhenAll(internetTasks);


                //var portales = port.ToList();
                var internet = inter.ToList();
                FillId(_portales, internet);

                foreach (var item in _portales)
                {
                    item.Internet = internet.First(c => c.Id == item.Id).Internet;
                }

                return _portales;
            }
            catch (Exception)
            {
                return null;

            }

        }

        #endregion

        #region GetPortals Helpers
        /// <summary>
        /// Llena el campo Id de cada objeto.
        /// </summary>
        /// <param name="portales"></param>
        /// <param name="internet"></param>
        private void FillId(List<PortalesEtecsa> portales, List<InterViewModel> internet)
        {
            int i = 1;
            portales.ForEach(c => c.Id = i++);
            if (internet != null)
            {
                i = 1;
                internet.ForEach(c => c.Id = i++);
            }

        }

        /// <summary>
        /// Obtiene una lista de Portales incluyendo Internet
        /// </summary>
        /// <param name="wireless"></param>
        /// <returns></returns>
        private async Task<List<PortalesEtecsa>> GetPortalsWithNoInter(List<WirelessInterfaceModel> wireless, List<Task<PortalesEtecsa>> tasks)
        {
            Init(_credential);

            foreach (var item in wireless)
            {

                if (item.Name != "wlan1" && !item.Disabled)
                {
                    tasks.Add(Task.Run(() => DoWork(_portalObjectViewModel, item)));

                }
                else if (item.Disabled)
                {
                    Log.WriteErrorMessage(Connection, item.Name + " se encuentra deshabilitada");
                }

            }
            var result = await Task.WhenAll(tasks);
            Dispose();
            var r = result.ToList();
            FillId(r, null);
            return r;



        }


        /// <summary>
        /// Checkeo si la interfaz tiene internet
        /// </summary>
        /// <param name="i"></param>
        /// <param name="name"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        private InterViewModel GetInternetInfo(string name, List<RouteViewModel> routes)
        {
            var r = routes.Where(c => c.Gateway.Contains(name)).First();

            return new InterViewModel() { Internet = _internetInfo.HasInternet(_credential, r.MarkRoute) };
        }

        /// <summary>
        /// Hace el trabajo de llenar el Portal con todo sus datos.
        /// </summary>
        /// <param name="withInter"></param>
        /// <param name="_portalObjectViewModel"></param>
        /// <param name="credential"></param>
        /// <param name="wireless"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private async Task<PortalesEtecsa> DoWork(PortalObjectViewModel _portalObjectViewModel, WirelessInterfaceModel wireless)
        {
            var o = new PortalObjectViewModel();

            var po = await o.GetPortalNautaAsync(wireless.Name, _credential);

            var portal = new PortalesEtecsa()
            {

                Interfaz = wireless.Name,
                DhcpClient = po.IpDhcpClient.Address,
                Mangle = string.Format("{0}" + " " + "{1}" + " " + "{2}",
                            po.Mangle.Chain,
                            po.Mangle.SrcAddressList,
                            po.Mangle.NewPacketMark),
                Nat = string.Format("{0}" + " " + "{1}" + " " + "{2}", po.Nat.Chain, po.Nat.OutInterface, po.Nat.SrcAddressList),
                Route = po.Route.Gateway,
                Enabled = wireless.Disabled,
                //NatComment = po.Nat.Wh
            };

            return portal;
        }
        #endregion

        #region Delete or Disable
        /// <summary>
        /// Deshablita un Portal
        /// </summary>
        /// <param name="wireless">Interfaz a deshabilitar</param>
        /// <param name="MikrotikCredentials">Credencial del Mikrotik</param>
        public async Task Disable(string wireless, MikrotikCredentials credential)
        {
            List<PortalesEtecsa> portales = new List<PortalesEtecsa>();
            portales = await GetPortalesAsync(/*portales,*/ credential, false);
            PortalesEtecsa i = portales.Where(c => c.Interfaz == wireless).First();

        }

        /// <summary>
        /// Elimina portales del Mikrotik dado un rango
        /// </summary>
        /// <param name="desde">Interfaz Inicial a eliminar</param>
        /// <param name="hasta">Interfaz Final a eliminar</param>
        /// <param name="MikrotikCredentials">Credencial del Mikrotik</param>
        public async Task DeleteByRange(string desde, string hasta, MikrotikCredentials credential)
        {
            if (_portales == null)
            {
                _portales = new List<PortalesEtecsa>();
            }
            if (_portales.Count == 0)
            {
                _portales = await GetPortalesAsync(credential, false);
            }

            // Cojo la interfaz inicial para comenzar a eliminar
            PortalesEtecsa i = _portales.Where(c => c.Interfaz == desde).First();

            // Cojo la últma interfaz para que sea donde pare la eliminación
            PortalesEtecsa f = _portales.Where(c => c.Interfaz == hasta).First();

            // Obtengo la lista de portales que se van a eliminar
            var p = _portales.Where(c => c.Id >= i.Id && c.Id <= f.Id);

            // Lista de tareas para usar paralelismo
            List<Task> tasks = new List<Task>();

            // Todas las wirelles
            var wi = new Wireless().GetAllWireless(_credential);

            // Todas las Rutas
            var r = new Routes().GetRoutes(_credential);

            foreach (var item in p)
            {
                var wireless = wi.First(c => c.Name == item.Interfaz);
                var route = r.First(c => c.Gateway.Contains(item.Interfaz));
                await DeletePortalAsync(item.Interfaz, credential, wireless, route);

            }

            await Task.WhenAll(tasks);
            foreach (var item in p)
            {
                _portales.Remove(item);
            }
        }

        /// <summary>
        /// Elimina un solo portal del Mikrotik
        /// </summary>
        /// <param name="w">Interfaz seleccionada por el usuario para ser eliminada</param>
        /// <param name="credential">Credencial del Mikrotik</param>
        public async Task DeletePortalAsync(string w, MikrotikCredentials credential, InterfaceWireless wireless, IpRoutes route)
        {
            try
            {
                Init(credential);
                var r = new Routes().GetRoute(wireless.Name, credential);
                var portalObjectViewModel = await Task.Run(() => _portalObjectViewModel.GetPortalNautaAsync(w, credential));

                DeleteOrDisable(true, portalObjectViewModel, wireless, route);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        ///  Elimina un solo portal del Mikrotik
        /// </summary>
        /// <param name="w"></param>
        /// <param name="connection"></param>
        /// <param name="wireless"></param>
        /// <param name="route"></param>
        /// <returns></returns>
        public void DeletePortal(string w, ITikConnection connection, InterfaceWireless wireless, IpRoutes route)
        {
            try
            {
                new QoS().DeleteQoSfromPortal(connection, wireless.Name);
                var r = new Routes().GetRoute(wireless.Name, connection);
                var portalObjectViewModel = _portalObjectViewModel.GetPortalNauta(w, connection);

                DeleteOrDisable(true, portalObjectViewModel, wireless, route);


            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Elimina o Deshabilita un portal en específico
        /// </summary>
        /// <param name="delete"></param>
        /// <param name="portalObjectViewModel"></param>
        private void DeleteOrDisable(bool delete, MkObjectListViewModel portalObjectViewModel, InterfaceWireless wi, IpRoutes route)
        {

            if (delete)
            {

                Connection.Delete(wi);
                Connection.Delete(portalObjectViewModel.IpDhcpClient);
                Connection.Delete(portalObjectViewModel.Nat);
                Connection.Delete(route);
                Connection.Delete(portalObjectViewModel.Mangle);
            }
            else
            {
                wi.Disabled = true;
                portalObjectViewModel.IpDhcpClient.Disabled = true;
                portalObjectViewModel.Nat.Disabled = true;
                portalObjectViewModel.Route.Disabled = true;
                portalObjectViewModel.Mangle.Disabled = true;
                Connection.Save(wi);
                Connection.Save(portalObjectViewModel.IpDhcpClient);
                Connection.Save(portalObjectViewModel.Nat);
                Connection.Save(route);
                Connection.Save(portalObjectViewModel.Mangle);

            }
        }
        #endregion
    }
}
