﻿namespace wzmCore.Mikrotik
{
    /// <summary>
    /// Wzm Core Objects to get the list
    /// </summary>
    public class WzmObjectMk
    {
        public DhcpClient DhcpClient { get; set; } = new DhcpClient();
        public Mangle Mangle { get; set; } = new Mangle();
        public Routes Routes { get; set; } = new Routes();
        public Nat Nat { get; set; } = new Nat();
        public Wireless Wireless { get; set; } = new Wireless();
    }
}
