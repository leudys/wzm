﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Extensions;
using wzmCore.PingEnviroment;
using wzmCore.Services.Helpers;
using wzmData.Models;

namespace wzmCore.Mikrotik.Portales
{
    public class MkLoginNauta
    {
        private PingSitioEtecsa _pingSitioEtecsa;

        public MkLoginNauta()
        {
            _pingSitioEtecsa = new PingSitioEtecsa();
            _pingSitioEtecsa.MaxCount = "2";


        }

        /// <summary>
        /// Comprueba que que haya acceso a la red Nacional
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="vpnServerNacional"></param>
        /// <returns></returns>
        public bool LoggedIn(ITikConnection connection, CuentaNauta cuenta, ILogger<object> logger = null, bool log = false)
        {
            if (log)
            {
                try
                {
                    LoggerHelper.Log(connection, logger, Services.Helpers.LogLevel.Warning, $"Haciendo PING a: {_pingSitioEtecsa.Host} con portal: {cuenta.Portal.Route}.");
                    var pings = _pingSitioEtecsa.MakePing(connection, cuenta.Portal.Route);
                    if (pings.All(c => c.PacketLoss.Equals("100")))
                    {
                        LoggerHelper.Log(connection, logger, Services.Helpers.LogLevel.Warning, $"El PING ha fallado en el portal: {cuenta.Portal.Route}.");
                        return false;
                    }
                    LoggerHelper.Log(connection, logger, Services.Helpers.LogLevel.Warning, $"El PING realizado correctamente en el portal: {cuenta.Portal.Route}.");
                    return true;
                }
                catch (Exception e)
                {
                    LoggerHelper.Log(connection, logger, wzmCore.Services.Helpers.LogLevel.Error, $"Error haciendo PING con la cuenta: {cuenta.User}.Error: {e.Message}");
                    connection.LogError(e.Message);
                    return false;
                }
            }
            else
            {
                try
                {

                    var pings = _pingSitioEtecsa.MakePing(connection, cuenta.Portal.Route);
                    if (pings.All(c => c.PacketLoss.Equals("100")))
                    {

                        return false;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    LoggerHelper.Log(connection, logger, wzmCore.Services.Helpers.LogLevel.Error, $"Error haciendo PING con la cuenta: {cuenta.User}.Error: {e.Message}");
                    connection.LogError(e.Message);
                    return false;
                }
            }

        }

        /// <summary>
        /// Conecta el mikrotik a la cuenta nacional y activa una VPN
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="cuenta"></param>
        /// <param name="vpns"></param>
        /// <param name="gateway"></param>
        /// <returns></returns>
        //public void Login(MikrotikCredentials credential, CuentaNauta cuenta, List<VpnServer> vpns, string gateway)
        //{

        //    using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
        //    {
        //        var pings = _pingSitioEtecsa.MakePing(connection, cuenta.Portal.Route);

        //        if (pings.All(c => c.PacketLoss.Equals("100")))
        //        {
        //            //Si no hay conexión a la cuenta nacional pues entonces las VPN han de estar caida
        //            // Ojo el ping pudo haber fallado y sin embargo haya conexión
        //            //Comprobar el estado de la VPN

        //            string login = MakeLoginNauta(connection, cuenta, gateway);

        //            if (login.Equals("ok") || login.Equals("connecting"))
        //            {
        //                StartVPN(vpns);
        //            }
        //            else
        //            {
        //                // Dio error la cuenta
        //            }
        //        }
        //    }
        //}

        //public void Login(ITikConnection connection, CuentaNauta cuenta, List<VpnServer> vpns, string gateway)
        //{


        //    var pings = _pingSitioEtecsa.MakePing(connection, cuenta.Portal.Route);

        //    if (pings.All(c => c.PacketLoss.Equals("100")))
        //    {
        //        //Si no hay conexión a la cuenta nacional pues entonces las VPN han de estar caida
        //        // Ojo el ping pudo haber fallado y sin embargo haya conexión
        //        //Comprobar el estado de la VPN

        //        string login = MakeLoginNauta(connection, cuenta, gateway);

        //        if (login.Equals("ok") || login.Equals("connecting"))
        //        {
        //            StartVPN(vpns);
        //        }
        //        else
        //        {
        //            // Dio error la cuenta
        //        }
        //    }

        //}

        public bool Login(ITikConnection connection, CuentaNauta cuenta, string gateway)
        {

            if (!LoggedIn(connection, cuenta))
            {
                //Si no hay conexión a la cuenta nacional pues entonces las VPN han de estar caida
                // Ojo el ping pudo haber fallado y sin embargo haya conexión
                //Comprobar el estado de la VPN

                string login = MakeLoginNauta(connection, cuenta.Portal, cuenta, gateway);

                if (login.Equals("ok") || login.Equals("connecting"))
                {
                    // si el login devuelve "connecting" puede ser un falso positivo
                    // vuelvo a hacer ping a "etecsa.cu"
                    if (!LoggedIn(connection, cuenta))
                    {
                        return false;
                    }

                    return true;
                }
                else
                {
                    return false;
                    // Dio error la cuenta
                }
            }
            return true;
        }

        public bool Login(ITikConnection connection, ITikConnection adminDevice, CuentaNauta cuenta, string gateway)
        {

            if (!LoggedIn(connection, cuenta))
            {
                //Si no hay conexión a la cuenta nacional pues entonces las VPN han de estar caida
                // Ojo el ping pudo haber fallado y sin embargo haya conexión
                //Comprobar el estado de la VPN

                string login = MakeLoginNauta(connection, adminDevice, cuenta.Portal, cuenta, gateway);

                if (login.Equals("ok") || login.Equals("connecting"))
                {
                    // si el login devuelve "connecting" puede ser un falso positivo
                    // vuelvo a hacer ping a "etecsa.cu"
                    if (!LoggedIn(connection, cuenta))
                    {
                        LoggerHelper.Log(connection, Services.Helpers.LogLevel.Error, $"No se ha podido conectar la cuenta nauta, razon:{login}");
                        LoggerHelper.Log(adminDevice, Services.Helpers.LogLevel.Error, $"No se ha podido conectar la cuenta nauta, razon:{login}");
                        return false;
                    }

                    return true;
                }
                else
                {
                    LoggerHelper.Log(connection, Services.Helpers.LogLevel.Error, $"No se ha podido conectar la cuenta nauta, razon:{login}");
                    LoggerHelper.Log(adminDevice, Services.Helpers.LogLevel.Error, $"No se ha podido conectar la cuenta nauta, razon:{login}");
                    return false;
                    // Dio error la cuenta
                }
            }
            return true;
        }

        /// <summary>
        /// Realiza un release al DHCP Client donde se encuentra la interfaz de la VPN
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="interfaz"></param>
        public void DisconnectAccount(ITikConnection connection, string interfaz, string user = "")
        {
            var lease = connection.LoadSingle<IpDhcpClient>(connection.CreateParameter("interface", interfaz));

            if (lease.Status == IpDhcpClient.ClientStatus.Bound)
            {
                LoggerHelper.Log(connection, Services.Helpers.LogLevel.Info, $"Desconectando usuario: {interfaz}.");
                lease.Release(connection);
            }


        }

        public void DisconnectAccount(ITikConnection connection, ILogger<object> logger, string interfaz, string user)
        {
            var lease = connection.LoadSingle<IpDhcpClient>(connection.CreateParameter("interface", interfaz));

            if (lease.Status == IpDhcpClient.ClientStatus.Bound)
            {
                LoggerHelper.Log(connection, logger, Services.Helpers.LogLevel.Info, $"Desconectando usuario: {user}.");
                lease.Release(connection);
            }


        }

        private void MakeLoginNauta(MikrotikCredentials credential, CuentaNauta cuenta, string gateway)
        {
            // Aqui se puede dar el caso que la interfaz se esté conectada
            // Comprobar que la interfaz esté conectada y que el estado del dhcp Client esté en bound
            string portalNauta = "10.180.0.30";
            using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                var routes = connection.LoadList<IpRoutes>();
                IpRoutes ipRoutes = new IpRoutes();
                if (!routes.Any(c => c.DstAddress.Contains(portalNauta)))
                {

                    ipRoutes.DstAddress = portalNauta;
                    //ipRoutes.Gateway = $"{gateway}%{cuenta.Portal.Interfaz}";

                    connection.Save(ipRoutes);
                }
                string result = string.Empty;

                ITikCommand cmd = connection.CreateCommand("/tool/fetch",
                    connection.CreateParameter("mode", "https"),
                    connection.CreateParameter("http-method", "post"),
                    connection.CreateParameter("url", "https://secure.etecsa.net:8443/LoginServlet"),
                    connection.CreateParameter("http-data", $"username={cuenta.User}@nauta.co.cu&password={cuenta.Password}"),
                    connection.CreateParameter("output", "user"),
                    connection.CreateParameter("as-value", ""));
                try
                {
                    result = cmd.ExecuteScalar();
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("302"))
                    {
                        //Se ha conectado
                        connection.Delete(ipRoutes);
                    }
                }


            }
        }

        /// <summary>
        /// Conecta la cuenta Nauta
        /// </summary>
        /// <param name="connection">Conexión del dispositivo conectado a Etecsa</param>
        /// <param name="portal">Portal donde se conectará la cuenta</param>
        /// <param name="cuenta">Usuario y Contraseña del portal</param>
        /// <param name="gateway">Gateway de Etecsa</param>
        /// <returns></returns>
        private string MakeLoginNauta(ITikConnection connection, PortalesEtecsa portal, CuentaNauta cuenta, string gateway)
        {
            // Aqui se puede dar el caso que la interfaz se esté conectada
            // Comprobar que la interfaz esté conectada y que el estado del dhcp Client esté en bound
            string portalNauta = "10.180.0.30";
            string result = string.Empty;
            var routes = connection.LoadList<IpRoutes>();
            IpRoutes ipRoutes = new IpRoutes();
            if (!routes.Any(c => c.DstAddress.Contains(portalNauta)))
            {

                ipRoutes.DstAddress = portalNauta;
                ipRoutes.Gateway = $"{gateway}%{portal.Interfaz}";
                connection.LogWarning($"Creando ruta para la conexion de la cuenta.");
                connection.Save(ipRoutes);
            }

            ITikCommand cmd = connection.CreateCommand("/tool/fetch",
                    connection.CreateParameter("mode", "https"),
                    connection.CreateParameter("http-method", "post"),
                    connection.CreateParameter("url", "https://secure.etecsa.net:8443/LoginServlet"),
                    connection.CreateParameter("http-data", $"username={cuenta.User}&password={cuenta.Password}"),
                    connection.CreateParameter("output", "user"),
                    connection.CreateParameter("as-value", ""));
            try
            {

                result = cmd.ExecuteScalar();
                connection.LogWarning($"Eliminando ruta");
                connection.Delete(ipRoutes);
                return result;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("302"))
                {
                    //Se ha conectado
                    try
                    {
                        connection.LogWarning($"Eliminando ruta");
                        connection.Delete(ipRoutes);
                        return "ok";
                    }
                    catch (Exception)
                    {
                        return "error";
                    }



                }
                else
                {
                    connection.LogError($"Error conectando cuenta:{e.Message}");
                    connection.LogWarning($"Eliminando ruta");
                    connection.Delete(ipRoutes);
                    return "error";
                }
            }


        }

        /// <summary>
        /// Conecta la cuenta Nauta
        /// </summary>
        /// <param name="connection">Conexión del dispositivo conectado a Etecsa</param>
        /// <param name="adminConnection">Conexión del dispositivo Administrativo, solo para realizar log</param>
        /// <param name="portal">Portal donde se conectará la cuenta</param>
        /// <param name="cuenta">Usuario y Contraseña del portal</param>
        /// <param name="gateway">Gateway de Etecsa</param>
        /// <returns></returns>
        private string MakeLoginNauta(ITikConnection connection, ITikConnection adminConnection, PortalesEtecsa portal, CuentaNauta cuenta, string gateway)
        {
            // Aqui se puede dar el caso que la interfaz no se esté conectada
            // Comprobar que la interfaz esté conectada y que el estado del dhcp Client esté en bound
            string portalNauta = "10.180.0.30";
            string result = string.Empty;
            var routes = connection.LoadList<IpRoutes>();
            IpRoutes ipRoutes = new IpRoutes();
            if (!routes.Any(c => c.DstAddress.Contains(portalNauta)))
            {

                ipRoutes.DstAddress = portalNauta;
                ipRoutes.Gateway = $"{gateway}%{portal.Interfaz}";
                connection.LogWarning($"Creando ruta para la conexion de la cuenta.");
                adminConnection.LogWarning($"Creando ruta para la conexion de la cuenta.");
                connection.Save(ipRoutes);
            }

            ITikCommand cmd = connection.CreateCommand("/tool/fetch",
                    connection.CreateParameter("mode", "https"),
                    connection.CreateParameter("http-method", "post"),
                    connection.CreateParameter("url", "https://secure.etecsa.net:8443/LoginServlet"),
                    connection.CreateParameter("http-data", $"username={cuenta.User}&password={cuenta.Password}"),
                    connection.CreateParameter("output", "user"),
                    connection.CreateParameter("as-value", ""));
            try
            {

                result = cmd.ExecuteScalar();
                connection.LogWarning($"Eliminando ruta.");
                adminConnection.LogWarning($"Eliminando ruta.");
                connection.Delete(ipRoutes);
                return result;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("302"))
                {
                    //Se ha conectado
                    try
                    {
                        connection.LogWarning($"Eliminando ruta.");
                        adminConnection.LogWarning($"Eliminando ruta.");
                        connection.Delete(ipRoutes);
                        return "ok";
                    }
                    catch (Exception)
                    {
                        return "error";
                    }



                }
                else
                {
                    connection.LogError($"Error conectando cuenta:{e.Message}");
                    connection.LogWarning($"Eliminando ruta");
                    adminConnection.LogError($"Error conectando cuenta:{e.Message}");
                    adminConnection.LogWarning($"Eliminando ruta");
                    connection.Delete(ipRoutes);
                    return "error";
                }
            }


        }
    }
}
