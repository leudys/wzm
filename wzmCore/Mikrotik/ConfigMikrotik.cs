﻿using System;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmCore.Mikrotik.Configuration;
using wzmData.Models;
using wzmCore.Data.Mikrotik;

namespace wzmCore.Mikrotik
{
    public class ConfigMikrotik : MikrotikBaseConnection
    {

        public void Config(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {

            try
            {
                ConfigWireless(credential, parameters);
                ConfigDhcp(credential, parameters);
                //ConfigDns(credential, parameters);
                ConfigDhcpClient(credential);
                ConfigNat(credential);
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        /// <summary>
        /// Configura el Nateo del Router.
        /// </summary>
        /// <param name="credential"></param>
        private void ConfigNat(MikrotikCredentials credential)
        {
            Init(credential);
            var nat = new FirewallNat()
            {
                Action = "masquerade",
                Chain = "srcnat",
                OutInterface = "wlan1",
                Disabled = false

            };
            Connection.Save(nat);
            Dispose();
        }

        /// <summary>
        /// Configura el DHCP Client de la Wireless principal para que se conecte.
        /// </summary>
        /// <param name="credential"></param>
        private void ConfigDhcpClient(MikrotikCredentials credential)
        {
            Init(credential);

            var dhcpClient = new DhcpClient().GetDhcpClients(Connection);
            var ip = new IpDhcpClient()
            {
                AddDefaultRoute = IpDhcpClient.AddDefaultRouteType.Yes,
                DefaultRouteDistance = "1",
                DhcpOptions = "hostname,clientid",
                UsePeerDns = false,
                UsePeerNtp = false,
                Comment = "Creado por WZM",
                Disabled = false,
                Interface = "wlan1",

            };
            Connection.Save(ip);

            dhcpClient = new DhcpClient().GetDhcpClients(Connection);
            var first = dhcpClient.FirstOrDefault();
            if (first.Disabled)
            {

                first.Disabled = false;
                Connection.Save(first);
            }
        }

        /// <summary>
        /// Configura el DNS en el Mikrotik.
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="parameters"></param>
        //private void ConfigDns(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        //{
        //    Init(credential);

        //    var dns = Connection.LoadAll<IpDns>();
        //    if (!dns.Any(c => c.Servers.Contains("181.225.231.120")) || !dns.Any(c => c.Servers.Contains("181.225.231.110")))
        //    {
        //        var newDns = new IpDns()
        //        {

        //            AllowRemoteRequests = false,
        //            Servers = string.Format("{0},{1}", parameters.DnsP, parameters.DnsS)

        //        };
        //        Connection.Save(newDns);
        //    }
        //}

        /// <summary>
        /// Configura el Servidor del DHCP
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="parameters"></param>
        private void ConfigDhcp(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            parameters.DhcpIp = credential.Dispositivo.Ip;
            ConfigIpPool(credential, parameters);
            ConfigDhcpServer(credential, parameters);
            ConfigDhcpNetwork(credential, parameters);

        }

        /// <summary>
        /// Configura el Servidor del DHCP
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="parameters"></param>
        private void ConfigDhcp(ITikConnection connection, ConfigMikrotikModel parameters)
        {

            ConfigIpPool(connection, parameters);
            ConfigDhcpServer(connection, parameters);
            ConfigDhcpNetwork(connection, parameters);

        }

        private void ConfigDhcpNetwork(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            new DHCPServer().ConfigDhcpNetwork(credential, parameters);
        }

        private void ConfigDhcpNetwork(ITikConnection connection, ConfigMikrotikModel parameters)
        {
            new DHCPServer().ConfigDhcpNetwork(connection, parameters);
        }

        private void ConfigDhcpServer(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            new DHCPServer().ConfigDhcpServer(credential, parameters);
        }

        private void ConfigDhcpServer(ITikConnection connection, ConfigMikrotikModel parameters)
        {
            new DHCPServer().ConfigDhcpServer(connection, parameters);
        }

        /// <summary>
        /// Configura el IP Pool para el Servidor DHCP.
        /// </summary>
        /// <param name="parameters"></param>
        private void ConfigIpPool(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            new Pool().ConfigIpPool(credential, parameters);
        }

        private void ConfigIpPool(ITikConnection connection, ConfigMikrotikModel parameters)
        {
            new Pool().ConfigIpPool(connection, parameters);
        }

        /// <summary>
        /// Configura la Wireless del Mikrotik.
        /// </summary>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <param name="parameters"></param>
        private void ConfigWireless(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            Init(credential);
            var mv = new Wireless();
            var p = new WirelessConfiguration();
            var v = mv.GetAllWireless(Connection).FirstOrDefault(c => c.Name == "wlan1");
            var pre = p.GetPreConfWireless();
            v.Ssid = pre.Ssid;
            v.Comment = "Creado por WZM";
            v.AmpduPriorities = pre.AmpduPriorities;
            v.Band = pre.Band;
            v.Frequency = pre.Frequency;
            v.FrequencyMode = pre.FrequencyMode;
            v.Mode = pre.Mode;
            v.PreambleMode = pre.PreambleMode;
            v.RadioName = pre.RadioName;
            v.ScanList = pre.ScanList;
            v.WirelessProtocol = pre.WirelessProtocol;
            v.Country = pre.Country;
            v.Disabled = pre.Disabled;

            Connection.Save(v);
            Dispose();
        }

        /// <summary>
        /// Configura la Wireless del Mikrotik.
        /// </summary>
        /// <param name="connection">Conexión del Mikrotik</param>
        /// <param name="parameters"></param>
        private void ConfigWireless(ITikConnection connection, ConfigMikrotikModel parameters)
        {

            var mv = new Wireless();
            var p = new WirelessConfiguration();
            var v = mv.GetAllWireless(connection).FirstOrDefault(c => c.Name == "wlan1");
            var pre = p.GetPreConfWireless();
            v.Ssid = pre.Ssid;
            v.Comment = "Creado por WZM";
            v.AmpduPriorities = pre.AmpduPriorities;
            v.Band = pre.Band;
            v.Frequency = pre.Frequency;
            v.FrequencyMode = pre.FrequencyMode;
            v.Mode = pre.Mode;
            v.PreambleMode = pre.PreambleMode;
            v.RadioName = pre.RadioName;
            v.ScanList = pre.ScanList;
            v.WirelessProtocol = pre.WirelessProtocol;
            v.Country = pre.Country;
            v.Disabled = pre.Disabled;

            connection.Save(v);

        }



        /// <summary>
        /// Checkea si la wifi principal ha sido configurada según los parámetros de WZM.
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public bool ConfiguredToEtecsa(MikrotikCredentials credential)
        {
            var mv = new Wireless();
            var p = new WirelessConfiguration();
            var v = mv.GetAllWireless(credential).FirstOrDefault(c => c.Name == "wlan1");
            var preConf = p.GetPreConfWireless();
            if (!(v.Ssid == preConf.Ssid))
            {
                return false;
            }
            if (!(v.AmpduPriorities == preConf.AmpduPriorities))
            {
                return false;
            }
            if (!(v.Band == preConf.Band))
            {
                return false;
            }
            if (!(v.Frequency == preConf.Frequency))
            {
                return false;
            }
            if (!(v.FrequencyMode == preConf.FrequencyMode))
            {
                return false;
            }
            if (!(v.Mode == preConf.Mode))
            {
                return false;
            }
            if (!(v.PreambleMode == preConf.PreambleMode))
            {
                return false;
            }
            if (!(v.RadioName == preConf.RadioName))
            {
                return false;
            }
            if (!(v.ScanList == preConf.ScanList))
            {
                return false;
            }
            if (!(v.WirelessProtocol == preConf.WirelessProtocol))
            {
                return false;
            }
            if (!(v.Country == preConf.Country))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
