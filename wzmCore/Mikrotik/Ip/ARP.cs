﻿using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.Data.Mikrotik;

namespace wzmCore.Mikrotik
{
    public class ARP : MikrotikBaseConnection
    {
        /// <summary>
        /// Obtiene la Talbla ARP del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<IpArp> GetAllArp(MikrotikCredentials credential)
        {
            Init(credential);
            var arpList = Connection.LoadAll<IpArp>().ToList();
            Dispose();
            return arpList;
        }

        /// <summary>
        /// Obtiene la Talbla ARP del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<IpArp> GetAllArp(ITikConnection connection)
        {

            var arpList = connection.LoadAll<IpArp>().ToList();

            return arpList;
        }

        /// <summary>
        /// Añade un Ip y MAC en la tabla ARP del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="ipArp"></param>
        public void Add(MikrotikCredentials credential, IpArpModel ipArp)
        {

            Init(credential);

            var arp = new IpArp()
            {
                Address = ipArp.Address,
                MacAddress = ipArp.MacAddress,
                Interface = ipArp.Interface
            };

            Connection.Save(arp);
            Dispose();
        }

        /// <summary>
        /// Añade un Ip y MAC en la tabla ARP del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="ipArp"></param>
        public void Add(ITikConnection connection, IpArpModel ipArp)
        {

            var arp = new IpArp()
            {
                Address = ipArp.Address,
                MacAddress = ipArp.MacAddress,
                Interface = ipArp.Interface
            };

            connection.Save(arp);

        }

        public void Update() { }

        public void Delete(ITikConnection connection, IpArp ipArp)
        {

            connection.Delete(ipArp);

        }


        public void Disable(ITikConnection connection, IpArp ipArp)
        {
            ipArp.Disabled = true;
            connection.Save(ipArp);
        }

        public void Enable(ITikConnection connection, IpArp ipArp)
        {
            ipArp.Disabled = false;
            connection.Save(ipArp);
        }
    }
}
