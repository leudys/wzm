﻿using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class DhcpClient : MikrotikBaseConnection
    {
        /// <summary>
        /// Agrega un DHCP Client al Mikrotik
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="wlan"></param>
        public void Add(ITikConnection connection, int wlan)
        {
            ITikCommand addDhcpClient = connection.CreateCommand("/ip/dhcp-client/add",
              connection.CreateParameter("default-route-distance", "1"),
              connection.CreateParameter("dhcp-options", "hostname,clientid"),
              connection.CreateParameter("disabled", "no"),
              connection.CreateParameter("interface", "wlan" + wlan),
              connection.CreateParameter("add-default-route", "no"),
              connection.CreateParameter("use-peer-dns", "no"),
              connection.CreateParameter("use-peer-ntp", "no"));
            addDhcpClient.ExecuteScalar();
        }

        /// <summary>
        /// Obtiene el Gateway del DHCP Client de la wlan1
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string GetGateWay(MikrotikCredentials credential)
        {
            Init(credential);
            string gateway = Connection.LoadAll<IpDhcpClient>().First().Gateway;
            Dispose();
            return gateway;

        }

        /// <summary>
        /// Realiza un Realese en el DHCP Client del Mikrotik
        /// </summary>
        /// <param name="wlan"></param>
        /// <param name="credential"></param>
        public void Release(string id, string wlan, MikrotikCredentials credential, string userName)
        {
            Init(credential);
            if (id != string.Empty)
            {
                //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
                var dhcpClient = Connection.LoadAll<IpDhcpClient>();



                // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan

                var dhcp = dhcpClient.First(c => c.Id == id);
                dhcp.Release(Connection);

                Connection.LogInfo($"{userName} ha realizado un realease a la interfaz {dhcp.Interface}");

                Dispose();

            }
            else
            {
                //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
                var dhcpClient = Connection.LoadAll<IpDhcpClient>();

                // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan
                dhcpClient.First(c => c.Interface == wlan).Release(Connection);

                Dispose();
            }
        }

        /// <summary>
        /// Realiza un Realese en el DHCP Client del Mikrotik
        /// </summary>
        /// <param name="wlan"></param>
        /// <param name="credential"></param>
        public void Release(string id, string wlan, ITikConnection connection, string userName)
        {
            
            if (id != string.Empty)
            {
                //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
                var dhcpClient = connection.LoadAll<IpDhcpClient>();



                // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan

                var dhcp = dhcpClient.First(c => c.Id == id);
                dhcp.Release(Connection);

                connection.LogInfo($"{userName}: ha cerrado sesión en {wlan}");

                Dispose();

            }
            else
            {
                //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
                var dhcpClient = Connection.LoadAll<IpDhcpClient>();

                // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan
                dhcpClient.First(c => c.Interface == wlan).Release(Connection);

                
            }
        }



        /// <summary>
        /// Realiza un Realese en el DHCP Client del Mikrotik y Cambia la MAC
        /// </summary>
        /// <param name="wlan"></param>
        /// <param name="newMac">Nueva MAC</param>
        /// <param name="userName"></param>
        /// <param name="credential"></param>
        public void ReleaseAndChangeMac(string wlan, string newMac, string userName, MikrotikCredentials credential)
        {
            Init(credential);


            //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
            var dhcpClient = Connection.LoadAll<IpDhcpClient>();

            // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan
            dhcpClient.First(c => c.Interface == wlan).Release(Connection);

            Connection.LogInfo($"{userName} ha cerrado la sesión.");

            var wireless = new Wireless().GetAllWireless(Connection).FirstOrDefault(c => c.Name.Equals(wlan));


            Connection.LogInfo($"Cambiando MAC: {wireless.MacAddress} para la Interfaz {wlan} por MAC: {newMac} ");

            wireless.MacAddress = newMac;

            Connection.Save(wireless);

            Dispose();

        }

        /// <summary>
        /// Libera todas las conexiones de Etecsa
        /// </summary>
        /// <param name="credential"></param>
        public void ReleaseAll(MikrotikCredentials credential)
        {
            Init(credential);
            List<IpDhcpClient> ipDhcpClient = (List<IpDhcpClient>)Connection.LoadAll<IpDhcpClient>();

            foreach (var item in ipDhcpClient)
            {
                if (item.Status == IpDhcpClient.ClientStatus.Bound)
                {
                    item.Release(Connection);
                }

            }
            Dispose();

        }

        /// <summary>
        /// Libera todas las conexiones de Etecsa
        /// </summary>
        /// <param name="credential"></param>
        public void ReleaseAll(ITikConnection connection)
        {

            List<IpDhcpClient> ipDhcpClient = (List<IpDhcpClient>)connection.LoadAll<IpDhcpClient>();

            foreach (var item in ipDhcpClient)
            {
                if (item.Status == IpDhcpClient.ClientStatus.Bound)
                {
                    item.Release(connection);
                }

            }


        }

        /// <summary>
        /// Renueva el IP del DHCP Client del Mikrotik
        /// </summary>
        /// <param name="wlan"></param>
        /// <param name="credential"></param>
        public void Renew(string id, string wlan, MikrotikCredentials credential)
        {
            Init(credential);
            if (id != string.Empty)
            {
                //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
                var dhcpClient = Connection.LoadAll<IpDhcpClient>();

                // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan

                dhcpClient.First(c => c.Id == id).Renew(Connection);
                Dispose();
            }
            else
            {
                //Obtengo los DHCP Client para buscar donde esta la wlan del cliente
                var dhcpClient = Connection.LoadAll<IpDhcpClient>();

                // Busco en el DHCP Client si el wlan del usuario se encuentra, y si se encuentra hace un release de esa wlan

                dhcpClient.First(c => c.Interface == wlan).Renew(Connection);
                Dispose();

            }

        }

        /// <summary>
        /// Renueva todas las IP de los clientes DHCP
        /// </summary>
        /// <param name="credential"></param>
        public void RenewAll(MikrotikCredentials credential)
        {
            Init(credential);
            List<IpDhcpClient> ipDhcpClient = (List<IpDhcpClient>)Connection.LoadAll<IpDhcpClient>();

            foreach (var item in ipDhcpClient)
            {
                item.Renew(Connection);
            }
            Dispose();
        }

        /// <summary>
        /// Obtiene todas los DHCP Client del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<IpDhcpClient> GetDhcpClients(ITikConnection connection)
        {

            List<IpDhcpClient> ipDhcpClient = (List<IpDhcpClient>)connection.LoadAll<IpDhcpClient>();

            return ipDhcpClient;

        }

        /// <summary>
        /// Obtiene un DhcpClient dado una interfaz.
        /// </summary>
        /// <param name="wInterface"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IpDhcpClient GetDhcpClient(string wInterface, MikrotikCredentials credential)
        {
            Init(credential);
            try
            {
                var d = Connection.LoadList<IpDhcpClient>(Connection.CreateParameter("interface", wInterface)).SingleOrDefault();
                return d;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                Dispose();
            }
        }

        /// <summary>
        /// Obtiene el DHCP Cliente dada un Interfaz
        /// </summary>
        /// <param name="wInterface"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IpDhcpClient GetDhcpClient(string wInterface, ITikConnection connection)
        {

            try
            {
                var d = connection.LoadList<IpDhcpClient>(connection.CreateParameter("interface", wInterface)).SingleOrDefault();
                return d;
            }
            catch (Exception e)
            {
               throw e;
            }

        }

    }
}
