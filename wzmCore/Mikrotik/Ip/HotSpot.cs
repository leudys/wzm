﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip.Hotspot;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class HotSpot : MikrotikBaseConnection
    {
        public List<HotspotUser> ListUsers(MikrotikCredentials credential)
        {
            Init(credential);
            return Connection.LoadAll<HotspotUser>().ToList();
        }

        public HotspotUser GetUserByName(string name, ITikConnection connection)
        {           
            return connection.LoadByName<HotspotUser>(name);
        }

        public HotspotUser GetUserByComment(string comment, ITikConnection connection)
        {
            return connection.LoadAll<HotspotUser>().FirstOrDefault(c => comment.Contains(c.Name));
        }

        public void DeleteUser(MikrotikCredentials credential, HotspotUser user)
        {
            Init(credential);
            Connection.Delete(user);
            Connection.Dispose();
        }

        public List<HotspotActive> ListActiveUsers(MikrotikCredentials credential)
        {
            Init(credential);
            return Connection.LoadAll<HotspotActive>().ToList();
        }

        public HotspotActive GetActiveUser(string comment, MikrotikCredentials credential)
        {
            Init(credential);
            return Connection.LoadByName<HotspotActive>(comment);
        }

        public HotspotActive GetActiveUser(string comment, ITikConnection tikConnection)
        {                        
            return tikConnection.LoadAll<HotspotActive>().FirstOrDefault(c => comment.Contains(c.UserName));
        }

        public void DesconnectUser(HotspotUser user, MikrotikCredentials credential)
        {

        }
    }
}
