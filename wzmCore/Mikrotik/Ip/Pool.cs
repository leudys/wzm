﻿using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using tik4net.Objects.Ip.DhcpServer;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.Data.Mikrotik;

namespace wzmCore.Mikrotik
{
    public class Pool : MikrotikBaseConnection
    {
        /// <summary>
        /// Config the Ip Pool of the Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="parameters"></param>
        public void ConfigIpPool(MikrotikCredentials credential, ConfigMikrotikModel parameters)
        {
            Init(credential);
            var p = Connection.LoadAll<IpPool>();
            if (p != null)
            {
                if (p.Count() > 0)
                {
                    foreach (var item in p)
                    {
                        Connection.Delete(item);
                    }
                }
                var pool = new IpPool()
                {
                    Name = "dhcp",
                    Ranges = string.Format("{0}-{1}", parameters.RangoInicialDhcp.ToString(), parameters.RangoFinalDhcp.ToString())
                };
                Connection.Save(pool);
                Dispose();
            }
        }

        /// <summary>
        /// Config the Ip Pool of the Mikrotik
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="parameters"></param>
        public void ConfigIpPool(ITikConnection connection, ConfigMikrotikModel parameters)
        {
            
            var p = connection.LoadAll<IpPool>();
            if (p != null)
            {
                if (p.Count() > 0)
                {
                    foreach (var item in p)
                    {
                        connection.Delete(item);
                    }
                }
                var pool = new IpPool()
                {
                    Name = "wzm",
                    Ranges = string.Format("{0}-{1}", parameters.RangoInicialDhcp.ToString(), parameters.RangoFinalDhcp.ToString())
                };
                connection.Save(pool);
               
            }
        }

        public List<IpPool> GetIpPools(ITikConnection tikConnection)
        {
            return tikConnection.LoadAll<IpPool>().ToList();
        }

        /// <summary>
        /// Checks if all Addresses of the Pool are used by the DHCP Server 
        /// </summary>
        /// <param name="tikConnection"></param>
        /// <returns></returns>
        public bool IsPoolFull(ITikConnection tikConnection, string dhcpServer)
        {

            var ipPool = GetIpPools(tikConnection);

            var server = tikConnection.LoadAll<IpDhcpServer>().FirstOrDefault(c => c.Name.Equals(dhcpServer));

            var dhcpLeases = tikConnection.LoadAll<DhcpServerLease>().Where(c => !c.Blocked && c.Server.Equals(server.Name));


            int cantIp = int.Parse(ipPool.FirstOrDefault(c => c.Name.Equals(server.AddressPool)).Ranges.Split('-')[0].Split('.')[3]) - int.Parse(ipPool.FirstOrDefault().Ranges.Split('.')[1].Split('.')[3]);


            if (cantIp == dhcpLeases.Count())
            {
                return true;
            }
            return false;
        }

        ///// <summary>
        ///// Comprueba que el DHCP no se encuentre lleno
        ///// </summary>
        ///// <param name="tikConnection"></param>
        ///// <returns></returns>
        //public bool IsPoolFull(ITikConnection tikConnection)
        //{

        //    var ipPool = GetIpPools(tikConnection);



        //    var dhcpLeases = tikConnection.LoadAll<DhcpServerLease>().Where(c => !c.Blocked);


        //    int cantIp = int.Parse(ipPool.FirstOrDefault().Ranges.Split('-')[0].Split('.')[3]) - int.Parse(ipPool.FirstOrDefault().Ranges.Split('.')[1].Split('.')[3]);


        //    if (cantIp == dhcpLeases.Count())
        //    {
        //        return true;
        //    }
        //    return false;
        //}

    }
}
