﻿using System.Collections.Generic;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Base;
using wzmData.Models;

namespace wzmCore.Mikrotik
{
    public class Address : MikrotikBaseConnection
    {
        public List<IpAddress> GetIpAddress(MikrotikCredentials credential)
        {
            Init(credential);
            List<IpAddress> ipAddresses = (List<IpAddress>)Connection.LoadAll<IpAddress>();
            return ipAddresses;
        }

        public List<IpAddress> GetIpAddress(ITikConnection connection)
        {
           
            List<IpAddress> ipAddresses = (List<IpAddress>)connection.LoadAll<IpAddress>();
            return ipAddresses;
        }

        public void AddAddress(MikrotikCredentials credential, IpAddress address)
        {
            Init(credential);
            Connection.Save(address);
            Connection.Dispose();
        }
    }
}
