﻿using Microsoft.Extensions.Configuration;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip.Web;
using wzmCore.Interfaces;
using wzmCore.Services;

namespace wzmCore.Mikrotik.WebProxyAdmin
{
    public class WebProxyManager
    {
        private IConfiguration _configuration;

        public WebProxyManager(IConfiguration configuration,IWzmCore wzmCore)
        {
            _configuration = configuration;
        }

        public WebProxyManager()
        {

        }

        public bool IsProxyEnabled(ITikConnection connection)
        {
            return connection.LoadSingle<WebProxy>().Enabled;

        }

        public void EnableWebProxy(ITikConnection connection)
        {
            var webProxy = connection.LoadSingle<WebProxy>();
            webProxy.Enabled = true;
            //webProxy.Port = "999";
            connection.Save(webProxy);
        }


        public bool IsRestrictionEnabled(ITikConnection connection)
        {
            
                var config = _configuration.GetSection("ServerConfiguration");
                var redirectTo = $"{config.GetValue<string>("ip")}:{config.GetValue<string>("httpPort")}";
                var restriction = connection.LoadAll<ProxyAccess>().FirstOrDefault(c => c.RedirectTo.Equals(redirectTo));
                if (restriction != null)
                {
                    if (!restriction.Disabled)
                    {

                        return true;
                    }
                    return false;
                }
                else
                {
                    AddMorososRestriction(connection, redirectTo);
                    return true;
                }
            

           
        }

        public void DisableWebProxy(ITikConnection connection, string redirectTo)
        {
            var webProxy = connection.LoadAll<WebProxy>().FirstOrDefault();
            webProxy.Enabled = false;
            connection.Save(webProxy);
        }

        public void DisableRestriction()
        {

        }

        public void EnableRestriction()
        {

        }

        public void AddMorososRestriction(ITikConnection connection, string redirectTo)
        {
            var restriction = new ProxyAccess() { RedirectTo = redirectTo, Action = "deny" };
            connection.Save(restriction);
        }
    }
}
