﻿using System;
using System.Collections.Generic;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Base;
using wzmData.Models;
using wzmCore.ViewModels;
using wzmCore.Data.Mikrotik;
using wzmCore.Extensions;

namespace wzmCore.Mikrotik
{
    public class Routes : MikrotikBaseConnection
    {
        private List<RouteViewModel> _markRoutesViewModels { get; set; }

        /// <summary>
        /// Agrega una Ruta al Mikrotik
        /// </summary>
        /// <param name="connection"></param>
        public void AddRoute(ITikConnection connection, string gateway, int wlan)
        {

            ITikCommand addRoute = connection.CreateCommand("/ip/route/add",
              connection.CreateParameter("gateway", gateway + "%wlan" + wlan),
              connection.CreateParameter("routing-mark", "salida" + wlan));
            addRoute.ExecuteScalar();
        }

        /// <summary>
        /// Obtiene todas las "Mark Route" de cada ruta.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public List<RouteViewModel> GetMarkRoutes(ITikConnection connection)
        {
            if (connection.IsOpened)
            {
                _markRoutesViewModels = new List<RouteViewModel>();

                List<IpRoutes> ipRoutes = connection.LoadAll<IpRoutes>().Where(c => !c.Disabled).ToList();
                ipRoutes.RemoveAll(c => c.MarkRoute == string.Empty);
                foreach (var item in ipRoutes)
                {
                    _markRoutesViewModels.Add(new RouteViewModel { Id = item.Id, MarkRoute = item.MarkRoute, Gateway = item.Gateway });
                }

                return _markRoutesViewModels;
            }

            else
            {
                return new List<RouteViewModel>();
            }
        }

        /// <summary>
        /// Obtiene todas las "Mark Route" de cada ruta.
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<RouteViewModel> GetMarkRoutes(MikrotikCredentials credential)
        {
            using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                _markRoutesViewModels = new List<RouteViewModel>();

                List<IpRoutes> ipRoutes = (List<IpRoutes>)connection.LoadAll<IpRoutes>();
                ipRoutes.RemoveAll(c => c.MarkRoute == string.Empty);
                foreach (var item in ipRoutes)
                {
                    _markRoutesViewModels.Add(new RouteViewModel { Id = item.Id, MarkRoute = item.MarkRoute, Gateway = item.Gateway });
                }

                return _markRoutesViewModels;
            }

        }




        /// <summary>
        /// Obtiene todas las Rutas que existen en el Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public List<IpRoutes> GetRoutes(MikrotikCredentials credential)
        {

            Init(credential);
            List<IpRoutes> routes = (List<IpRoutes>)Connection.LoadAll<IpRoutes>();
            Dispose();
            return routes;
        }

        /// <summary>
        /// Obtiene todas las Rutas que existen en el Mikrotik
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public List<IpRoutes> GetRoutes(ITikConnection connection)
        {

            return (List<IpRoutes>)connection.LoadAll<IpRoutes>();

        }

        public RouteViewModel GetRoute(string wInterface, ITikConnection connection)
        {

            try
            {

                var mrList = GetMarkRoutes(connection);

                return mrList.First(c => c.Gateway.Contains(wInterface));
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public RouteViewModel GetRoute(string wInterface, MikrotikCredentials credential)
        {

            try
            {
                Init(credential);
                var mrList = GetMarkRoutes(Connection);

                return mrList.First(c => c.Gateway.Contains(wInterface));
            }
            catch (Exception e)
            {

                throw e;
            }

        }
    }
}
