﻿
namespace wzmCore.Mikrotik
{
    public class Ip
    {
        public Routes Routes => new Routes();

        public DhcpClient DhcpClient => new DhcpClient();

        public Address Address => new Address();

        public DHCPServer DhcpServer => new DHCPServer();

        public ARP IpArp => new ARP();

        public Pool Pool => new Pool();

        public HotSpot HotSpot => new HotSpot();
    }
}
