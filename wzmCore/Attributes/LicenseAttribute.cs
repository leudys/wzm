﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using wzmCore.WzmLicense;

namespace wzm.Attributes
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class LicenseAttribute : ActionFilterAttribute
    {
        private readonly bool _validated;
        
        public LicenseAttribute(bool validated)
        {
            _validated = validated;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (!_validated)
            //{
            //    filterContext.Result = new RedirectToRouteResult(
            //  new RouteValueDictionary
            //  {
            //            { "controller", "Account" },
            //            { "action", "Login" }
            //  });
            //}
      
        }
    }
}
