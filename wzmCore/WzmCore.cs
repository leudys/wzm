﻿using System;
using System.Collections.Generic;
using wzmCore.Administration;
using wzmCore.Interfaces;
using wzmCore.Utils;
using wzmCore.WzmLicense;
using wzmData.Models;

namespace wzmCore
{
    public class WzmCore : IWzmCore
    {
        public WzmCore()
        {
            Random = new Random();
            UserManagement = new UserManagement();
            MikrotikManager = new MikrotikManager();
            LicenseManager = new LicenseManager();
            UbiquitiManager = new UbiquitiManager();
            Users = new List<HostsServices>();
            Internet = new List<PortalesInternet>();
        }

        public UserManagement UserManagement { get; private set; }

        public Random Random { get; private set; }

        public MikrotikManager MikrotikManager { get; private set; }

        public UbiquitiManager UbiquitiManager { get; private set; }

        public LicenseManager LicenseManager { get; private set; }

        public FileHelper FileHelper { get; set; }
        public List<HostsServices> Users { get; set; }

        public List<PortalesInternet> Internet { get; set; }                         

    }
}
