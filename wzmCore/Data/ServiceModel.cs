﻿using System;
using wzmData.Enum;

namespace wzmCore.Data
{
    public class ServiceModel
    {
        public ServiceModel()
        {
            CronExpression = "0/1 * * * * ? ";
        }

        public Type ServiceType { get; set; }

        public string CronExpression { get; set; }
    }
}
