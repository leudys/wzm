﻿namespace wzmCore.Data.Mikrotik
{
    public class PingModel
    {
        public string Wlan { get; set; }

        public string MarkRoute { get; set; }

        public bool Connected { get; set; }

        public bool Renewed { get; set; }

        public bool Relesed { get; set; }
    }
}
