﻿using System.ComponentModel.DataAnnotations;

namespace wzmCore.Data.Mikrotik
{
    public class FirewallAddressViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Ip")]
       
        public string Address { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Salida")]
        public string List { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Usuario")]
        public string Usuario { get; set; }

        public bool Disabled { get; set; }
    }
}
