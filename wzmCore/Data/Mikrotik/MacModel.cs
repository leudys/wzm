﻿namespace wzmCore.Data.Mikrotik
{
    public class MacModel
    {
        public string Mac { get; set; }

        public string Manufacturer { get; set; }

        public string Company { get; set; }
    }
}
