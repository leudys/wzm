﻿using tik4net.Objects.Ip;
using tik4net.Objects.Ip.Firewall;

namespace wzmCore.Data.Mikrotik
{
    /// <summary>
    /// Objetos de Mikrotik <see cref="Wlan"/> <see cref="DhcpClient"/> <see cref="Nat"/> <see cref="Route"/> <see cref="Manlge"/>
    /// </summary>
    public class MkObjects
    {
        public WirelessInterfaceModel Wlan { get; set; }
        public IpDhcpClient DhcpClient { get; set; }
        public FirewallNat Nat { get; set; }
        public IpRoutes Route { get; set; }
        public FirewallMangle Manlge { get; set; }
    }
}
