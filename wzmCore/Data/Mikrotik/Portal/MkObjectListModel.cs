﻿using tik4net.Objects.Ip;
using tik4net.Objects.Ip.Firewall;

namespace wzmCore.Data.Mikrotik
{

    /// <summary>
    /// Lista de Objetos del Mikrotik que componen el portal.
    /// </summary>
    public class MkObjectListViewModel
    {
        public IpDhcpClient IpDhcpClient { get; set; } = new IpDhcpClient();
        public FirewallNat Nat { get; set; } = new FirewallNat();
        public FirewallMangle Mangle { get; set; } = new FirewallMangle();
        public RouteViewModel Route { get; set; } = new RouteViewModel();
        public WirelessInterfaceModel Wireless { get; set; } = new WirelessInterfaceModel();
    }
}
