﻿namespace wzmCore.Data.Mikrotik
{
    public class RouteViewModel
    {
        public string Id { get; set; }

        public string MarkRoute { get; set; }

        public string Gateway { get; set; }

        public bool Disabled { get; set; }

        public string UserComment { get; set; }
    }
}
