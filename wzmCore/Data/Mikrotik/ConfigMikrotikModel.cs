﻿using System.ComponentModel.DataAnnotations;
using System.Net;

namespace wzmCore.Data.Mikrotik
{
    public class ConfigMikrotikModel
    {

        [Display(Name = "SSID")]
        public string SsId { get; set; }


        [Display(Name = "Ip del Mikrotik")]
        public string EtherIp { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Ip Inicial del Servidor Dhcp")]
        public string RangoInicialDhcp { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Ip Final del Servidor Dhcp")]
        public string RangoFinalDhcp { get; set; }

        public string DhcpIp { get; set; }

        //[Display(Name = "DNS 1 Principal")]
        //public string Dns1P { get; set; }

        //[Display(Name = "DNS 1 Secundario")]
        //public string Dns1S { get; set; }

        //[Display(Name = "DNS 2 Principal")]
        //public string Dns2P { get; set; }

        //[Display(Name = "DNS 2 Secundario")]
        //public string Dns2S { get; set; }
    }
}
