﻿using System.ComponentModel.DataAnnotations;

namespace wzmCore.Data.Mikrotik
{
    public class IpArpModel
    {
        [Required(ErrorMessage ="El campo {0} es obligatorio.")]
        [Display(Name = "Ip")]
        public string Address { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Interfaz")]
        public string Interface { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "MacAddress")]
        public string MacAddress { get; set; }

        public string Comment { get; set; }
    }
}
