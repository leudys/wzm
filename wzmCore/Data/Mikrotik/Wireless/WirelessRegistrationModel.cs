﻿namespace wzmCore.Data.Mikrotik
{
    public class WirelessRegistrationModel
    {
        public int Id { get; set; }
        public string Interface { get; set; }
        public string MacAddress { get; set; }
        public string SignalStrength { get; set; }
        public string TxRate { get; set; }
        public string RxRate { get; set; }
        public string Ccq { get; set; }

    }
}
