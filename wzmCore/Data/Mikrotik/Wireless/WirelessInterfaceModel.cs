﻿using static tik4net.Objects.Interface.InterfaceWireless;

namespace wzmCore.Data.Mikrotik
{
    public class WirelessInterfaceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Ssid { get; set; }
        public string FrequencyMode { get; set; }
        public WirelessMode Mode { get; set; }
        public string MacAddress { get; set; }
        public string MasterInterface { get; set; }
        public bool Disabled { get; set; }
        public string Comment { get; set; }
        public bool Internet { get; set; }

    }
}
