﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wzmCore.Data
{
    /// <summary>
    /// Clase para saber si un ip se le ha asignado a un usuario
    /// </summary>
    public class AssignedIp
    {
        public AssignedIp()
        {
            Assigned = false;
        }
        /// <summary>
        /// Si el IP se ha asignado
        /// </summary>
        public bool Assigned { get; set; }
        /// <summary>
        /// Ip que se le ha asignado al Usuario
        /// </summary>
        public string Ip { get; set; }
    }
}
