﻿using wzmData.Models;
using wzmData.User;

namespace wzmCore.Data
{
    public class DeviceOperations
    {
        public DeviceOperations()
        {
            Validation = new UserValidation();
            Device = new UserDevice();
            DeviceToNationalNetwork = new DeviceToNationalNetwork();
        }
        public UserValidation Validation { get; private set; }

        public UserDevice Device { get; set; }

        public DeviceToNationalNetwork DeviceToNationalNetwork { get; set; }
    }
}
