﻿using System;
using System.Linq;

namespace wzmCore.Services
{
    public class MacChanger
    {
        private readonly Random _randomMac;

        private readonly Random _randomByte;

        public MacChanger()
        {
            _randomMac = new Random();
            _randomByte = new Random();
        }

        private byte[] Bytes = new byte[] { 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84 };

        public string GetRandomWifiMacAddress()
        {

            var buffer = new byte[6];
            _randomMac.NextBytes(buffer);
            buffer[0] = Bytes[_randomByte.Next(0, 12)];
            return string.Join(":", buffer.Select(x => string.Format("{0}", x.ToString("X2"))).ToArray());

        }
    }

    public static class MacAddress
    {
        private static readonly Random Random = new Random();

        public static string GetSignatureRandomMac(string generic = "AA")
        {
            string[] macBytes = new[]
            {
            generic,
            generic,
            generic,
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X")
        };

            return string.Join(":", macBytes);
        }

        public static string GetRandomMac()
        {
            string[] macBytes = new[]
            {
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X"),
            Random.Next(1, 256).ToString("X")
        };

            return string.Join("-", macBytes);
        }
    }
}
