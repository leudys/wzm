﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;
using tik4net.Objects;
using tik4net.Objects.Interface.Wireless;
using wzmCore.Base;
using wzmCore.Mikrotik;
using wzmData.Models;
namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class AntiClonJob : MikrotikBaseConnection, IJob
    {

        private WirelessRegistrationTable _registrationTable;

        private IServiceProvider _serviceProvider;

        private readonly ILogger<AntiClonJob> _logger;

        public AntiClonJob(IServiceProvider serviceProvider, ILogger<AntiClonJob> logger)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }


        /// <summary>
        /// Obtiene del Mikrotik la Tabla de Registro de la interfaz principal.
        /// </summary>
        private void GetRegistrationTable()
        {

            _registrationTable = Connection.LoadList<WirelessRegistrationTable>(Connection.CreateParameter("interface", "wlan1")).SingleOrDefault();

        }


        /// <summary>
        /// Comprueba si el Dipositivo está conectado a un Clon
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public bool IsConnectedToClon()
        {

            GetRegistrationTable();

            if (_registrationTable.Bridge || _registrationTable.RouterosVersion.Length > 0 || _registrationTable.RadioName.Length > 0 || _registrationTable.TxSignalStrength.Length > 0)
            {
                Connection.LogError(string.Format($"Se ha encontrado un Clon con Mac: {_registrationTable.MacAddress}. WZM está tomando las medidas necesarias."));
                _logger.LogWarning(string.Format($"Se ha encontrado un Clon con Mac: {_registrationTable.MacAddress}. WZM está tomando las medidas necesarias."));
                ApplyRestrictions();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Aplica las restricciones al Mikrotik para que no se conecte más.
        /// </summary>
        private void ApplyRestrictions()
        {
            BlockConnectionList();
            RemoveAllRegistrationTables();
            ReleaseAllDhcpClient();

        }

        /// <summary>
        /// Elimina todas las conecciones de la Tabla de Registro.
        /// </summary>
        private void RemoveAllRegistrationTables()
        {

            var r = Connection.LoadAll<WirelessRegistrationTable>();
            foreach (var item in r)
            {
                Connection.Delete(item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ReleaseAllDhcpClient()
        {
            Connection.LogInfo("Soltando todas las IP DHCP Client.");
            new DhcpClient().ReleaseAll(Connection);

        }

        /// <summary>
        /// Bloquea 
        /// </summary>
        private void BlockConnectionList()
        {
            Connection.LogInfo(string.Format("Bloqueando la MAC: ", _registrationTable.MacAddress));
            var connectList = new WirelessAccessList() { Interface = "wlan1", MacAddress = _registrationTable.MacAddress, SignalRange = $"{_registrationTable.SignalStrength}..0", Disabled = false };
            Connection.Save(connectList);
        }


        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                using (var applicationDbContext = new ApplicationDbContext(_serviceProvider.CreateScope().ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
                {
                    if (Credential == null)
                    {
                        SetCredential();

                    }
                    else
                    {
                        var credential = applicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);
                        if (!credential.User.Equals(Credential.User) || !credential.Password.Equals(Credential.Password) || !credential.Dispositivo.Ip.Equals(Credential.Dispositivo.Ip))
                        {
                            DisposeMkConnection();
                            SetCredential();
                        }
                    }
                    Init();

                    await Task.Run(() => IsConnectedToClon());

                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
        }
    }
}
