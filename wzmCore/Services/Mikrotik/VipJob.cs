﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net.Objects;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Administration;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using wzmCore.Base;
using wzmCore.Services.System;
using tik4net.Objects.Interface;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class VipJob : ApplicationService
    {
        #region Private Members

        private List<HostsServices> _cantidadHost;
        private List<HostsServices> _temp;

        private InternetInformation _internetInfo { get; set; }

        private Wireless _wireless { get; set; }

        private Routes _routes { get; set; }

        private AddressList _addressList;

        private Nat _nat { get; set; }

        private Mangle _mangle { get; set; }

        private UserManagement _usersManagement;

        #endregion



        public VipJob(IServiceProvider serviceProvider, ILogger<VipJob> logger, EtecsaInternetPortalsJob etecsaInternet, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider, etecsaInternet)
        {

            _routes = new Routes();
            _internetInfo = new InternetInformation();
            _wireless = new Wireless();
            _addressList = new AddressList();
            _usersManagement = new UserManagement();
            _addressList = new AddressList();

            _nat = new Nat();
            _mangle = new Mangle();


        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            try
            {
                if (Connection.IsOpened)
                {
                    RemovePortalException(wzmData.Enum.ServiceType.Vip);

                    _cantidadHost = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => !c.Disabled && c.IsBalanced == false && c.Vip == true).ToList();
                    //_temp = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => !c.Disabled && c.IsBalanced == false && c.Vip == true).ToList();
                    _temp = _cantidadHost.ToList();
                    await ChangePortalAsync(PortalesEtecsa);
                }
                else
                {
                    Logger.LogCritical("La conexión con el dispositivo se ha interrumpido.");
                }

            }
            catch (Exception e)
            {
                Logger.LogCritical(e.Message);
            }



        }

        private async Task<List<HostsServices>> ChangePortalAsync(List<PortalesInternet> portales)
        {

            try
            {
                return await Task.Run(() => ChangePortalTest(portales.ToList().OrderBy(c => c.Id).ToList()));
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                return null;
            }

        }

        /// <summary>
        /// Cambia de Portal
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="credential"></param>
        private List<HostsServices> ChangePortal()
        {

            var mkAddressList = _addressList.GetAddressList(Connection).ToList();
            var mkR = _routes.GetMarkRoutes(Connection);
            var mkWirelles = _wireless.GetAllWireless(Connection).Where(c => c.InterfaceType.Equals("virtual")).ToList();

            // ¿Si el host ya tiene internet, que sentido tiene cambiarlo para otro portal?, esto se podria mejorar viendo el consumo de ancho de banda de cada portal
            _usersManagement.UserWithInternet(mkAddressList, ref _cantidadHost, Connection);

            int cantWithInternet = _cantidadHost.Where(c => c.HasInternet).Count();
            //_cantidadPortales = GetCantidadPortales(Connection);

            // Compruebo si todos los host ya tienen Internet
            if (cantWithInternet != _cantidadHost.Where(c => !c.IsBalanced).ToList().Count)
            {

                for (int i = 0; i < mkWirelles.Count; i++)
                {
                    var w = mkWirelles[i].Name;
                    var r = mkR.Where(c => c.Gateway.Contains(w)).First();

                    // Analizo si hay internet en ese portal  
                    // Pudiera obtener primero cuales son los portales que tienen internet y despues cambiar los usuarios de forma aleatoria para cada uno de ellos
                    // de esa manera si tengo 5 usuaios con el servicio no me los pondra en un solo portal
                    // si no que me los regara por todos los portales que tengan Internet.
                    if (_internetInfo.HasInternet(Connection, r.MarkRoute))
                    {

                        try
                        {
                            // Hago los cambios en la lista de host y optimizo solo haciendolo a los que no tienen internet

                            var usersWithoutInternet = _cantidadHost.Where(c => !c.HasInternet);

                            if (usersWithoutInternet.Count() > 0)
                            {
                                string users = string.Empty;
                                // Este proceso es muy costoso para el mikrotik 
                                var adressLists = _addressList.GetAddressList(Credential);
                                foreach (var host in usersWithoutInternet)
                                {
                                    // ¿Pero que sucede si el usuario no se encuentra en ningun addressList?
                                    var userAddressList = new FirewallAddressList();

                                    if (adressLists.Any(c => c.Address.Equals(host.Device.Ip)))
                                    {
                                        userAddressList = adressLists.FirstOrDefault(c => c.Address.Equals(host.Device.Ip));
                                    }
                                    else
                                    {

                                        userAddressList.List = _addressList.SetAddressList(host.Device, Connection);
                                        userAddressList.Address = host.Device.Ip;
                                    }

                                    if (userAddressList.List != r.MarkRoute)
                                    {

                                        host.HasInternet = true;
                                        FirewallAddressViewModel FirewalladdressList = new FirewallAddressViewModel() { List = r.MarkRoute, Address = userAddressList.Address, Usuario = userAddressList.Comment, Disabled = userAddressList.Disabled, Id = userAddressList.Id };
                                        _addressList.UpdateAddressList(FirewalladdressList, Connection);
                                        host.PortalActual = r.MarkRoute;

                                        ApplicationDbContext.Update(host);
                                        ApplicationDbContext.SaveChanges();

                                        if (users != string.Empty)
                                        {
                                            users = users + "," + host.Device.Comment;
                                        }
                                        else
                                        {
                                            users = host.Device.Comment;
                                        }
                                    }

                                }
                                if (users != string.Empty)
                                {
                                    Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Se ha encontrado Internet en el portal {r.MarkRoute}.{Environment.NewLine}[{DateTime.Now.TimeOfDay}] Se ha movido a [{users}] a ese portal.");
                                    Connection.LogWarning($"Se ha encontrado Internet en el portal {r.MarkRoute}.{Environment.NewLine}Se ha movido a [{users}] a ese portal.");
                                }

                            }



                        }
                        catch (Exception e)
                        {
                            Logger.LogError(e.Message);
                        }
                    }
                }
                if (_cantidadHost.All(c => !c.HasInternet))
                {
                    foreach (var item in _cantidadHost)
                    {
                        if (!item.PortalActual.Equals(item.Device.PortalDeOrigen))
                        {
                            try
                            {
                                _usersManagement.ReturnToOriginalPortal(item.Device, Connection);
                                Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Moviendo usuario {item.Device.Comment} a portal {item.Device.PortalDeOrigen}.{Environment.NewLine}[{DateTime.Now.TimeOfDay}] Condición: Internet en el portal {item.PortalActual} ha terminado.");
                                Connection.LogWarning($"[{DateTime.Now.TimeOfDay}] Moviendo usuario {item.Device.Comment} a portal {item.Device.PortalDeOrigen}.{Environment.NewLine} Condición: Internet en el portal {item.PortalActual} ha terminado.");
                                item.PortalActual = item.Device.PortalDeOrigen;
                                item.HasInternet = false;
                                ApplicationDbContext.HostsServices.Update(item);
                                ApplicationDbContext.SaveChanges();


                            }
                            catch (Exception e)
                            {
                                Logger.LogError(e.Message);
                            }
                        }

                    }



                }
            }
            return _cantidadHost;

        }

        private List<HostsServices> ChangePortal(List<PortalesInternet> portalesInternet)
        {
            // AddressList
            var mkAddressList = _addressList.GetAddressList(Connection).ToList();
            // Rutas
            var mkR = _routes.GetMarkRoutes(Connection);
            // Wireless
            var mkWireless = _wireless.GetAllWireless(Connection).Where(c => c.InterfaceType.Equals("virtual")).ToList();

            // ¿Si el host ya tiene internet, que sentido tiene cambiarlo para otro portal?, esto se podria mejorar viendo el consumo de ancho de banda de cada portal
            _usersManagement.UserWithInternet(mkAddressList, ref _cantidadHost, Connection);


            // Compruebo si todos los host ya tienen Internet
            if (_cantidadHost.Where(c => c.HasInternet).Count() != _cantidadHost.Count)
            {
                foreach (var item in portalesInternet)
                {
                    // Analizo si hay internet en ese portal  
                    // Pudiera obtener primero cuales son los portales que tienen internet y despues cambiar los usuarios de forma aleatoria para cada uno de ellos
                    // de esa manera si tengo 5 usuaios con el servicio no me los pondra en un solo portal
                    // si no que me los regara por todos los portales que tengan Internet.
                    try
                    {
                        // Hago los cambios en la lista de host y optimizo solo haciendolo a los que no tienen internet

                        var usersWithoutInternet = _cantidadHost.Where(c => !c.HasInternet);

                        if (usersWithoutInternet.Count() > 0)
                        {
                            string users = string.Empty;
                            var r = mkR.Where(c => c.Gateway.Contains(item.Portal)).First();
                            var adressLists = _addressList.GetAddressList(Credential);
                            foreach (var host in usersWithoutInternet)
                            {
                                var userAddressList = new FirewallAddressList();
                                if (adressLists.Any(c => c.Address.Equals(host.Device.Ip)))
                                {
                                    userAddressList = adressLists.FirstOrDefault(c => c.Address.Equals(host.Device.Ip));
                                }
                                else
                                {

                                    userAddressList.List = _addressList.SetAddressList(host.Device, Connection);
                                    userAddressList.Address = host.Device.Ip;
                                }
                                // ¿Pero que sucede si el usuario no se encuentra en ningun addressList?



                                if (userAddressList.List != r.MarkRoute)
                                {

                                    host.HasInternet = true;
                                    FirewallAddressViewModel FirewalladdressList = new FirewallAddressViewModel() { List = r.MarkRoute, Address = userAddressList.Address, Usuario = userAddressList.Comment, Disabled = userAddressList.Disabled, Id = userAddressList.Id };
                                    _addressList.UpdateAddressList(FirewalladdressList, Connection);
                                    host.PortalActual = r.MarkRoute;

                                    ApplicationDbContext.Update(host);
                                    ApplicationDbContext.SaveChanges();

                                    if (users != string.Empty)
                                    {
                                        users = users + "," + host.Device.Comment;
                                    }
                                    else
                                    {
                                        users = host.Device.Comment;
                                    }
                                }

                            }
                            if (users != string.Empty)
                            {
                                Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Se ha encontrado Internet en el portal {r.MarkRoute}.{Environment.NewLine}[{DateTime.Now.TimeOfDay}] Se ha movido a [{users}] a ese portal.");
                                Connection.LogWarning($"Se ha encontrado Internet en el portal {r.MarkRoute}.{Environment.NewLine}Se ha movido a [{users}] a ese portal.");
                            }

                        }

                    }
                    catch (Exception e)
                    {
                        Logger.LogError(e.Message);
                    }
                }

                if (_cantidadHost.All(c => !c.HasInternet))
                {
                    foreach (var item in _cantidadHost)
                    {
                        if (item.PortalActual != null)
                        {
                            if (!item.PortalActual.Equals(item.Device.PortalDeOrigen))
                            {
                                try
                                {
                                    _usersManagement.ReturnToOriginalPortal(item.Device, Connection);
                                    Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Moviendo usuario {item.Device.Comment} a portal {item.Device.PortalDeOrigen}.{Environment.NewLine}[{DateTime.Now.TimeOfDay}] Condición: Internet en el portal {item.PortalActual} ha terminado.");
                                    Connection.LogWarning($"[{DateTime.Now.TimeOfDay}] Moviendo usuario {item.Device.Comment} a portal {item.Device.PortalDeOrigen}.{Environment.NewLine} Condición: Internet en el portal {item.PortalActual} ha terminado.");
                                    item.PortalActual = item.Device.PortalDeOrigen;
                                    item.HasInternet = false;
                                    ApplicationDbContext.HostsServices.Update(item);
                                    ApplicationDbContext.SaveChanges();


                                }
                                catch (Exception e)
                                {
                                    Logger.LogError(e.Message);
                                }
                            }
                        }


                    }



                }
            }
            return _cantidadHost;

        }

        private List<HostsServices> ChangePortalTest(List<PortalesInternet> portalesInternet)
        {
            // AddressList
            //var mkAddressList = _addressList.GetAddressList(Connection).ToList();
            // Rutas
            //var mkR = _routes.GetMarkRoutes(Connection);
            // Wireless
            // _interfaceWireless = _wireless.GetOnlyStationPseudobridge(Connection);

            // ¿Si el host ya tiene internet, que sentido tiene cambiarlo para otro portal?, esto se podria mejorar viendo el consumo de ancho de banda de cada portal

            // Metodo mas que probado... pero costoso
            //_usersManagement.UserWithInternet(mkAddressList, ref _cantidadHost, Connection);

            // Con este proceso siempre estaré confiando en la actualizacion de los protales que tienen internet
            _usersManagement.UserWithInternet(portalesInternet, ref _cantidadHost);

            //Este proceso es costoso para el mikrotik
            //foreach (var item in _temp)
            //{
            //    mkAddressList = _addressList.GetAddressList(Connection).ToList();
            //    if (_usersManagement.UserIsBalanced(item.Device.Ip, mkAddressList, "wzmBalanceo"))
            //    {

            //        _cantidadHost.Remove(item);
            //    }

            //}


            ChangeDevicePortal(portalesInternet, _cantidadHost/*.Where(c => c.HasInternet == false)*/.ToList()/*, mkR*/);

            if (_cantidadHost.All(c => !c.HasInternet))
            {
                foreach (var item in _cantidadHost)
                {
                    if (item.PortalActual != null)
                    {
                        if (!item.PortalActual.Equals(item.Device.PortalDeOrigen))
                        {
                            try
                            {
                                _usersManagement.ReturnToOriginalPortal(item.Device, Connection);
                                Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Moviendo usuario {item.Device.Comment} al portal {item.Device.PortalDeOrigen}.{Environment.NewLine}[{DateTime.Now.TimeOfDay}] Condición: Internet en el portal {item.PortalActual} ha terminado.");
                                Connection.LogWarning($"[{DateTime.Now.TimeOfDay}] Moviendo usuario {item.Device.Comment} al portal {item.Device.PortalDeOrigen}.{Environment.NewLine} Condición: Internet en el portal {item.PortalActual} ha terminado.");
                                item.PortalActual = item.Device.PortalDeOrigen;
                                item.HasInternet = false;
                                ApplicationDbContext.HostsServices.Update(item);
                                ApplicationDbContext.SaveChanges();
                            }
                            catch (Exception e)
                            {
                                Logger.LogError(e.Message);
                            }
                        }
                    }
                }
            }

            return _cantidadHost;

        }

        private void ChangeDevicePortal(List<PortalesInternet> portalesInternet, List<HostsServices> hosts/*, List<RouteViewModel> mkR*/)
        {
            if (portalesInternet.Count() > 0)
            {

                while (hosts.Count() != 0)
                {
                    // este proceso es costoso para el mikrotik, mientras mas addressList exista en el mikrotik mas costoso sera este proceso
                    //var adressLists = _addressList.GetAddressList(Connection);

                    // Espero de esta forma sea mas eficiente

                    foreach (var item in portalesInternet)
                    {

                        if (hosts.Count() == 0)
                        {
                            break;
                        }
                        else
                        {
                            var host = hosts[0];
                            var adressList = Connection.LoadList<FirewallAddressList>(Connection.CreateParameter("address", host.Device.Ip));
                            //var r = mkR.Where(c => c.Gateway.Contains(item.Portal)).First();
                            // este proceso es costoso para el mikrotik, mientras mas addressList exista en el mikrotik mas costoso sera este proceso
                            //var adressLists = _addressList.GetAddressList(Credential);


                            var userAddressList = new FirewallAddressList();
                            if (adressList.Count() > 0)
                            {
                                userAddressList = adressList.FirstOrDefault(c => c.Address.Equals(host.Device.Ip));
                            }
                            else
                            {

                                userAddressList.List = _addressList.SetAddressList(host.Device, Connection);
                                userAddressList.Address = host.Device.Ip;
                            }

                            //if (adressLists.Any(c => c.Address.Equals(host.Device.Ip)))
                            //{
                            //    userAddressList = adressLists.FirstOrDefault(c => c.Address.Equals(host.Device.Ip));
                            //}
                            //else
                            //{

                            //    userAddressList.List = _addressList.SetAddressList(host.Device, _interfaceWireless, mkR, Connection);
                            //    userAddressList.Address = host.Device.Ip;
                            //}


                            if (userAddressList.List != item.Route)
                            {

                                host.HasInternet = true;

                                RefreshDatabaseInstance();

                                var temphost = ApplicationDbContext.HostsServices.Include(c => c.Device).FirstOrDefault(c => c.Id.Equals(host.Id));

                                if (!temphost.IsBalanced)
                                {
                                    FirewallAddressViewModel FirewalladdressList = new FirewallAddressViewModel() { List = item.Route, Address = userAddressList.Address, Usuario = userAddressList.Comment, Disabled = userAddressList.Disabled, Id = userAddressList.Id };
                                    _addressList.UpdateAddressList(FirewalladdressList, Connection);
                                    temphost.PortalActual = item.Route;

                                    ApplicationDbContext.Update(temphost);
                                    ApplicationDbContext.SaveChanges();

                                    Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Se ha movido al portal {item.Route} al usuario [{host.Device.Comment}].");
                                    Connection.LogWarning($"[{DateTime.Now.TimeOfDay}] Se ha movido al portal {item.Route} al usuario [{host.Device.Comment}].");
                                }

                                //FirewallAddressViewModel FirewalladdressList = new FirewallAddressViewModel() { List = r.MarkRoute, Address = userAddressList.Address, Usuario = userAddressList.Comment, Disabled = userAddressList.Disabled, Id = userAddressList.Id };
                                //_addressList.UpdateAddressList(FirewalladdressList, Connection);
                                //host.PortalActual = r.MarkRoute;

                                //ApplicationDbContext.Update(host);
                                //ApplicationDbContext.SaveChanges();

                                //Logger.LogInformation($"[{DateTime.Now.TimeOfDay}] Se ha movido al portal {r.MarkRoute} al [{host.Device.Comment}].");
                                //Connection.LogWarning($"[{DateTime.Now.TimeOfDay}] Se ha movido al portal {r.MarkRoute} al [{host.Device.Comment}].");
                            }

                            hosts.Remove(host);
                        }
                    }
                }
            }

        }

        private void GetRelation(List<PortalesInternet> portalesInternet)
        {
            if (_cantidadHost.Count() > portalesInternet.Count())
            {

            }
        }
    }
}
