﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Interfaces;
using wzmData.Models;
using wzmCore.Base;
using wzmCore.Services.System;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class BalanceoNTHJob : Balanceo
    {
        
        public BalanceoNTHJob(IServiceProvider serviceProvider, EtecsaInternetPortalsJob etecsaInternet, ILogger<BalanceoNTHJob> logger, IWzmCore wzmCore) : base(serviceProvider, logger, wzmCore, etecsaInternet)
        {
            BalanceoComment = "wzm_balanceo_NTH";
           
        }
               
        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            try
            {
                if (Connection.IsOpened)
                {
                    RemovePortalException(wzmData.Enum.ServiceType.BalanceadorNTH);
                    Hosts = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => !c.Disabled && c.Balanceo).ToList();
                    await UpdateBalanceoRulesAsync(Hosts, PortalesEtecsa);
                }
                else
                {
                    Logger.LogCritical("La conexión con el dispositivo se ha interrumpido.");
                }

            }
            catch (Exception e)
            {

                Logger.LogCritical(e.Message);
            }

        }

    }


}

