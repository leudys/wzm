﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Base;
using wzmCore.Interfaces;
using wzmCore.Services.System;
using wzmData.Models;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class BalanceoPCCJob : Balanceo
    {
        private EtecsaInternetPortalsJob _etecsaInternet;

        public BalanceoPCCJob(IServiceProvider serviceProvider, EtecsaInternetPortalsJob etecsaInternet, ILogger<BalanceoPCCJob> logger, IWzmCore wzmCore) : base(serviceProvider, logger, wzmCore, etecsaInternet)
        {
            BalanceoComment = "wzm_balanceo_PCC";
            _etecsaInternet = etecsaInternet;
        }

        internal override async Task<bool> ExisteBalanceo(List<BalanceoModel> portales, List<FirewallMangle> mangles, List<HostsServices> hosts)
        {
            if (portales.Count < 2)
            {
                if (mangles.Count > 0)
                {
                    RemoveMangles();
                }
                if (IsNatActivated())
                {
                    RemoveNat();
                }


                await RemoveUsersFromBalancing(hosts);
                return false;
            }
            return true;
        }

        protected override void AddMangleAndNat(List<BalanceoModel> portales, int nhilos)
        {
            int i = -1;
            foreach (var item in portales)
            {
                i++;
                string pcc = $"{nhilos}/{i}";
                Mangle.AddCustomMangle(Connection,
                    new FirewallMangle()
                    {
                        Action = FirewallMangle.ActionType.MarkConnection,
                        Chain = "prerouting",
                        Comment = BalanceoComment,
                        DstAddressType = "!local",
                        NewConnectionMark = item.ConnectionMark,
                        PerConnectionClassifier = $"both-addresses-and-ports:{pcc}",
                        Passthrough = true,
                        InIterface = MasterInterface,
                        SrcAddressList = AaddressList,

                    });

                Mangle.AddCustomMangle(Connection, new FirewallMangle()
                {
                    Action = FirewallMangle.ActionType.MarkRouting,
                    Chain = "prerouting",
                    Comment = BalanceoComment,
                    ConnectionMark = item.ConnectionMark,
                    NewRoutingMark = item.Route,
                    Passthrough = false,
                    InIterface = MasterInterface,
                    SrcAddressList = AaddressList
                });

                var nat = new FirewallNat()
                {
                    Action = "masquerade",
                    Chain = "srcnat",
                    Comment = BalanceoComment,
                    OutInterface = item.Interfaz,
                    Disabled = false,
                    RoutingMark = item.Route,
                    SrcAddressList = AaddressList
                };
                Nat.AddCustomNat(nat, null, Connection);
            }
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            try
            {
                await base.Execute(context);

                if (Connection.IsOpened)
                {
                    RemovePortalException(wzmData.Enum.ServiceType.BalanceadorPCC);
                    Hosts = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => !c.Disabled && c.Balanceo).ToList();
                    await UpdateBalanceoRulesAsync(Hosts, PortalesEtecsa);
                }
                else
                {
                    Logger.LogCritical("La conexión con el dispositivo se ha interrumpido.");
                }
            }
            catch (Exception e)
            {
                Logger.LogCritical(e.Message);
            }

        }
    }
}
