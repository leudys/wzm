﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmCore.Data.Mikrotik;
using wzmCore.Base;
using wzmCore.Extensions;
using static tik4net.Objects.Ip.IpDhcpClient;
using wzmCore.Services;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class ReconnectionByPingJob : ApplicationService
    {

        #region Private Members
        private List<IpDhcpClient> _dhcpClients;
        private static Routes _route => new Routes();

        private List<PingModel> _reconnectionList = new List<PingModel>();

        private List<ReconnectionList> _reconnection;

        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="logger"></param>
        public ReconnectionByPingJob(IServiceProvider serviceProvider, ILogger<ReconnectionByPingJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {
            _reconnection = new List<ReconnectionList>();
            _dhcpClients = new List<IpDhcpClient>();
        }
        #endregion

        /// <summary>
        /// Ejecuta la acción de reconexión de interfaces del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            SetCredential();
            _reconnectionList.Clear();

            // Obtengo los DHCP Clients
            var temp = Connection.LoadAll<IpDhcpClient>().Where(c => c.Disabled == false).ToList();

            // Obtengo las rutas para poder coger el MarkRoute del DHCP Client al cual peretence
            var routes = _route.GetMarkRoutes(Connection);

            //Compruebo si el dispositivo está configurado para multiprtales o no
            if (temp.Count() > 0)
            {
                // Está configurado para multiportales

                await ModelForMultipleInterfacesAsync(routes, temp);
            }

        }
        public override bool SetCredential()
        {
            Credential = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true);
            VLAN_ETECSA = false;
            return VLAN_ETECSA;
        }
        #region Reconnection

        /// <summary>
        /// Llena la lista <see cref="List<PingModel>"/>
        /// </summary>
        /// <param name="routes"></param>
        private async Task ModelForMultipleInterfacesAsync(List<RouteViewModel> routes, List<IpDhcpClient> temp)
        {
            _dhcpClients.Clear();
            _dhcpClients = temp;

            if (_dhcpClients.Count >= 20)
            {
                SetReconnectionList(10);

            }
            else
            {
                if (_dhcpClients.Count >= 10)
                {
                    SetReconnectionList(5);

                }
                else
                {
                    SetReconnectionList(2);

                }
            }


            await _reconnection.ReconnectAsync(routes, Logger);
        }

        private void SetReconnectionList(int chunk)
        {
            var sd = _dhcpClients.ChunkBy(chunk);

            if (sd.Count != _reconnection.Count)
            {
                if (sd.Count - _reconnection.Count < 0)
                {
                    int index1 = _reconnection.IndexOf(_reconnection.Last());
                    int index2 = sd.IndexOf(sd.Last());

                    for (int i = index2; i <= index1; i++)
                    {
                        _reconnection.CloseConnection(i);
                        _reconnection.RemoveAt(i);
                    }
                }
                if (sd.Count - _reconnection.Count > 0)
                {
                    if (_reconnection.Count != 0)
                    {
                        int index1 = _reconnection.IndexOf(_reconnection.Last());
                        index1 = index1 + 1;

                        for (int i = index1; i <= sd.Count; i++)
                        {
                            _reconnection.Add(new ReconnectionList() { IpDhcpClients = sd[index1], Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password) });
                        }
                    }
                    else
                    {
                        foreach (var item in sd)
                        {
                            _reconnection.Add(new ReconnectionList() { IpDhcpClients = item, Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password) });
                        }
                    }

                }
            }
            else
            {
                UpdateDhcpClients(sd);
            }

        }

        private void UpdateDhcpClients(List<List<IpDhcpClient>> sd)
        {
            int tempIndex = 0;
            foreach (var item in sd)
            {
                // Siempre actualizo los clientes DHCP, pero mantengo abierta la conexion hacia el dispositivo
                _reconnection[tempIndex].IpDhcpClients.Clear();
                _reconnection[tempIndex].IpDhcpClients.AddRange(item);
                tempIndex++;
            }
        }

        #endregion
    }
}

internal class ReconnectionList
{
    internal ITikConnection Connection { get; set; }

    internal int RebindingCount { get; set; }

    internal List<IpDhcpClient> IpDhcpClients { get; set; }

    internal List<RouteViewModel> Routes { get; set; }

    internal ILogger<object> Logger { get; set; }

    internal async Task ReconnectAsync()
    {
        var reconnectionList = new List<PingModel>();

        foreach (var item in IpDhcpClients)
        {
            if (item.Status != ClientStatus.Bound)
            {
                Log.WriteErrorMessage(Connection, $"WZM: El DHCP Client {item.Interface} se encuentra {item.Status}");
            }
            else
            {
                if (item.Interface.Equals("wlan1"))
                {
                    item.Renew(Connection);
                }
                else
                {
                    if (Routes.Any(c => c.Gateway.Contains(item.Interface)))
                    {
                        string markroute = Routes.First(c => c.Gateway.Contains(item.Interface)).MarkRoute;
                        var connected = await ConnectedToEtecsa.IsConnectedAsync(Connection, markroute);
                        reconnectionList.Add(new PingModel() { Wlan = item.Interface, Connected = connected, MarkRoute = markroute });
                    }
                }
            }


        }

        if (reconnectionList.Any(c => !c.Connected))
        {
            List<PingModel> reconnectedList = await GetReconnectedList(reconnectionList.Where(c => !c.Connected));
            if (reconnectedList.Any(c => !c.Connected))
            {
                Thread.Sleep(5000);
                await CheckReconnection(reconnectedList);
            }
        }
    }

    private async Task<List<PingModel>> GetReconnectedList(IEnumerable<PingModel> enumerable)
    {

        foreach (var item in enumerable)
        {

            var connected = await ConnectedToEtecsa.IsConnectedAsync(Connection, item.MarkRoute);

            if (!connected)
            {
                Connection.LogWarning($"[Reconnect by Ping] Reconectando interfaz {item.Wlan} usando método renew.");
                Logger.LogInformation($"Reconectando interfaz {item.Wlan} usando método renew.");
                item.Renewed = true;
                ReconnectWithRenew(item.Wlan);
            }


        }

        return enumerable.ToList();
    }

    private async Task CheckReconnection(List<PingModel> reconnectedList)
    {
        string l = string.Empty;

        foreach (var item in reconnectedList)
        {
            if (l != string.Empty)
            {
                l = $"{l},{item.Wlan}";
            }

            else
            {
                l = item.Wlan;
            }
        }


        Logger.LogInformation($"Chequeando que las interfaces {l} se hayan conectado.");
        Connection.LogWarning($"[Reconnect by Ping] Chequeando que las interfaces {l} se hayan conectado.");
        //_logger.LogInformation($"Chequeando que las interfaces {l} se hayan conectado.");
        foreach (var item in reconnectedList)
        {
            await Check(item);
        }



    }

    /// <summary>
    /// Chequea cuál o cuales interfaces se reconectaron
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    private async Task Check(PingModel item)
    {

        var connected = await ConnectedToEtecsa.IsConnectedAsync(Connection, item.MarkRoute);
        if (!connected)
        {
            Logger.LogInformation("Reconectando interfaz " + item.Wlan + " usando método release.");
            //Connection.LogWarning($"[Reconnect by Ping] Reconectando interfaz " + item.Wlan + " usando método renew.");

            ReconnectWithRelease(item.Wlan);
        }
        else
        {
            Logger.LogInformation("La interfaz " + item.Wlan + " se ha reconectado correctamente.");
            Connection.LogWarning($"[Reconnect by Ping] La interfaz " + item.Wlan + " se ha reconectado correctamente.");
        }

    }

    /// <summary>
    /// Intenta una reconexión utilizando el método Release del DHCP Client
    /// </summary>
    /// <param name="wlan"></param>
    private void ReconnectWithRelease(string wlan)
    {

        IpDhcpClients.Find(c => c.Interface == wlan).Release(Connection);
        Connection.LogWarning($"Se ha reconectado la interfaz: { wlan} utilizando Release");
        Logger.LogInformation($"Se ha reconectado la interfaz: { wlan} utilizando Release");
        Connection.LogInfo(string.Format("Cambiando Mac", wlan));


    }

    /// <summary>
    /// Intenta una reconexión utilizando el método Renew del DHCP Client
    /// </summary>
    /// <param name="wlan"></param>
    private void ReconnectWithRenew(string wlan)
    {
        IpDhcpClients.Find(c => c.Interface.Equals(wlan)).Renew(Connection);
    }
}

internal static class ReconnectionListExtension
{
    internal static async Task ReconnectAsync(this List<ReconnectionList> reconnectionList, List<RouteViewModel> routes, ILogger<object> logger)
    {

        List<Task> tasks = new List<Task>();

        foreach (var item in reconnectionList)
        {
            item.Routes = routes;
            item.Logger = logger;
            tasks.Add(item.ReconnectAsync());
            //Thread.Sleep(TimeSpan.FromSeconds(1));
        }
        try
        {
            await Task.WhenAll(tasks);
            //reconnectionList.CloseConnection();
        }
        catch (Exception e)
        {
            logger.LogError(e.Message);
        }

    }

    internal static void CloseConnection(this List<ReconnectionList> reconnectionList)
    {
        foreach (var item in reconnectionList)
        {
            if (item.Connection.IsOpened)
            {
                item.Connection.Dispose();
            }
        }
    }

    internal static void CloseConnection(this List<ReconnectionList> reconnectionList, int index)
    {
        var connection = reconnectionList[index].Connection;


        if (connection.IsOpened)
        {
            connection.Dispose();
        }

    }
}
