﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;
using tik4net.Objects;
using tik4net.Objects.Ip;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmCore.Base;
using System.Threading;
using tik4net.Objects.Interface.Wireless;
using tik4net.Objects.Interface;
using wzmCore.Services.Helpers;
using Microsoft.EntityFrameworkCore;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class ReconnectionByDefaultJob : ApplicationService
    {
        private static Wireless _wireless = new Wireless();
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="logger"></param>
        public ReconnectionByDefaultJob(IServiceProvider serviceProvider, ILogger<ReconnectionByDefaultJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {


        }
        #endregion

        /// <summary>
        /// Ejecuta la acción de reconexión de interfaces del Mikrotik
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            SetCredential();
            var registration = Connection.LoadList<WirelessRegistrationTable>(Connection.CreateParameter("rx-rate", "---")).ToList();

            if (registration.Any(c => c.RxRate.Equals("---")))
            {
                var dhcpClients = Connection.LoadAll<IpDhcpClient>().Where(c => c.Disabled == false).ToList();

                foreach (var item in registration)
                {
                    if (dhcpClients.Any(c => c.Interface.Equals(item.Interface)))
                    {
                        var dh = dhcpClients.FirstOrDefault(c => c.Interface.Equals(item.Interface));
                        if (!dh.Disabled)
                        {
                            LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Warning, $"[Reconnection by Default] La interfaz {item.Interface} se encuentra desconectada. Se le aplicara RENEW");
                            dhcpClients.Find(c => c.Interface.Equals(item.Interface)).Renew(Connection);
                            // Espero un segundo...

                        }

                    }
                }
            }
        }

        public override bool SetCredential()
        {
            Credential = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true);
            VLAN_ETECSA = false;
            return VLAN_ETECSA;
        }

    }

    [DisallowConcurrentExecution]
    public class ReconnectionByDefaultDisconnectedJob : ApplicationService
    {
        public ReconnectionByDefaultDisconnectedJob(IServiceProvider serviceProvider, ILogger<ReconnectionByDefaultJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {

        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            var wireless = Connection.LoadList<InterfaceWireless>(Connection.CreateParameter("running", "false")).ToList();

            foreach (var item in wireless)
            {

                LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Warning, $"[Reconnection by Default] La interfaz {item.Name} no está corriendo, se deshabilitara/habilitara.");
                if (item.Disabled)
                {
                    item.Disabled = false;
                    Connection.Save(item);
                }
                else
                {
                    item.Disabled = true;
                    Connection.Save(item);
                    item.Disabled = false;
                    Connection.Save(item);
                }
                Thread.Sleep(TimeSpan.FromSeconds(20));

            }
        }



    }
}
