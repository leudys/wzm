﻿using System;
using System.Collections.Generic;
using System.Linq;
using wzmCore.Data;
using wzmCore.Services.Helpers;
using wzmCore.Services.System;
using wzmData.Enum;

namespace wzmCore.Services
{
    public static class ServicesGenerator
    {

        //public static List<ServiceModel> GetServices()
        //{
        //    return Generate();
        //}

        public static List<ServiceModel> GetStaticServices()
        {
            List<ServiceModel> services = new List<ServiceModel>();
            //services.Add(new ServiceModel() { ServiceType = typeof(ScanJob) });
            services.Add(new ServiceModel() { ServiceType = typeof(ResetTimeJob), CronExpression = "0 0 8 * * ?" });
            //services.Add(new ServiceModel() { ServiceType = typeof(LicenseChecker) });
            //services.Add(new ServiceModel() { ServiceType = typeof(ActiveDevices), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(ReconnectionByDefaultDisconnectedJob), CronExpression = "0/5 * * * * ?" });
            //services.Add(new ServiceModel() { ServiceType = typeof(ServerToInternet), CronExpression = "0 30 0 ? * * *" });
            //services.Add(new ServiceModel() { ServiceType = typeof(TracerRouteJob), CronExpression = "0 0 * ? * * *" });
            //services.Add(new ServiceModel() { ServiceType = typeof(DeviceSignalMonitor), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(EtecsaInternetPortalsJob) });
            
            return services;
        }

        public static List<ServiceModel> GetAll()
        {
            List<ServiceModel> services = new List<ServiceModel>();

            services.Add(new ServiceModel() { ServiceType = typeof(ScanJob) });
            services.Add(new ServiceModel() { ServiceType = typeof(EtecsaInternetPortalsJob) });
            services.Add(new ServiceModel() { ServiceType = typeof(ReconnectionByPingJob), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(ReconnectionByDefaultJob), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(DeviceSignalMonitor), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(VipJob) });
            //services.Add(new ServiceModel() { ServiceType = typeof(VpnHelper), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(AntiClonJob) });
            services.Add(new ServiceModel() { ServiceType = typeof(LicenseChecker) });
            services.Add(new ServiceModel() { ServiceType = typeof(NautaConnectorJob), CronExpression = "1 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(SimpleQueueJob), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(BalanceoNTHJob), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(BalanceoPCCJob), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(ActiveDevices), CronExpression = "0/5 * * * * ?" });
            services.Add(new ServiceModel() { ServiceType = typeof(TracerRouteJob), CronExpression = "0 0 * ? * * *" });
            services.Add(new ServiceModel() { ServiceType = typeof(ServerToInternet), CronExpression = "0 30 0 ? * * *" });
            return services;
        }

        public static ServiceModel GetServiceModel(Type type)
        {
            return GetAll().FirstOrDefault(c => c.ServiceType == type);

        }
    }
}
