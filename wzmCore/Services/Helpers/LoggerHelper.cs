﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using tik4net;
using tik4net.Objects;

namespace wzmCore.Services.Helpers
{
    public static class LoggerHelper
    {
        private const string _mk = "Wzm: ";

        public static void Log(ILogger<object> logger, LogLevel logLevel, string message)
        {
            switch (logLevel)
            {
                case LogLevel.Warning:
                    logger.LogWarning(message);
                    break;
                case LogLevel.Error:
                    logger.LogError(message);
                    break;
                case LogLevel.Info:
                    logger.LogInformation(message);
                    break;

            }
        }

        public static void Log(ITikConnection connection, LogLevel logLevel, string message)
        {
            switch (logLevel)
            {
                case LogLevel.Warning:
                    connection.LogWarning($"{_mk}{message}");
                    break;
                case LogLevel.Error:
                    connection.LogError($"{_mk}{message}");
                    break;
                case LogLevel.Info:
                    connection.LogInfo($"{_mk}{message}");
                    break;
                default:
                    break;
            }
        }

        public static void Log(ITikConnection connection, ILogger<object> logger, LogLevel logLevel, string message)
        {
            switch (logLevel)
            {
                case LogLevel.Warning:
                    connection.LogWarning($"{_mk}{message}");
                    if (logger != null)
                    {
                        logger.LogWarning(message);
                    }

                    break;
                case LogLevel.Error:
                    connection.LogError($"{_mk}{message}");
                    logger.LogError(message);
                    break;
                case LogLevel.Info:
                    connection.LogWarning($"{_mk}{message}");
                    logger.LogInformation(message);
                    break;
                default:
                    break;
            }
        }
    }

    public enum LogLevel
    {
        Warning,
        Error,
        Info
    }
}
