﻿using System;
using System.Linq;
using tik4net;
using wzmData.Models;
using wzmCore.PingEnviroment;
using wzmCore.Extensions;

namespace wzmCore.Services
{
    public class InternetInformation
    {

        private PingGoogle _pingGoogle;

        public string GetHost
        {
            get
            {
                return _pingGoogle.Host;
            }
        }

        public string SetHost
        {
            set
            {
                _pingGoogle.Host = value;
            }
        }
                
        public InternetInformation()
        {
            _pingGoogle = new PingGoogle();
        }

        public InternetInformation(string host)
        {
            _pingGoogle = new PingGoogle(host);
        }

        /// <summary>
        /// Comprueba que haya internet en una salida especifica
        /// </summary>
        /// <param name="credential"></param>
        /// <param name="routingTable"></param>
        /// <returns></returns>
        public bool HasInternet(MikrotikCredentials credential, string routingTable)
        {
            try
            {

                using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                {
                    var ping = _pingGoogle.MakePing(connection, routingTable);
                    if (ping.All(c => c.PacketLoss.Equals("100")) || ping.Count == 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                return false;

            }


        }

        /// <summary>
        /// Comprueba que haya internet en una salida especifica
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="routingTable"></param>
        /// <returns></returns>
        public bool HasInternet(ITikConnection connection, string routingTable)
        {
            try
            {

                var ping = _pingGoogle.MakePing(connection, routingTable);
                if (ping.All(c => c.PacketLoss.Equals("100")) || ping.Count == 0)
                {

                    return false;
                }
                return true;


            }
            catch (Exception)
            {
                return false;

            }


        }

    }
}
