﻿using System;

namespace wzmCore.Services.Helpers
{

    public static class MathQueue
    {

        private static int ClientBandWidth = 1000;

        
        public static int SimpleQueuePerClient(int portales, int clients)
        {
            var queueLimit = (decimal)((ClientBandWidth * portales) / clients);

            if (queueLimit < 0 || queueLimit == 0)
            {
                return ClientBandWidth / clients;
            }
            if (queueLimit > ClientBandWidth)
            {
                return ClientBandWidth;
            }
            else
            {
                return (int)Math.Round(queueLimit);
            }

        }

    }
}
