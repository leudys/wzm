﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Quartz;
using SshCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface.Wireless;
using wzmCore.Administration;
using wzmCore.Base;
using wzmCore.Extensions;
using wzmCore.Interfaces;
using wzmData.Enum;
using wzmData.Models;
using wzmData.Ssh;
using wzmData.Ubiquiti;

namespace wzmCore.Services.Helpers
{
    [DisallowConcurrentExecution]
    public class DeviceSignalMonitor : ApplicationService
    {

        private List<DeviceCredentials> _credentials;
        public List<DeviceStatus> DeviceStatus { get; private set; }

        private List<Task> _monitoringTasks;
        public DeviceSignalMonitor(IWzmCore wzmCore, ILogger<DeviceSignalMonitor> log, IServiceProvider serviceProvider) : base(wzmCore, log, serviceProvider)
        {

            _monitoringTasks = new List<Task>();
            DeviceStatus = new List<DeviceStatus>();


        }

        public override async Task Execute(IJobExecutionContext context)
        {
            RefreshDatabaseInstance();

            _credentials = ApplicationDbContext.DeviceCredentials.Include(c => c.Dispositivo).ToList();
            UpddateDevices();
            UpdateCredentials();

            _monitoringTasks.Clear();


            foreach (var item in DeviceStatus)
            {
                if (item.Credential.Dispositivo.Fabricante == DeviceTypeEnum.Mikrotik)
                {
                    _monitoringTasks.Add(item.GetInfo(DeviceTypeEnum.Mikrotik));
                }
                if (item.Credential.Dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti)
                {
                    _monitoringTasks.Add(item.GetInfo(DeviceTypeEnum.Ubiquiti));
                }

            }

            try
            {
                await Task.WhenAll(_monitoringTasks);



            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
            }

        }


        private void UpddateDevices()
        {
            int ds = DeviceStatus.Count();
            int aDS = ApplicationDbContext.DeviceSignals.Count();
            if (ds != aDS)
            {
                foreach (var item in _monitoringTasks)
                {
                    item?.Dispose();
                }

                if (ds > aDS)
                {
                    // Eliminar dispositivo

                    var toDelete = new List<DeviceStatus>();

                    foreach (var item in DeviceStatus)
                    {
                        if (!ApplicationDbContext.DeviceSignals.Any(c => c.DispositivoId.Equals(item.Id)))
                        {
                            toDelete.Add(item);
                        }

                    }

                    foreach (var item in toDelete)
                    {
                        DeviceStatus.Remove(item);
                    }
                }
                if (ds < aDS)
                {
                    // Añadir dispositivo                    

                    foreach (var item in ApplicationDbContext.DeviceSignals)
                    {
                        if (!DeviceStatus.Any(c => c.Id.Equals(item.DispositivoId)))
                        {
                            if (_credentials.Any(c => c.DispositivoId.Equals(item.DispositivoId)))
                            {
                                DeviceStatus.Add(new DeviceStatus()
                                {
                                    Id = item.DispositivoId,
                                    Credential = _credentials.FirstOrDefault(c => c.DispositivoId.Equals(item.DispositivoId))
                                });
                            }
                        }

                    }
                }



            }

        }

        private void UpdateCredentials()
        {
            var tempcr = ApplicationDbContext.DeviceCredentials;
            foreach (var item in tempcr)
            {
                if (DeviceStatus.Any(c => c.Id.Equals(item.DispositivoId)))
                {
                    var cr = DeviceStatus.Find(c => c.Id.Equals(item.DispositivoId));

                    if (!cr.Credential.User.Equals(item.User))
                    {
                        cr.Credential.User = item.User;
                    }
                    if (!cr.Credential.Password.Equals(item.Password))
                    {
                        cr.Credential.User = item.Password;
                    }
                }


            }
        }
    }

    public class DeviceStatus
    {
        public DeviceStatus()
        {
            AirMaxConnection = new SshConnection();
            _mikrotikManager = new MikrotikManager();
        }

        private MikrotikManager _mikrotikManager;
        private ITikConnection TikConnection { get; set; }

        public DeviceCredentials Credential { get; set; }

        public int Id { get; set; }

        private SshConnection AirMaxConnection { get; }

        public bool Connected { get; set; }

        public int Tx { get; private set; }

        public int Rx { get; private set; }

        public int? TaskId { get; set; }

        public int Signal { get; set; }

        public async Task SetSignal_Ubi()
        {
            try
            {
                if (!Connected)
                {
                    Login login = new Login() { Ip = Credential.Dispositivo.Ip, User = Credential.User, Password = Credential.Password, Port = 22 };
                    await AirMaxConnection.Initialize(login);
                    Connected = true;
                }


                var info = (UbiquitiObject)AirMaxConnection.GetJson(CommandType.UbiquitiObject);

                UpdateSignalLevel(info.Wireless.Signal, info.Wireless.Rssi, /*info.Wireless.Noisef,*/ info.Wireless.Rx_chainmask, info.Wireless.Chainrssi);
                Signal = int.Parse(info.Wireless.Signal);
                TaskId = Task.CurrentId;
            }
            catch (Exception)
            {
                Connected = false;
                Tx = 0;
                Rx = 0;
                Signal = 0;
            }
        }

        private void SetSignal_Mk()
        {
            try
            {
                if (!Connected)
                {
                    TikConnection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password);
                    Connected = true;
                }
                var registration = TikConnection.LoadAll<WirelessRegistrationTable>();
                Signal = int.Parse(registration.FirstOrDefault().SignalStrength.Split('@')[0]);
                Tx = int.Parse(registration.FirstOrDefault().SignalStrengthCh0);
                Rx = int.Parse(registration.FirstOrDefault().SignalStrengthCh1);
                TaskId = Task.CurrentId;
            }
            catch (Exception)
            {
                Connected = false;
                Signal = 0;
                Tx = 0;
                Rx = 0;
            }


        }

        private void UpdateSignalLevel(string signal, int rssi, /*string baseNoise,*/ string chainMask, int[] horizontal)
        {
            var d = int.Parse(signal) - rssi;

            if (chainMask != null || chainMask != string.Empty)
            {
                if (int.Parse(chainMask) > 1)
                {
                    Tx = RssiToSignal(horizontal[0], d);
                    Rx = RssiToSignal(horizontal[1], d);
                }
            }


        }

        private int RssiToSignal(int noise, int d)
        {
            return 0 + d + noise;
        }

        public async Task GetInfo(DeviceTypeEnum deviceType)
        {
            switch (deviceType)
            {
                case DeviceTypeEnum.Ubiquiti:
                    await SetSignal_Ubi();
                    break;
                case DeviceTypeEnum.Mikrotik:
                    SetSignal_Mk();
                    break;


            }
        }
    }
}



