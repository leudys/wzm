﻿using System.Linq;
using System.Threading.Tasks;
using tik4net;
using wzmCore.Data.Mikrotik;
using wzmCore.Extensions;
using wzmCore.PingEnviroment;

namespace wzmCore.Services
{
    public static class ConnectedToEtecsa
    {
        private static PingPortalEtecsa _pingEtecsa => new PingPortalEtecsa();

        public static bool IsConnected(ITikConnection connection, string routingTable)
        {

            var ping = _pingEtecsa.MakePing(connection, routingTable).Last();

            if (ping.PacketLoss.Equals("100"))
            {
                return false;
            }

            if (!ping.Time.Equals(""))
            {
                if (ping.Time.GetNum() > 300)
                {
                    return false;
                }
            }

            return true;

        }

        public static async Task<bool> IsConnectedAsync(ITikConnection connection, string routingTable)
        {

            var pings = await Task.Run(() => _pingEtecsa.MakePing(connection, routingTable));


            if (pings.All(c => c.PacketLoss.Equals("100")))
            {
                return false;
            }

            //if (!ping.Time.Equals(""))
            //{
            //    if (ping.Time.GetNum() > 300)
            //    {
            //        return false;
            //    }
            //}

            return true;

        }

        public static async Task<PingModel> IsConnectedAsync(ITikConnection connection, string routingTable, string wlanInterface)
        {

            var ping = await Task.Run(() => _pingEtecsa.MakePing(connection, routingTable));

            if (ping.All(c => c.PacketLoss.Equals("100")) || ping.Count == 0)
            {
                return new PingModel()
                {
                    Connected = false,
                    MarkRoute = routingTable,
                    Wlan = wlanInterface
                };


            }


            return new PingModel()
            {
                Connected = true,
                MarkRoute = routingTable,
                Wlan = wlanInterface
            };

        }
    }
}
