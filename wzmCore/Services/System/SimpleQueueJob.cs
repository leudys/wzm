﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net.Objects;
using tik4net.Objects.Ip.Firewall;
using tik4net.Objects.Queue;
using wzmCore.Base;
using wzmCore.Interfaces;
using wzmCore.Services.Helpers;
using wzmData.Models;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class SimpleQueueJob : ApplicationService
    {


        public SimpleQueueJob(IWzmCore wzmCore, ILogger<SimpleQueueJob> logger, EtecsaInternetPortalsJob etecsaInternet, IServiceProvider serviceProvider) : base(wzmCore, logger, serviceProvider, etecsaInternet)
        {

        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);

            if (Connection.IsOpened)
            {
                LimitQueue();
            }
            else
            {
                Logger.LogCritical("La conexión con el dispositivo se ha interrumpido.");
            }

        }

        private void LimitQueue()
        {
            try
            {
                // Obtengo los usuarios que estén habilitados y que tengan balanceo o vip
                var users = ApplicationDbContext.HostsServices.Include(c => c.Device).Where(c => !c.Disabled && (c.Balanceo || c.Vip)).ToList();
                var limiteAuto = users.Where(c => c.Limit).ToList();
                var limiteStatico = users.Where(c => !c.Limit).ToList();
                if (limiteAuto.Count() > 0)
                {
                    RemovePortalException(wzmData.Enum.ServiceType.Vip);

                    // Limito a los usuarios
                    var limit = MathQueue.SimpleQueuePerClient(PortalesEtecsa.Count, users.Count());

                    var queues = Connection.LoadAll<QueueSimple>().ToList();

                    UpdateParents(queues);

                    UpdatePremium_LimitAuto(queues, limiteAuto, limit);
                    UpdatePremium_LimitStatic(queues, limiteStatico, limit);
                }


            }
            catch (Exception e)
            {
                LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Error, e.Message);
            }



        }
        private void UpdateParents(List<QueueSimple> queues)
        {
            //if (!queues.Any(c => c.Name.Equals("Reuso_Portales")))
            //{
            //    var simpleQueue = new QueueSimple()
            //    {
            //        Name = "Reuso_Portales",
            //        MaxLimit = $"{PortalesEtecsa.Count * 2}M/{PortalesEtecsa.Count * 2}M",
            //        Queue = "pcq-upload-default/pcq-download-default",
            //        Target = "ether1"
            //    };
            //    Connection.Save(simpleQueue);
            //}
            //else
            //{
            //    var queue = queues.FirstOrDefault(c => c.Name.Equals("Reuso_Portales"));

            //    if (PortalesEtecsa.Count() != 0)
            //    {
            //        if (int.Parse(queue.MaxLimit.Split('/')[1]) / 1000000 != PortalesEtecsa.Count() * 2)
            //        {
            //            queue.MaxLimit = $"{PortalesEtecsa.Count * 2}M/{PortalesEtecsa.Count * 2}M";
            //            queue.Queue = "pcq-upload-default/pcq-download-default";

            //            Connection.Save(queue);
            //        }
            //    }
            //    else
            //    {

            //        if (int.Parse(queue.MaxLimit.Split('/')[1]) / 1000000 != 2)
            //        {
            //            queue.MaxLimit = $"2M/2M";
            //            Connection.Save(queue);
            //        }
            //    }
            //}

            //if (!queues.Any(c => c.Name.Equals("Usuarios")))
            //{
            //    var simpleQueue = new QueueSimple()
            //    {
            //        Name = "Usuarios",
            //        MaxLimit = $"{PortalesEtecsa.Count * 2}M/{PortalesEtecsa.Count * 2}M",
            //        Parent = "Reuso_Portales",
            //        Queue = "pcq-upload-default/pcq-download-default",
            //        //Target = "ether1"
            //    };
            //    Connection.Save(simpleQueue);
            //}
            //else
            //{
            //    var queue = queues.FirstOrDefault(c => c.Name.Equals("Usuarios"));

            //    if (PortalesEtecsa.Count() != 0)
            //    {
            //        if (int.Parse(queue.MaxLimit.Split('/')[1]) / 1000000 != PortalesEtecsa.Count() * 2)
            //        {
            //            queue.MaxLimit = $"{PortalesEtecsa.Count * 2}M/{PortalesEtecsa.Count * 2}M";
            //            queue.Queue = "pcq-upload-default/pcq-download-default";
            //            Connection.Save(queue);
            //        }
            //    }
            //    else
            //    {

            //        if (int.Parse(queue.MaxLimit.Split('/')[1]) / 1000000 != 2)
            //        {
            //            queue.MaxLimit = $"2M/2M";
            //            Connection.Save(queue);
            //        }
            //    }
            //}

            if (!queues.Any(c => c.Name.Equals("Premium")))
            {
                var simpleQueue = new QueueSimple()
                {
                    Name = "Premium",
                    MaxLimit = $"{PortalesEtecsa.Count()}M/{PortalesEtecsa.Count()}M",
                    Target = "0.0.0.0/0",
                    Queue = "pcq-upload-default/pcq-download-default"
                };
                Connection.Save(simpleQueue);
            }
            else
            {
                var queue = queues.FirstOrDefault(c => c.Name.Equals("Premium"));

                if (PortalesEtecsa.Count() != 0)
                {
                    if (int.Parse(queue.MaxLimit.Split('/')[1]) / 1000000 != PortalesEtecsa.Count())
                    {
                        queue.MaxLimit = $"{PortalesEtecsa.Count()}M/{PortalesEtecsa.Count()}M";
                        Connection.Save(queue);
                    }
                }
                //else
                //{
                //    var usersQ = queues.Where(c => c.Parent.Equals("Premium") && !c.Name.Equals("Premium"));

                //    foreach (var item in usersQ)
                //    {
                //        Connection.Delete(item);
                //    }

                //    if (int.Parse(queue.MaxLimit.Split('/')[1]) / 1000000 != 1)
                //    {
                //        queue.MaxLimit = $"1M/1M";
                //        Connection.Save(queue);
                //    }
                //}
            }

        }
        private void UpdateUsers(List<QueueSimple> queues, List<HostsServices> users)
        {
            List<FirewallAddressList> addressList = new List<FirewallAddressList>();
            foreach (var item in PortalesEtecsa)
            {
                addressList.AddRange(Connection.LoadList<FirewallAddressList>(Connection.CreateParameter("list", $"{item.Route}")));
            }

            foreach (var item in users)
            {
                if (addressList.Any(c => c.Address.Equals(item.Device.Ip)))
                {
                    addressList.Remove(addressList.FirstOrDefault(c => c.Address.Equals(item.Device.Ip)));
                }
            }

            var sf = addressList.GroupBy(c => c.List);

            foreach (var item in sf)
            {
                if (item.Count() > 1)
                {
                    if (!queues.Any(c => c.Name.Equals(item.Key)))
                    {
                        QueueSimple parent = new QueueSimple()
                        {
                            Name = item.Key,
                            LimitAt = "1M/1M",
                            MaxLimit = "2M/2M",
                            Parent = "Usuarios"
                        };
                        Connection.Save(parent);
                    }
                    else
                    {
                        var parentQueue = queues.FirstOrDefault(c => c.Name.Equals(item.Key));
                        var limitAt = 2 / item.Count();

                        foreach (var value in item)
                        {
                            if (!queues.Any(c => c.Name.Equals(value.Comment)))
                            {
                                QueueSimple device = new QueueSimple()
                                {
                                    Name = value.Comment,
                                    LimitAt = $"{limitAt}M/{limitAt}M",
                                    MaxLimit = "2M/2M",
                                    Target = value.Address,
                                    Parent = item.Key,
                                    Queue = "pcq-upload-default/pcq-download-default"
                                };
                                Connection.Save(device);
                            }

                            if (parentQueue.Target == string.Empty)
                            {
                                parentQueue.Target = value.Address;
                                Connection.Save(parentQueue);
                            }
                            else
                            {
                                if (!parentQueue.Target.Contains(value.Address))
                                {
                                    parentQueue.Target = parentQueue.Target + $",{value.Address}/32";
                                    Connection.Save(parentQueue);
                                }
                            }

                        }
                    }
                }
                else
                {
                    var value = item.FirstOrDefault();
                    var parentQueue = queues.FirstOrDefault(c => c.Name.Equals("Usuarios"));
                    if (!queues.Any(c => c.Name.Equals(value.Comment)))
                    {
                        QueueSimple device = new QueueSimple()
                        {
                            Name = value.Comment,
                            LimitAt = "1M/1M",
                            MaxLimit = "2M/2M",
                            Target = value.Address,
                            Parent = "Usuarios",
                            Queue = "pcq-upload-default/pcq-download-default"
                        };
                        Connection.Save(device);
                    }
                    if (parentQueue.Target == string.Empty)
                    {
                        parentQueue.Target = value.Address;
                        Connection.Save(parentQueue);
                    }
                    else
                    {
                        if (!parentQueue.Target.Contains(value.Address))
                        {
                            parentQueue.Target = parentQueue.Target + $",{value.Address}/32";
                            Connection.Save(parentQueue);
                        }
                    }

                }
            }

        }

        private void UpdatePremium_LimitAuto(List<QueueSimple> queues, List<HostsServices> users, int limit)
        {

            foreach (var item in users)
            {
                if (queues.Any(c => c.Target.Equals(item.Device.Ip + "/32")))
                {
                    var queue = queues.FirstOrDefault(c => c.Target.Equals(item.Device.Ip + "/32"));

                    var queueLimit = int.Parse(queue.LimitAt.Split('/')[1]) / 1000;

                    if (queueLimit < limit)
                    {
                        queue.MaxLimit = $"1M/1M";
                        queue.LimitAt = $"{limit}K/{limit}K";
                        if (!queue.Parent.Equals("Premium"))
                        {
                            queue.Parent = "Premium";
                        }
                        LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Info, $"Incrementando con {users.Count()} usuarios y {PortalesEtecsa.Count()} portales con Internet");
                        LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Info, $"Incrementando ancho de banda al dispositivo {item.Device.Comment} en {limit}K");

                        Connection.Save(queue);
                        OnChangedQueueHandler(limit);
                    }

                    if (queueLimit > limit)
                    {
                        queue.MaxLimit = $"1M/1M";
                        queue.LimitAt = $"{limit}K/{limit}K";

                        if (!queue.Parent.Equals("Premium"))
                        {
                            queue.Parent = "Premium";
                        }
                        LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Info, $"Reduciendo con {users.Count()} usuarios y {PortalesEtecsa.Count()} portales con Internet");
                        LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Info, $"Reduciendo ancho de banda al dispositivo {item.Device.Comment} en {queueLimit}K");

                        Connection.Save(queue);
                        OnChangedQueueHandler(limit);
                    }

                }
                else
                {
                    QueueSimple queue = new QueueSimple()
                    {
                        Name = item.Device.Comment,
                        Target = item.Device.Ip,
                        MaxLimit = $"1M/1M",
                        LimitAt = $"{limit}K/{limit}K",
                        Parent = "Premium",
                        Queue = "pcq-upload-default/pcq-download-default"
                    };
                    LoggerHelper.Log(Connection, Logger, Helpers.LogLevel.Info, $"Creando limite de ancho de banda al dispositivo {item.Device.Comment} en {limit}K");

                    Connection.Save(queue);
                    OnChangedQueueHandler(limit);
                }
            }
        }

        private void UpdatePremium_LimitStatic(List<QueueSimple> queues, List<HostsServices> users, int limit)
        {
            foreach (var item in users)
            {
                if (queues.Any(c => c.Target.Equals(item.Device.Ip + "/32")))
                {
                    var queue = queues.FirstOrDefault(c => c.Target.Equals(item.Device.Ip + "/32"));

                    var limitAt = int.Parse(queue.LimitAt.Split('/')[1]) / 1000;
                    var maxLimit = int.Parse(queue.MaxLimit.Split('/')[1]) / 1000;

                    if (limit > maxLimit)
                    {
                        if (limit < limitAt)
                        {
                            queue.LimitAt = $"{limit}K/{limit}K";
                            Connection.Save(queue);
                        }
                       
                    }
                    else
                    {
                        if (limit != limitAt)
                        {
                            queue.LimitAt = $"{limit}K/{limit}K";
                            Connection.Save(queue);
                        }
                        
                    }
                }

            }
        }

        public delegate void ChangedQueue(int queue);
        public event ChangedQueue ChangedQueueHandler;

        protected virtual void OnChangedQueueHandler(int queue)
        {
            var handler = ChangedQueueHandler;
            if (handler != null) handler(queue);
        }
    }
}
