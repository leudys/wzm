﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Base;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using wzmCore.Interfaces;
using static tik4net.Objects.Interface.InterfaceWireless;
using wzmCore.Extensions;
using tik4net;
using tik4net.Objects.Interface;
using wzmCore.Services;
using tik4net.Objects;
using Microsoft.EntityFrameworkCore;
using wzmData.Enum;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class ResetTimeJob : ApplicationService
    {



        public ResetTimeJob(IServiceProvider serviceProvider, ILogger<ResetTimeJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {


        }
        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            try
            {
                var hotSpot = new HotSpot();
                var daily = ApplicationDbContext.ResetTime.Include(c => c.Time).Include(c => c.User).ThenInclude(c => c.Device).ThenInclude(c =>c.ApplicationUser).Where(c => c.Time.Daily);
                var credential = (MikrotikCredentials)ApplicationDbContext.AdminDevices.Include(c => c.DeviceCredential).FirstOrDefault(c => c.Fabricante == DeviceTypeEnum.Mikrotik).DeviceCredential.FirstOrDefault(); 
                using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                {
                    foreach (var item in daily)
                    {
                        try
                        {
                            var active = hotSpot.GetActiveUser(item.User.Name, connection);
                            if (active != null)
                            {
                                active.Desconnect(connection);
                                var user = hotSpot.GetUserByName(active.UserName, connection);
                                if (user != null)
                                {
                                    user.ResetCounters(connection);
                                }
                                
                            }
                            else
                            {
                                var user = hotSpot.GetUserByComment(item.User.Name, connection);
                                if (user != null)
                                {
                                    user.ResetCounters(connection);
                                }                                                               
                            }
                        }
                        catch (Exception e)
                        {

                            throw e;
                        }
                    }
                }
                
            }
            catch (Exception e)
            {
                Logger.LogCritical(e.Message);
            }

        }


    }
}

