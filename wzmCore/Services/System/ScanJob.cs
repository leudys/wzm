﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UbiquitiCore.Discovery;
using wzmCore.Base;
using wzmCore.Interfaces;


namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class ScanJob : ApplicationService
    {
        public List<Device> Devices { get; private set; }

        public DeviceDiscovery DeviceDiscovery { get; private set; }

        private CancellationTokenSource _cts;
        private Task discoveryTask;
        

        public ScanJob(ILogger<ScanJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, null)
        {
           
        }


        private void DeviceDiscovery_DeviceDiscovered(Device device)
        {
            AddDevice(device);
        }

        public void ClearDevices()
        {
            if (_cts != null) _cts.Cancel();
            Devices.Clear();
        }

        public void AddDevice(Device device)
        {

            Devices.Add(device);


        }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (IsLicenseValid())
            {
                Devices = new List<Device>();
                DeviceDiscovery = new DeviceDiscovery();
                DeviceDiscovery.DeviceDiscovered += DeviceDiscovery_DeviceDiscovered;

                if (discoveryTask != null && (discoveryTask.Status == TaskStatus.WaitingForActivation || discoveryTask.Status == TaskStatus.Running))
                {
                    _cts.Cancel();
                    Devices.Clear();
                }

                try
                {
                    var cts = new CancellationTokenSource();
                    _cts = cts;
                    discoveryTask = DeviceDiscovery.DiscoveryAsync(_cts.Token).ContinueWith((t) => cts.Dispose());



                    await discoveryTask;
                }
                catch (Exception e)
                {
                    Logger.LogError(e.Message);
                }
            }


        }
    }
}
