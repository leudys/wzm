﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Base;
using wzmCore.Interfaces;
using wzmCore.Utils;
using wzmData.Models;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    
    public class TracerRouteJob : ApplicationService
    {
        private EtecsaInternetPortalsJob _etecsaInternet;
        public TracerRouteJob(IServiceProvider serviceProvider, EtecsaInternetPortalsJob etecsaInternet, ILogger<MorososJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {
            _etecsaInternet = etecsaInternet;
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
                        

            if (_etecsaInternet.Portales.Count() > 0)
            {
                var tracer = MikrotikTracerRoute.TracerRoute(Connection, 3000, _etecsaInternet.Portales.FirstOrDefault().Route, "1", "google.com").OrderBy(c => c.Address).ToList();

                foreach (var item in tracer)
                {
                    if (item.Address.Contains("172"))
                    {
                        if (ApplicationDbContext.TracerRoute.Any())
                        {
                            if (!ApplicationDbContext.TracerRoute.Any(c => double.Parse(c.Latencia) < double.Parse(item.Worst)))
                            {

                                var savedTracer = ApplicationDbContext.TracerRoute.FirstOrDefault();

                                savedTracer.Latencia = item.Worst;
                                savedTracer.Host = item.Address;
                                ApplicationDbContext.TracerRoute.Update(savedTracer);
                                ApplicationDbContext.SaveChanges();

                            }
                        }
                        else
                        {
                            var savedTracer = new BestHost() { Host = item.Address, Latencia = item.Worst };
                            ApplicationDbContext.TracerRoute.Add(savedTracer);
                            ApplicationDbContext.SaveChanges();
                        }
                    }
                }
                              
            }

        }
    }
}
