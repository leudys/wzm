﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using wzmCore.Administration;
using wzmData.Enum;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.Mikrotik.WebProxyAdmin;
using wzmCore.Base;
using wzmCore.Extensions;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class MorososJob : ApplicationService
    {

        IServiceProvider _serviceProvider;
        private readonly string _morososAddressList;
        private readonly IConfiguration _configuration;
        private readonly IWzmCore _wzmCore;

        public MorososJob(IServiceProvider serviceProvider, ILogger<MorososJob> logger, IConfiguration configuration, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _morososAddressList = "morososWZM";
            _configuration = configuration;
            _wzmCore = wzmCore;
        }

        public List<ApplicationUsers> Clientes { get; private set; }

        public override async Task Execute(IJobExecutionContext context)
        {
            if (IsLicenseValid())
            {
                if (Clientes != null)
                {
                    Clientes.Clear();
                }
                else
                {
                    Clientes = new List<ApplicationUsers>();
                }

                try
                {
                    using (var applicationDbContext = new ApplicationDbContext(_serviceProvider.CreateScope().ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
                    {
                        List<ApplicationUsers> users = new List<ApplicationUsers>();

                        List<ApplicationUsers> tempUsers = applicationDbContext.GetApplicationClients();



                        // Si la licencia es de Pueba restrinjo el servicio para que solo trabaje con 20 usuarios.
                        if (LicenseType() == Enum.LicenseType.Trial)
                        {
                            if (tempUsers.Count() > 21)
                            {
                                users = tempUsers.GetRange(1, 20);
                                Logger.LogInformation("Obteniendo solo 20 usuarios, por restricción de Licencia.");
                            }
                        }
                        else
                        {
                            users = tempUsers;
                        }



                        var clasifiacodrPagos = applicationDbContext.ClasificadorPagos.Where(c => (c.DiaPago == DateTime.Now.Day || c.DiaPago < DateTime.Now.Day) && c.PaymentType != PaymentType.Gratis);
                        var morosos = new List<ApplicationUsers>();
                        var excentos = applicationDbContext.ExcentoPagos.Where(c => c.Mes == DateTime.Now.Month && c.Year == DateTime.Now.Year);
                        if (clasifiacodrPagos != null)
                        {
                            if (clasifiacodrPagos.Count() > 0)
                            {
                                foreach (var item in clasifiacodrPagos)
                                {
                                    var usersClasificadorPago = users.Where(c => c.ClasificadorPagoId == item.Id);
                                    foreach (var userClasificador in usersClasificadorPago)
                                    {

                                        if (!applicationDbContext.Pagos.Any(c => c.UsuarioId.Equals(userClasificador.Id) && c.FechaPago.Month == DateTime.Now.Month && c.FechaPago.Year == DateTime.Now.Year))
                                        {
                                            if (userClasificador.ClienteDesde.HasValue)
                                            {
                                                // Si el usuario es nuevo en la red tiene derecho a no pagar, o sea pagar por mes vencido.
                                                if (userClasificador.ClienteDesde.Value.Month != DateTime.Now.Month)
                                                {
                                                    if (!excentos.Any(c => c.UserId.Equals(userClasificador.Id)))
                                                    {
                                                        morosos.Add(userClasificador);
                                                    }

                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }

                        // Disable user in Device
                        UserManagement userManagement = new UserManagement();
                        WebProxyManager proxyManager = new WebProxyManager(_configuration, _wzmCore);

                        var mc = applicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);

                        if (mc != null)
                        {
                            ITikConnection connection = ConnectionFactory.OpenConnection(mc.Dispositivo.Firmware.GetApiVersion(), mc.Dispositivo.Ip, mc.User, mc.Password);

                            if (!proxyManager.IsProxyEnabled(connection))
                            {

                                proxyManager.EnableWebProxy(connection);
                            }
                            string redirectTo = applicationDbContext.MorososService.FirstOrDefault().RedirecTo;
                            proxyManager.IsRestrictionEnabled(connection);


                            new Nat().AddMorososNat(connection, _morososAddressList);


                            foreach (var item in morosos)
                            {
                                userManagement.AddMoroso(item, _morososAddressList, connection);
                                Logger.LogInformation($"Se ha movido a {item.UserName} a Morosos");
                                item.UserPaymentState = UserPaymentState.Atrasado;
                                applicationDbContext.Update(item);
                                await applicationDbContext.SaveChangesAsync();
                            }
                        }

                        else
                        {
                            Logger.LogCritical("No se ha podido ejecutar el servicio Morosos, porque no existen credenciales para el Mikrotik.");
                        }

                    }


                }
                catch (Exception e)
                {

                    Logger.LogCritical(e.Message);
                }
            }
        }


    }
}

