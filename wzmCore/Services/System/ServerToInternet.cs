﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using wzmCore.Interfaces;
using wzmData.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using tik4net;
using wzmCore.Data.Mikrotik;
using wzmCore.Services.Helpers;
using System.Net.NetworkInformation;
using wzmCore.Base;
using Quartz;
using System.Threading.Tasks;
using wzmCore.Mikrotik;
using wzmCore.Extensions;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class ServerToInternet : ApplicationService
    {

        private IConfiguration _configuration;
        private readonly InternetInformation _internetInformation;
        private string _hostAdress;
        private readonly IWzmCore _wzmCore;
        

        public ServerToInternet(IWzmCore wzmCore, ILogger<object> logger, IConfiguration configuration, IServiceProvider service) : base(wzmCore, logger, service)
        {
            _wzmCore = wzmCore;
            _configuration = configuration;
            _hostAdress = "google.com";
            _internetInformation = new InternetInformation();
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            await CheckInternetConnection();
        }



        private async Task CheckInternetConnection()
        {
            try
            {

                //var portales = ApplicationDbContext.PortalNauta.ToList();

                //if (portales.Count > 0)
                //{
                //    if (ApplicationDbContext.TracerRoute.Count() > 0)
                //    {
                //        _hostAdress = ApplicationDbContext.TracerRoute.FirstOrDefault().Host;
                //    }
                //    var response = await PingAsync.SendPingAsync(_hostAdress);

                //    if (response.Status != IPStatus.Success)
                //    {

                //        var ip = _configuration.GetSection("ServerConfiguration").GetValue<string>("ip");

                //        if (ApplicationDbContext.MikrotikCredentials.Any(c => c.Selected))
                //        {
                //            var credenciales = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefault(c => c.Selected);

                //            var portal = GetActualInternetPortal(credenciales, portales);

                //            if (portal != null)
                //            {
                //                if (Connection.IsOpened)
                //                {
                //                    var adressList = new AddressList();
                //                    var allAdressList = adressList.GetAddressList(Connection);

                //                    if (allAdressList.Any(c => c.Address.Equals(ip)))
                //                    {
                //                        var addressList = allAdressList.FirstOrDefault(c => c.Address.Equals(ip));
                //                        addressList.List = portal.Route;

                //                        adressList.UpdateAddressList(addressList, credenciales);
                //                    }
                //                    else
                //                    {
                //                        adressList.CreateAddressList(new FirewallAddressViewModel() { Address = ip, List = portales.FirstOrDefault().Route, Disabled = false, Usuario = "wzmServer" }, credenciales);
                //                    }
                //                }


                //                Thread.Sleep(5000);
                //                response = await PingAsync.SendPingAsync(_hostAdress);
                //                if (response.Status != IPStatus.Success)
                //                {
                //                    _wzmCore.LicenseManager.InvalidateLicense(wzmCore.Enum.InvalidateReason.NoInternetAccess);

                //                    //_logger.LogCritical("Se ha detectado Internet en el Mikrotik, pero no en el host. Se ha invalidado la licencia.");
                //                }
                //            }
                //        }
                //    }
                //}


            }
            catch (Exception e)
            {

                Logger.LogCritical(e.Message);
            }
        }

        private PortalesInternet GetActualInternetPortal(MikrotikCredentials credenciales, List<PortalesInternet> portales)
        {
            using (ITikConnection connection = ConnectionFactory.OpenConnection(credenciales.Dispositivo.Firmware.GetApiVersion(), credenciales.Dispositivo.Ip, credenciales.User, credenciales.Password))
            {
                foreach (var item in portales)
                {
                    if (_internetInformation.HasInternet(connection, item.Route))
                    {
                        return item;
                    }
                }
            }
            return null;
        }
    }
}
