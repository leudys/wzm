﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Base;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using wzmCore.Interfaces;
using static tik4net.Objects.Interface.InterfaceWireless;
using wzmCore.Extensions;
using tik4net;
using tik4net.Objects.Interface;
using wzmCore.Services;
using tik4net.Objects;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class EtecsaInternetPortalsJob : ApplicationService
    {
        #region Private
        private List<WirelessList> _wirelessList;
        private Routes _rutas => new Routes();
        private Wireless _wireless => new Wireless();
        private List<InterfaceWireless> _mkwireless;
        private List<Interface> _interfaces;
        private string _bestHost;
        private int _id = 1;
        #endregion

        public delegate void UpdatedInternetGates(List<PortalesInternet> Portales);
        public event UpdatedInternetGates UpdatedInternetGatesHandler;

        protected virtual void OnInternetDetectedHandler(List<PortalesInternet> Portales)
        {
            var handler = UpdatedInternetGatesHandler;
            if (handler != null) handler(Portales);
        }

        /// <summary>
        /// Portales que se encuentran con Internet
        /// </summary>
        public List<PortalesInternet> Portales { get; set; }

        public EtecsaInternetPortalsJob(IServiceProvider serviceProvider, ILogger<EtecsaInternetPortalsJob> logger, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {
            _wirelessList = new List<WirelessList>();
            _mkwireless = new List<InterfaceWireless>();
            Portales = new List<PortalesInternet>();

        }
        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
            try
            {
                if (VLAN_ETECSA)
                {
                    var temp = Connection.LoadAll<Interface>().Where(c => c.Type.Equals("vlan")).ToList();
                    await GetPortalesInternet(temp);
                    OnInternetDetectedHandler(Portales);
                }
                else
                {
                    var temp = _wireless.GetAllWireless(Connection);
                    await GetPortalesInternet(temp);
                    OnInternetDetectedHandler(Portales);
                }

            }
            catch (Exception e)
            {
                Logger.LogCritical(e.Message);
            }

        }


        /// <summary>
        /// Obtiene los nombres de las interfaces que tienen internet.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns>List<string></returns>
        private async Task GetPortalesInternet(List<InterfaceWireless> temp)
        {

            GetBestHost();
            // Las rutas 
            var mkRoutes = new List<RouteViewModel>();

            if (temp.Count > 2)
            {
                _mkwireless = temp.Where(c => c.Mode == WirelessMode.Station || c.Mode == WirelessMode.StationPseudobridge && !c.Disabled).ToList();
                mkRoutes = _rutas.GetMarkRoutes(Connection);


                if (_mkwireless.Count >= 20)
                {
                    SetPortalList(10);
                }
                else
                {
                    if (_mkwireless.Count >= 10)
                    {
                        SetPortalList(5);
                    }
                    else
                    {
                        SetPortalList(2);
                    }
                }


                List<Task> tasks = new List<Task>();

                foreach (var item in _wirelessList)
                {
                    item.Routes = mkRoutes;
                    item.Logger = Logger;
                    item.SetHost = _bestHost;
                    item.Portales = Portales.ToList();
                    tasks.Add(item.FindInternet());
                }
                try
                {
                    await Task.WhenAll(tasks);


                    //wirelessList.CloseConnection();
                }
                catch (Exception e)
                {
                    Logger.LogError(e.Message);
                }
            }

        }

        private async Task GetPortalesInternet(List<Interface> temp)
        {

            GetBestHost();
            // Las rutas 
            var mkRoutes = new List<RouteViewModel>();

            if (temp.Count > 2)
            {
                _interfaces = temp.Where(c => !c.Disabled).ToList();
                mkRoutes = _rutas.GetMarkRoutes(Connection);


                if (_interfaces.Count >= 20)
                {
                    SetPortalList(10, true);
                }
                else
                {
                    if (_interfaces.Count >= 10)
                    {
                        SetPortalList(5, true);
                    }
                    else
                    {
                        SetPortalList(2, true);
                    }
                }


                List<Task> tasks = new List<Task>();

                foreach (var item in _wirelessList)
                {
                    item.Routes = mkRoutes;
                    item.Logger = Logger;
                    item.SetHost = _bestHost;
                    item.Portales = Portales.ToList();
                    tasks.Add(item.FindInternet(true));

                }
                try
                {
                    await Task.WhenAll(tasks);


                    //wirelessList.CloseConnection();
                }
                catch (Exception e)
                {
                    Logger.LogError(e.Message);
                }
            }

        }

        private string GetBestHost()
        {
            return _bestHost = "8.8.8.8";
            //if (ApplicationDbContext.TracerRoute.Count() > 0)
            //{
            //    var bestHost = ApplicationDbContext.TracerRoute.FirstOrDefault().Host;

            //    if (_bestHost == null)
            //    {
            //        return _bestHost = bestHost;
            //    }
            //    else
            //    {
            //        if (!_bestHost.Equals(bestHost))
            //        {
            //            return _bestHost = bestHost; ;
            //        }
            //        return _bestHost;
            //    }


            //}
            //else
            //{
            //    return "8.8.8.8";
            //}
        }

        private void SetPortalList(int chunk, bool vlan = false)
        {
            if (vlan)
            {
                List<List<Interface>> _interface = _interfaces.ChunkBy(chunk);

                if (_interface.Count != _wirelessList.Count)
                {
                    if (_interface.Count - _wirelessList.Count < 0)
                    {
                        int index1 = _wirelessList.IndexOf(_wirelessList.Last());
                        int index2 = _interface.IndexOf(_interface.Last());

                        for (int i = index2; i <= index1; i++)
                        {
                            var xc = _wirelessList[i].Portales;

                            foreach (var item in xc)
                            {
                                if (Portales.Any(c => c.Portal.Equals(item.Portal)))
                                {
                                    Portales.Remove(item);
                                }
                            }

                            _wirelessList.CloseConnection(i);
                            _wirelessList.RemoveAt(i);
                        }
                    }
                    if (_interface.Count - _wirelessList.Count > 0)
                    {
                        if (_wirelessList.Count != 0)
                        {
                            int index1 = _wirelessList.IndexOf(_wirelessList.Last());
                            index1 = index1 + 1;

                            for (int i = index1; i <= _interface.Count; i++)
                            {
                                _wirelessList.Add(new WirelessList { VLAN = _interface[index1], Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password) });
                                _wirelessList[i].InternetDetectedHandler += DetectedInternet;
                                _wirelessList[i].InternetFinishedHandler += FinishedInternet;
                            }
                        }
                        else
                        {
                            int _temp = 0;
                            foreach (var item in _interface)
                            {
                                _wirelessList.Add(new WirelessList { VLAN = item, Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password) });
                                _wirelessList[_temp].InternetDetectedHandler += DetectedInternet;
                                _wirelessList[_temp].InternetFinishedHandler += FinishedInternet;
                                _temp++;
                            }
                        }

                    }
                }
                else
                {
                    Update(_interface);
                }
            }

            else
            {
                List<List<InterfaceWireless>> _w = _mkwireless.ChunkBy(chunk);

                if (_w.Count != _wirelessList.Count)
                {
                    if (_w.Count - _wirelessList.Count < 0)
                    {
                        int index1 = _wirelessList.IndexOf(_wirelessList.Last());
                        int index2 = _w.IndexOf(_w.Last());

                        for (int i = index2; i <= index1; i++)
                        {
                            var xc = _wirelessList[i].Portales;

                            foreach (var item in xc)
                            {
                                if (Portales.Any(c => c.Portal.Equals(item.Portal)))
                                {
                                    Portales.Remove(item);
                                }
                            }

                            _wirelessList.CloseConnection(i);
                            _wirelessList.RemoveAt(i);
                        }
                    }
                    if (_w.Count - _wirelessList.Count > 0)
                    {
                        if (_wirelessList.Count != 0)
                        {
                            int index1 = _wirelessList.IndexOf(_wirelessList.Last());
                            index1 = index1 + 1;

                            for (int i = index1; i <= _w.Count; i++)
                            {
                                _wirelessList.Add(new WirelessList { Wireless = _w[index1], Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password) });
                                _wirelessList[i].InternetDetectedHandler += DetectedInternet;
                                _wirelessList[i].InternetFinishedHandler += FinishedInternet;
                            }
                        }
                        else
                        {
                            int _temp = 0;
                            foreach (var item in _w)
                            {
                                _wirelessList.Add(new WirelessList { Wireless = item, Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password) });
                                _wirelessList[_temp].InternetDetectedHandler += DetectedInternet;
                                _wirelessList[_temp].InternetFinishedHandler += FinishedInternet;
                                _temp++;
                            }
                        }

                    }
                }
                else
                {
                    Update(_w);
                }
            }


        }

        private void Update(List<List<InterfaceWireless>> sd)
        {
            int tempIndex = 0;
            foreach (var item in sd)
            {
                // Siempre actualizo los clientes DHCP, pero mantengo abierta la conexion hacia el dispositivo
                _wirelessList[tempIndex].Wireless.Clear();
                _wirelessList[tempIndex].Wireless.AddRange(item);
                tempIndex++;
            }
        }

        private void Update(List<List<Interface>> vlan)
        {
            int tempIndex = 0;
            foreach (var item in vlan)
            {
                // Siempre actualizo los clientes DHCP, pero mantengo abierta la conexion hacia el dispositivo
                _wirelessList[tempIndex].VLAN.Clear();
                _wirelessList[tempIndex].VLAN.AddRange(item);
                tempIndex++;
            }
        }

        private void FinishedInternet(PortalesInternet Portal)
        {
            Portales.Remove(Portal);

        }

        private void DetectedInternet(PortalesInternet Portal)
        {
            Portal.Id = _id;
            Portales.Add(Portal);
            _id++;

        }
    }
}

internal class WirelessList
{
    public WirelessList()
    {
        Internet = new InternetInformation();
    }

    internal ITikConnection Connection { get; set; }

    internal List<InterfaceWireless> Wireless { get; set; }

    internal List<Interface> VLAN { get; set; }

    internal ILogger<object> Logger { get; set; }

    internal List<RouteViewModel> Routes { get; set; }

    private InternetInformation Internet { get; set; }

    internal List<PortalesInternet> Portales { get; set; }

    internal async Task FindInternet(bool vlan = false)
    {
        if (vlan)
        {
            foreach (var item in VLAN)
            {
                try
                {                    
                    if (Routes.Any(c => c.Gateway.Split('%')[1].Equals(item.Name)))
                    {
                        var r = Routes.FirstOrDefault(c => c.Gateway.Split('%')[1].Equals(item.Name));

                        if (await Task.Run(() => Internet.HasInternet(Connection, r.MarkRoute)))
                        {

                            // Añado a la base de datos, de esta manera no hay q esperar a que se busque en todos los portales, para que los demas servicios funcionen.

                            if (!Portales.Any(c => c.Portal.Equals(item.Name)))
                            {
                                OnInternetDetectedHandler(new PortalesInternet() { Portal = item.Name, Route = r.MarkRoute });

                                //ApplicationDbContext.PortalNauta.Add();
                                //await ApplicationDbContext.SaveChangesAsync();
                                Logger.LogInformation($"Se ha encontrado Internet en la Interfaz {item.Name}");
                                Connection.LogWarning($"[Looking for Internet by Default] Se ha encontrado Internet en la Interfaz {item.Name}");
                            }
                        }
                        else
                        {
                            if (Portales.Any(c => c.Portal.Equals(item.Name)))
                            {
                                var toDelete = Portales.FirstOrDefault(c => c.Portal.Equals(item.Name));
                                //ApplicationDbContext.PortalNauta.Remove(toDelete);
                                //await ApplicationDbContext.SaveChangesAsync();
                                OnInternetFinishedHandler(toDelete);
                                Logger.LogInformation($"Se ha terminado el Internet en la Interfaz {item.Name}");
                                Connection.LogWarning($"[Looking for Internet by Default] Se ha terminado el Internet en la Interfaz {item.Name}");
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    try
                    {
                        var r = Routes.FirstOrDefault(c => c.Gateway.Contains(item.Name));
                                               

                        if (r != null)
                        {
                            if (await Task.Run(() => Internet.HasInternet(Connection, r.MarkRoute)))
                            {

                                // Añado a la base de datos, de esta manera no hay q esperar a que se busque en todos los portales, para que los demas servicios funcionen.

                                if (!Portales.Any(c => c.Portal.Equals(item.Name)))
                                {
                                    OnInternetDetectedHandler(new PortalesInternet() { Portal = item.Name, Route = r.MarkRoute });

                                    //ApplicationDbContext.PortalNauta.Add();
                                    //await ApplicationDbContext.SaveChangesAsync();
                                    Logger.LogInformation($"Se ha encontrado Internet en la Interfaz {item.Name}");
                                    Connection.LogWarning($"[Looking for Internet by Default] Se ha encontrado Internet en la Interfaz {item.Name}");
                                }
                            }
                            else
                            {
                                if (Portales.Any(c => c.Portal.Equals(item.Name)))
                                {
                                    var toDelete = Portales.FirstOrDefault(c => c.Portal.Equals(item.Name));
                                    //ApplicationDbContext.PortalNauta.Remove(toDelete);
                                    //await ApplicationDbContext.SaveChangesAsync();
                                    OnInternetFinishedHandler(toDelete);
                                    Logger.LogInformation($"Se ha terminado el Internet en la Interfaz {item.Name}");
                                    Connection.LogWarning($"[Looking for Internet by Default] Se ha terminado el Internet en la Interfaz {item.Name}");
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                }
            }
        }
        else
        {
            foreach (var item in Wireless)
            {
                try
                {
                    Routes.Any(c => c.Gateway.Split('%')[1].Equals(item.Name));
                }
                catch (Exception)
                {

                    throw;
                }

                if (Routes.Any(c => c.Gateway.Split('%')[1].Equals(item.Name)))
                {
                    var r = Routes.FirstOrDefault(c => c.Gateway.Split('%')[1].Equals(item.Name));
                    if (await Task.Run(() => Internet.HasInternet(Connection, r.MarkRoute)))
                    {

                        // Añado a la base de datos, de esta manera no hay q esperar a que se busque en todos los portales, para que los demas servicios funcionen.

                        if (!Portales.Any(c => c.Portal.Equals(item.Name)))
                        {
                            OnInternetDetectedHandler(new PortalesInternet() { Portal = item.Name, Route = r.MarkRoute });

                            //ApplicationDbContext.PortalNauta.Add();
                            //await ApplicationDbContext.SaveChangesAsync();
                            Logger.LogInformation($"Se ha encontrado Internet en la Interfaz {item.Name}");
                            Connection.LogWarning($"[Looking for Internet by Default] Se ha encontrado Internet en la Interfaz {item.Name}");
                        }
                    }
                    else
                    {
                        if (Portales.Any(c => c.Portal.Equals(item.Name)))
                        {
                            var toDelete = Portales.FirstOrDefault(c => c.Portal.Equals(item.Name));
                            //ApplicationDbContext.PortalNauta.Remove(toDelete);
                            //await ApplicationDbContext.SaveChangesAsync();
                            OnInternetFinishedHandler(toDelete);
                            Logger.LogInformation($"Se ha terminado el Internet en la Interfaz {item.Name}");
                            Connection.LogWarning($"[Looking for Internet by Default] Se ha terminado el Internet en la Interfaz {item.Name}");
                        }
                    }
                }
            }
        }



    }

    public delegate void InternetDetected(PortalesInternet Portal);
    public event InternetDetected InternetDetectedHandler;
    public delegate void InternetFinished(PortalesInternet Portal);
    public event InternetFinished InternetFinishedHandler;

    internal string SetHost
    {
        set
        {
            if (!Internet.GetHost.Equals(value))
            {
                Internet.SetHost = value;
            }


        }
    }

    protected virtual void OnInternetDetectedHandler(PortalesInternet Portal)
    {
        var handler = InternetDetectedHandler;
        if (handler != null) handler(Portal);
    }

    protected virtual void OnInternetFinishedHandler(PortalesInternet Portal)
    {
        var handler = InternetFinishedHandler;
        if (handler != null) handler(Portal);
    }
}

internal static class PortalListExtension
{

    internal static void CloseConnection(this List<WirelessList> wirelessList)
    {
        foreach (var item in wirelessList)
        {
            if (item.Connection.IsOpened)
            {
                item.Connection.Dispose();
            }
        }
    }

    internal static void CloseConnection(this List<WirelessList> wirelessList, int index)
    {
        var connection = wirelessList[index].Connection;


        if (connection.IsOpened)
        {
            connection.Dispose();
        }

    }
}
