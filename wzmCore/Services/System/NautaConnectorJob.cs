﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Ip.Hotspot;
using wzmCore.Base;
using wzmCore.Interfaces;
using wzmCore.Mikrotik.Portales;
using wzmCore.Services.Helpers;
using wzmData.Models;

namespace wzmCore.Services.System
{
    [DisallowConcurrentExecution]
    public class NautaConnectorJob : ApplicationService
    {
        private MkLoginNauta _mkNauta;
        private ITikConnection _connection;
        public NautaConnectorJob(IWzmCore wzmCore, ILogger<object> logger, EtecsaInternetPortalsJob portales, IServiceProvider serviceScope) : base(wzmCore, logger, serviceScope, portales)
        {
            _mkNauta = new MkLoginNauta();

        }

        public override async Task Execute(IJobExecutionContext context)
        {
            try
            {
                await base.Execute(context);
                var d = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true);
                if (_connection == null)
                {
                    _connection = ConnectionFactory.OpenConnection(TikConnectionType.Api_v2, d.Dispositivo.Ip, d.User, d.Password);
                }
                if (!_connection.IsOpened)
                {
                    _connection = ConnectionFactory.OpenConnection(TikConnectionType.Api_v2, d.Dispositivo.Ip, d.User, d.Password);
                }
                RemovePortalException(wzmData.Enum.ServiceType.Vip);

                var activos = Connection.LoadAll<HotspotActive>().ToList();

                int portalToConnect = WzmCommon.Portales.PortalToConnect.PortalQty(activos.Count);
                if (portalToConnect > 0)
                {
                    var cuentas = ApplicationDbContext.CuentaNautas.Include(c => c.Portal).ToList();
                    if (cuentas.Count != 0)
                    {
                        portalToConnect = cuentas.Count < portalToConnect ? cuentas.Count : portalToConnect;

                        for (int i = 0; i < portalToConnect; i++)
                        {

                            await ConectarUnaCuenta(cuentas[i]);

                        }
                    }

                }

                else
                {
                    DisconnectNauta(ApplicationDbContext.CuentaNautas.Include(c => c.Portal).ToList());

                }

            }
            catch (Exception e)
            {
                LoggerHelper.Log(Logger, Helpers.LogLevel.Error, e.Message);
            }
        }

        private async Task ConectarUnaCuenta(CuentaNauta cuenta)
        {
            if (!_mkNauta.LoggedIn(_connection, cuenta, Logger))
            {
                cuenta.Connected = DoLogin(cuenta);
                ApplicationDbContext.Update(cuenta);
                await ApplicationDbContext.SaveChangesAsync();
            }

        }


        private bool DoLogin(CuentaNauta cuentas)
        {
            try
            {
                LoggerHelper.Log(Connection, Helpers.LogLevel.Warning, $"Conectando cuenta {cuentas.User} en el portal {cuentas.Portal.Interfaz}.");
                if (_mkNauta.Login(_connection, cuentas, "10.197.0.1"))
                {
                    var c = ApplicationDbContext.CuentaNautas.Find(cuentas.Id);
                    c.Connected = true;
                    ApplicationDbContext.Update(c);
                    ApplicationDbContext.SaveChanges();
                    LoggerHelper.Log(_connection, Helpers.LogLevel.Warning, $"Cuenta {cuentas.User} conectada.");

                    return true;
                }
                else
                {
                    var cuenta = ApplicationDbContext.CuentaNautas.Find(cuentas.Id);
                    cuenta.Connected = false;
                    ApplicationDbContext.Update(cuenta);
                    ApplicationDbContext.SaveChanges();
                    return false;
                }

            }
            catch (Exception e)
            {
                var cuenta = ApplicationDbContext.CuentaNautas.Find(cuentas.Id);
                cuenta.Connected = false;
                ApplicationDbContext.Update(cuenta);
                ApplicationDbContext.SaveChanges();
                LoggerHelper.Log(_connection, Logger, Helpers.LogLevel.Error, e.Message);
                return false;
            }

        }

        private void DisconnectNauta(List<CuentaNauta> cuentas)
        {
            foreach (var item in cuentas)
            {
                if (_mkNauta.LoggedIn(Connection, item))
                {
                    _mkNauta.DisconnectAccount(Connection, Logger, item.Portal.Interfaz, item.User);

                }
            }


        }
    }
}

