﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using wzmCore.Base;
using wzmCore.Interfaces;
using wzmCore.PingEnviroment;
using wzmCore.Services.Helpers;
using wzmCore.ViewModels;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class ActiveDevices : ApplicationService
    {
        public List<ActiveDevicesViewModel> Devices { get; private set; }

        private List<Task<PingReply>> pingTasks = new List<Task<PingReply>>();
        public ActiveDevices(ILogger<ActiveDevices> logger, IServiceProvider serviceProvider, IWzmCore wzmCore) : base(wzmCore, logger, serviceProvider)
        {
            Devices = new List<ActiveDevicesViewModel>();
        }

        public override async Task Execute(IJobExecutionContext context)
        {

            //_activeDevicesViewModels.Clear();
            await base.Execute(context);
            pingTasks.Clear();
            
            var dispositivos = ApplicationDbContext.Dispositivos.ToList();


            foreach (var item in dispositivos)
            {
                pingTasks.Add(PingAsync.SendPingAsync(item.Ip));
            }

            Task.WaitAll(pingTasks.ToArray());

            foreach (var item in pingTasks)
            {
                try
                {

                    if (item.Result.Status == IPStatus.Success)
                    {
                        int index = pingTasks.IndexOf(item);
                        var dispositivo = dispositivos.ElementAt(index);

                        if (!Devices.Any(c => c.Mac.Equals(dispositivo.Mac)))
                        {
                            Devices.Add(new ActiveDevicesViewModel() { Active = false, Alarm = dispositivo.Alarm, Mac = dispositivo.Mac, Name = dispositivo.Hostname, Ip = dispositivo.Ip, Ubicacion = dispositivo.Ubicacion });
                        }
                        else
                        {
                            Devices.FirstOrDefault(c => c.Mac.Equals(dispositivo.Mac)).Active = true;
                            Devices.FirstOrDefault(c => c.Mac.Equals(dispositivo.Mac)).Alarm = dispositivo.Alarm;
                        }


                    }
                    else
                    {
                        int index = pingTasks.IndexOf(item);
                        var dispositivo = dispositivos.ElementAt(index);
                        if (dispositivo != null)
                        {
                            if (!Devices.Any(c => c.Mac.Equals(dispositivo.Mac)))
                            {
                                Devices.Add(new ActiveDevicesViewModel() { Active = false, Alarm = dispositivo.Alarm, Mac = dispositivo.Mac, Name = dispositivo.Hostname, Ip = dispositivo.Ip, Ubicacion = dispositivo.Ubicacion });
                            }
                            else
                            {
                                Devices.FirstOrDefault(c => c.Mac.Equals(dispositivo.Mac)).Active = false;
                                Devices.FirstOrDefault(c => c.Mac.Equals(dispositivo.Mac)).Alarm = dispositivo.Alarm;
                            }
                        }


                    }



                }
                catch (Exception e)
                {

                    Logger.LogError(e.Message);
                }

            }
        }
    }
}
