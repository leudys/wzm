﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using WzmCommon.Files;
using wzmCore.Interfaces;
using wzmCore.Utils;
using wzmCore.ViewModels;
using wzmCore.WzmLicense.Crypt;

namespace wzmCore.Services
{
    [DisallowConcurrentExecution]
    public class LicenseChecker : IJob
    {

        private readonly IWzmCore _wzmCore;
        private Stopwatch _stopWatch;
        private TimeSpan _timeRunning;
        private DateTime _lastDay;
        private HttpRequestResponse _httpHandler;
        private Timer _saverToWeb;
        private bool _startedTimer;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="wzmCore">Instancia unica de WZMCore</param>
        /// <param name="logger">Logger</param>
        public LicenseChecker(IWzmCore wzmCore, ILogger<object> logger)
        {
            _wzmCore = wzmCore;
            _stopWatch = Stopwatch.StartNew();
            _timeRunning = _stopWatch.Elapsed;
            _httpHandler = new HttpRequestResponse();

            //Compruebo que el Tiempo de Ejecución exista en el equipo local
            //si no existe intenta buscarlo en la web.

            GatherNeccesaryFiles();

            //CheckLastDayWithServer();
        }

        /// <summary>
        /// Comprueba la existencia del último día que se utilizó la app tanto en la PC local como en el Servidor Web 
        /// </summary>

        /// <summary>
        /// Comprueba la existencia del último día que se utilizó la app tanto en la PC local como en el Servidor Web 
        /// </summary>
        private void TimeChecker()
        {
            if (FilePath.ExistFile(Files.Time))
            {
                try
                {
                    using (var stream = new StreamReader(FilePath.GetFilePath(Files.Time)))
                    {
                        _timeRunning = TimeSpan.Parse(ElectronicCodeBook.Decrypt(stream.ReadToEnd(), true, SecretKeys.StringKey));

                        CheckRunningTimeWithServer();
                    }
                }
                catch (Exception)
                {

                }

            }
            else
            {
                CheckRunningTimeWithServer();

                using (var stream = new StreamWriter(FilePath.GetFilePath(Files.Time)))
                {
                    // Si el usuario elimino el archivo o reinstaló la pc, este codigo es inutil.
                    _timeRunning.Add(_stopWatch.Elapsed);
                    stream.Write(ElectronicCodeBook.Encrypt(_timeRunning.ToString(), true, SecretKeys.StringKey));

                }


            }
        }

        private void GatherNeccesaryFiles()
        {
            GetLocalRunnginTime();

            GetLocalLastDayFile();

        }

        /// <summary>
        /// Comprueba el último día que existe en el Servidor
        /// </summary>
        private void CheckLastDayWithServer()
        {
            // Si la Web se encuentra accesible
            if (_httpHandler.IsWebOnline())
            {
                // Si ya se ha logueado a la Web
                if (_httpHandler.LoggedIn)
                {
                    GetLocalLastDayFile();
                    var lastDay = _httpHandler.ExistLastDay(_wzmCore.LicenseManager.GetLicenseId());

                    if (lastDay.Exist)
                    {
                        if (_lastDay < lastDay.Value)
                        {
                            _lastDay = lastDay.Value;

                            // Escribo los datos en la PC local.
                            using (var stream = new StreamWriter(FilePath.GetFilePath(Files.LastDay)))
                            {
                                stream.Write(ElectronicCodeBook.Encrypt(_lastDay.ToString(), true, SecretKeys.StringKey));
                            }
                        }
                        if (_lastDay > lastDay.Value)
                        {
                            _httpHandler.SaveLastDay(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _lastDay);
                        }
                    }
                    else
                    {

                        _httpHandler.SaveLastDay(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _lastDay);
                    }
                }

                else
                {
                    // Intento loguearme a la Web
                    if (_httpHandler.LoginWeb())
                    {
                        var lastDay = _httpHandler.ExistLastDay(_wzmCore.LicenseManager.GetLicenseId());

                        if (lastDay.Exist)
                        {
                            if (_lastDay < lastDay.Value)
                            {
                                _lastDay = lastDay.Value;
                            }
                            if (_lastDay > lastDay.Value)
                            {
                                _httpHandler.SaveLastDay(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _lastDay);
                            }
                        }
                        else
                        {
                            _httpHandler.SaveLastDay(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _lastDay);
                        }
                    }
                    // Si falla el Login por cualquier motivo
                    else
                    {
                        GetLocalLastDayFile();
                    }
                }

            }
            else
            {
                GetLocalLastDayFile();
            }
        }

        /// <summary>
        /// Intento coger el ultimo día que se encuentra en la PC local, si es que se encuentra.
        /// </summary>
        private void GetLocalLastDayFile()
        {
            //Intento cargar el archivo desde la PC local.

            // Si el archivo existe en la PC local
            if (FilePath.ExistFile(Files.LastDay))
            {
                try
                {
                    // Intento cargar el ultimo día.
                    using (var stream = new StreamReader(FilePath.GetFilePath(Files.LastDay)))
                    {
                        var localDay = DateTime.Parse(ElectronicCodeBook.Decrypt(stream.ReadToEnd(), true, SecretKeys.StringKey));
                        // Como este método será llamado varias veces he de comprobar que el día almacenado sea mayor que el ultimo dia en el sistema.
                        if (_lastDay < localDay)
                        {
                            _lastDay = localDay;
                        }

                    }
                }
                catch (Exception)
                {

                }

            }
            else
            {
                // Si no existe el archivo, creo uno con la fecha actual.
                using (var stream = new StreamWriter(FilePath.GetFilePath(Files.LastDay)))
                {
                    stream.Write(ElectronicCodeBook.Encrypt(DateTime.Now.ToString(), true, SecretKeys.StringKey));

                    _lastDay = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Intento coger el tiempo de ejecucion de la app en la PC local.
        /// </summary>
        private void GetLocalRunnginTime()
        {
            if (FilePath.ExistFile(Files.Time))
            {
                try
                {
                    using (var stream = new StreamReader(FilePath.GetFilePath(Files.Time)))
                    {
                        _timeRunning = TimeSpan.Parse(ElectronicCodeBook.Decrypt(stream.ReadToEnd(), true, SecretKeys.StringKey));
                    }
                }
                catch (Exception)
                {

                }

            }

            else
            {

                using (var stream = new StreamWriter(FilePath.GetFilePath(Files.Time)))
                {
                    // Si el usuario elimino el archivo o reinstaló la pc, este codigo es inutil.
                    _timeRunning.Add(_stopWatch.Elapsed);
                    stream.Write(ElectronicCodeBook.Encrypt(_timeRunning.ToString(), true, SecretKeys.StringKey));

                }


            }
        }

        /// <summary>
        /// Realiza operaciones con el Server para determinar el tiempo que la app esté corriendo.
        /// </summary>
        private void CheckRunningTimeWithServer()
        {
            if (_httpHandler.IsWebOnline())
            {
                if (_httpHandler.LoginWeb())
                {
                    var runningTimeWeb = _httpHandler.ExistRunningTime(_wzmCore.LicenseManager.GetLicenseId());
                    if (runningTimeWeb.Exist)
                    {
                        var time = TimeSpan.FromDays(runningTimeWeb.Value);
                        if (_timeRunning < time)
                        {
                            _timeRunning = time;
                        }
                        else
                        {
                            _httpHandler.SaveRunningTime(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _timeRunning.TotalDays);
                        }

                    }
                    else
                    {
                        _httpHandler.SaveRunningTime(_wzmCore.LicenseManager.GetLicenseId(), _wzmCore.LicenseManager.GetLicenseEmail(), _timeRunning.TotalDays);
                    }
                }
            }
        }

        private TimeSpan _tempTime;

        public async Task Execute(IJobExecutionContext context)
        {
            if (_wzmCore.LicenseManager.ValidateWriteLastDay(_lastDay))
            {
                if (_lastDay.Date != DateTime.Now.Date)
                {
                    _lastDay = DateTime.Now;

                    File.Delete(FilePath.GetFilePath(Files.LastDay));

                    using (var stream = new StreamWriter(FilePath.GetFilePath(Files.LastDay)))
                    {
                        // Si el usuario elimino el archivo o reinstaló la pc, este codigo es inutil.

                        stream.Write(ElectronicCodeBook.Encrypt(DateTime.Now.ToString(), true, SecretKeys.StringKey));

                    }
                }
            }

            if (_wzmCore.LicenseManager.ValidateRunningTime(_tempTime, _lastDay))
            {
                await Task.Run(() => _wzmCore.LicenseManager.Validate());

            }
            _tempTime = _timeRunning.Add(_stopWatch.Elapsed);
            // Salvo el tiempo transcurrido en la pc local
            using (var streamWriter = new StreamWriter(FilePath.GetFilePath(Files.Time)))
            {
                streamWriter.Write(ElectronicCodeBook.Encrypt(_tempTime.ToString(), true, SecretKeys.StringKey));

                if (!_startedTimer)
                {
                    _saverToWeb = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(10));
                    _startedTimer = true;
                }
            }



        }

        /// <summary>
        /// Ejecuta el Tick del Timer
        /// </summary>
        /// <param name="state"></param>
        private void DoWork(object state)
        {
            CheckLastDayWithServer();
            CheckRunningTimeWithServer();
        }

    }
}
