﻿using System;
using System.Linq;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.System;
using wzmCore.Base;
using wzmCore.Extensions;
using wzmCore.Mikrotik;
using wzmCore.Mikrotik.Configuration;
using wzmCore.Mikrotik.WebProxyAdmin;
using wzmCore.Services;
using wzmData.Models;

namespace wzmCore.Administration
{
    public class MikrotikManager : MikrotikBaseConnection
    {
        public MikrotikManager()
        {
            PortalNauta = new PortalNauta();
            MACChanger = new MacChanger();


        }

        public int GetLicenseLevel(ITikConnection connection)
        {
            return connection.LoadAll<License>().FirstOrDefault().Level;
        }

        public int GetLicenseLevel(MikrotikCredentials credential)
        {
            using (var connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                return connection.LoadAll<License>().FirstOrDefault().Level;
            }
        }

        public PortalNauta PortalNauta { get; private set; }

        public TikInterface Interface { get; } = new TikInterface();

        public Ip Ip { get; } = new Ip();

        public Firewall Firewall => new Firewall();

        public InternetInformation InternetInfo { get; } = new InternetInformation();

        public ConfigMikrotik ConfigMikrotik { get; } = new ConfigMikrotik();

        public MkQueueSimple QueueSimple { get; } = new MkQueueSimple();

        public MkQueueTree QueueTree { get; } = new MkQueueTree();

        public MkQueueType QueueType { get; } = new MkQueueType();

        public MacChanger MACChanger { get; }

        public QoS QoS { get; } = new QoS();

        public WebProxyManager WebProxyManager { get; } = new WebProxyManager();

        /// <summary>
        /// Cuando el usuario introduce las credenciales en el Mikroik, estas son comprobadas.
        /// </summary>
        /// <param name="credential"></param>
        public void TrySaveCredential(MikrotikCredentials credential)
        {
            Init(credential as MikrotikCredentials);
            Dispose();

        }

        public bool CanConnectToMikrotik(MikrotikCredentials credential)
        {
            try
            {
                using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(),credential.Dispositivo.Ip,credential.User,credential.Password))
                {
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

    }
}
