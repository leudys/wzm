﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using tik4net.Objects;
using tik4net.Objects.Interface;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.Ubiquiti;
using wzmCore.Utils;
using wzmData.Enum;
using wzmCore.Services;
using wzmCore.Enum;
using wzmData.ViewModels;
using wzmCore.Extensions;
using tik4net.Objects.Queue;
using wzmData.Ubiquiti;
using wzmData.User;
using SshCore;
using wzmData.Ssh;
using WzmCommon.Files;

namespace wzmCore.Administration
{
    public class UserManagement
    {
        private static Random _random;

        private AddressList _addressList;

        private InternetInformation _internetInfo;

        public DeviceManager DeviceManager { get; private set; }

        public UserManagement()
        {
            _random = new Random();
            _internetInfo = new InternetInformation();
            _addressList = new AddressList();
            DeviceManager = new DeviceManager();
        }

        #region Private
        private async Task<ImportDevices> Import(Dispositivo dispositivo, List<UserDevice> devices, UbiquitiCredentials ubiquitiCredential, int random, MikrotikCredentials mikrotikCredential, List<Dispositivo> dispositivos)
        {
            ImportDevices importedUsers = new ImportDevices();
            List<Task> tasks = new List<Task>();
            List<UbiquitiMac> macList = new List<UbiquitiMac>();
            //List<DhcpServerLease> leases = new List<DhcpServerLease>();
            UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();
            DHCPServer dHCPServer = new DHCPServer();
            if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti)
            {
                // Si el dispositivo es M2 o M5, trabajo con el, sino redirecciono y muestro problemas de compatibilidad.
                if (dispositivo.Platform.Contains("M2") || dispositivo.Platform.Contains("M5"))
                {

                    try
                    {
                        // Obtengo los usuarios que se encuentran en el ACL del dispositivo.
                        tasks.Add(Task.Run(async () => macList = await ubiquitiACLManager.GetDeviceUsersAsync(ubiquitiCredential, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random))));
                        // Obtengo los Leases que existan en el Mikrotik
                        //tasks.Add(Task.Run(() => leases = dHCPServer.GetServerLeases(mikrotikCredential).ToList()));

                        await Task.WhenAll(tasks);

                        importedUsers.Dispositivos = DeviceManager.GetDevices(devices, dispositivos, macList, mikrotikCredential);

                        return importedUsers;

                    }
                    catch (Exception e)
                    {
                        importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "Error", Description = e.Message });
                        return importedUsers;
                    }


                }
                else
                {
                    importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "IncompatibleDevice", Description = "WZM hasta el momento solamente puede importar usuarios del ACL a dispositivos Ubiquiti M2 ó M5." });

                    return importedUsers;

                }
            }
            else
            {
                importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "IncompatibleDevice", Description = "WZM hasta el momento solamente puede importar usuarios del ACL a dispositivos Ubiquiti M2 ó M5." });

                return importedUsers;
            }
        }

        private async Task<ImportDevices> Import(Dispositivo dispositivo, List<UserDevice> devices, UbiquitiCredentials ubiquitiCredential, int random, MikrotikCredentials mikrotikEtecsa, MikrotikCredentials adminDevice, List<Dispositivo> dispositivos)
        {
            ImportDevices importedUsers = new ImportDevices();
            List<Task> tasks = new List<Task>();
            List<UbiquitiMac> macList = new List<UbiquitiMac>();

            UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();
            DHCPServer dHCPServer = new DHCPServer();
            if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti)
            {
                // Si el dispositivo es M2 o M5, trabajo con el, sino redirecciono y muestro problemas de compatibilidad.
                if (dispositivo.Platform.Contains("M2") || dispositivo.Platform.Contains("M5"))
                {

                    try
                    {
                        // Obtengo los usuarios que se encuentran en el ACL del dispositivo.
                        tasks.Add(Task.Run(async () => macList = await ubiquitiACLManager.GetDeviceUsersAsync(ubiquitiCredential, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random))));
                        // Obtengo los Leases que existan en el Mikrotik


                        await Task.WhenAll(tasks);

                        importedUsers.Dispositivos = DeviceManager.GetDevices(devices, dispositivos, macList, mikrotikEtecsa, adminDevice);

                        return importedUsers;

                    }
                    catch (Exception e)
                    {
                        importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "Error", Description = e.Message });
                        return importedUsers;
                    }


                }
                else
                {
                    importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "IncompatibleDevice", Description = "WZM hasta el momento solamente puede importar usuarios del ACL a dispositivos Ubiquiti M2 ó M5." });

                    return importedUsers;

                }
            }
            else
            {
                importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "IncompatibleDevice", Description = "WZM hasta el momento solamente puede importar usuarios del ACL a dispositivos Ubiquiti M2 ó M5." });

                return importedUsers;
            }
        }
        //private async Task<ImportDevices> Import(Dispositivo dispositivo, List<UserDevice> devices, UbiquitiCredentials ubiquitiCredential, int random, MikrotikCredentials mikrotikCredential, List<Dispositivo> dispositivos)
        //{
        //    ImportDevices importedUsers = new ImportDevices();
        //    List<Task> tasks = new List<Task>();
        //    List<UbiquitiMac> macList = new List<UbiquitiMac>();
        //    List<DhcpServerLease> leases = new List<DhcpServerLease>();
        //    UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();
        //    DHCPServer dHCPServer = new DHCPServer();
        //    if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti)
        //    {
        //        // Si el dispositivo es M2 o M5, trabajo con el, sino redirecciono y muestro problemas de compatibilidad.
        //        if (dispositivo.Platform.Contains("M2") || dispositivo.Platform.Contains("M5"))
        //        {

        //            try
        //            {
        //                // Obtengo los usuarios que se encuentran en el ACL del dispositivo.
        //                tasks.Add(Task.Run(async () => macList = await ubiquitiACLManager.GetDeviceUsersAsync(ubiquitiCredential, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random))));
        //                // Obtengo los Leases que existan en el Mikrotik
        //                tasks.Add(Task.Run(() => leases = dHCPServer.GetServerLeases(mikrotikCredential).ToList()));

        //                await Task.WhenAll(tasks);

        //                importedUsers.Dispositivos = DeviceManager.GetDevices(devices, dispositivos, macList, mikrotikCredential);

        //                return importedUsers;

        //            }
        //            catch (Exception e)
        //            {
        //                importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "Error", Description = e.Message });
        //                return importedUsers;
        //            }


        //        }
        //        else
        //        {
        //            importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "IncompatibleDevice", Description = "WZM hasta el momento solamente puede importar usuarios del ACL a dispositivos Ubiquiti M2 ó M5." });

        //            return importedUsers;

        //        }
        //    }
        //    else
        //    {
        //        importedUsers.UserValidation.Failed(new UserValidationErrors() { Code = "IncompatibleDevice", Description = "WZM hasta el momento solamente puede importar usuarios del ACL a dispositivos Ubiquiti M2 ó M5." });

        //        return importedUsers;
        //    }
        //}
        #endregion

        /// <summary>
        /// Cambia un usuario hacia otro portal
        /// </summary>
        /// <param name="user">Usuario a ser cambiado.</param>
        /// <param name="credentials">Credenciales del Mikrotik</param>
        /// <param name="portal">Portal donde se va a mover el usuario</param>
        /// <returns></returns>
        public UserValidation ChangeUserToAnotherPortal(ApplicationUsers user, MikrotikCredentials credential, string portal)
        {

            UserValidation userValidation = new UserValidation();
            try
            {
                using (ITikConnection conection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                {
                    foreach (var device in user.UserDevices)
                    {
                        DeviceManager.ChangeDeviceToAnotherPortal(device, conection, portal);
                    }
                    return userValidation;
                }
            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Code = "ChangeUserToAnotherPortal", Description = e.Message });
                return userValidation;
            }

        }

        /// <summary>
        /// Cambia un usuario hacia otro portal
        /// </summary>
        /// <param name="user">Usuario a ser cambiado.</param>
        /// <param name="credentials">Credenciales del Mikrotik</param>
        /// <param name="portal">Portal donde se va a mover el usuario</param>
        /// <returns></returns>
        public UserValidation ChangeDeviceToAnotherPortal(UserDevice dispositivo, MikrotikCredentials credential, string portal)
        {

            return DeviceManager.ChangeDeviceToAnotherPortal(dispositivo, credential, portal);


        }


        /// <summary>
        /// Devuelve todos los dispositivos de un usuario a su Portal de Origen
        /// </summary>
        /// <param name="user"></param>
        /// <param name="credential"></param>
        public void ReturnToOriginalPortal(ApplicationUsers user, MikrotikCredentials credential)
        {
            using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                foreach (var device in user.UserDevices)
                {
                    var adressList = connection.LoadAll<FirewallAddressList>();

                    if (adressList.Any(c => c.Address.Equals(device.Ip)))
                    {
                        var userAddressList = adressList.FirstOrDefault(c => c.Address.Equals(device.Ip));

                        if (!userAddressList.List.Equals(device.PortalDeOrigen))
                        {
                            userAddressList.List = device.PortalDeOrigen;

                            connection.Save(userAddressList);
                        }
                    }


                }



            }
        }

        /// <summary>
        /// Devuelve un dispositivo a su Portal de Origen
        /// </summary>
        /// <param name="device">Usuario que se moverá a su portal de origen.</param>
        /// <param name="connection">Conexión al Mikrotik.</param>
        public void ReturnToOriginalPortal(UserDevice device, ITikConnection connection)
        {

            var adressList = connection.LoadAll<FirewallAddressList>();

            if (adressList.Any(c => c.Address.Equals(device.Ip)))
            {
                var userAddressList = adressList.FirstOrDefault(c => c.Address.Equals(device.Ip));

                if (!userAddressList.List.Equals(device.PortalDeOrigen))
                {
                    userAddressList.List = device.PortalDeOrigen;

                    connection.Save(userAddressList);
                }
            }


        }

        /// <summary>
        /// Devuelve un dispositivo a su Portal de Origen
        /// </summary>
        /// <param name="device">Usuario que se moverá a su portal de origen.</param>
        /// <param name="credential">Conexión al Mikrotik.</param>
        public void ReturnToOriginalPortal(UserDevice device, MikrotikCredentials credential)
        {
            using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                var adressList = connection.LoadAll<FirewallAddressList>();

                if (adressList.Any(c => c.Address.Equals(device.Ip)))
                {
                    var userAddressList = adressList.FirstOrDefault(c => c.Address.Equals(device.Ip));

                    if (!userAddressList.List.Equals(device.PortalDeOrigen))
                    {
                        userAddressList.List = device.PortalDeOrigen;

                        connection.Save(userAddressList);
                    }
                }
            }



        }

        /// <summary>
        /// Elimina el Queue de un dispositivo
        /// </summary>
        /// <param name="device"></param>
        /// <param name="credential"></param>
        public void RemoveSimpleQueue(UserDevice device, MikrotikCredentials credential)
        {
            using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
            {
                var queues = connection.LoadAll<QueueSimple>();

                if (queues.Any(c => c.Target.Contains(device.Ip) && c.Name.Equals(device.Comment)))
                {
                    var queue = queues.FirstOrDefault(c => c.Target.Contains(device.Ip) && c.Name.Equals(device.Comment));
                    connection.Delete(queue);
                }
            }

        }

        /// <summary>
        /// Devuelve todos los dispositivos a su portal de Origen
        /// </summary>
        /// <param name="usuarios">Usuarios que serán retornados a su portal</param>
        public void ReturnToOriginalPortal(bool fromBalancing, List<HostsServices> devicesServices, ITikConnection connection)
        {
            var mkAddressList = connection.LoadAll<FirewallAddressList>().ToList();
            var mkR = new Routes().GetMarkRoutes(connection);
            var mkWirelles = connection.LoadAll<InterfaceWireless>().Where(c => c.InterfaceType.Equals("virtual")).ToList();

            //Retornar los usuarios al portal de origen
            foreach (var item in devicesServices.Where(c => c.IsBalanced == fromBalancing).ToList())
            {

                if (mkAddressList.Any(c => c.Address.Equals(item.Device.Ip) && !c.List.Equals(item.Device.PortalDeOrigen)))
                {
                    _addressList.SetOrChangeAddressList(item.Device, connection);
                }
            }
        }



        /// <summary>
        /// Comprueba que los dispositivos ya tengan internet
        /// </summary>
        /// <param name="mkAddressList"></param>
        /// <param name="credential"></param>
        public void UserWithInternet(List<FirewallAddressList> mkAddressList, ref List<HostsServices> services, ITikConnection connection)
        {
            try
            {
                foreach (var host in services)
                {

                    if (mkAddressList.Any(c => c.Address.Equals(host.Device.Ip)))
                    {
                        var a = mkAddressList.Where(c => c.Address.Equals(host.Device.Ip));


                        foreach (var item in a)
                        {
                            try
                            {
                                if (_internetInfo.HasInternet(connection, item.List))
                                {
                                    host.HasInternet = true;

                                    break;
                                }
                                else
                                {
                                    host.HasInternet = false;
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }

                    }


                }
            }
            //catch (Exception) { }
            catch (InvalidOperationException)
            {

                UserWithInternet(mkAddressList, ref services, connection);
            }

        }

        /// <summary>
        /// Comprueba que usuario tenga internet.
        /// Si el usuario no se encuentra en ninguna ruta de los portales existentes que tengan internet, entonces este no posee internet
        /// </summary>
        /// <param name="portales"></param>
        /// <param name="services"></param>
        public void UserWithInternet(List<PortalesInternet> portales, ref List<HostsServices> services)
        {
            try
            {

                foreach (var host in services)
                {

                    if (portales.Any(c => c.Route.Contains(host.PortalActual)))
                    {
                        host.HasInternet = true;
                    }
                    else
                    {
                        host.HasInternet = false;
                    }

                }
            }
            catch (Exception) { }

        }

        /// <summary>
        /// Comprueba si el usuario se encuentra en balanceo
        /// </summary>
        /// <returns></returns>
        public bool UserIsBalanced(string host, IEnumerable<FirewallAddressList> a, string addressList)
        {
            var ad = a.Where(c => c.Address.Equals(host)).FirstOrDefault();

            if (ad !=null)
            {
                if (ad.List != addressList)
                {
                    return false;
                }

                return true;
            }
            return false;

        }


        /// <summary>
        /// Cambia el dispositvo del cliente
        /// </summary>
        /// <param name="oldMac">MAC del Dispositivo anterior</param>
        /// <param name="newMac">MAC del nuevo dispositivo</param>
        /// <param name="mikrotikCredential"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <returns></returns>
        public async Task<bool> ChangeClientDeviceDynamicIpAsync(ChangeUserDevice changedDevice, UserDevice device, Dispositivo dispositivo, MikrotikCredentials mikrotikCredential, UbiquitiCredentials ubiquitiCredential, int random)
        {
            ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password);
            try
            {

                // Ip´s del DHCP Server
                var dHCPServer = new DHCPServer();
                if (dHCPServer.ChangeLeaseMac(changedDevice.OldMac, changedDevice.NewMac, connection, device.Comment))
                {
                    if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti && dispositivo.WirelessMode == WirelessMode.Ap && (dispositivo.WirelessSecurity == WirelessSecurity.ACL || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WPA))
                    {
                        SshConnection sshConnection = new SshConnection();

                        ScpConnection scpConnection = new ScpConnection();
                        Login login = new Login()
                        {
                            Ip = ubiquitiCredential.Dispositivo.Ip,
                            User = ubiquitiCredential.User,
                            Password = ubiquitiCredential.Password,
                            Port = 22
                        };
                        List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                        await Task.WhenAll(tasks);

                        var ubiquitiManager = new UbiquitiACLManager();

                        var oldDevice = await ubiquitiManager.GetDeviceByMacAsync(scpConnection, changedDevice.OldMac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                        if (oldDevice != null)
                        {
                            var newUser = new UbiquitiMac() { Comment = oldDevice.Comment, Id = oldDevice.Id, Mac = oldDevice.Mac, Status = oldDevice.Status };

                            await ubiquitiManager.UpdateDeviceAsync(scpConnection, sshConnection, oldDevice, newUser, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                            sshConnection.Dispose();
                            scpConnection.Dispose();

                            return true;
                        }
                        var newDevice = await ubiquitiManager.GetDeviceByMacAsync(scpConnection, changedDevice.NewMac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                        if (newDevice != null)
                        {
                            return true;
                        }
                        else
                        {
                            return ubiquitiManager.AddDeviceAsync(device.Comment, changedDevice.NewMac, scpConnection, sshConnection, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random)).Result.Succeeded;
                        }

                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch { return false; }
        }

        /// <summary>
        /// Genera una contraseña débil basado en el nombre del usuario y la fecha del sistema
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public string GenerateWeakPassword(string username)
        {
            char d = char.Parse(username[0].ToString().ToUpper());

            string newNick = string.Empty;

            for (int i = 0; i < username.Length; i++)
            {
                if (i != 0)
                {
                    newNick += username[i].ToString().ToLower();
                }
                else
                {
                    newNick = d.ToString();
                }
            }

            return $"{newNick}." + DateTime.Now.Year;
        }

        /// <summary>
        /// Deshabilita el servicio de Internet a todos los dispositivos de un usuario en el Mikrotik
        /// </summary>
        /// <param name="user">Usuario a Deshabilitar</param>
        /// <param name="mikrotikCredential">Credenciales del Mikrotik</param>
        public void DisableUserInternetAsync(ApplicationUsers user, MikrotikCredentials mikrotikCredential)
        {

            using (ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {
                var arp = new ARP();
                var firewall = new AddressList();
                var leases = new DHCPServer();

                foreach (var item in user.UserDevices)
                {
                    if (leases.GetServerLeases(connection).Any(c => c.MacAddress.Equals(item.Mac)))
                    {
                        var userLease = leases.GetServerLeases(connection).FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                        leases.DisableLease(connection, userLease);
                    }


                    if (arp.GetAllArp(connection).Any(c => c.MacAddress.Equals(item.Mac)))
                    {
                        var userArp = arp.GetAllArp(connection).FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                        try
                        {
                            arp.Disable(connection, userArp);
                        }
                        catch (Exception)
                        {


                        }

                    }

                    if (firewall.GetAddressList(connection).Any(c => c.Address.Equals(item.Ip)))
                    {
                        var userAddressList = firewall.GetAddressList(connection).FirstOrDefault(c => c.Address.Equals(item.Ip));

                        firewall.Disable(userAddressList, connection);
                    }
                }


            }
        }

        /// <summary>
        /// Deshabilita el servicio de Internet a todos los dispositivos de un usuario en el Mikrotik
        /// </summary>
        /// <param name="user">Usuario a Deshabilitar</param>
        /// <param name="connection"></param>
        public void DisableUserInternetAsync(ApplicationUsers user, ITikConnection connection)
        {


            var arp = new ARP();
            var firewall = new AddressList();
            var leases = new DHCPServer();

            foreach (var item in user.UserDevices)
            {
                if (leases.GetServerLeases(connection).Any(c => c.MacAddress.Equals(item.Mac)))
                {
                    var userLease = leases.GetServerLeases(connection).FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                    leases.DisableLease(connection, userLease);
                }


                if (arp.GetAllArp(connection).Any(c => c.MacAddress.Equals(item.Mac)))
                {
                    var userArp = arp.GetAllArp(connection).FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                    try
                    {
                        arp.Disable(connection, userArp);
                    }
                    catch (Exception)
                    {


                    }

                }

                if (firewall.GetAddressList(connection).Any(c => c.Address.Contains(item.Ip)))
                {
                    var userAddressList = firewall.GetAddressList(connection).FirstOrDefault(c => c.Address.Contains(item.Ip));

                    firewall.Disable(userAddressList, connection);
                }
            }




        }

        /// <summary>
        /// Añade un usuario moroso al Mikrotik
        /// </summary>
        /// <param name="user"></param>
        /// <param name="markRoute"></param>
        /// <param name="connection"></param>
        public void AddMoroso(ApplicationUsers user, string markRoute, ITikConnection connection)
        {
            foreach (var item in user.UserDevices)
            {
                _addressList.UpdateAddressList(null, null, markRoute, item.Ip, connection);
            }
        }
        /// <summary>
        /// Habilita el servicio de Internet a un usuario en el Mikrotik
        /// </summary>
        /// <param name="client">Cliente para retirarle el servicio</param>
        /// <param name="mikrotikCredential"></param>
        /// <returns></returns>
        public void EnableUserInternet(ApplicationUsers user, MikrotikCredentials mikrotikCredential)
        {
            try
            {
                using (ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                {
                    var arp = new ARP();
                    var firewall = new AddressList();
                    var leases = new DHCPServer();

                    foreach (var item in user.UserDevices)
                    {
                        if (leases.GetServerLeases(connection).Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var userLease = leases.GetServerLeases(connection).FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                            leases.EnableLease(connection, userLease);
                        }


                        if (arp.GetAllArp(connection).Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var userArp = arp.GetAllArp(connection).FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                            arp.Enable(connection, userArp);
                        }

                        if (firewall.GetAddressList(connection).Any(c => c.Address.Equals(item.Ip)))
                        {
                            var userAddressList = firewall.GetAddressList(connection).FirstOrDefault(c => c.Address.Equals(item.Ip));

                            firewall.Enable(userAddressList, connection);
                        }
                    }



                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Importa dispositivos desde un dispositivo Ubiquiti
        /// </summary>
        /// <param name="mikrotikCredential"></param>
        /// <param name="devices"></param>
        /// <param name="dispositivo"></param>
        /// <param name="dispositivos"></param>
        /// <param name="usuarios"></param>
        /// <param name="licenseType"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public async Task<ImportDevices> ImportFromUbiquitiAsync(MikrotikCredentials mikrotikCredential, List<UserDevice> devices, Dispositivo dispositivo, List<Dispositivo> dispositivos, LicenseType licenseType, UbiquitiCredentials ubiquitiCredential, int random)
        {
            ImportDevices importUbiquitiUser = new ImportDevices();

            if (licenseType == LicenseType.Trial)
            {
                if (devices.Count >= 20)
                {
                    importUbiquitiUser.UserValidation.Failed(new UserValidationErrors() { Description = "No se pueden importar los usuarios por restricción de licencia." });

                }
                else
                {
                    importUbiquitiUser.UserValidation.Failed(new UserValidationErrors() { Description = "Solamente puedes tener 20 usuarios en el periódo de prueba de WZM." });

                    if (devices.Count() + importUbiquitiUser.Dispositivos.Count() > 20)
                    {
                        int userToRemove = devices.Count() - importUbiquitiUser.Dispositivos.Count();

                        if (userToRemove < 0)
                        {
                            userToRemove = userToRemove * -1;

                            for (int i = 0; i < userToRemove; i++)
                            {
                                importUbiquitiUser.Dispositivos.RemoveAt(i);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < userToRemove; i++)
                            {
                                importUbiquitiUser.Dispositivos.RemoveAt(i);
                            }
                        }
                    }

                    return importUbiquitiUser;
                }
            }

            return await Import(dispositivo, devices, ubiquitiCredential, random, mikrotikCredential, dispositivos);
        }

        /// <summary>
        /// Importa dispositivos desde un dispositivo Ubiquiti
        /// </summary>
        /// <param name="mikrotikEtecsa"></param>
        /// <param name="adminDevice"></param>
        /// <param name="devices"></param>
        /// <param name="dispositivo"></param>
        /// <param name="dispositivos"></param>
        /// <param name="licenseType"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public async Task<ImportDevices> ImportFromUbiquitiAsync(MikrotikCredentials mikrotikEtecsa, MikrotikCredentials adminDevice, List<UserDevice> devices, Dispositivo dispositivo, List<Dispositivo> dispositivos, LicenseType licenseType, UbiquitiCredentials ubiquitiCredential, int random)
        {
            ImportDevices importUbiquitiUser = new ImportDevices();

            if (licenseType == LicenseType.Trial)
            {
                if (devices.Count >= 20)
                {
                    importUbiquitiUser.UserValidation.Failed(new UserValidationErrors() { Description = "No se pueden importar los usuarios por restricción de licencia." });

                }
                else
                {
                    importUbiquitiUser.UserValidation.Failed(new UserValidationErrors() { Description = "Solamente puedes tener 20 usuarios en el periódo de prueba de WZM." });

                    if (devices.Count() + importUbiquitiUser.Dispositivos.Count() > 20)
                    {
                        int userToRemove = devices.Count() - importUbiquitiUser.Dispositivos.Count();

                        if (userToRemove < 0)
                        {
                            userToRemove = userToRemove * -1;

                            for (int i = 0; i < userToRemove; i++)
                            {
                                importUbiquitiUser.Dispositivos.RemoveAt(i);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < userToRemove; i++)
                            {
                                importUbiquitiUser.Dispositivos.RemoveAt(i);
                            }
                        }
                    }

                    return importUbiquitiUser;
                }
            }

            return await Import(dispositivo, devices, ubiquitiCredential, random, mikrotikEtecsa, adminDevice, dispositivos);
        }


        /// <summary>
        /// Llena la información de los dispositivos
        /// </summary>
        /// <param name="withInternet"></param>
        /// <param name="ip"></param>
        /// <param name="wlan"></param>
        /// <param name="credential"></param>
        /// <returns></returns>
        public ClientInfoViewModel FillClientInfo(bool withInternet, string ip, string wlan, MikrotikCredentials credential)
        {
            ClientInfoViewModel clientInfo = new ClientInfoViewModel();
            if (withInternet)
            {
                if (wlan != string.Empty)
                {
                    using (ITikConnection connection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                    {
                        var r = new Routes().GetMarkRoutes(connection).First(c => c.Gateway.Contains(wlan)).MarkRoute;
                        var i = new InternetInformation().HasInternet(credential, r);
                        if (i)
                        {
                            clientInfo.Ip = ip; clientInfo.Wlan = wlan; clientInfo.Internet = "Sí";

                        }
                        else
                        {
                            clientInfo.Ip = ip; clientInfo.Wlan = wlan; clientInfo.Internet = "No";
                        }

                    }
                }
                else
                {
                    clientInfo.Ip = ip; clientInfo.Wlan = string.Empty; clientInfo.Internet = "No";
                }



                return clientInfo;
            }
            else
            {                
                clientInfo.Ip = ip;
                clientInfo.Wlan = new Mikrotik.Wireless().GetWlanGivenIp(ip, credential);
                return clientInfo;
            }

        }

        public SyncModel SyncWithMkDevice(List<UserDevice> dispositivos, MikrotikCredentials adminDevice, MikrotikCredentials mikrotikCredentials, bool deleteIfnoExist)
        {

            SyncModel syncModel = new SyncModel();

            var adminConnection = ConnectionFactory.OpenConnection(adminDevice.Dispositivo.Firmware.GetApiVersion(), adminDevice.Dispositivo.Ip, adminDevice.User, adminDevice.Password);
            var mainConnection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password);

            var dhcp = new DHCPServer();
            var adressList = new AddressList();
            var arp = new ARP();

            var adminDhcp = dhcp.GetServerLeases(adminConnection);
            var mainAdressList = adressList.GetAddressList(mainConnection);
            var mainArp = arp.GetAllArp(adminConnection);
            bool updatedAdressList = false;
            foreach (var item in dispositivos)
            {
                updatedAdressList = false;
                if (adminDhcp.Any(c => c.MacAddress.Equals(item.Mac)))
                {
                    var lease = adminDhcp.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                    if (!lease.Address.Equals(item.Ip))
                    {
                        if (mainAdressList.Any(c => c.Address.Equals(item.Ip)))
                        {
                            var adress = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Ip));

                            if (adress.Comment.Equals(item.Comment))
                            {

                                adress.Address = lease.Address;
                                item.Ip = lease.Address;
                                if (!adress.List.Equals(item.PortalDeOrigen))
                                {
                                    item.PortalDeOrigen = adress.List;
                                }

                                syncModel.ToUpdate.Add(item);
                            }


                        }
                        else
                        {
                            if (mainAdressList.Any(c => c.Address.Equals(lease.Address)))
                            {
                                var adress = mainAdressList.FirstOrDefault(c => c.Address.Equals(lease.Address));

                                if (!adress.Comment.Equals("morososWzm") || !adress.Comment.Equals("wzmBalanceo"))
                                {
                                    item.Ip = lease.Address;
                                    if (!lease.Comment.Equals(item.Comment))
                                    {
                                        item.Comment = lease.Comment;
                                    }
                                    if (!adress.List.Equals(item.PortalDeOrigen))
                                    {
                                        item.PortalDeOrigen = adress.List;
                                        updatedAdressList = true;
                                    }
                                    syncModel.ToUpdate.Add(item);
                                }


                            }
                        }


                    }

                }
                else
                {
                    if (!mainArp.Any(c => c.MacAddress.Equals(item.Mac)))
                    {
                        syncModel.ToDelete.Add(item);
                    }
                    else
                    {
                        var tempArp = mainArp.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                        if (!tempArp.Address.Equals(item.Ip))
                        {
                            if (mainAdressList.Any(c => c.Address.Equals(item.Ip)))
                            {
                                var adress = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Ip));
                                if (!adress.Comment.Equals("morososWzm") || !adress.Comment.Equals("wzmBalanceo"))
                                {
                                    if (adress.Comment.Equals(item.Comment))
                                    {

                                        adress.Address = tempArp.Address;
                                        item.Ip = tempArp.Address;
                                        if (!adress.List.Equals(item.PortalDeOrigen))
                                        {
                                            item.PortalDeOrigen = adress.List;
                                            updatedAdressList = true;
                                        }

                                        syncModel.ToUpdate.Add(item);
                                    }
                                }

                            }
                            else
                            {
                                if (mainAdressList.Any(c => c.Address.Equals(tempArp.Address)))
                                {
                                    var adress = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Ip));
                                    item.Ip = tempArp.Address;

                                    if (!adress.List.Equals(item.PortalDeOrigen))
                                    {
                                        item.PortalDeOrigen = adress.List;
                                        updatedAdressList = true;
                                    }
                                    syncModel.ToUpdate.Add(item);
                                }
                            }
                        }
                    }
                }

                if (!updatedAdressList)
                {
                    if (mainAdressList.Any(c => c.Address.Equals(item.Ip)))
                    {
                        var adress = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Ip));

                        if (!adress.List.Equals(item.PortalDeOrigen))
                        {
                            item.PortalDeOrigen = adress.List;
                            syncModel.ToUpdate.Add(item);
                        }
                    }
                }


            }

            foreach (var item in adminDhcp.Where(c => c.Dynamic == false))
            {
                if (!dispositivos.Any(c => c.Mac.Equals(item.MacAddress)))
                {
                    var device = new UserDevice()
                    {
                        Ip = item.Address,
                        Mac = item.MacAddress,
                        Comment = item.Comment,
                        DhcpServer = item.Server,
                        TipoIp = IpTypeEnum.Automatica,
                        DeviceConnectionType = DeviceConecctionType.Desconocido,

                    };

                    if (mainAdressList.Any(c => c.Address.Equals(item.Address)))
                    {
                        device.PortalDeOrigen = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Address)).List;
                    }
                    else
                    {
                        device.PortalDeOrigen = "NotInternetAccess";
                    }

                    syncModel.ToAdd.Add(device);
                }
            }

            foreach (var item in mainArp.Where(c => c.Dynamic == false))
            {
                if (!dispositivos.Any(c => c.Mac.Equals(item.MacAddress)))
                {
                    var device = new UserDevice()
                    {
                        Ip = item.Address,
                        Mac = item.MacAddress,
                        Comment = item.Comment,

                        TipoIp = IpTypeEnum.Automatica,
                        DeviceConnectionType = DeviceConecctionType.Desconocido,

                    };

                    if (mainAdressList.Any(c => c.Address.Equals(item.Address)))
                    {
                        var adress = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Address));
                        device.PortalDeOrigen = mainAdressList.FirstOrDefault(c => c.Address.Equals(item.Address)).List;
                        device.Comment = adress.Comment;
                        syncModel.ToAdd.Add(device);
                    }

                }
            }

            return syncModel;
        }
    }

    public class SyncModel
    {
        public SyncModel()
        {
            ToDelete = new List<UserDevice>();
            ToUpdate = new List<UserDevice>();
            ToAdd = new List<UserDevice>();
        }

        public List<UserDevice> ToDelete { get; set; }

        public List<UserDevice> ToUpdate { get; set; }

        public List<UserDevice> ToAdd { get; set; }
    }

}
