﻿using System;
using System.Linq;
using System.Threading.Tasks;
using tik4net;
using wzmCore.Data;
using wzmCore.Mikrotik;
using wzmCore.Data.Mikrotik;
using wzmData.Enum;
using wzmData.Models;
using tik4net.Objects.Ip.Firewall;
using tik4net.Objects;
using System.Collections.Generic;
using wzmCore.Ubiquiti;
using wzmCore.Utils;
using tik4net.Objects.Ip;
using tik4net.Objects.Ip.DhcpServer;
using wzmData.ViewModels;
using wzmCore.Extensions;
using wzmData.Ubiquiti;
using SshCore;
using wzmData.User;
using wzmData.Ssh;
using WzmCommon.Files;

namespace wzmCore.Administration
{
    public class DeviceManager
    {
        #region Private
        /// <summary>
        /// Añade un dispositivo del cliente al Mikrotik
        /// </summary>
        /// <param name="device">Dispositivo</param>
        /// <param name="connection"></param>
        /// <param name="connection">Conexión al Mikrotik</param>
        /// <returns></returns>
        private DeviceOperations AddDevice(UserDevice device, ITikConnection connection)
        {
            DeviceOperations deviceValidation = new DeviceOperations();
            deviceValidation.Device = device;

            try
            {
                var arp = new ARP();
                var firewall = new AddressList();

                if (device.TipoIp == IpTypeEnum.Fija)
                {
                    if (arp.GetAllArp(connection).Any(c => c.Address.Equals(device.Ip) && c.MacAddress.Equals(device.Mac) && firewall.GetAddressList(connection).Any(d => d.Address.Equals(device.Ip))))
                    {
                        deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "UserExist", Description = "El usuario ya existe en la tabla ARP del dispositivo." });
                        return deviceValidation;

                    }
                    else
                    {
                        var ipArp = new IpArpModel() { Address = device.Ip, Interface = "ether1", MacAddress = device.Mac, Comment = device.Comment };
                        new ARP().Add(connection, ipArp);
                        var addresslist = new FirewallAddressList() { Address = device.Ip, Comment = device.Comment, Disabled = false, List = device.PortalDeOrigen };
                        new Mikrotik.Firewall().AddressList.Add(addresslist, connection);
                        return deviceValidation;
                    }


                }
                else
                {
                    var dhcpServer = new DHCPServer();
                    var leases = dhcpServer.GetServerLeases(connection);
                    if (!leases.Any(c => c.MacAddress.Equals(device.Mac)))
                    {
                        var assignedIp = new DHCPServer().AssignIpAndAdressListToUser(device.Mac, device.DhcpServer, device.Comment, device.PortalDeOrigen, connection);
                        if (!assignedIp.Assigned)
                        {
                            deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "DHCP", Description = "El usuario no ha podido ser agregado puesto que no existe alguna Ip disponible para asiganarle." });
                            return deviceValidation;
                        }
                        else
                        {
                            deviceValidation.Device.Ip = assignedIp.Ip;
                            return deviceValidation;
                        }

                    }
                    else
                    {
                        var userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));
                        deviceValidation.Device.Ip = userLease.Address;

                        if (userLease.Dynamic)
                        {
                            userLease.MakeStatic(connection);
                        }

                        var addressList = new FirewallAddressList()
                        {
                            Address = userLease.Address,
                            Comment = device.Comment,
                            Disabled = false,
                            List = device.PortalDeOrigen
                        };

                        connection.Save(addressList);

                    }

                    return deviceValidation;
                }
            }
            catch (Exception e)
            {
                deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "AddUser", Description = e.Message });
                return deviceValidation;
            }

        }

        /// <summary>
        /// Crea un dispositivo
        /// </summary>
        /// <param name="ubiquitiMac">Dispositivo del Usuario</param>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <param name="portal">Portal del Dispositivo</param>
        /// <param name="lease">DHCP Server Lease del Dispositivo</param>
        /// <param name="ip">Ip del Dispositivo</param>
        /// <returns></returns>
        private UserDevice CreateUserDevice(UbiquitiMac ubiquitiMac, MikrotikCredentials credential, string portal = null, DhcpServerLease lease = null, string ip = null)
        {
            UserDevice userDevice = new UserDevice();
            if (lease != null)
            {
                userDevice.TipoIp = IpTypeEnum.Automatica;
                userDevice.DhcpServer = lease.Server;
                userDevice.Ip = lease.Address;
                userDevice.Mac = ubiquitiMac.Mac;
                userDevice.Comment = ubiquitiMac.Comment;

            }
            else
            {
                userDevice.TipoIp = IpTypeEnum.Fija;
                userDevice.Ip = ip;
                userDevice.Mac = ubiquitiMac.Mac;
                userDevice.Comment = ubiquitiMac.Comment;
            }
            if (portal != null)
            {
                userDevice.PortalDeOrigen = portal;
            }
            else
            {
                userDevice.PortalDeOrigen = new AddressList().SetAddressList(userDevice, credential);
            }

            return userDevice;
        }

        /// <summary>
        /// Cambia el IP del cliente a estática, agragandolo en la Tabla ARP y eliminándolo del DHCP Server
        /// </summary>
        /// <param name="userMac">MAC del Usuario</param>
        /// <param name="userName">Nombre del Usuario</param>
        /// <param name="oldIp">Ip Antigua</param>
        /// <param name="newIp">Nueva Ip</param>
        /// <param name="mikrotikCredential">Credenciales del Dispositivo</param>
        private DeviceOperations ChangeOrUpdateDeviceToStaticIp(UserDevice device, string newIp, ITikConnection connection)
        {
            DeviceOperations userOperation = new DeviceOperations();
            userOperation.Device = device;

            try
            {
                var leases = new DHCPServer();
                var serverLeases = leases.GetServerLeases(connection);
                var arp = new ARP();
                var arps = arp.GetAllArp(connection);

                // Elimino el Lease
                if (serverLeases.Any(c => c.MacAddress.Equals(device.Mac)))
                {
                    leases.DeleteLease(connection, serverLeases.FirstOrDefault(c => c.MacAddress.Equals(device.Mac)));
                }

                // Si la mac del Usuario no se encuentra en el la Tabla ARP, lo añado
                if (arps.Any(c => c.MacAddress.Equals(device.Mac)))
                {
                    var userArp = arps.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));

                    connection.Delete(userArp);

                }
                else
                {
                    var userArp = new IpArp() { Address = newIp, Comment = device.Comment, Interface = "ether1", MacAddress = device.Mac };

                    connection.Save(userArp);
                }

                // Modifico el AdressList con el nuevo Ip
                var al = new AddressList();
                var adressLists = new AddressList().GetAddressList(connection);

                if (adressLists.Any(c => c.Address.Equals(device.Ip)))
                {
                    var adressList = adressLists.FirstOrDefault(c => c.Address.Equals(device.Ip));

                    adressList.Address = newIp;

                    connection.Save(adressList);
                    userOperation.Device.Ip = newIp;
                    userOperation.Device.PortalDeOrigen = adressList.List;
                    return userOperation;
                }
                if (adressLists.Any(c => c.Address.Equals(newIp)))
                {
                    var userAddressList = adressLists.FirstOrDefault(c => c.Address.Equals(newIp));
                    userOperation.Device.Ip = newIp;
                    userOperation.Device.PortalDeOrigen = userAddressList.List;
                    return userOperation;

                }
                else
                {
                    userOperation.Device.PortalDeOrigen = new AddressList().SetAddressList(userOperation.Device, connection);
                    return userOperation;
                }

            }
            catch (Exception e)
            {
                userOperation.Validation.Failed(new UserValidationErrors() { Code = "", Description = e.Message });

                return userOperation;
            }


        }

        /// <summary>
        /// Cambia el Nombre del Usuario en el Mikrotik
        /// </summary>
        /// <param name="clients">Cliente</param>
        /// <param name="mikrotikCredentials"></param>
        private void UpdateDeviceNameInDevices(UserDevice device, MikrotikCredentials mikrotikCredentials)
        {
            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                var addressLists = new AddressList().GetAddressList(connection);
                if (device.TipoIp == IpTypeEnum.Fija)
                {
                    var arps = new ARP().GetAllArp(connection);

                    if (arps.Any(c => c.MacAddress.Equals(device.Mac)))
                    {
                        var arp = arps.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));
                        arp.Comment = device.Comment;
                        connection.Save(arp);
                    }

                }
                else
                {
                    var dhcpleases = new DHCPServer().GetServerLeases(connection);

                    if (dhcpleases.Any(c => c.Address.Equals(device.Ip)))
                    {
                        var dhcplease = dhcpleases.FirstOrDefault(c => c.Address.Equals(device.Ip));
                        dhcplease.Comment = device.Comment;
                        connection.Save(dhcplease);
                    }
                }

                if (addressLists.Any(c => c.Address.Equals(device.Ip)))
                {
                    var addressList = addressLists.FirstOrDefault(c => c.Address.Equals(device.Ip));

                    addressList.Comment = device.Comment;

                    connection.Save(addressList);
                }
            }

        }

        /// <summary>
        /// Cambia el IP del usuario a dinámica, agragándolo al DHCP Server y eliminándolo de la Tabla ARP
        /// </summary>
        /// <param name="userOperations">Objeto que contiene el <see cref="DeviceOperations"/> para almacenar la operación.</param>
        /// <param name="dhcpServer"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        private DeviceOperations ChangeDeviceToDynamicIp(UserDevice device, string dhcpServer, ITikConnection connection)
        {
            DeviceOperations userOperations = new DeviceOperations();
            userOperations.Device = device;
            var dhcp = new DHCPServer();
            var arp = new ARP();
            var arps = arp.GetAllArp(connection);
            var al = new AddressList();
            var adressLists = al.GetAddressList(connection).ToList();

            if (arps.Any(c => c.MacAddress.Equals(userOperations.Device.Mac)))
            {
                arp.Delete(connection, arps.FirstOrDefault(c => c.MacAddress.Equals(userOperations.Device.Mac)));
            }
            if (dhcp.GetServerLeases(connection).Any(c => c.MacAddress.Equals(userOperations.Device.Mac)))
            {
                var userLease = dhcp.GetServerLeases(connection).FirstOrDefault(c => c.MacAddress.Equals(userOperations.Device.Mac));

                if (userLease.Dynamic)
                {
                    userLease.MakeStatic(connection);

                }
                if (userLease.Disabled)
                {
                    userLease.Disabled = false;
                    connection.Save(userLease);
                }

                if (adressLists.Any(c => c.Address.Equals(userLease.Address)))
                {
                    userOperations.Device.PortalDeOrigen = adressLists.FirstOrDefault(c => c.Address.Equals(userLease.Address)).List;
                    userOperations.Device.Ip = userLease.Address;
                    userOperations.Device.DhcpServer = dhcpServer;
                }
                else if (adressLists.Any(c => c.Address.Equals(userOperations.Device.Ip)))
                {
                    var userAddressList = adressLists.FirstOrDefault(c => c.Address.Equals(userOperations.Device.Ip));

                    userAddressList.Address = userLease.Address;
                    userOperations.Device.PortalDeOrigen = userAddressList.List;

                    connection.Save(userAddressList);
                }
                else
                {
                    userOperations.Device.Ip = userLease.Address;
                    userOperations.Device.DhcpServer = userLease.ActiveServer;
                    userOperations.Device.PortalDeOrigen = new AddressList().SetAddressList(userOperations.Device, connection);
                }

                return userOperations;
            }
            else
            {
                var assignedIp = dhcp.AssignIpToDevice(userOperations.Device.Mac, dhcpServer, userOperations.Device.Comment, connection);
                if (assignedIp.Assigned)
                {
                    // Modifico el AdressList con el nuevo Ip


                    if (adressLists.Any(c => c.Address.Equals(userOperations.Device.Ip)))
                    {
                        var adressList = adressLists.FirstOrDefault(c => c.Address.Equals(userOperations.Device.Ip));

                        adressList.Address = assignedIp.Ip;

                        connection.Save(adressList);
                    }
                    else
                    {
                        userOperations.Device.Ip = assignedIp.Ip;
                        userOperations.Device.PortalDeOrigen = new AddressList().SetAddressList(userOperations.Device, connection);
                    }
                    return userOperations;
                }
                else
                {
                    userOperations.Validation.Failed(new UserValidationErrors() { Code = "", Description = "No se ha podido asignar un Ip al Usuario." });
                    return userOperations;
                }
            }

        }
        #endregion

        /// <summary>
        /// Añade un dispositivo del cliente al Mikrotik
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="connection"></param>
        /// <param name="mikrotikCredentials"></param>
        /// <returns></returns>
        public DeviceOperations AddDeviceToMikrotik(UserDevice device, MikrotikCredentials mikrotikCredentials)
        {
            DeviceOperations deviceValidation = new DeviceOperations();
            deviceValidation.Device = device;

            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                try
                {
                    var arp = new ARP();
                    var firewall = new AddressList();
                    var adressLists = firewall.GetAddressList(connection);
                    if (device.TipoIp == IpTypeEnum.Fija)
                    {
                        var arps = arp.GetAllArp(connection);

                        if (arps.Any(c => c.Address.Equals(device.Ip) && c.MacAddress.Equals(device.Mac) && adressLists.Any(d => d.Address.Equals(device.Ip))))
                        {
                            deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "UserExist", Description = "El dispositivo ya existe en el dispositivo." });
                            return deviceValidation;

                        }
                        else
                        {
                            if (!arps.Any(c => c.MacAddress.Equals(device.Mac)))
                            {
                                var ipArp = new IpArpModel() { Address = device.Ip, Interface = "ether1", MacAddress = device.Mac, Comment = device.Comment };
                                arp.Add(connection, ipArp);
                            }
                            else
                            {
                                var dArp = arps.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));

                                if (!dArp.Address.Equals(device.Ip))
                                {
                                    dArp.Address = device.Ip;
                                }
                            }

                            var addresslist = new FirewallAddressList() { Address = device.Ip, Comment = device.Comment, Disabled = false, List = device.PortalDeOrigen };
                            new Mikrotik.Firewall().AddressList.Add(addresslist, connection);
                            return deviceValidation;
                        }

                    }
                    else
                    {
                        var dhcpServer = new DHCPServer();
                        var leases = dhcpServer.GetServerLeases(connection);
                        if (!leases.Any(c => c.MacAddress.Equals(device.Mac)))
                        {
                            var assignedIp = new DHCPServer().AssignIpAndAdressListToUser(device.Mac, device.DhcpServer, device.Comment, device.PortalDeOrigen, connection);
                            if (!assignedIp.Assigned)
                            {
                                deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "DHCP", Description = "No existe Ip disponible para asiganar al dispositivo." });
                                return deviceValidation;
                            }
                            else
                            {
                                deviceValidation.Device.Ip = assignedIp.Ip;
                                return deviceValidation;
                            }

                        }
                        else
                        {
                            var userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));
                            deviceValidation.Device.Ip = userLease.Address;

                            if (userLease.Dynamic)
                            {
                                userLease.MakeStatic(connection);
                            }

                            if (!adressLists.Any(c => c.Address.Equals(userLease.Address)))
                            {
                                var addressList = new FirewallAddressList()
                                {
                                    Address = userLease.Address,
                                    Comment = device.Comment,
                                    Disabled = false,
                                    List = device.PortalDeOrigen
                                };

                                connection.Save(addressList);
                            }
                            userLease = dhcpServer.GetServerLeases(connection).FirstOrDefault(c => c.MacAddress.Equals(device.Mac));
                            userLease.Comment = device.Comment;
                            connection.Save(userLease);

                        }

                        return deviceValidation;
                    }
                }
                catch (Exception e)
                {
                    deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "AddUser", Description = e.Message });
                    return deviceValidation;
                }
            }




        }

        /// <summary>
        /// Añade un dispositivo al DHCP Server del Mikrotik
        /// </summary>
        /// <param name="device"></param>
        /// <param name="mikrotikCredentials"></param>
        /// <returns></returns>
        public DeviceOperations AddDeviceIp(UserDevice device, MikrotikCredentials mikrotikCredentials)
        {
            DeviceOperations deviceValidation = new DeviceOperations();
            deviceValidation.Device = device;

            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                try
                {
                    var arp = new ARP();

                    if (device.TipoIp == IpTypeEnum.Fija)
                    {
                        var arps = arp.GetAllArp(connection);

                        if (arps.Any(c => c.Address.Equals(device.Ip) && c.MacAddress.Equals(device.Mac)))
                        {
                            deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "UserExist", Description = "El dispositivo ya existe en el dispositivo." });
                            return deviceValidation;

                        }
                        else
                        {
                            if (!arps.Any(c => c.MacAddress.Equals(device.Mac)))
                            {
                                var ipArp = new IpArpModel() { Address = device.Ip, Interface = "ether1", MacAddress = device.Mac, Comment = device.Comment };
                                arp.Add(connection, ipArp);
                            }
                            else
                            {
                                var dArp = arps.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));

                                if (!dArp.Address.Equals(device.Ip))
                                {
                                    dArp.Address = device.Ip;
                                }
                            }

                            return deviceValidation;
                        }

                    }
                    else
                    {
                        var dhcpServer = new DHCPServer();
                        var leases = dhcpServer.GetServerLeases(connection);
                        if (!leases.Any(c => c.MacAddress.Equals(device.Mac)))
                        {
                            var assignedIp = new DHCPServer().AssignIpToDevice(device.Mac, device.DhcpServer, device.Comment, connection);
                            if (!assignedIp.Assigned)
                            {
                                deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "DHCP", Description = "No existe Ip disponible para asiganar al dispositivo." });
                                return deviceValidation;
                            }
                            else
                            {
                                deviceValidation.Device.Ip = assignedIp.Ip;
                                return deviceValidation;
                            }

                        }
                        else
                        {
                            var userLease = leases.FirstOrDefault(c => c.MacAddress.Equals(device.Mac));
                            deviceValidation.Device.Ip = userLease.Address;

                            if (userLease.Dynamic)
                            {
                                userLease.MakeStatic(connection);
                            }

                            userLease = dhcpServer.GetServerLeases(connection).FirstOrDefault(c => c.MacAddress.Equals(device.Mac));
                            userLease.Comment = device.Comment;
                            connection.Save(userLease);

                        }

                        return deviceValidation;
                    }
                }
                catch (Exception e)
                {
                    deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "AddUser", Description = e.Message });
                    return deviceValidation;
                }
            }




        }

        /// <summary>
        /// Añade un dispositivo del cliente al Address List
        /// </summary>
        /// <param name="device"></param>
        /// <param name="mikrotikCredentials"></param>
        /// <returns></returns>
        public DeviceOperations AddDeviceToFirewallAddressList(UserDevice device, MikrotikCredentials mikrotikCredentials)
        {
            DeviceOperations deviceValidation = new DeviceOperations();
            deviceValidation.Device = device;

            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                try
                {

                    var firewall = new AddressList();
                    var adressLists = firewall.GetAddressList(connection);

                    if (adressLists.Any(d => d.Address.Equals(device.Ip)))
                    {
                        deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "UserExist", Description = "El dispositivo ya existe en el dispositivo." });
                        return deviceValidation;

                    }
                    else
                    {

                        var addresslist = new FirewallAddressList()
                        {
                            Address = device.Ip,
                            Comment = device.Comment,
                            Disabled = false,
                            List = device.PortalDeOrigen
                        };
                        connection.Save(addresslist);
                        return deviceValidation;
                    }



                }
                catch (Exception e)
                {
                    deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "AddUser", Description = e.Message });
                    return deviceValidation;
                }
            }




        }

        /// <summary>
        /// Da acceso a un dispositivo a la red Nacional
        /// </summary>
        /// <param name="device"></param>
        /// <param name="mikrotikCredentials"></param>
        /// <returns></returns>
        public DeviceOperations ToNationalNetwork(DeviceToNationalNetwork device, MikrotikCredentials mikrotikCredentials)
        {
            DeviceOperations deviceValidation = new DeviceOperations();
            deviceValidation.DeviceToNationalNetwork = device;

            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                try
                {

                    var firewall = new AddressList();
                    var adressLists = firewall.GetAddressList(connection);
                    var nat = new Nat();
                    var nats = nat.GetAllNats(connection);
                    var mangle = new Mangle();
                    var mangles = mangle.GetAllMangle(connection);
                    var route = new Routes();
                    var routes = route.GetRoutes(connection);
                    var mainIp = device.PublicIp.GetNetwork();

                    var ips = connection.LoadAll<IpAddress>();
                    var nacionalInterface = ips.FirstOrDefault(c => c.Address.Contains(mainIp));

                    // Firewall Address List
                    //if (!adressLists.Any(c => c.Address.Equals(device.UserDevice.Ip)))
                    //{
                    //    var userAddress = new FirewallAddressList()
                    //    {
                    //        Address = device.UserDevice.Ip,
                    //        Comment = device.UserDevice.Comment,
                    //        List = device.UserDevice.Comment + "JC"
                    //    };

                    //    connection.Save(userAddress);
                    //}

                    //Mangle
                    if (!mangles.Any(c => c.SrcAddress.Equals(device.UserDevice.Ip)))
                    {
                        var deviceMangle = new FirewallMangle()
                        {
                            Action = FirewallMangle.ActionType.MarkRouting,
                            Chain = "prerouting",
                            NewRoutingMark = device.UserDevice.Comment + "_JC",
                            Passthrough = false,
                            //SrcAddressList = device.UserDevice.Comment + "JC",
                            SrcAddress = device.UserDevice.Ip,
                            SrcMacAddress = device.UserDevice.Mac,
                            Comment = $"::::::::::::::::::::::: Conexión {device.UserDevice.Comment} JC::::::::::::::::::::::::"
                        };
                        connection.Save(deviceMangle);
                    }

                    // Nat
                    if (!nats.Any(c => c.ToAddresses.Equals(device.PublicIp)))
                    {
                        var firewallNat = new FirewallNat()
                        {
                            Action = "src-nat",
                            Chain = "srcnat",
                            Comment = device.UserDevice.Comment,
                            RoutingMark = device.UserDevice.Comment + "_JC",
                            ToAddresses = device.PublicIp,
                            OutInterface = nacionalInterface.Interface
                        };
                        connection.Save(firewallNat);
                    }
                    var mask = nacionalInterface.Address.Split("/");

                    if (!ips.Any(c => c.Address.Equals($"{device.PublicIp}/{mask[1]}")))
                    {


                        if (mask.Length == 2)
                        {
                            var userIpAddress = new IpAddress()
                            {
                                Address = $"{device.PublicIp}/{mask[1]}",
                                Interface = nacionalInterface.Interface,
                                Network = nacionalInterface.Network,

                                Comment = $"Ip_{device.UserDevice.Comment}"
                            };
                            connection.Save(userIpAddress);
                        }
                        else
                        {
                            var userIpAddress = new IpAddress()
                            {
                                Address = $"{device.PublicIp}",
                                Interface = nacionalInterface.Interface,
                                Network = nacionalInterface.Network,

                                Comment = $"Ip_{device.UserDevice.Comment}"
                            };
                            connection.Save(userIpAddress);

                        }

                    }

                    //route
                    if (!routes.Any(c => c.MarkRoute.Equals(device.UserDevice.Comment + "_JC")))
                    {
                        List<IpRoutes> rutas = new List<IpRoutes>()
                        {
                            new IpRoutes()
                            {
                                 DstAddress = "10.83.0.0/16",
                            Gateway = $"{mainIp}.1%{nacionalInterface.Interface}",
                            MarkRoute = device.UserDevice.Comment + "_JC",
                            },
                            new IpRoutes()
                            {
                                 DstAddress = "10.86.0.0/16",
                            Gateway = $"{mainIp}.1%{nacionalInterface.Interface}",
                            MarkRoute = device.UserDevice.Comment + "_JC",
                            },   new IpRoutes()
                            {
                                 DstAddress = "10.96.0.0/16",
                            Gateway = $"{mainIp}.1%{nacionalInterface.Interface}",
                            MarkRoute = device.UserDevice.Comment + "_JC",
                            },   new IpRoutes()
                            {
                                 DstAddress = "10.97.0.0/16",
                            Gateway = $"{mainIp}.1%{nacionalInterface.Interface}",
                            MarkRoute = device.UserDevice.Comment + "_JC",
                            },
                        };

                        foreach (var item in rutas)
                        {
                            connection.Save(item);
                        }


                    }
                    return deviceValidation;
                }
                catch (Exception e)
                {
                    deviceValidation.Validation.Failed(new UserValidationErrors() { Code = "AddUser", Description = e.Message });
                    return deviceValidation;
                }
            }




        }

        /// <summary>
        /// Actualiza un dispositivo 
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="mikrotikCredential"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDeviceAsync(UserDevice device, MikrotikCredentials mikrotikCredential, UbiquitiCredentials ubiquitiCredential, int random)
        {
            ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password);
            try
            {


                // Ip´s del DHCP Server
                var leases = new DHCPServer().GetServerLeases(connection);

                // Portal donde se debe encontrar el usuario
                var salidaUser = new Mikrotik.Wireless().GetPortalGivenIp(device.Ip, connection);

                SshConnection sshConnection = new SshConnection();

                ScpConnection scpConnection = new ScpConnection();
                Login login = new Login()
                {
                    Ip = ubiquitiCredential.Dispositivo.Ip,
                    User = ubiquitiCredential.User,
                    Password = ubiquitiCredential.Password,
                    Port = 22
                };
                List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                await Task.WhenAll(tasks);



                UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();

                // Obtengo el usuario en el dispositivo donde se encuentra
                var userInDevice = await ubiquitiACLManager.GetDeviceByMacAsync(scpConnection, device.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                if (userInDevice != null)
                {
                    if (!userInDevice.Mac.Equals(device.Mac))
                    {
                        var newUser = new UbiquitiMac() { Comment = device.Comment, Mac = device.Mac, Status = StatusEnum.Habilitado };
                        await ubiquitiACLManager.UpdateDeviceAsync(scpConnection, sshConnection, userInDevice, newUser, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                    }

                }

                var firewall = new Mikrotik.Firewall();
                // Si el usuario se encuentra en un portal
                if (salidaUser != string.Empty)
                {


                    // Compruebo que el usuario no haya sido cambiado de portal por el administrador
                    if (salidaUser != device.PortalDeOrigen)
                    {

                        var oldAddressList = firewall.AddressList.GetAddressList(connection).FirstOrDefault(c => c.Address.Contains(device.Ip));
                        oldAddressList.List = device.PortalDeOrigen;

                        firewall.AddressList.UpdateAddressList(oldAddressList, connection);
                    }
                }
                // EL usuario no se encuentra en algun adrressList
                else
                {
                    var addressList = new FirewallAddressViewModel() { Address = device.Ip, Usuario = device.Comment, List = device.PortalDeOrigen };
                    firewall.AddressList.CreateAddressList(addressList, connection);
                }
                connection.Dispose();
                return true;
            }
            catch (Exception)
            {
                if (connection != null)
                {
                    connection.Dispose();
                }

                return false;
            }

        }

        /// <summary>
        /// Elimina un usuario de un dispositivo Ubiquiti
        /// </summary>
        /// <param name="userMac"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDeviceFromUbiquiti(string userMac, UbiquitiCredentials ubiquitiCredential, int random)
        {
            try
            {
                SshConnection sshConnection = new SshConnection();

                ScpConnection scpConnection = new ScpConnection();
                Login login = new Login()
                {
                    Ip = ubiquitiCredential.Dispositivo.Ip,
                    User = ubiquitiCredential.User,
                    Password = ubiquitiCredential.Password,
                    Port = 22
                };
                List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                await Task.WhenAll(tasks);

                UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();


                if (await ubiquitiACLManager.FindDevice(scpConnection, userMac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random)))
                {
                    var userInDevice = await ubiquitiACLManager.GetDeviceByMacAsync(scpConnection, userMac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                    await ubiquitiACLManager.DeleteDeviceAsync(userInDevice, sshConnection, scpConnection, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));


                }

                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }

        /// <summary>
        /// Elimina un usuario de un dispositivo Ubiquiti
        /// </summary>
        /// <param name="userMac"></param>
        /// <param name="sshConnection"></param>
        /// <param name="scpConnection"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDeviceFromUbiquiti(string userMac, SshConnection sshConnection, ScpConnection scpConnection, int random)
        {
            try
            {
                UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();


                if (await ubiquitiACLManager.FindDevice(scpConnection, userMac, FileName.ConstructFileName(userMac, random)))
                {
                    var userInDevice = await ubiquitiACLManager.GetDeviceByMacAsync(scpConnection, userMac, FileName.ConstructFileName(userMac, random));
                    await ubiquitiACLManager.DeleteDeviceAsync(userInDevice, sshConnection, scpConnection, FileName.ConstructFileName(userMac, random));
                    return true;

                }
                return false;
            }
            catch (Exception)
            {

                return false;
            }

        }

        /// <summary>
        /// Áñade el usuario a un Dispositivo Ubiquiti
        /// </summary>
        /// <param name="userMac"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <returns></returns>
        public async Task<UserValidation> AddDeviceToUbiquitiAsync(UserDevice device, UbiquitiCredentials ubiquitiCredential, int random)
        {
            UserValidation userValidation = new UserValidation();
            try
            {
                SshConnection sshConnection = new SshConnection();

                ScpConnection scpConnection = new ScpConnection();
                Login login = new Login()
                {
                    Ip = ubiquitiCredential.Dispositivo.Ip,
                    User = ubiquitiCredential.User,
                    Password = ubiquitiCredential.Password,
                    Port = 22
                };
                List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                await Task.WhenAll(tasks);

                UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();



                if (!await ubiquitiACLManager.FindDevice(scpConnection, device.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random)))
                {

                    return await ubiquitiACLManager.AddDeviceAsync(device, scpConnection, sshConnection, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));


                }
                userValidation.Failed(new UserValidationErrors() { Description = "El usuario ya se encuentra en el dispositivo." });
                return userValidation;
            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Description = e.Message });
                return userValidation;
            }

        }

        /// <summary>
        /// Áñade un dispositivo a un Dispositivo Ubiquiti y al Mikrotik para su acceso a Internet
        /// </summary>
        /// <param name="userMac"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <returns></returns>
        public async Task<DeviceOperations> AddDeviceToUbiquitiAndMikrotik(UserDevice device, UbiquitiCredentials ubiquitiCredential, MikrotikCredentials mikrotikCredential, int random)
        {

            try
            {
                SshConnection sshConnection = new SshConnection();

                ScpConnection scpConnection = new ScpConnection();
                Login login = new Login()
                {
                    Ip = ubiquitiCredential.Dispositivo.Ip,
                    User = ubiquitiCredential.User,
                    Password = ubiquitiCredential.Password,
                    Port = 22
                };
                List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                await Task.WhenAll(tasks);

                UbiquitiACLManager ubiquitiACLManager = new UbiquitiACLManager();



                if (!await ubiquitiACLManager.FindDevice(scpConnection, device.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random)))
                {

                    //Añado el Usuario al Mikrotik
                    var userValidation = AddDeviceToMikrotik(device, mikrotikCredential);

                    if (userValidation.Validation.Succeeded)
                    {
                        var aclValidation = await ubiquitiACLManager.AddDeviceAsync(device, scpConnection, sshConnection, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                        if (aclValidation.Succeeded)
                        {
                            return userValidation;
                        }
                        else
                        {
                            await
                              ubiquitiACLManager.DeleteDeviceAsync(device, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                            return userValidation;
                        }
                    }
                    else
                    {
                        // Si el usuario ya se encuentra en el mikrotik pero no en el dispositivo ubiquit, lo añado al dispositivo ubiquiti.
                        // Le muestro al usuario el error, y sugiero que utilice la función de Importar desde el Mikrotik.
                        if (userValidation.Validation.Errors.FirstOrDefault().Code.Equals("UserExist"))
                        {
                            var aclValidation = await ubiquitiACLManager.AddDeviceAsync(device, scpConnection, sshConnection, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                        }

                        return userValidation;
                    }
                }
                else
                {
                    var userInUbiquiti = await ubiquitiACLManager.GetDeviceByMacAsync(scpConnection, device.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                    if (userInUbiquiti.Status == StatusEnum.Deshabilitado)
                    {
                        await ubiquitiACLManager.EnableUserAsync(scpConnection, sshConnection, device.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                    }

                    var userValidation = AddDeviceToMikrotik(device, mikrotikCredential);

                    if (userValidation.Validation.Succeeded)
                    {
                        return userValidation;
                    }
                    else
                    {
                        await
                              ubiquitiACLManager.DeleteDeviceAsync(device, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));

                        return userValidation;
                    }
                }
                // userValidation.Failed(new UserValidationErrors() { Code = nameof(AddUserToUbiquiti), Description = "El usuario ya se encuentra en el dispositivo." });
                //return userValidation;
            }
            catch (Exception e)
            {
                var userValidation = new DeviceOperations();
                userValidation.Validation.Failed(new UserValidationErrors() { Description = e.Message });
                return userValidation;
            }

        }

        /// <summary>
        /// Cambia el Nombre del Usuario en el dispositivo donde se encuentre y en el Mikrotik
        /// </summary>
        /// <param name="client"></param>
        /// <param name="ubiquitiCredentials"></param>
        /// <param name="random"></param>
        /// <param name="mikrotikCredentials"></param>
        public async Task UpdateDeviceNameInDevices(UserDevice device, UbiquitiCredentials ubiquitiCredentials, int random, MikrotikCredentials mikrotikCredentials)
        {
            if (device.DeviceConnectionType == DeviceConecctionType.Wifi)
            {
                switch (device.ApplicationUser.Dispositivo.Fabricante)
                {


                    case DeviceTypeEnum.Ubiquiti:
                        SshConnection sshConnection = new SshConnection();

                        ScpConnection scpConnection = new ScpConnection();
                        Login login = new Login()
                        {
                            Ip = ubiquitiCredentials.Dispositivo.Ip,
                            User = ubiquitiCredentials.User,
                            Password = ubiquitiCredentials.Password,
                            Port = 22
                        };
                        List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                        await Task.WhenAll(tasks);

                        var ubiquitiManagement = new UbiquitiACLManager();
                        var user = await ubiquitiManagement.GetDeviceByMacAsync(scpConnection, device.Mac, FileName.ConstructFileName(device.Mac, random));
                        if (user != null)
                        {

                            await ubiquitiManagement.UpdateDeviceAsync(scpConnection, sshConnection, user, new UbiquitiMac() { Comment = device.Comment, Id = user.Id, Mac = device.Mac, Status = user.Status }, FileName.ConstructFileName(device.ApplicationUser.Dispositivo.Mac, random));
                        }

                        break;
                    case DeviceTypeEnum.Mikrotik:
                        break;
                    case DeviceTypeEnum.TPLink:
                        break;
                    case DeviceTypeEnum.Otros:
                        break;
                    default:
                        break;
                }
            }
            UpdateDeviceNameInDevices(device, mikrotikCredentials);

        }

        /// <summary>
        /// Actualiza el ip de un dispositivo
        /// </summary>
        /// <param name="device"></param>
        /// <param name="deviceIp"></param>
        /// <param name="mikrotikCredential"></param>
        /// <returns></returns>
        public DeviceOperations UpdateDeviceIp(UserDevice device, DeviceViewModel deviceIp, MikrotikCredentials mikrotikCredential)
        {
            DeviceOperations userOperation = new DeviceOperations();

            DHCPServer dHCPServer = new DHCPServer();

            using (ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {

                if (deviceIp.TipoIp != device.TipoIp)
                {

                    try
                    {

                        if (deviceIp.TipoIp == IpTypeEnum.Automatica)
                        {
                            if (dHCPServer.AvailableIp(connection, deviceIp.DhcpServer))
                            {
                                return ChangeDeviceToDynamicIp(device, deviceIp.DhcpServer, connection);
                            }
                            else
                            {
                                userOperation.Validation.Failed(new UserValidationErrors() { Code = "NoIp", Description = "No hay Ip disponible en el servidor DHCP seleccionado." });
                            }


                        }
                        else
                        {
                            //ChangeOrUpdateClientToStaticIp(u.Mac, u.GetUsername, u.Ip, clientIpViewModel.Ip, mikrotikCredentials);
                            return userOperation;
                        }



                    }
                    catch (Exception e)
                    {
                        userOperation.Validation.Failed(new UserValidationErrors() { Code = "", Description = e.Message });
                        return userOperation;
                    }

                }
                if (deviceIp.ActualizarIpAutomática)
                {
                    if (device.TipoIp == IpTypeEnum.Automatica)
                    {
                        return ChangeDeviceToDynamicIp(device, deviceIp.DhcpServer, connection);
                    }
                }
                if (device.TipoIp == IpTypeEnum.Fija)
                {
                    if (!device.Ip.Equals(deviceIp.Ip))
                    {
                        return ChangeOrUpdateDeviceToStaticIp(device, deviceIp.Ip, connection);
                    }
                }
            }
            return userOperation;
        }

        /// <summary>
        /// Cambia el dispositvo del cliente
        /// </summary>
        /// <param name="oldMac"></param>
        /// <param name="newMac"></param>
        /// <param name="mikrotikCredential"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <returns></returns>
        public bool ChangeClientDeviceStaticIp(string oldMac, string newMac, MikrotikCredentials mikrotikCredential)
        {


            ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password);
            try
            {


                // Ip´s del DHCP Server

                var arp = new ARP();


                if (arp.GetAllArp(connection).Any(c => c.MacAddress.Equals(oldMac)))
                {
                    var userArp = arp.GetAllArp(connection).FirstOrDefault(c => c.MacAddress.Equals(oldMac));
                    arp.Delete(connection, userArp);
                    arp.Add(connection, new IpArpModel() { Address = userArp.Address, Comment = userArp.Comment, Interface = userArp.Interface, MacAddress = newMac });
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Deshabilita un usuario en todos los dispositivos
        /// </summary>
        /// <param name="client">Cliente a Deshabilitar</param>
        /// <param name="ubiquitiCredential">Credenciales del dispositivo Ubiquiti</param>
        /// <param name="mikrotikCredential">Credenciales del dispositivo Mikrotik</param>
        /// <param name="random"></param>
        /// <returns></returns>
        public async Task DisableDeviceAsync(List<UserDevice> devices, Dispositivo dispositivo, UbiquitiCredentials ubiquitiCredential, MikrotikCredentials mikrotikCredential, int random)
        {
            try
            {
                if (devices.Any(c => c.DeviceConnectionType == DeviceConecctionType.Wifi))
                {

                    if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti && dispositivo.WirelessMode == WirelessMode.Ap && (dispositivo.WirelessSecurity == WirelessSecurity.ACL || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WPA))
                    {
                        if (ubiquitiCredential != null)
                        {
                            SshConnection sshConnection = new SshConnection();
                            ScpConnection scpConnection = new ScpConnection();
                            Login login = new Login()
                            {
                                Ip = ubiquitiCredential.Dispositivo.Ip,
                                User = ubiquitiCredential.User,
                                Password = ubiquitiCredential.Password,
                                Port = 22
                            };
                            List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                            await Task.WhenAll(tasks);
                            var ubiquitiManager = new UbiquitiACLManager();
                            foreach (var item in devices)
                            {
                                if (item.DeviceConnectionType == DeviceConecctionType.Wifi)
                                {

                                    var userInDevice = await ubiquitiManager.GetDeviceByMacAsync(scpConnection, item.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                                    if (userInDevice != null)
                                    {
                                        if (userInDevice.Status != StatusEnum.Deshabilitado)
                                        {
                                            await ubiquitiManager.DisableUserAsync(scpConnection, sshConnection, item.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                                        }

                                    }
                                }
                            }
                        }
                    }


                }
                using (ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                {
                    var arp = new ARP();
                    var firewall = new AddressList();
                    var leases = new DHCPServer();
                    var deviceLeases = leases.GetServerLeases(connection);
                    var arps = arp.GetAllArp(connection);
                    var firewallAdressList = firewall.GetAddressList(connection);
                    foreach (var item in devices)
                    {
                        if (deviceLeases.Any(c => c.MacAddress.Contains(item.Mac)))
                        {
                            var userLease = deviceLeases.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                            leases.DisableLease(connection, userLease);
                        }
                        if (arps.Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var userArp = arps.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                            try
                            {
                                arp.Disable(connection, userArp);
                            }
                            catch (Exception)
                            {


                            }

                        }
                        if (firewallAdressList.Any(c => c.Address.Equals(item.Ip)))
                        {
                            var userAddressList = firewallAdressList.FirstOrDefault(c => c.Address.Equals(item.Ip));

                            firewall.Disable(userAddressList, connection);
                        }

                    }
                }

            }
            catch (Exception)
            {

            }

        }


        /// <summary>
        /// Habilita un dispositivo del usuario en todos los dispositivos
        /// </summary>
        /// <param name="client">Cliente a habilitar</param>
        /// <param name="ubiquitiCredential">Credenciales del Dispositivo Ubiquiti</param>
        /// <param name="mikrotikCredential">Credenciales del Dispositivo Mikrotik</param>
        /// <param name="random"></param>
        /// <returns></returns>
        public async Task EnableDeviceAsync(List<UserDevice> devices, Dispositivo dispositivo, UbiquitiCredentials ubiquitiCredential, MikrotikCredentials mikrotikCredential, int random)
        {
            try
            {
                if (devices.Any(c => c.DeviceConnectionType == DeviceConecctionType.Wifi))
                {

                    if (dispositivo.Fabricante == DeviceTypeEnum.Ubiquiti && dispositivo.WirelessMode == WirelessMode.Ap && (dispositivo.WirelessSecurity == WirelessSecurity.ACL || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivo.WirelessSecurity == WirelessSecurity.ACL_WPA))
                    {
                        if (ubiquitiCredential != null)
                        {
                            SshConnection sshConnection = new SshConnection();
                            ScpConnection scpConnection = new ScpConnection();
                            Login login = new Login()
                            {
                                Ip = ubiquitiCredential.Dispositivo.Ip,
                                User = ubiquitiCredential.User,
                                Password = ubiquitiCredential.Password,
                                Port = 22
                            };
                            List<Task> tasks = new List<Task>() { sshConnection.Initialize(login), scpConnection.Initialize(login) };

                            await Task.WhenAll(tasks);
                            var ubiquitiManager = new UbiquitiACLManager();
                            foreach (var item in devices)
                            {
                                if (item.DeviceConnectionType == DeviceConecctionType.Wifi)
                                {

                                    var userInDevice = await ubiquitiManager.GetDeviceByMacAsync(scpConnection, item.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                                    if (userInDevice != null)
                                    {
                                        if (userInDevice.Status == StatusEnum.Deshabilitado)
                                        {
                                            await ubiquitiManager.EnableUserAsync(scpConnection, sshConnection, item.Mac, FileName.ConstructFileName(ubiquitiCredential.Dispositivo.Mac, random));
                                        }

                                    }
                                }
                            }
                        }
                    }


                }
                using (ITikConnection connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
                {
                    var arp = new ARP();
                    var firewall = new AddressList();
                    var leases = new DHCPServer();
                    var deviceLeases = leases.GetServerLeases(connection);
                    var arps = arp.GetAllArp(connection);
                    var firewallAdressList = firewall.GetAddressList(connection);
                    foreach (var item in devices)
                    {
                        if (deviceLeases.Any(c => c.MacAddress.Contains(item.Mac)))
                        {
                            var userLease = deviceLeases.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                            leases.EnableLease(connection, userLease);
                        }
                        if (arps.Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var userArp = arps.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));

                            try
                            {
                                arp.Enable(connection, userArp);
                            }
                            catch (Exception)
                            {


                            }

                        }
                        if (firewallAdressList.Any(c => c.Address.Equals(item.Ip)))
                        {
                            var userAddressList = firewallAdressList.FirstOrDefault(c => c.Address.Equals(item.Ip));

                            firewall.Enable(userAddressList, connection);
                        }

                    }
                }

            }
            catch (Exception)
            {

            }

        }

        /// <summary>
        /// Obtiene la información de un dispositivo para su importación al sistema
        /// </summary>
        /// <param name="mikrotikCredentials">Credenciales del Mikrotik</param>
        /// <param name="deviceConecctionType">Tipo de Conexión del Usuario</param>
        /// <param name="mac">MAC del Usuario</param>
        /// <param name="userId">Dispositivo en el cual está conectado</param>
        /// <param name="pagoId">Clasificador de Pago</param>
        /// <returns><see cref="DeviceOperations"/></returns>
        public DeviceOperations ImportDeviceFromMikrotik(MikrotikCredentials mikrotikCredentials, DeviceConecctionType deviceConecctionType, string userId, string mac = null, string ip = null)
        {
            DeviceOperations importMikrotikUser = new DeviceOperations();

            var dhcpserver = new DHCPServer();
            var arp = new ARP();
            var firewall = new AddressList();

            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredentials.Dispositivo.Firmware.GetApiVersion(), mikrotikCredentials.Dispositivo.Ip, mikrotikCredentials.User, mikrotikCredentials.Password))
            {
                var leases = dhcpserver.GetServerLeases(connection);
                var arps = arp.GetAllArp(connection);
                var addressList = firewall.GetAddressList(connection);
                if (mac != null)
                {
                    if (!leases.Any(c => c.MacAddress.Equals(mac)) && !arps.Any(c => c.MacAddress.Equals(mac)))
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Leases", Description = "La mac del usuario introducida no se encuentra en el Mikrotik." });
                        return importMikrotikUser;
                    }


                    if (leases.Any(c => c.MacAddress.Equals(mac)))
                    {
                        var userLease = leases.FirstOrDefault(c => c.MacAddress.Contains(mac));
                        importMikrotikUser.Device.Ip = userLease.Address;

                        importMikrotikUser.Device.TipoIp = IpTypeEnum.Automatica;

                        if (userLease.Comment != string.Empty)
                        {
                            importMikrotikUser.Device.Comment = userLease.Comment;
                        }
                    }

                    else
                    {
                        if (arps.Any(c => c.MacAddress.Contains(mac)))
                        {
                            var userArp = arps.FirstOrDefault(c => c.MacAddress.Contains(mac));
                            importMikrotikUser.Device.Ip = userArp.Address;
                            importMikrotikUser.Device.TipoIp = IpTypeEnum.Fija;

                            if (userArp.Comment != string.Empty)
                            {
                                importMikrotikUser.Device.Comment = userArp.Comment;
                            }

                        }
                    }
                    if (addressList.Any(c => c.Address.Equals(importMikrotikUser.Device.Ip)))
                    {
                        var userAdressList = addressList.FirstOrDefault(c => c.Address.Equals(importMikrotikUser.Device.Ip));
                        importMikrotikUser.Device.PortalDeOrigen = userAdressList.List;

                        if (userAdressList.Comment != string.Empty)
                        {
                            if (importMikrotikUser.Device.Comment == null)
                            {
                                importMikrotikUser.Device.Comment = userAdressList.Comment;
                            }
                        }
                    }
                    else
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Adress List", Description = "El dispositivo no posee ningún adress list." });
                        return importMikrotikUser;
                    }

                    if (importMikrotikUser.Device.Comment.Equals(string.Empty))
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "NullUsername", Description = "No se ha podido resolver el nombre del dispositivo." });
                        return importMikrotikUser;
                    }
                    importMikrotikUser.Device.DeviceConnectionType = deviceConecctionType;
                    importMikrotikUser.Device.Mac = mac;
                    importMikrotikUser.Device.ApplicationUserId = userId;

                    return importMikrotikUser;
                }
                if (ip != null)
                {
                    if (!leases.Any(c => c.Address.Equals(ip)) && !arps.Any(c => c.Address.Equals(ip)))
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Leases", Description = "La IP introducida no se encuentra en el Mikrotik." });
                        return importMikrotikUser;
                    }


                    if (leases.Any(c => c.Address.Equals(ip)))
                    {
                        var userLease = leases.FirstOrDefault(c => c.Address.Contains(ip));
                        importMikrotikUser.Device.Mac = userLease.MacAddress;

                        importMikrotikUser.Device.TipoIp = IpTypeEnum.Automatica;

                        if (userLease.Comment != string.Empty)
                        {
                            importMikrotikUser.Device.Comment = userLease.Comment;
                        }
                    }

                    else
                    {
                        if (arps.Any(c => c.Address.Contains(ip)))
                        {
                            var userArp = arps.FirstOrDefault(c => c.Address.Contains(ip));
                            importMikrotikUser.Device.Mac = userArp.MacAddress;
                            importMikrotikUser.Device.TipoIp = IpTypeEnum.Fija;

                            if (userArp.Comment != string.Empty)
                            {
                                importMikrotikUser.Device.Comment = userArp.Comment;
                            }

                        }
                    }
                    if (addressList.Any(c => c.Address.Equals(ip)))
                    {
                        var userAdressList = addressList.FirstOrDefault(c => c.Address.Equals(ip));
                        importMikrotikUser.Device.PortalDeOrigen = userAdressList.List;

                        if (userAdressList.Comment != string.Empty)
                        {
                            if (importMikrotikUser.Device.Comment == null)
                            {
                                importMikrotikUser.Device.Comment = userAdressList.Comment;
                            }
                        }
                    }
                    else
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Adress List", Description = "El usuario no posee ningún adress list." });
                        return importMikrotikUser;
                    }

                    if (importMikrotikUser.Device.Comment.Equals(string.Empty))
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "NullUsername", Description = "No se ha podido resolver el nombre del dispositivo." });
                        return importMikrotikUser;
                    }
                    importMikrotikUser.Device.DeviceConnectionType = deviceConecctionType;
                    importMikrotikUser.Device.Ip = ip;
                    importMikrotikUser.Device.ApplicationUserId = userId;

                    return importMikrotikUser;
                }
                importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Null", Description = "No se ha proporcionado ni la MAC ni la IP." });
                return importMikrotikUser;
            }
        }

        /// <summary>
        /// Obtiene la información de un dispositivo para su importación al sistema
        /// </summary>
        /// <param name="mikrotikEtecsa"></param>
        /// <param name="adminDevice"></param>
        /// <param name="deviceConecctionType"></param>
        /// <param name="mac"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DeviceOperations ImportDeviceFromMikrotik(MikrotikCredentials mikrotikEtecsa, MikrotikCredentials adminDevice, DeviceConecctionType deviceConecctionType, string userId, string mac = null, string ip = null)
        {
            DeviceOperations importMikrotikUser = new DeviceOperations();

            var dhcpserver = new DHCPServer();
            var arp = new ARP();
            var firewall = new AddressList();

            using (var connection = ConnectionFactory.OpenConnection(mikrotikEtecsa.Dispositivo.Firmware.GetApiVersion(), mikrotikEtecsa.Dispositivo.Ip, mikrotikEtecsa.User, mikrotikEtecsa.Password))
            {
                var leases = dhcpserver.GetServerLeases(adminDevice);
                var arps = arp.GetAllArp(adminDevice);
                var addressList = firewall.GetAddressList(connection);

                if (mac != null)
                {
                    if (!leases.Any(c => c.MacAddress.Equals(mac)) && !arps.Any(c => c.MacAddress.Equals(mac)))
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Leases", Description = "La mac del usuario introducida no se encuentra en el Mikrotik." });
                        return importMikrotikUser;
                    }


                    if (leases.Any(c => c.MacAddress.Equals(mac)))
                    {
                        var userLease = leases.FirstOrDefault(c => c.MacAddress.Contains(mac));
                        importMikrotikUser.Device.Ip = userLease.Address;
                        importMikrotikUser.Device.DhcpServer = userLease.Server;
                        importMikrotikUser.Device.TipoIp = IpTypeEnum.Automatica;

                        if (userLease.Comment != string.Empty)
                        {
                            importMikrotikUser.Device.Comment = userLease.Comment;
                        }
                    }

                    else
                    {
                        if (arps.Any(c => c.MacAddress.Contains(mac)))
                        {
                            var userArp = arps.Where(c => c.MacAddress.Contains(mac));

                            if (userArp.Count() > 1)
                            {
                                foreach (var item in userArp)
                                {
                                    if (addressList.Any(c => c.Address.Equals(item.Address)))
                                    {
                                        importMikrotikUser.Device.Ip = item.Address;
                                        importMikrotikUser.Device.TipoIp = IpTypeEnum.Fija;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                importMikrotikUser.Device.Ip = userArp.ElementAt(0).Address;
                                importMikrotikUser.Device.TipoIp = IpTypeEnum.Fija;

                                if (userArp.ElementAt(0).Comment != string.Empty)
                                {
                                    importMikrotikUser.Device.Comment = userArp.ElementAt(0).Comment;
                                }
                            }


                        }
                    }
                    if (addressList.Any(c => c.Address.Equals(importMikrotikUser.Device.Ip)))
                    {
                        var userAdressList = addressList.FirstOrDefault(c => c.Address.Equals(importMikrotikUser.Device.Ip));
                        importMikrotikUser.Device.PortalDeOrigen = userAdressList.List;

                        if (userAdressList.Comment != string.Empty)
                        {
                            if (importMikrotikUser.Device.Comment == null)
                            {
                                importMikrotikUser.Device.Comment = userAdressList.Comment;
                            }
                        }
                    }
                    else
                    {

                        importMikrotikUser.Device.PortalDeOrigen = "NoInternetAccess";
                    }

                    if (importMikrotikUser.Device.Comment != null)
                    {
                        if (importMikrotikUser.Device.Comment.Equals(string.Empty))
                        {
                            importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "NullUsername", Description = "No se ha podido resolver el nombre del dispositivo." });
                            return importMikrotikUser;
                        }
                    }
                    else
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "NullUsername", Description = "No se ha podido resolver el nombre del dispositivo." });
                        return importMikrotikUser;
                    }

                    importMikrotikUser.Device.DeviceConnectionType = deviceConecctionType;
                    importMikrotikUser.Device.Mac = mac;
                    importMikrotikUser.Device.ApplicationUserId = userId;

                    return importMikrotikUser;
                }
                if (ip != null)
                {

                    if (!leases.Any(c => c.Address.Equals(ip)) && !arps.Any(c => c.Address.Equals(ip)))
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Leases", Description = "La mac del usuario introducida no se encuentra en el Mikrotik." });
                        return importMikrotikUser;
                    }


                    if (leases.Any(c => c.Address.Equals(ip)))
                    {
                        var userLease = leases.FirstOrDefault(c => c.Address.Contains(ip));
                        importMikrotikUser.Device.Mac = userLease.MacAddress;
                        importMikrotikUser.Device.DhcpServer = userLease.Server;
                        importMikrotikUser.Device.TipoIp = IpTypeEnum.Automatica;

                        if (userLease.Comment != string.Empty)
                        {
                            importMikrotikUser.Device.Comment = userLease.Comment;
                        }
                    }

                    else
                    {
                        if (arps.Any(c => c.Address.Contains(ip)))
                        {
                            var userArp = arps.Where(c => c.Address.Contains(ip));

                            if (userArp.Count() > 1)
                            {
                                foreach (var item in userArp)
                                {
                                    if (addressList.Any(c => c.Address.Equals(item.Address)))
                                    {
                                        importMikrotikUser.Device.Mac = item.MacAddress;
                                        importMikrotikUser.Device.TipoIp = IpTypeEnum.Fija;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                importMikrotikUser.Device.Mac = userArp.ElementAt(0).MacAddress;
                                importMikrotikUser.Device.TipoIp = IpTypeEnum.Fija;

                                if (userArp.ElementAt(0).Comment != string.Empty)
                                {
                                    importMikrotikUser.Device.Comment = userArp.ElementAt(0).Comment;
                                }
                            }


                        }
                    }
                    if (addressList.Any(c => c.Address.Equals(ip)))
                    {
                        var userAdressList = addressList.FirstOrDefault(c => c.Address.Equals(ip));
                        importMikrotikUser.Device.PortalDeOrigen = userAdressList.List;

                        if (userAdressList.Comment != string.Empty)
                        {
                            if (importMikrotikUser.Device.Comment == null)
                            {
                                importMikrotikUser.Device.Comment = userAdressList.Comment;
                            }
                        }
                    }
                    else
                    {

                        importMikrotikUser.Device.PortalDeOrigen = "NoInternetAccess";
                    }

                    if (importMikrotikUser.Device.Comment != null)
                    {
                        if (importMikrotikUser.Device.Comment.Equals(string.Empty))
                        {
                            importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "NullUsername", Description = "No se ha podido resolver el nombre del dispositivo." });
                            return importMikrotikUser;
                        }
                    }
                    else
                    {
                        importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "NullUsername", Description = "No se ha podido resolver el nombre del dispositivo." });
                        return importMikrotikUser;
                    }

                    importMikrotikUser.Device.DeviceConnectionType = deviceConecctionType;
                    importMikrotikUser.Device.Ip = ip;
                    importMikrotikUser.Device.ApplicationUserId = userId;

                    return importMikrotikUser;
                }
                importMikrotikUser.Validation.Failed(new UserValidationErrors() { Code = "Null", Description = "No se ha proporcionado ni la MAC ni la IP." });
                return importMikrotikUser;
            }
        }

        /// <summary>
        /// Cambia los dispositivos de un usuario hacia otro dispositivo
        /// </summary>
        /// <param name="devices">Dispositivos a Cambiar</param>
        /// <param name="credentialOldDevice">Credenciales del Dispositivo a eliminar</param>
        /// <param name="credentialNewDevice">Credenciales del Dispositivo a agregar</param>
        /// <returns></returns>
        public async Task<UserValidation> ChangeUserDeviceToAnotherDevice(List<UserDevice> devices, Dispositivo dispositivoOrigen, Dispositivo dispositivoDestino, int random)
        {
            UserValidation userValidation = new UserValidation();
            if (dispositivoDestino.Fabricante == DeviceTypeEnum.Ubiquiti && (dispositivoDestino.Platform.Contains("M2") || dispositivoDestino.Platform.Contains("M5")))
            {
                if (dispositivoDestino.WirelessSecurity == WirelessSecurity.ACL || dispositivoDestino.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivoDestino.WirelessSecurity == WirelessSecurity.ACL_WPA)
                {
                    if (devices.Any(c => c.DeviceConnectionType == DeviceConecctionType.Wifi))
                    {

                        var credentialDispositivoDestino = dispositivoDestino.DeviceCredential.FirstOrDefault() as UbiquitiCredentials;

                        if (credentialDispositivoDestino != null)
                        {
                            try
                            {


                                SshConnection sshConnectionNewDevice = new SshConnection();

                                ScpConnection scpConnectionNewDevice = new ScpConnection();
                                Login login = new Login()
                                {
                                    Ip = credentialDispositivoDestino.Dispositivo.Ip,
                                    User = credentialDispositivoDestino.User,
                                    Password = credentialDispositivoDestino.Password,
                                    Port = 22
                                };
                                List<Task> tasks = new List<Task>() { sshConnectionNewDevice.Initialize(login), scpConnectionNewDevice.Initialize(login) };

                                await Task.WhenAll(tasks);

                                var um = new UbiquitiACLManager();

                                foreach (var item in devices)
                                {
                                    if (item.DeviceConnectionType == DeviceConecctionType.Wifi)
                                    {

                                        if (!await um.FindDevice(scpConnectionNewDevice, item.Mac, FileName.ConstructFileName(credentialDispositivoDestino.Dispositivo.Mac, random)))
                                        {
                                            await um.AddDeviceAsync(item, scpConnectionNewDevice, sshConnectionNewDevice, FileName.ConstructFileName(credentialDispositivoDestino.Dispositivo.Mac, random));

                                            return userValidation;

                                        }
                                    }
                                }


                            }
                            catch (Exception e)
                            {
                                userValidation.Failed(new UserValidationErrors() { Code = "Exception", Description = e.Message });
                            }
                        }
                        else
                        {
                            userValidation.Failed(new UserValidationErrors() { Code = "UbiquitiCredentials", Description = $"El dispositivo {dispositivoDestino.Ip} no posee credenciales." });
                        }

                    }
                }


            }
            if (dispositivoOrigen.Fabricante == DeviceTypeEnum.Ubiquiti && (dispositivoOrigen.Platform.Contains("M2") || dispositivoOrigen.Platform.Contains("M5")))
            {
                if (dispositivoDestino.WirelessSecurity == WirelessSecurity.ACL || dispositivoDestino.WirelessSecurity == WirelessSecurity.ACL_WEP || dispositivoDestino.WirelessSecurity == WirelessSecurity.ACL_WPA)
                {
                    if (devices.Any(c => c.DeviceConnectionType == DeviceConecctionType.Wifi))
                    {
                        var credentialOrigen = dispositivoOrigen.DeviceCredential.FirstOrDefault() as UbiquitiCredentials;

                        if (credentialOrigen != null)
                        {
                            try
                            {
                                SshConnection sshConnectionOldDevice = new SshConnection();

                                ScpConnection scpConnectionOldDevice = new ScpConnection();
                                Login login = new Login()
                                {
                                    Ip = credentialOrigen.Dispositivo.Ip,
                                    User = credentialOrigen.User,
                                    Password = credentialOrigen.Password,
                                    Port = 22
                                };
                                List<Task> tasks = new List<Task>() { sshConnectionOldDevice.Initialize(login), scpConnectionOldDevice.Initialize(login), };

                                await Task.WhenAll(tasks);

                                var um = new UbiquitiACLManager();

                                foreach (var item in devices)
                                {
                                    if (item.DeviceConnectionType == DeviceConecctionType.Wifi)
                                    {

                                        if (await um.FindDevice(scpConnectionOldDevice, item.Mac, FileName.ConstructFileName(credentialOrigen.Dispositivo.Mac, random)))
                                        {
                                            await um.DeleteDeviceAsync(item, sshConnectionOldDevice, scpConnectionOldDevice, FileName.ConstructFileName(credentialOrigen.Dispositivo.Mac, random));

                                            return userValidation;

                                        }

                                    }


                                }
                            }
                            catch (Exception e)
                            {

                                userValidation.Failed(new UserValidationErrors() { Code = "Exception", Description = e.Message });
                            }

                        }
                    }
                }
            }

            return userValidation;

        }

        /// <summary>
        /// Obtiene una lista de los usuarios dada el ACL del equipo Ubiquiti.
        /// </summary>
        /// <param name="usuariosEnBD">Usuarios en la Base de Datos</param>
        /// <param name="dispositivos">Dispositivos</param>
        /// <param name="macList"></param>
        /// <param name="credential"></param>
        /// <param name="dispositivoId"></param>
        /// <returns></returns>
        internal List<UserDevice> GetDevices(List<UserDevice> savedDevices, List<Dispositivo> dispositivos, List<UbiquitiMac> macList, MikrotikCredentials credential)
        {
            var leases = new DHCPServer().GetServerLeases(credential);
            var arp = new ARP().GetAllArp(credential);
            var addressList = new AddressList();
            List<UserDevice> userDevices = new List<UserDevice>();
            foreach (var item in macList.Where(c => c.Status == StatusEnum.Habilitado))
            {
                if (!savedDevices.Any(c => c.Mac.Equals(item.Mac)))
                {
                    // Analizo si la mac es un dipositivo, para despues analizar si este esta como estacion o como ap
                    if (dispositivos.Any(c => c.Mac.Equals(item.Mac)))
                    {
                        if (arp.Any(c => c.MacAddress.Equals(item.Mac)) || leases.Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var dispositivo = dispositivos.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                            string ip = string.Empty;
                            string portal = string.Empty;
                            DhcpServerLease lease = null;
                            UserDevice userDevice = new UserDevice();
                            if (leases.Any(c => c.MacAddress.Equals(item.Mac)))
                            {
                                lease = leases.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                                ip = lease.Address;
                            }
                            else
                            {
                                ip = arp.FirstOrDefault(c => c.MacAddress.Equals(item.Mac)).Address;
                            }

                            if (addressList.GetAddressList(credential).Any(c => c.Address.Equals(ip)))
                            {
                                portal = addressList.GetAddressList(credential).FirstOrDefault(c => c.Address.Equals(ip)).List;

                                userDevice = CreateUserDevice(item, credential, portal, lease, ip);

                            }
                            else
                            {
                                userDevice = CreateUserDevice(item, credential, null, lease, ip);
                            }

                            userDevices.Add(userDevice);
                        }

                    }
                    else
                    {
                        if (arp.Any(c => c.MacAddress.Equals(item.Mac)) || leases.Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var dispositivo = dispositivos.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                            string ip = string.Empty;
                            string portal = string.Empty;
                            DhcpServerLease lease = null;
                            UserDevice userDevice = new UserDevice();
                            if (leases.Any(c => c.MacAddress.Equals(item.Mac)))
                            {
                                lease = leases.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                                ip = lease.Address;
                            }
                            else
                            {
                                ip = arp.FirstOrDefault(c => c.MacAddress.Equals(item.Mac)).Address;
                            }

                            if (addressList.GetAddressList(credential).Any(c => c.Address.Equals(ip)))
                            {
                                portal = addressList.GetAddressList(credential).FirstOrDefault(c => c.Address.Equals(ip)).List;

                                userDevice = CreateUserDevice(item, credential, portal, lease, ip);

                            }
                            else
                            {
                                userDevice = CreateUserDevice(item, credential, null, lease, ip);
                            }

                            userDevices.Add(userDevice);

                        }

                    }
                }
            }
            return userDevices;
        }

        /// <summary>
        /// Obtiene una lista de los usuarios dada el ACL del equipo Ubiquiti.
        /// </summary>
        /// <param name="savedDevices"></param>
        /// <param name="dispositivos"></param>
        /// <param name="macList"></param>
        /// <param name="mikrotikEtecsa">Mikrotik de acceso a Internet</param>
        /// <param name="adminDevice">Dispositivo Administrador</param>
        /// <returns></returns>
        internal List<UserDevice> GetDevices(List<UserDevice> savedDevices, List<Dispositivo> dispositivos, List<UbiquitiMac> macList, MikrotikCredentials mikrotikEtecsa, MikrotikCredentials adminDevice)
        {
            var leases = new DHCPServer().GetServerLeases(adminDevice);
            var arp = new ARP().GetAllArp(adminDevice);
            var addressList = new AddressList();
            List<UserDevice> userDevices = new List<UserDevice>();
            foreach (var item in macList.Where(c => c.Status == StatusEnum.Habilitado))
            {
                if (!savedDevices.Any(c => c.Mac.Equals(item.Mac)))
                {
                    // Analizo si la mac es un dipositivo, para despues analizar si este esta como estacion o como ap
                    if (dispositivos.Any(c => c.Mac.Equals(item.Mac)))
                    {
                        if (arp.Any(c => c.MacAddress.Equals(item.Mac)) || leases.Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var dispositivo = dispositivos.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                            string ip = string.Empty;
                            string portal = string.Empty;
                            DhcpServerLease lease = null;
                            UserDevice userDevice = new UserDevice();
                            if (leases.Any(c => c.MacAddress.Equals(item.Mac)))
                            {
                                lease = leases.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                                ip = lease.Address;
                            }
                            else
                            {
                                ip = arp.FirstOrDefault(c => c.MacAddress.Equals(item.Mac)).Address;
                            }

                            if (addressList.GetAddressList(mikrotikEtecsa).Any(c => c.Address.Equals(ip)))
                            {
                                portal = addressList.GetAddressList(mikrotikEtecsa).FirstOrDefault(c => c.Address.Equals(ip)).List;

                                userDevice = CreateUserDevice(item, mikrotikEtecsa, portal, lease, ip);

                            }
                            else
                            {
                                userDevice = CreateUserDevice(item, mikrotikEtecsa, null, lease, ip);
                            }

                            userDevices.Add(userDevice);
                        }

                    }
                    else
                    {
                        if (arp.Any(c => c.MacAddress.Equals(item.Mac)) || leases.Any(c => c.MacAddress.Equals(item.Mac)))
                        {
                            var dispositivo = dispositivos.FirstOrDefault(c => c.Mac.Equals(item.Mac));

                            string ip = string.Empty;
                            string portal = string.Empty;
                            DhcpServerLease lease = null;
                            UserDevice userDevice = new UserDevice();
                            if (leases.Any(c => c.MacAddress.Equals(item.Mac)))
                            {
                                lease = leases.FirstOrDefault(c => c.MacAddress.Equals(item.Mac));
                                ip = lease.Address;
                            }
                            else
                            {
                                ip = arp.FirstOrDefault(c => c.MacAddress.Equals(item.Mac)).Address;
                            }

                            if (addressList.GetAddressList(mikrotikEtecsa).Any(c => c.Address.Equals(ip)))
                            {
                                portal = addressList.GetAddressList(mikrotikEtecsa).FirstOrDefault(c => c.Address.Equals(ip)).List;

                                userDevice = CreateUserDevice(item, mikrotikEtecsa, portal, lease, ip);

                            }
                            else
                            {
                                userDevice = CreateUserDevice(item, mikrotikEtecsa, null, lease, ip);
                            }

                            userDevices.Add(userDevice);

                        }

                    }
                }
            }
            return userDevices;
        }


        /// <summary>
        /// Elimina un usuario del AddressList y el DHCP Server o la tabla ARP
        /// </summary>
        /// <param name="userMac"></param>
        /// <param name="ubiquitiCredential"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public void DeleteDeviceFromMikrotik(string userMac, string userIp, MikrotikCredentials mikrotikCredential)
        {
            using (var connection = ConnectionFactory.OpenConnection(mikrotikCredential.Dispositivo.Firmware.GetApiVersion(), mikrotikCredential.Dispositivo.Ip, mikrotikCredential.User, mikrotikCredential.Password))
            {
                var dhcpServer = new DHCPServer();
                var leases = dhcpServer.GetServerLeases(connection);


                if (leases.Any(c => c.MacAddress.Equals(userMac)))
                {
                    dhcpServer.DeleteLease(connection, leases.FirstOrDefault(c => c.MacAddress.Equals(userMac)));
                }

                var firewall = new Mikrotik.Firewall();
                var adressList = firewall.AddressList.GetAddressList(connection);

                if (adressList.Any(c => c.Address.Equals(userIp)))
                {
                    firewall.AddressList.DeleteAddressList(adressList.FirstOrDefault(c => c.Address.Equals(userIp)), connection);
                }

                var arp = new ARP();
                var arps = arp.GetAllArp(connection);

                if (arps.Any(c => c.MacAddress.Equals(userMac)))
                {
                    arp.Delete(connection, arps.FirstOrDefault(c => c.MacAddress.Equals(userMac)));

                }
            }

        }

        /// <summary>
        /// Cambia un dispositivo hacia otro portal
        /// </summary>
        /// <param name="dispositivo">Usuario a ser cambiado.</param>
        /// <param name="credential">Credenciales del Mikrotik</param>
        /// <param name="portal">Portal donde se va a mover el usuario</param>
        /// <returns></returns>
        internal UserValidation ChangeDeviceToAnotherPortal(UserDevice dispositivo, MikrotikCredentials credential, string portal)
        {

            UserValidation userValidation = new UserValidation();
            try
            {
                using (ITikConnection conection = ConnectionFactory.OpenConnection(credential.Dispositivo.Firmware.GetApiVersion(), credential.Dispositivo.Ip, credential.User, credential.Password))
                {


                    var allAddressList = conection.LoadAll<FirewallAddressList>().ToList();
                    if (dispositivo.PortalDeOrigen == null)
                    {
                        if (allAddressList.Any(c => c.Address.Equals(dispositivo.Ip)))
                        {
                            var userAdressList = allAddressList.FirstOrDefault(c => c.Address.Equals(dispositivo.Ip));

                            if (!userAdressList.List.Equals(portal))
                            {
                                userAdressList.List = portal;
                                conection.Save(userAdressList);
                                return userValidation;
                            }

                        }
                        else
                        {
                            new AddressList().Add(new FirewallAddressList() { Address = dispositivo.Ip, List = portal, Comment = dispositivo.Comment, Disabled = false }, conection);
                        }
                        return userValidation;
                    }

                    if (allAddressList.Any(d => d.Address.Equals(dispositivo.Ip) && d.List.Equals(dispositivo.PortalDeOrigen)))
                    {
                        var addressList = allAddressList.FirstOrDefault(d => d.Address.Equals(dispositivo.Ip) && d.List.Equals(dispositivo.PortalDeOrigen));

                        addressList.List = portal;

                        conection.Save(addressList);

                        return userValidation;

                    }

                    // Si el usuario ya no se encuentra en el portal donde la app lo tiene.
                    else
                    {
                        var addressLists = allAddressList.Where(c => c.Address.Equals(dispositivo.Ip)).ToList();

                        if (addressLists.Count() == 0)
                        {
                            FirewallAddressList firewall = new FirewallAddressList()
                            {
                                Address = dispositivo.Ip,
                                List = portal,
                                Comment = dispositivo.Comment
                            };

                            conection.Save(firewall);
                            return userValidation;
                        }

                        if (addressLists.Count() == 1)
                        {
                            if (!addressLists[0].List.Equals(portal))
                            {
                                addressLists[0].List = portal;

                                conection.Save(addressLists[0]);
                            }
                            return userValidation;
                        }
                        else
                        {
                            userValidation.Failed(new UserValidationErrors() { Code = "AddressList", Description = $"Se ha encontrado que existen {addressLists.Count()} usuarios con el mismo IP en el dispositivo." });
                            return userValidation;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Code = "ChangeUserToAnotherPortal", Description = e.Message });
                return userValidation;
            }

        }

        /// <summary>
        /// Cambia un dispositivo hacia otro portal
        /// </summary>
        /// <param name="dispositivo"></param>
        /// <param name="connection"></param>
        /// <param name="portal"></param>
        /// <returns></returns>
        internal UserValidation ChangeDeviceToAnotherPortal(UserDevice dispositivo, ITikConnection connection, string portal)
        {

            UserValidation userValidation = new UserValidation();
            try
            {
                var allAddressList = connection.LoadAll<FirewallAddressList>().ToList();
                if (dispositivo.PortalDeOrigen == null)
                {
                    if (allAddressList.Any(c => c.Address.Equals(dispositivo.Ip)))
                    {
                        var userAdressList = allAddressList.FirstOrDefault(c => c.Address.Equals(dispositivo.Ip));

                        if (!userAdressList.List.Equals(portal))
                        {
                            userAdressList.List = portal;
                            connection.Save(userAdressList);
                            return userValidation;
                        }

                    }
                    else
                    {
                        new AddressList().Add(new FirewallAddressList() { Address = dispositivo.Ip, List = portal, Comment = dispositivo.Comment, Disabled = false }, connection);
                    }
                    return userValidation;
                }

                if (allAddressList.Any(d => d.Address.Equals(dispositivo.Ip) && d.List.Equals(dispositivo.PortalDeOrigen)))
                {
                    var addressList = allAddressList.FirstOrDefault(d => d.Address.Equals(dispositivo.Ip) && d.List.Equals(dispositivo.PortalDeOrigen));

                    addressList.List = portal;

                    connection.Save(addressList);

                    return userValidation;

                }

                // Si el usuario ya no se encuentra en el portal donde la app lo tiene.
                else
                {
                    var addressLists = allAddressList.Where(c => c.Address.Equals(dispositivo.Ip)).ToList();

                    if (addressLists.Count() == 0)
                    {
                        FirewallAddressList firewall = new FirewallAddressList()
                        {
                            Address = dispositivo.Ip,
                            List = portal,
                            Comment = dispositivo.Comment
                        };

                        connection.Save(firewall);
                        return userValidation;
                    }

                    if (addressLists.Count() == 1)
                    {
                        if (!addressLists[0].List.Equals(portal))
                        {
                            addressLists[0].List = portal;

                            connection.Save(addressLists[0]);
                        }
                        return userValidation;
                    }
                    else
                    {
                        userValidation.Failed(new UserValidationErrors() { Code = "AddressList", Description = $"Se ha encontrado que existen {addressLists.Count()} usuarios con el mismo IP en el dispositivo." });
                        return userValidation;
                    }
                }

            }
            catch (Exception e)
            {
                userValidation.Failed(new UserValidationErrors() { Code = "ChangeUserToAnotherPortal", Description = e.Message });
                return userValidation;
            }

        }



    }
}
