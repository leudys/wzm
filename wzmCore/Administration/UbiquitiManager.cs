﻿using SshCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UbiquitiCore.Discovery;
using wzmCore.Ubiquiti;
using wzmData.Models;
using wzmData.Ssh;

namespace wzmCore.Administration
{
    public class UbiquitiManager
    {
        public UbiquitiManager()
        {
            ScpConnection = new ScpConnection();
            SshConnection = new SshConnection();
            UbiquitiACLManager = new UbiquitiACLManager();
            UbiquitiWirelessManager = new UbiquitiWirelessManager();
        }

        public ScpConnection ScpConnection { get; private set; }

        public SshConnection SshConnection { get; private set; }

        public UbiquitiACLManager UbiquitiACLManager { get; private set; }

        public UbiquitiWirelessManager UbiquitiWirelessManager { get; private set; }

        /// <summary>
        /// Comprueba si se puede realizar una conexión al dispositivo
        /// </summary>
        /// <param name="credenciales">Credenciales</param>
        /// <param name="devices">Dispositivos encontrados en la red</param>
        /// <param name="dispositivo">Dispositivo al cual se comprobará la conexion</param>
        /// <param name="dbContext">Base de Datos para salvar los cambios.</param>
        /// <returns></returns>
        public async Task<bool> CanConnectToUbiquitiDevice(List<DeviceCredentials> credenciales, List<Device> devices, Dispositivo dispositivo, ApplicationDbContext dbContext)
        {
            if (credenciales.Any(c => c.Dispositivo.Id == dispositivo.Id))
            {
                var dc = credenciales.FirstOrDefault(c => c.Dispositivo.Id == dispositivo.Id) as UbiquitiCredentials;
                Login login = new Login()
                {
                    Ip = dc.Dispositivo.Ip,
                    User = dc.User,
                    Password = dc.Password,
                    Port = 22
                };
                try
                {
                    await SshConnection.Initialize(login);
                    return true;
                }
                catch (Exception)
                {

                    if (devices.Any(c => c.FormatedMacAddress.Equals(dispositivo.Mac)))
                    {
                        var d = devices.FirstOrDefault(c => c.FormatedMacAddress.Equals(dispositivo.Mac));

                        if (!d.IpAddress.Equals(dispositivo.Ip))
                        {
                            dc.Dispositivo.Ip = d.IpAddress;

                            dbContext.UbiquitiCredentials.Update(dc);
                            dbContext.SaveChanges();

                            try
                            {
                                await SshConnection.Initialize(login);

                                return true;
                            }
                            catch (Exception)
                            {

                                return false;
                            }

                        }
                    }
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> CanConnectToUbiquitiDevice(UbiquitiCredentials ubiquitiCredential)
        {
            try
            {
                Login login = new Login()
                {
                    Ip = ubiquitiCredential.Dispositivo.Ip,
                    User = ubiquitiCredential.User,
                    Password = ubiquitiCredential.Password,
                    Port = 22
                };
                await SshConnection.Initialize(login);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
