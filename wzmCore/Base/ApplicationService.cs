﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wzmCore.Interfaces;
using wzmCore.Services.System;
using wzmData.Enum;
using wzmData.Models;

namespace wzmCore.Base
{
    public class ApplicationService : MikrotikBaseConnection, IJob
    {

        private readonly IWzmCore _wzmCore;
        internal ILogger<object> Logger { get; private set; }
        protected IServiceProvider ServiceProvider;
        internal List<PortalesInternet> PortalesEtecsa { get; private set; }
        private List<PortalException> _portalException;
        private EtecsaInternetPortalsJob InternetJob;
        

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="wzmCore"></param>
        /// <param name="logger"></param>
        /// <param name="serviceProvider">Proveedor de Base de Datos</param>
        public ApplicationService(IWzmCore wzmCore, ILogger<object> logger, IServiceProvider serviceProvider)
        {
            _wzmCore = wzmCore;
            Logger = logger;
            ServiceProvider = serviceProvider;
            PortalesEtecsa = new List<PortalesInternet>();
            _portalException = new List<PortalException>();
        }

        public ApplicationService(IWzmCore wzmCore, ILogger<object> logger, IServiceProvider serviceProvider, EtecsaInternetPortalsJob internetJob)
        {
            _wzmCore = wzmCore;
            Logger = logger;
            ServiceProvider = serviceProvider;
            PortalesEtecsa = new List<PortalesInternet>();
            _portalException = new List<PortalException>();
            InternetJob = internetJob;
        }

        public List<PortalesInternet> Internet
        {
            get
            {
                if (InternetJob != null)
                {
                    return InternetJob.Portales.OrderBy(c => c.Portal).ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public ApplicationService(IWzmCore wzmCore, ILogger<object> logger)
        {
            _wzmCore = wzmCore;
            Logger = logger;
        }

        public virtual async Task Execute(IJobExecutionContext context)
        {
            if (IsLicenseValid())
            {
                try
                {
                    var serviceScopeFactory = ServiceProvider.GetRequiredService<IServiceScopeFactory>();
                    var scope = serviceScopeFactory.CreateScope();
                    ApplicationDbContext = new ApplicationDbContext(scope.ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
                    scope.Dispose();
                    if (Credential == null)
                    {
                        SetCredential();
                    }
                    else
                    {
                        MikrotikCredentials credential = new MikrotikCredentials();
                        if (VLAN_ETECSA)
                        {
                            if (ApplicationDbContext.AdminDevicesRoles.Any(c => c.DeviceRoleId.Equals(3)))
                            {
                                int id = ApplicationDbContext.AdminDevicesRoles.FirstOrDefault(c => c.DeviceRoleId.Equals(3)).AdminDeviceId;


                                credential = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.DispositivoId.Equals(id));
                                
                            }
                        }
                        else
                        {
                            credential = await ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).FirstOrDefaultAsync(c => c.Selected);
                        }
                                                
                        if (!credential.User.Equals(Credential.User) || !credential.Password.Equals(Credential.Password) || !credential.Dispositivo.Ip.Equals(Credential.Dispositivo.Ip))
                        {
                            DisposeMkConnection();
                            SetCredential();
                        }
                    }
                    Init();


                }
                catch (ArgumentNullException)
                {

                }
                catch (InvalidOperationException) { }
                catch (Exception e)
                {
                    Logger.LogError(e.Message);

                }
            }
            else
            {
                Logger.LogCritical("La licencia no es correcta.");
            }
        }

        public void RefreshDatabaseInstance()
        {
            var serviceScopeFactory = ServiceProvider.GetRequiredService<IServiceScopeFactory>();
            var scope = serviceScopeFactory.CreateScope();
            ApplicationDbContext = new ApplicationDbContext(scope.ServiceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>());
            scope.Dispose();
        }

        public bool IsLicenseValid()
        {
            return _wzmCore.LicenseManager.Validated;
        }

        public void RemovePortalException(ServiceType serviceType)
        {
            try
            {
                if (ApplicationDbContext.PortalExceptions.Any(c => c.ServiceType == serviceType))
                {
                    var e = ApplicationDbContext.PortalExceptions.Where(c => c.ServiceType == serviceType).ToList();

                    if (e.Count() != _portalException.Count())
                    {
                        _portalException = e;
                    }

                    if (_portalException.Count() > 0)
                    {

                        if (Internet?.Count > 0)
                        {
                            PortalesEtecsa = Internet;

                            foreach (var item in e)
                            {
                                if (Internet.Any(c => c.Route.Equals(item.Portal)))
                                {
                                    var es = Internet.FirstOrDefault(c => c.Route.Equals(item.Portal));

                                    PortalesEtecsa.Remove(es);
                                }
                            }
                        }
                        else
                        {
                            PortalesEtecsa = Internet;
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);                
            }
            


        }

        public Enum.LicenseType LicenseType()
        {
            return _wzmCore.LicenseManager.GetLicenseType();
        }
    }
}
