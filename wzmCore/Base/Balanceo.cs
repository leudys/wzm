﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using tik4net.Objects;
using tik4net.Objects.Interface;
using tik4net.Objects.Ip.Firewall;
using wzmCore.Administration;
using wzmCore.Interfaces;
using wzmCore.Mikrotik;
using wzmData.Models;
using wzmCore.Data.Mikrotik;
using static tik4net.Objects.Interface.InterfaceWireless;
using Quartz;
using System.Threading.Tasks;
using wzmCore.Extensions;
using wzmCore.Services.Helpers;
using wzmCore.Services.System;

namespace wzmCore.Base

{
    public class Balanceo : ApplicationService
    {
        public List<string> PrefijoMarcaConexion { get; set; } = new List<string>();
        public string AaddressList = "wzmBalanceo";
        public TikInterface Interfaces { get; set; }
        public Routes Rutas { get; set; }

        public string BalanceoComment { get; set; }
        public Mangle Mangle { get; set; }
        public Nat Nat { get; set; }
        public AddressList FirewallAddresList { get; set; }
        public UserManagement UserManagement;
        public List<HostsServices> Hosts;

        public string MasterInterface { get; set; } = "ether1";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceProvider">Service Provider para crear la instancia de la BD</param>
        /// <param name="logger">Logger</param>
        /// <param name="configuration">Obtener la configuración de la app</param>
        /// <param name="wzmCore"></param>
        public Balanceo(IServiceProvider serviceProvider, ILogger<object> logger, IWzmCore wzmCore, EtecsaInternetPortalsJob internetJob) : base(wzmCore, logger, serviceProvider, internetJob)
        {
            Rutas = new Routes();

            Interfaces = new TikInterface();
            Mangle = new Mangle();
            Nat = new Nat();
            UserManagement = new UserManagement();
            FirewallAddresList = new AddressList();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        /// <param name="wzmCore"></param>
        public Balanceo(ILogger<object> logger, IWzmCore wzmCore) : base(wzmCore, logger)
        {
            Rutas = new Routes();
            Interfaces = new TikInterface();
            Mangle = new Mangle();
            Nat = new Nat();
            UserManagement = new UserManagement();
            FirewallAddresList = new AddressList();
        }


        internal async Task UpdateBalanceoRulesAsync(List<HostsServices> hosts, List<PortalesInternet> portalesEtecsa)
        {
            var portales = GetBalanceoModel(portalesEtecsa);
            var mangles = GetActiveMangleRules();

            if (await ExisteBalanceo(portales, mangles, hosts))
            {
                await AplicarBalanceo(portales, mangles, hosts);
            }
        }


        /// <summary>
        /// Crea las reglas que se aplicarán en el dispositivo
        /// </summary>
        /// <param name="portales"></param>
        /// <returns></returns>
        private List<BalanceoModel> GetBalanceoModel(List<PortalesInternet> portales)
        {
            var pOrdenados = portales.GetOrderedWlan();

            List<BalanceoModel> balanceoModel = new List<BalanceoModel>();
            foreach (var item in pOrdenados)
            {
                //var r = mkRoutes.Where(c => c.Gateway.Split('%')[1].Equals(item.Portal)).FirstOrDefault();
                balanceoModel.Add(new BalanceoModel() { ConnectionMark = "cm_" + item.Portal, Interfaz = item.Portal, Route = item.Route, MarkRoute = "rm_" + item.Portal });
            }

            return balanceoModel;
        }


        /// <summary>
        /// Obtiene los mangles que contengan el comentario wzm_balanceo
        /// </summary>
        /// <returns></returns>
        private List<FirewallMangle> GetActiveMangleRules()
        {

            var mangles = Connection.LoadList<FirewallMangle>(Connection.CreateParameter("comment", BalanceoComment)).ToList();
            if (mangles != null)
            {
                if (mangles.Count() > 0)
                {
                    return mangles;
                }
            }

            return new List<FirewallMangle>();
        }


        /// <summary>
        /// Obtiene todas las Interfaces Wireless
        /// </summary>
        /// <returns></returns>
        private List<InterfaceWireless> GetAllWireless()
        {
            return Interfaces.Wireless.GetAllWireless(Connection).Where(c => !c.Disabled && (c.Mode == WirelessMode.StationPseudobridge || c.Mode == WirelessMode.Station) && c.Name != "wlan1" && c.InterfaceType == "virtual").ToList();
        }

        /// <summary>
        /// Obtiene los nat que se encuentren activos con el comentario de wzm
        /// </summary>
        /// <returns></returns>
        protected bool IsNatActivated()
        {

            if (Nat.GetAllNats(Connection).Any(c => c.Comment.Equals(BalanceoComment) && !c.Disabled))
            {

                return true;
            }

            return false;

        }

        protected void RemoveNat()
        {
            LoggerHelper.Log(Connection, Services.Helpers.LogLevel.Info, "[Balanceador] Eliminando NAT");
            Nat.DeleteNatMultiByComment(Connection, BalanceoComment);
        }

        /// <summary>
        /// Elimina todos los Mangles del Dispositivo que comprendan en el balanceo
        /// </summary>
        /// <param name="mangles"></param>
        protected void RemoveMangles()
        {
            LoggerHelper.Log(Connection, Services.Helpers.LogLevel.Info, "[Balanceador] Eliminando Mangle");
            Mangle.DeleteMangleMultiByComment(Connection, BalanceoComment);

        }


        /// <summary>
        /// Añade los mangles y nat
        /// </summary>
        /// <param name="portales"></param>
        /// <param name="nhilos"></param>
        protected virtual void AddMangleAndNat(List<BalanceoModel> portales, int nhilos)
        {
            int i = 0;
            foreach (var item in portales)
            {
                i++;
                string nth = $"{nhilos},{i}";
                Mangle.AddCustomMangle(Connection,
                  new FirewallMangle()
                  {
                      Chain = "prerouting",
                      SrcAddressList = AaddressList,
                      DstAddressType = "!local",
                      Nth = nth,
                      Action = FirewallMangle.ActionType.MarkConnection,
                      NewConnectionMark = item.ConnectionMark,
                      Disabled = false,
                      Comment = BalanceoComment,
                      Passthrough = true,
                      ConnectionState = "new"
                  });
                Mangle.AddCustomMangle(Connection,
                    new FirewallMangle()
                    {
                        Chain = "prerouting",
                        ConnectionMark = item.ConnectionMark,
                        SrcAddressList = AaddressList,
                        Action = FirewallMangle.ActionType.MarkRouting,
                        NewRoutingMark = item.Route,
                        Passthrough = false,
                        Comment = BalanceoComment,
                        Disabled = false


                    });
                var nat = new FirewallNat()
                {
                    Action = "masquerade",
                    Chain = "srcnat",
                    Comment = BalanceoComment,
                    OutInterface = item.Interfaz,
                    Disabled = false,
                    RoutingMark = item.Route,
                    SrcAddressList = AaddressList
                };
                Nat.AddCustomNat(nat, null, Connection);

            }
        }

        private async Task AplicarBalanceo(List<BalanceoModel> portales, List<FirewallMangle> mangles, List<HostsServices> hosts)
        {
            try
            {
                if (mangles.Count != (portales.Count * 2))
                {

                    if (mangles.Count > 0)
                    {
                        RemoveMangles();
                    }

                    if (IsNatActivated())
                    {
                        RemoveNat();
                    }

                    AddMangleAndNat(portales, portales.Count());
                    Logger.LogInformation($"{DateTime.Now.TimeOfDay} Condiciones para balanceo aplicadas.");
                    Connection.LogWarning($"[Balanceador] {DateTime.Now.TimeOfDay} Condiciones para balanceo aplicadas.");
                    var addresses = FirewallAddresList.GetAddressList(Connection);

                    foreach (var item in hosts)
                    {
                        if (!UserManagement.UserIsBalanced(item.Device.Ip, addresses, AaddressList))
                        {
                            var a = addresses.Where(c => c.Address.Equals(item.Device.Ip)).ToList();
                            if (a.Count > 0)
                            {
                                var ad = a.FirstOrDefault();
                                FirewallAddressViewModel addressList = new FirewallAddressViewModel() { List = AaddressList, Address = ad.Address, Usuario = ad.Comment, Disabled = ad.Disabled, Id = ad.Id };
                                FirewallAddresList.UpdateAddressList(addressList, Connection);
                            }
                            item.IsBalanced = true;
                            item.HasInternet = true;
                            item.PortalActual = AaddressList;
                            await SyncWithDbAsync(item);
                            Logger.LogInformation($"{DateTime.Now.TimeOfDay} Balanceador: Moviendo usuario {item.Device.Comment} a {AaddressList}");
                            Connection.LogWarning($"[Balanceador] {DateTime.Now.TimeOfDay} Balanceador: Moviendo usuario {item.Device.Comment} a {AaddressList}");
                        }
                        else
                        {
                            if (item.IsBalanced == false)
                            {
                                item.IsBalanced = true;
                                await SyncWithDbAsync(item);
                            }

                            if (item.HasInternet == false)
                            {
                                item.HasInternet = true;
                                await SyncWithDbAsync(item);
                            }


                        }

                    }
                }
                else
                {
                    if (!IsNatActivated())
                    {
                        RemoveMangles();
                        AddMangleAndNat(portales, portales.Count());
                    }

                    var addresses = FirewallAddresList.GetAddressList(Connection);
                    foreach (var item in hosts)
                    {
                        if (!UserManagement.UserIsBalanced(item.Device.Ip, addresses, AaddressList))
                        {
                            var ad = addresses.Where(c => c.Address.Equals(item.Device.Ip)).FirstOrDefault();

                            FirewallAddressViewModel addressList = new FirewallAddressViewModel() { List = AaddressList, Address = ad.Address, Usuario = ad.Comment, Disabled = ad.Disabled, Id = ad.Id };
                            FirewallAddresList.UpdateAddressList(addressList, Connection);

                            await SetUserInfoAsync(item);


                        }
                        else
                        {
                            if (!item.IsBalanced)
                            {
                                item.IsBalanced = true;
                                await SyncWithDbAsync(item);
                            }

                            if (!item.HasInternet)
                            {
                                item.HasInternet = true;
                                await SyncWithDbAsync(item);
                            }


                        }

                    }
                }
            }
            catch (Exception e)
            {

                Logger.LogError(e.Message);
            }


        }

        private async Task SetUserInfoAsync(HostsServices item)
        {
            //Desde la API no se puede añadir variables globales
            //var globalVariable = Connection.LoadAll<EnvironmentVariables>();

            //if (!globalVariable.Any(c => c.Name.Equals("balanceo")))
            //{
            //  var command=  Connection.CreateCommand("/global/", Connection.CreateParameter("name", "balanceo"), Connection.CreateParameter("value", "1" /*item.Id.ToString()*/));
            //    command.ExecuteScalar();
            //}
            //else
            //{
            //    var v = globalVariable.FirstOrDefault(c => c.Name.Equals("balanceo"));

            //    if (v.Value.Length > 0)
            //    {
            //        var values = v.Value.Split(';');

            //        if (!values.Any(c => c.Equals(item.Id)))
            //        {
            //            v.Value = v.Value = $",{item.Id}";
            //        }
            //    }
            //    else
            //    {
            //        v.Value = item.Id.ToString();

            //        Connection.CreateCommand("/global/set", Connection.CreateParameter("value", item.Id.ToString())).ExecuteScalar();
            //    }
            //}

            item.IsBalanced = true;
            item.HasInternet = true;
            item.PortalActual = AaddressList;
            await SyncWithDbAsync(item);


        }

        /// <summary>
        /// Elimina los usuarios del balanceo y los devuelve a su portal original
        /// </summary>
        /// <param name="hosts"></param>
        protected async Task RemoveUsersFromBalancing(List<HostsServices> hosts)
        {

            if (hosts.Any(c => c.IsBalanced))
            {

                UserManagement.ReturnToOriginalPortal(true, hosts.Where(c => c.IsBalanced).ToList(), Connection);
                foreach (var item in hosts)
                {
                    Connection.LogWarning($"[Balanceador] Devolviendo dispositivo {item.Device.Comment} a su portal de origen.");
                    item.IsBalanced = false;
                    item.HasInternet = false;
                    item.PortalActual = item.Device.PortalDeOrigen;
                    await SyncWithDbAsync(item);
                }
            }
            else
            {
                var list = FirewallAddresList.GetAddressList(Connection);
                foreach (var item in hosts)
                {
                    if (UserManagement.UserIsBalanced(item.Device.Ip, list, AaddressList))
                    {
                        Connection.LogWarning($"[Balanceador] Devolviendo dispositivo {item.Device.Comment} a su portal de origen.");
                        UserManagement.ReturnToOriginalPortal(item.Device, Connection);
                        item.IsBalanced = false;
                        item.HasInternet = false;
                        item.PortalActual = item.Device.PortalDeOrigen;
                        await SyncWithDbAsync(item);
                    }
                }
            }

        }

        /// <summary>
        /// Chequea que exista balanceo
        /// </summary>
        /// <param name="portales"></param>
        /// <param name="mangles"></param>
        /// <param name="hosts"></param>
        /// <returns></returns>
        internal virtual async Task<bool> ExisteBalanceo(List<BalanceoModel> portales, List<FirewallMangle> mangles, List<HostsServices> hosts)
        {


            if (portales.Count < 2)
            {
                if (mangles.Count > 0)
                {
                    RemoveMangles();
                }
                if (IsNatActivated())
                {
                    RemoveNat();
                }
                await RemoveUsersFromBalancing(hosts);
                return false;
            }
            return true;

        }


        /// <summary>
        /// Sincroniza los cambios realizados con la base de datos
        /// </summary>
        /// <param name="item"></param>
        private async Task SyncWithDbAsync(HostsServices item)
        {
            try
            {
                ApplicationDbContext.Update(item);
                await ApplicationDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
            }

        }

        public override async Task Execute(IJobExecutionContext context)
        {
            await base.Execute(context);
        }


    }

    public class BalanceoModel
    {
        // ej. salida1
        public string Route { get; set; }

        // ej. marcaUno
        public string ConnectionMark { get; set; }

        public string MarkRoute { get; set; }

        public string Interfaz { get; set; }
    }
}
