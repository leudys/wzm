﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using tik4net;
using wzmCore.ExpressionHelp;
using wzmCore.Extensions;
using wzmData.Models;

namespace wzmCore.Base
{
    public abstract class MikrotikBaseConnection : BaseCore
    {

        /// <summary>
        /// Mikrotik Connection Object
        /// </summary>
        protected ITikConnection Connection { get; set; }

        /// <summary>
        /// Make an instance of <see cref="Connection"/>
        /// </summary>
        /// <param name="crendetial"></param>
        protected void Init(MikrotikCredentials crendetial)
        {
            //// Para no tener conexion al Mk innecesarias
            //Dispose();
            EstablishConnection(crendetial);
        }

        /// <summary>
        /// Make an instance of <see cref="Connection"/>
        /// </summary>
        /// <param name="crendetial"></param>
        protected void Init()
        {
            if (Credential != null)
            {
                EstablishConnection();
            }

        }

        /// <summary>
        /// Dispose the object <see cref="Connection"/>
        /// </summary>
        protected void DisposeMkConnection()
        {
            if (Connection.IsOpened)
            {
                Connection.Dispose();
            }

        }

        /// <summary>
        /// Establish the connection to the Mikrotik
        /// </summary>
        /// <param name="crendetial"></param>
        private void EstablishConnection(MikrotikCredentials crendetial)
        {
            // Si la conexión al dispositivo se ha establecido, no vuelve a conectar.
            if (Connection == null)
            {

                Connection = ConnectionFactory.OpenConnection(crendetial.Dispositivo.Firmware.GetApiVersion(), crendetial.Dispositivo.Ip, crendetial.User, crendetial.Password);


            }
            if (!Connection.IsOpened)
            {
                Connection = ConnectionFactory.OpenConnection(crendetial.Dispositivo.Firmware.GetApiVersion(), crendetial.Dispositivo.Ip, crendetial.User, crendetial.Password);
            }

        }

        /// <summary>
        /// Establish the connection to the Mikrotik
        /// </summary>
        private void EstablishConnection()
        {
            // Si la conexión al dispositivo se ha establecido, no vuelve a conectar.
            if (Connection == null)
            {
                Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password);
            }
            if (!Connection.IsOpened)
            {
                Connection = ConnectionFactory.OpenConnection(Credential.Dispositivo.Firmware.GetApiVersion(), Credential.Dispositivo.Ip, Credential.User, Credential.Password);
            }

        }

        /// <summary>
        /// A function that runs async
        /// </summary>
        /// <param name="updatingFlag"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public async Task RunCommand(Expression<Func<bool>> updatingFlag, Func<Task> action)
        {
            if (updatingFlag.GetPropertyValue()) return;
            updatingFlag.SetPropertyValue(true);

            try
            {
                await action();
            }
            finally
            {
                updatingFlag.SetPropertyValue(false);
            }
        }

    }
}
