﻿using System.Threading;


namespace wzmCore.Base
{
    public class BaseService : BaseCore
    {
        public bool Stopped { get; set; }
        public Timer Timer { get; set; }
        public bool Locked { get; set; }

        /// <summary>
        /// Disposes only the Timer
        /// </summary>
        public virtual void DisposeTimer()
        {
            Timer?.Dispose();
        }

        /// <summary>
        /// Dispose all resources
        /// </summary>
        public virtual void DisposeAll()
        {
            ApplicationDbContext?.Dispose();
            Scope?.Dispose();
            Timer?.Dispose();
        }


    }
}
