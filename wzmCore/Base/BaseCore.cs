﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using wzmData.Models;

namespace wzmCore.Base
{
    public abstract class BaseCore : IDisposable
    {
        internal MikrotikCredentials Credential { get; set; }
        internal bool VLAN_ETECSA { get; set; }
        protected ApplicationDbContext ApplicationDbContext { get; set; }

        protected IServiceScope Scope { get; private set; }

        protected IServiceScopeFactory ServiceScopeFactory { get; set; }

        /// <summary>
        /// Crea una instancia con las credenciales
        /// </summary>
        /// <param name="db"></param>
        public virtual bool SetCredential()
        {
            try
            {
                //Rol VLAN
                if (ApplicationDbContext.AdminDevicesRoles.Any(c => c.DeviceRoleId.Equals(3)))
                {
                    int id = ApplicationDbContext.AdminDevicesRoles.FirstOrDefault(c => c.DeviceRoleId.Equals(3)).AdminDeviceId;

                    
                    Credential = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.DispositivoId.Equals(id));
                    VLAN_ETECSA = true;
                }

                else
                {
                    Credential = ApplicationDbContext.MikrotikCredentials.Include(c => c.Dispositivo).First(c => c.Selected == true);
                    VLAN_ETECSA = false;
                }

               
                return true;
            }
            catch (Exception e)
            {

                throw e;
            }


        }

        /// <summary>
        /// Crea una instancia con las credenciales
        /// </summary>
        /// <param name="db"></param>
        protected void SetCredential(MikrotikCredentials credential)
        {

            Credential = credential;

        }


        public virtual void Dispose()
        {
            ApplicationDbContext?.Dispose();

        }
    }
}
