﻿using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.Threading.Tasks;
using wzmData.Models;
using System.Collections.Generic;
using wzmData.Ssh;
using wzmData.Ubiquiti;
using wzmData.Ubiquiti.UbiquitiProperties;

namespace SshCore
{
    public class SshConnection : IDisposable
    {
        private bool _disposed;

        //private VpnServer _vpn;
        private UbiquitiCredentials _credentials;
        //public SshConnection(VpnServer vpn)
        //{
        //    _vpn = vpn;
        //}

        public SshConnection() { }

        public SshConnection(UbiquitiCredentials ubiquitiCredential) { _credentials = ubiquitiCredential; }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _sshClient.Dispose();
                }
            }
            _disposed = true;
        }

        private SshClient _sshClient;

        public ConnectionInfo Info { get; private set; }

        public string Dhcp { get; private set; }

        public string ApInformation { get; private set; }

        public string Config { get; set; }

        public async Task Initialize(Login login)
        {

            Info = new ConnectionInfo(login.Ip, login.Port, login.User, new PasswordAuthenticationMethod(login.User, login.Password))
            {
                Timeout = TimeSpan.FromSeconds(10)
            };
            _sshClient = new SshClient(Info);
            await Task.Run(() => _sshClient.Connect());
        }

        public async Task<bool> InitializeAsync()
        {
            try
            {
                //if (_vpn != null)
                //{
                //    Info = new ConnectionInfo(_vpn.Host, 22, _vpn.User, new PasswordAuthenticationMethod(_vpn.User, _vpn.Password))
                //    {
                //        Timeout = TimeSpan.FromSeconds(10)
                //    };
                //    _sshClient = new SshClient(Info);
                //    await Task.Run(() => _sshClient.Connect());
                //    return _sshClient.IsConnected;
                //}
                if (_credentials != null)
                {
                    Info = new ConnectionInfo(_credentials.Dispositivo.Ip, _credentials.Puerto, _credentials.User, new PasswordAuthenticationMethod(_credentials.User, _credentials.Password))
                    {
                        Timeout = TimeSpan.FromSeconds(10)
                    };
                    _sshClient = new SshClient(Info);
                    await Task.Run(() => _sshClient.Connect());
                    return _sshClient.IsConnected;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public void Initialize()
        {
            //if (_vpn != null)
            //{
            //    Info = new ConnectionInfo(_vpn.Host, 22, _vpn.User, new PasswordAuthenticationMethod(_vpn.User, _vpn.Password))
            //    {
            //        Timeout = TimeSpan.FromSeconds(10)
            //    };
            //    _sshClient = new SshClient(Info);
            //    _sshClient.Connect();
            //}
            if (_credentials != null)
            {
                Info = new ConnectionInfo(_credentials.Dispositivo.Ip, _credentials.Puerto, _credentials.User, new PasswordAuthenticationMethod(_credentials.User, _credentials.Password))
                {
                    Timeout = TimeSpan.FromSeconds(10)
                };
                _sshClient = new SshClient(Info);
                _sshClient.Connect();
            }
        }

        public bool IsConnected
        {
            get
            {
                return _sshClient.IsConnected;
            }
        }

        /// <summary>
        /// Aplica los cambios en el dispositivo
        /// </summary>
        /// <param name="softRestart">Si el cambio conlleva reinicio del equipo o reinicio del software</param>
        /// <returns></returns>
        public async Task CommitChanges(bool softRestart)
        {
            if (_sshClient != null && _sshClient.IsConnected)
            {
                await SendCommand(CommandType.CommitChanges);

                if (softRestart)
                {
                    await SendCommand(CommandType.SoftRestart);

                }
                else
                {
                    await SendCommand(CommandType.SaveAndReboot);
                }
                _sshClient.Disconnect();
            }


        }

        private async Task SendCommand(CommandType commandtype)
        {
            try
            {
                switch (commandtype)
                {
                    case CommandType.ShutdownWifi:
                        await Task.Run(() => _sshClient.RunCommand("ifconfig ath0 down"));
                        break;

                    case CommandType.OnWifi:
                        await Task.Run(() => _sshClient.RunCommand("ifconfig ath0 up"));
                        break;

                    case CommandType.Desconectar:
                        await Task.Run(() => _sshClient.RunCommand("killall -SIGUSR2 udhcpc"));
                        break;

                    case CommandType.Reconectar:
                        await Task.Run(() => _sshClient.RunCommand("killall -SIGUSR1 udhcpc"));
                        break;

                    case CommandType.Restart:
                        await Task.Run(() => _sshClient.RunCommand("reboot"));
                        break;
                    case CommandType.SoftRestart:
                        await Task.Run(() => _sshClient.RunCommand("/usr/etc/rc.d/rc.softrestart save"));
                        break;
                    case CommandType.CommitChanges:
                        await Task.Run(() => _sshClient.RunCommand("cfgmtd -f /tmp/system.cfg -w;"));
                        break;
                    case CommandType.SaveAndReboot:
                        await Task.Run(() => _sshClient.RunCommand("save;reboot;"));
                        break;
                }

            }
            catch (Exception)
            {
                //CreateNewInstance();
            }

        }

        public string RunCommand(string command)
        {
            try
            {
                return _sshClient.RunCommand(command).Result;
            }
            catch (Exception)
            {

                return string.Empty;
            }


        }

        public async Task TrySaveCredentials(Login login)
        {
            await Initialize(login);
        }

        /// <summary>
        /// Obtiene un objeto conteniendo información de ciertas partes del dispositivo.
        /// </summary>
        /// <param name="commandtype">Comando a ejecutar.</param>
        /// <returns></returns>
        public object GetJson(CommandType commandtype)
        {

            try
            {
                switch (commandtype)
                {

                    case CommandType.UbiquitiObject:
                        return JsonConvert.DeserializeObject<UbiquitiObject>(_sshClient.RunCommand("/usr/www/status.cgi").Result.Replace("Content-Type: application/json", ""));

                    case CommandType.Dhcp:
                        return _sshClient.RunCommand("/usr/www/dhcpcinfo.cgi").Result.Replace("Content-Type:  application/json", "");


                    case CommandType.Stations:
                        return _sshClient.RunCommand("/usr/www/stalist.cgi").Result;

                    case CommandType.ApInformation:
                        return JsonConvert.DeserializeObject<List<ApInfo>>(_sshClient.RunCommand("/usr/www/sta.cgi").Result.Replace("Content-Type: text/html", ""));

                    case CommandType.Interfaces:
                        var result = _sshClient.RunCommand("/usr/www/iflist.cgi").Result.Replace("Content-Type: text/html", "");
                        return null;

                    default:
                        return null;
                }

            }
            catch (Exception)
            {
                return null;

            }

        }


    }
}
