﻿using Renci.SshNet;
using System;
using System.IO;
using System.Threading.Tasks;
using wzmData.Ssh;

namespace SshCore
{
    public class ScpConnection : IDisposable
    {

        private ScpClient _scpClient;
        public ConnectionInfo Info { get; private set; }

        bool _disposed;


        public async Task Initialize(Login login)
        {

            Info = new ConnectionInfo(login.Ip, login.Port, login.User, new PasswordAuthenticationMethod(login.User, login.Password))
            {
                Timeout = TimeSpan.FromSeconds(60)
            };
            _scpClient = new ScpClient(Info);
            await Task.Run(() => _scpClient.Connect());
        }

        public async Task UploadConfiguration(FileStream config, bool acl)
        {

            await Task.Run(() => _scpClient.Upload(config, "/tmp/system.cfg"));
        }



        public async Task DownloadConfiguration(FileStream fileStream)
        {

            await Task.Run(() => _scpClient.Download("/tmp/system.cfg", fileStream));

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _scpClient.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
