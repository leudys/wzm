﻿using System.Collections.Generic;
using System.Linq;

namespace wzmData.User
{
    public class UserValidation
    {
        public UserValidation()
        {
            Succeeded = true;
        }

        private List<UserValidationErrors> _errors = new List<UserValidationErrors>();

        public bool Succeeded { get; protected set; }

        public IEnumerable<UserValidationErrors> Errors
        {
            get
            {
                return _errors;
            }
        }


        public UserValidation Failed(UserValidationErrors[] errors)
        {
            UserValidation identityResult = new UserValidation()
            {
                Succeeded = false
            };
            if (errors != null)
                identityResult._errors.AddRange(errors);
            return identityResult;
        }

        public void Failed(UserValidationErrors error)
        {
            Succeeded = false;
            if (error != null)
                _errors.Add(error);

        }

        public override string ToString()
        {
            if (!Succeeded)
            {
                                
                return string.Format("{0} : {1}", "Error", string.Join(",", Errors.Select(x => x.Description)));
            }

            return "Succeeded";
        }
    }
}
