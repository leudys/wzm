﻿
namespace wzmData.User
{
   public class UserValidationErrors
    {
       
        public string Code { get; set; }
        
        public string Description { get; set; }
    }
}
