﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace wzmData.ViewModels
{
    public class ChangeUserDevice
    {
        public string UserId { get; set; }

        public int Id { get; set; }
                
        public string OldMac { get; set; }
                
        [Remote("CompareMac", "Remote", AdditionalFields = "OldMac")]
        [StringLength(17, ErrorMessage = "El campo {0} ha de tener 17 caracteres", MinimumLength = 17)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string NewMac { get; set; }
    }
}
