﻿
namespace wzmData.ViewModels
{
    public class ClientInfoViewModel
    {

        public string Ip { get; set; }

        public string Wlan { get; set; }

        public string Internet { get; set; }

        public string Nick { get; set; }

    }
}
