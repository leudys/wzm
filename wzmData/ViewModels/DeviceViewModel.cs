﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzmData.ViewModels
{
    public class DeviceViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [Display(Name = "Tipo de Ip")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public IpTypeEnum TipoIp { get; set; }

        [Display(Name = "DHCP Server")]
        public string DhcpServer { get; set; }

        [Display(Name = "Actualizar Ip Automática")]
        public bool ActualizarIpAutomática { get; set; }

        public string Ip { get; set; }

    }
}
