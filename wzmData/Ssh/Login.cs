﻿namespace wzmData.Ssh
{
    public class Login
    {
        public string Ip { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public int Port { get; set; }
    }
}
