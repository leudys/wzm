﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Enum
{
    public enum DeviceTypeEnum
    {
        Ubiquiti,
        Mikrotik,
        [Display(Name = "Tp-Link")]
        TPLink,
        Otros
    }
}
