﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace wzmData.Enum
{
   public enum UserPaymentState
    {
        Atrasado,
        [Display(Name = "Al Día")]
        AlDia,
    }
}
