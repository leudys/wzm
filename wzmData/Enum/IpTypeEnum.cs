﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace wzmData.Enum
{
   public enum IpTypeEnum
    {
   
        [Display(Name="Automática")]
        Automatica,
        Fija
    }
}
