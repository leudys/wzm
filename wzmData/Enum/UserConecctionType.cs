﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Enum
{
    public enum DeviceConecctionType
    {
        [Display(Name="Wi-Fi")]
        Wifi,
        LAN,
        Desconocido
    }
}
