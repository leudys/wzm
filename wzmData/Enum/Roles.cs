﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Enum
{
    public enum Roles
    {
        DHCP,
        [Display(Name = "Firewall Adress List")]
        FIREWALL_JUEGOS,
        VLAN,
        HotSpot
    }
}
