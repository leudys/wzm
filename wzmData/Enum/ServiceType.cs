﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Enum
{
    public enum ServiceType
    {
        [Display(Name = "Reconexión Automática")]
        ReconexionAutomatica,
        [Display(Name = "Localizar Portales con Internet")]
        Vip,
        [Display(Name = "Prevención de Clones")]
        AntiClon,

        [Display(Name = "Balanceador NTH")]
        BalanceadorNTH,

        [Display(Name = "Balanceador PCC")]
        BalanceadorPCC,

        [Display(Name = "Morosos")]
        Morosos,

        [Display(Name = "Cambiar MAC")]
        CambiarMac,

        [Display(Name = "Administraciíon de Usuarios")]
        AdministracionUsuarios,
        UbiquitiScan,
        LicenseChecker,
        InternetFinder,
        ActiveDevices,
        TracerRoute,
        ServerToInternet,
        [Display(Name = "Control de Ancho de Banda")]
        SimpleQueueLimit,
        [Display(Name = "Conector de Cuentas Nauta")]
        NautaConnector
    }
}
