﻿using System.Collections.Generic;
using wzmData.Ubiquiti.UbiquitiProperties;

namespace wzmData.Ubiquiti
{
    public class UbiquitiObject
    {
        public string Content { get; set; }
        public Host Host { get; set; }

        public Wireless Wireless { get; set; }

        public Airview Airview { get; set; }

        public Services Services { get; set; }

        public Firewall Firewall { get; set; }

        public string Genuine { get; set; }

        public List<Interfaces> Interfaces { get; set; }

        public string FirmwareToString()
        {
            return Host.Fwversion + " " + Host.Fwprefix;
        }
    }
}
