﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Status
    {
        public bool Plugged { get; set; }

        public int Speed { get; set; }

        public int Duplex { get; set; }
    }
}
