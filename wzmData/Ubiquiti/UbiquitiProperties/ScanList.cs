﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class ScanList
    {
        // ReSharper disable once InconsistentNaming
        public string scan_status { get; set; }

        public string Mac { get; set; }

        public string Mode { get; set; }

        // ReSharper disable once InconsistentNaming
        public string mtik_name { get; set; }

        public string Frequency { get; set; }

        public string Channel { get; set; }

        public string Quality { get; set; }

        // ReSharper disable once InconsistentNaming
        public string signal_level { get; set; }

        // ReSharper disable once InconsistentNaming
        public string noise_level { get; set; }

        public string Encryption { get; set; }

        public string Essid { get; set; }

        // ReSharper disable once InconsistentNaming
        public string pairwise_ciphers { get; set; }

        // ReSharper disable once InconsistentNaming
        public string group_cipher { get; set; }

        // ReSharper disable once InconsistentNaming
        public string auth_suites { get; set; }

        public int Htcap { get; set; }

        // ReSharper disable once InconsistentNaming
        public string airmax_ie { get; set; }

        // ReSharper disable once InconsistentNaming
        public string ieee_mode { get; set; }

    }
}
