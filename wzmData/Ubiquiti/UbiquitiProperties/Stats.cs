﻿

namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Stats
    {
        public string rx_nwids { get; set; }
        public string rx_crypts { get; set; }

        public string rx_frags { get; set; }
        public string tx_retries { get; set; }
        public string missed_beacons { get; set; }
        public string err_other { get; set; }


    }
}
