﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Airmax
    {
        public string Priority { get; set; }

        public string Quality { get; set; }

        public string Beam { get; set; }

        public string Signal { get; set; }

        public string Capacity { get; set; }

    }
}
