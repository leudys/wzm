﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Firewall
    {
        public bool Iptables { get; set; }

        public bool Ebtables { get; set; }

        public bool Ip6Tables { get; set; }

        public bool Eb6Tables { get; set; }
    }
}
