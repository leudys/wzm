﻿using System;

namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Host
    {
        public string Uptime { get; set; }
        public DateTime Time { get; set; }
        public string Fwversion { get; set; }
        public string Fwprefix { get; set; }
        public string Hostname { get; set; }
        public string Devmodel { get; set; }
        public string Netrole { get; set; }
        public decimal Totalram { get; set; }
        public decimal Freeram { get; set; }
        public decimal Cputotal { get; set; }
        public decimal Cpubusy { get; set; }
    }
}
