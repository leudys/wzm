﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{

    public class Dhcpc
    {
        public string ContentType { get; set; }
        public Dhcp dhcpc { get; set; }
    }
}
