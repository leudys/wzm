﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class ApInfo
    {
        public string Mac { get; set; }

        public string Name { get; set; }

        public string Lastip { get; set; }

        public string Associd { get; set; }

        public string Aprepeater { get; set; }

        public int Tx { get; set; }

        public int Rx { get; set; }

        public int Signal { get; set; }

        public int Rssi { get; set; }

        public int [] Chainrssi { get; set; }

        public int Rx_chainmask { get; set; }

        public int Ccq { get; set; }

        public int Idle { get; set; }

        public int Tx_latency { get; set; }

        public ulong Uptime { get; set; }

        public int Ack { get; set; }

        public int Distance { get; set; }

        public int Txpower { get; set; }

        public int Noisefloor { get; set; }

        public int [] Tx_ratedata { get; set; }

        public Airmax Airmax { get; set; }

        public Stats Stats { get; set; }

        public string [] Rates { get; set; }

        public int [] Signals { get; set; }

        public Remote Remote { get; set; }
        
    }
}
