﻿

namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Wireless
    {
        public string Mode { get; set; }
        public string Essid { get; set; }
        // ReSharper disable once InconsistentNaming
        public bool Hide_essid { get; set; }
        public string Apmac { get; set; }
        public string Countrycode { get; set; }
        public string Channel { get; set; }
        public string Frequency { get; set; }
        public string Dfs { get; set; }
        public string Opmode { get; set; }
        public string Antenna { get; set; }
        public string Chains { get; set; }
        public string Signal { get; set; }
        public int Rssi { get; set; }
        public string Txpower { get; set; }
        public string Ack { get; set; }
        public string Distance { get; set; }
        public string Ccq { get; set; }
        public string Txrate { get; set; }
        public string Rxrate { get; set; }
        public string Security { get; set; }
        public string Qos { get; set; }
        public string Rstatus { get; set; }
        // ReSharper disable once InconsistentNaming
        public int cac_nol { get; set; }
        // ReSharper disable once InconsistentNaming
        public int nol_chans { get; set; }
        public Polling Polling { get; set; }
        public Stats Stats { get; set; }
        public bool Wds { get; set; }
        public bool Aprepeater { get; set; }
        public string Chwidth { get; set; }
        public string Chanbw { get; set; }
        public string Cwmmode { get; set; }
        // ReSharper disable once InconsistentNaming
        public string Rx_chainmask { get; set; }
        // ReSharper disable once InconsistentNaming
        public string Tx_Chainmask { get; set; }
        public int[] Chainrssi { get; set; }
        public int[] Chainrssimgmt { get; set; }
        public int[] Chainrssiext { get; set; }
    }
}
