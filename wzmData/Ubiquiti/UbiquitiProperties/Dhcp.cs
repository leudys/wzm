﻿using System.Collections.Generic;

namespace wzmData.Ubiquiti.UbiquitiProperties
{

    public class Dhcp
    {
        public string status { get; set; }

        public string sysuptime { get; set; }

        public string error { get; set; }

        public List<Info> info { get; set; }
    }
}
