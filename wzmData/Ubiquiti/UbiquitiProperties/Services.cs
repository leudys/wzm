﻿

namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Services
    {
        public bool Dhcpc { get; set; }

        public bool Dhcpd { get; set; }

        public bool Pppoe { get; set; }
    }
}
