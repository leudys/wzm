﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Remote
    {
        public string Hostname { get; set; }

        public string Platform { get; set; }

        public string Version { get; set; }

        public string Signal { get; set; }

        public string Tx_power { get; set; }
    }
}
