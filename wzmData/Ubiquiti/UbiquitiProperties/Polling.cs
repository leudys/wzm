﻿

namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Polling
    {
        public bool Enabled { get; set; }
        public string Quality { get; set; }
        public string Capacity { get; set; }
        public string Priority { get; set; }
        public string Noack { get; set; }
        public string Airselect { get; set; }
    }
}
