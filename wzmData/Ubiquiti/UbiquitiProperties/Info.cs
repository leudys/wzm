﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Info
    {
        public string Ifname { get; set; }

        public bool Status { get; set; }

        public decimal Pid { get; set; }

        public string Leased { get; set; }

        public string Leasetime { get; set; }

        // ReSharper disable once InconsistentNaming
        public string leasetime_str { get; set; }

        // ReSharper disable once InconsistentNaming
        public string leasetime_left { get; set; }

        public string Serverid { get; set; }

        public string Ip { get; set; }

        public string Netmask { get; set; }

        public string Gateway { get; set; }

        public string Hostname { get; set; }

        public string Domain { get; set; }

        public string[] Dns { get; set; }

        public string Error { get; set; }

    }
}
