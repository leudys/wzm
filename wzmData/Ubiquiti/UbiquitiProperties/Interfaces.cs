﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class Interfaces
    {
        public string Ifname { get; set; }

        public string Hwaddr { get; set; }

        public bool Enabled { get; set; }

        public Status Status { get; set; }

        public Services Services { get; set; }
    }
}
