﻿
namespace wzmData.Ubiquiti.UbiquitiProperties
{
    public class StationList
    {
        public string Mac { get; set; }

        public string Name { get; set; }

        public string LastIp { get; set; }

        public string Rx { get; set; }

        public string Tx { get; set; }

        public string Signal { get; set; }

        public decimal Rssi { get; set; }

        // ReSharper disable once InconsistentNaming
        public int tx_latency { get; set; }

        public double Distance { get; set; }
        public Remote Remote { get; set; }
    }
}
