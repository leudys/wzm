﻿using System.Collections.Generic;
using wzmData.Models;
using wzmData.User;

namespace wzmData.Ubiquiti
{
    public class ImportDevices
    {
        public List<UserDevice> Dispositivos { get; set; }

        public UserValidation UserValidation { get; set; }

        public ImportDevices()
        {
            UserValidation = new UserValidation();
            Dispositivos = new List<UserDevice>();
        }
    }
}
