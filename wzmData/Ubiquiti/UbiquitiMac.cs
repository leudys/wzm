﻿using wzmData.Enum;

namespace wzmData.Ubiquiti
{
    public class UbiquitiMac
    {
        public int Id { get; set; }

        public string Mac { get; set; }

        public string Comment { get; set; }

        public string Ip { get; set; }

        public StatusEnum Status { get; set; }
    }
}
