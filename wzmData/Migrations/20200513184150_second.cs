﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeviceToNationalNetworks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PublicIp = table.Column<string>(nullable: false),
                    Disabled = table.Column<bool>(nullable: false),
                    AddressList = table.Column<string>(nullable: false),
                    UserDeviceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceToNationalNetworks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceToNationalNetworks_UsersDevices_UserDeviceId",
                        column: x => x.UserDeviceId,
                        principalTable: "UsersDevices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeviceToNationalNetworks_UserDeviceId",
                table: "DeviceToNationalNetworks",
                column: "UserDeviceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceToNationalNetworks");
        }
    }
}
