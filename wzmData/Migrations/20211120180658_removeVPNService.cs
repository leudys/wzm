﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class removeVPNService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "IncludeUsers",
            //    table: "ApplicationServices");

            //migrationBuilder.DropColumn(
            //    name: "Persistent",
            //    table: "ApplicationServices");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IncludeUsers",
                table: "ApplicationServices",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Persistent",
                table: "ApplicationServices",
                nullable: true);
        }
    }
}
