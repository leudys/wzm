﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class big : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PersistentConnection",
                table: "VpnServers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "VpnServerId",
                table: "VpnServers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AsInternet",
                table: "VpnServers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VpnServerId",
                table: "HostsServices",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Persistent",
                table: "ApplicationServices",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Time",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Daily = table.Column<bool>(nullable: false),
                    Monthly = table.Column<bool>(nullable: false),
                    DayOfMonth = table.Column<int>(nullable: true),
                    Hora = table.Column<int>(nullable: true),
                    Minutos = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Time", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResetTime",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(nullable: true),
                    TimeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResetTime", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResetTime_Time_TimeId",
                        column: x => x.TimeId,
                        principalTable: "Time",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResetTime_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_VpnServerId",
                table: "VpnServers",
                column: "VpnServerId");

            migrationBuilder.CreateIndex(
                name: "IX_HostsServices_VpnServerId",
                table: "HostsServices",
                column: "VpnServerId");

            migrationBuilder.CreateIndex(
                name: "IX_ResetTime_TimeId",
                table: "ResetTime",
                column: "TimeId");

            migrationBuilder.CreateIndex(
                name: "IX_ResetTime_UserId",
                table: "ResetTime",
                column: "UserId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_HostsServices_VpnServers_VpnServerId",
            //    table: "HostsServices",
            //    column: "VpnServerId",
            //    principalTable: "VpnServers",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_VpnServers_VpnServers_VpnServerId",
            //    table: "VpnServers",
            //    column: "VpnServerId",
            //    principalTable: "VpnServers",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HostsServices_VpnServers_VpnServerId",
                table: "HostsServices");

            migrationBuilder.DropForeignKey(
                name: "FK_VpnServers_VpnServers_VpnServerId",
                table: "VpnServers");

            migrationBuilder.DropTable(
                name: "ResetTime");

            migrationBuilder.DropTable(
                name: "Time");

            migrationBuilder.DropIndex(
                name: "IX_VpnServers_VpnServerId",
                table: "VpnServers");

            migrationBuilder.DropIndex(
                name: "IX_HostsServices_VpnServerId",
                table: "HostsServices");

            migrationBuilder.DropColumn(
                name: "PersistentConnection",
                table: "VpnServers");

            migrationBuilder.DropColumn(
                name: "VpnServerId",
                table: "VpnServers");

            migrationBuilder.DropColumn(
                name: "AsInternet",
                table: "VpnServers");

            migrationBuilder.DropColumn(
                name: "VpnServerId",
                table: "HostsServices");

            migrationBuilder.DropColumn(
                name: "Persistent",
                table: "ApplicationServices");
        }
    }
}
