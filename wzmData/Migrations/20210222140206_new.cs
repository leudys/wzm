﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class @new : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CuentaNautas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    User = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CuentaNautas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VpnServers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    User = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Host = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    VpnServerNacionalId = table.Column<int>(nullable: true),
                    AddressList = table.Column<string>(nullable: true),
                    CuentaNautaId = table.Column<int>(nullable: true),
                    PortalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VpnServers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VpnServers_VpnServers_VpnServerNacionalId",
                        column: x => x.VpnServerNacionalId,
                        principalTable: "VpnServers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VpnServers_CuentaNautas_CuentaNautaId",
                        column: x => x.CuentaNautaId,
                        principalTable: "CuentaNautas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VpnServers_Portales_PortalId",
                        column: x => x.PortalId,
                        principalTable: "Portales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_VpnServerNacionalId",
                table: "VpnServers",
                column: "VpnServerNacionalId");

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_CuentaNautaId",
                table: "VpnServers",
                column: "CuentaNautaId");

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_PortalId",
                table: "VpnServers",
                column: "PortalId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VpnServers");

            migrationBuilder.DropTable(
                name: "CuentaNautas");
        }
    }
}
