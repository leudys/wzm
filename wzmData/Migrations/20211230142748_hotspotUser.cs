﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class hotspotUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HotSpotUser",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HotSpotUser",
                table: "AspNetUsers");
        }
    }
}
