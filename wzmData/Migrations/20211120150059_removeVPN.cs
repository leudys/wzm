﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class removeVPN : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_HostsServices_VpnServers_VpnServerId",
            //    table: "HostsServices");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_ResetTime_HostsServices_UserId1",
            //    table: "ResetTime");

            //migrationBuilder.DropTable(
            //    name: "VpnServers");

            //migrationBuilder.DropIndex(
            //    name: "IX_ResetTime_UserId1",
            //    table: "ResetTime");

            //migrationBuilder.DropIndex(
            //    name: "IX_HostsServices_VpnServerId",
            //    table: "HostsServices");

            //migrationBuilder.DropColumn(
            //    name: "UserId1",
            //    table: "ResetTime");

            //migrationBuilder.DropColumn(
            //    name: "VpnServerId",
            //    table: "HostsServices");

            //migrationBuilder.AlterColumn<int>(
            //    name: "UserId",
            //    table: "ResetTime",
            //    nullable: false,
            //    oldClrType: typeof(string),
            //    oldNullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "PortalId",
            //    table: "CuentaNautas",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.CreateIndex(
            //    name: "IX_ResetTime_UserId",
            //    table: "ResetTime",
            //    column: "UserId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_CuentaNautas_PortalId",
            //    table: "CuentaNautas",
            //    column: "PortalId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_CuentaNautas_Portales_PortalId",
            //    table: "CuentaNautas",
            //    column: "PortalId",
            //    principalTable: "Portales",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ResetTime_HostsServices_UserId",
            //    table: "ResetTime",
            //    column: "UserId",
            //    principalTable: "HostsServices",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CuentaNautas_Portales_PortalId",
                table: "CuentaNautas");

            migrationBuilder.DropForeignKey(
                name: "FK_ResetTime_HostsServices_UserId",
                table: "ResetTime");

            migrationBuilder.DropIndex(
                name: "IX_ResetTime_UserId",
                table: "ResetTime");

            migrationBuilder.DropIndex(
                name: "IX_CuentaNautas_PortalId",
                table: "CuentaNautas");

            migrationBuilder.DropColumn(
                name: "PortalId",
                table: "CuentaNautas");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "ResetTime",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "UserId1",
                table: "ResetTime",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VpnServerId",
                table: "HostsServices",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VpnServers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AddressList = table.Column<string>(nullable: true),
                    Desabled = table.Column<bool>(nullable: false),
                    DisconnectOnPingFailed = table.Column<bool>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    Host = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Running = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    VpnServerNacionalId = table.Column<int>(nullable: true),
                    AsInternet = table.Column<bool>(nullable: true),
                    CuentaNautaId = table.Column<int>(nullable: true),
                    PersistentConnection = table.Column<bool>(nullable: true),
                    PortalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VpnServers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VpnServers_VpnServers_VpnServerNacionalId",
                        column: x => x.VpnServerNacionalId,
                        principalTable: "VpnServers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VpnServers_CuentaNautas_CuentaNautaId",
                        column: x => x.CuentaNautaId,
                        principalTable: "CuentaNautas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VpnServers_Portales_PortalId",
                        column: x => x.PortalId,
                        principalTable: "Portales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ResetTime_UserId1",
                table: "ResetTime",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_HostsServices_VpnServerId",
                table: "HostsServices",
                column: "VpnServerId");

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_VpnServerNacionalId",
                table: "VpnServers",
                column: "VpnServerNacionalId");

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_CuentaNautaId",
                table: "VpnServers",
                column: "CuentaNautaId");

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_PortalId",
                table: "VpnServers",
                column: "PortalId");

            migrationBuilder.AddForeignKey(
                name: "FK_HostsServices_VpnServers_VpnServerId",
                table: "HostsServices",
                column: "VpnServerId",
                principalTable: "VpnServers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ResetTime_HostsServices_UserId1",
                table: "ResetTime",
                column: "UserId1",
                principalTable: "HostsServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
