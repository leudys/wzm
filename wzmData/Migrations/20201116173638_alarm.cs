﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class alarm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Alarm",
                table: "Dispositivos",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Alarm",
                table: "Dispositivos");
        }
    }
}
