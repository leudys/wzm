﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class g : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IncludeUsers",
                table: "ApplicationServices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IncludeUsers",
                table: "ApplicationServices");
        }
    }
}
