﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class chabges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_ResetTime_AspNetUsers_UserId",
            //    table: "ResetTime");

            //migrationBuilder.DropIndex(
            //    name: "IX_ResetTime_UserId",
            //    table: "ResetTime");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "ResetTime",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ResetTime_UserId",
                table: "ResetTime",
                column: "UserId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ResetTime_HostsServices_UserId",
            //    table: "ResetTime",
            //    column: "UserId",
            //    principalTable: "HostsServices",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ResetTime_HostsServices_UserId1",
                table: "ResetTime");

            migrationBuilder.DropIndex(
                name: "IX_ResetTime_UserId1",
                table: "ResetTime");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "ResetTime");

            migrationBuilder.CreateIndex(
                name: "IX_ResetTime_UserId",
                table: "ResetTime",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ResetTime_AspNetUsers_UserId",
                table: "ResetTime",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
