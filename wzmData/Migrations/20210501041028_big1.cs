﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class big1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_VpnServers_VpnServers_VpnServerId",
            //    table: "VpnServers");

            //migrationBuilder.DropIndex(
            //    name: "IX_VpnServers_VpnServerId",
            //    table: "VpnServers");

            //migrationBuilder.DropColumn(
            //    name: "VpnServerId",
            //    table: "VpnServers");

            //migrationBuilder.AlterColumn<bool>(
            //    name: "PersistentConnection",
            //    table: "VpnServers",
            //    nullable: true,
            //    oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "PersistentConnection",
                table: "VpnServers",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VpnServerId",
                table: "VpnServers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VpnServers_VpnServerId",
                table: "VpnServers",
                column: "VpnServerId");

            migrationBuilder.AddForeignKey(
                name: "FK_VpnServers_VpnServers_VpnServerId",
                table: "VpnServers",
                column: "VpnServerId",
                principalTable: "VpnServers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
