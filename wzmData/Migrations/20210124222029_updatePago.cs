﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class updatePago : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Pagado",
                table: "Pagos",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NoPortal",
                table: "HostsServices",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pagado",
                table: "Pagos");

            migrationBuilder.DropColumn(
                name: "NoPortal",
                table: "HostsServices");
        }
    }
}
