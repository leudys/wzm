﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace wzmData.Migrations
{
    public partial class onPing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DisconnectOnPingFailed",
                table: "VpnServers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisconnectOnPingFailed",
                table: "VpnServers");
        }
    }
}
