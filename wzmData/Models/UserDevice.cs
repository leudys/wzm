﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using wzmData.Enum;

namespace wzmData.Models
{
    public class UserDevice
    {
        public int Id { get; set; }
        [Remote("IsMacValid", "Remote")]
        [StringLength(17, ErrorMessage = "El campo {0} ha de tener 17 caracteres", MinimumLength = 17)]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Mac { get; set; }

        public string Ip { get; set; }

        [Display(Name = "Tipo de Ip")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public IpTypeEnum TipoIp { get; set; }

        public string DhcpServer { get; set; }


        [Display(Name = "Tipo de Conexión")]
        public DeviceConecctionType DeviceConnectionType { get; set; }

        [Display(Name = "Portal de Origen")]
        [Required(ErrorMessage ="El campo {0} es obligatorio.")]
        public string PortalDeOrigen { get; set; }

        [Display(Name ="Usuario")]
        //[Required(ErrorMessage ="El campo {0} es obligatorio.")]
        public string ApplicationUserId { get; set; }

        public virtual ApplicationUsers ApplicationUser { get; set; }

        [Display(Name ="Nombre del Dispositivo")]
        [Required(ErrorMessage ="El campo {0} es obligatorio.")]
        public string Comment { get; set; }

        public bool HasInternet { get; set; }
 
        public bool Disabled { get; set; }

        public int? AdminDeviceId { get; set; }

        public virtual AdminDevices AdminDevice { get; set; }
              
        public virtual ICollection<HostsServices> HostsServices { get; set; }

        public virtual ICollection<DeviceToNationalNetwork> DeviceToNationalNetworks { get; set; }
        public UserDevice()
        {
            HostsServices = new HashSet<HostsServices>();
            DeviceToNationalNetworks = new HashSet<DeviceToNationalNetwork>();
        }
        [Display(Name ="Ip Pública")]
        [NotMapped]
        public string PublicIp { get; set; }
        [NotMapped]
        public string AddressList { get; set; }

        [NotMapped]
        public bool NationalNetwork { get; set; }

        [NotMapped]
        public bool Internet { get; set; }
    }
}
