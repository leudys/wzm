﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
   public class ExcentoPago
    {
        public int Id { get; set; }
       
        public int Mes { get; set; }

        public int Year { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUsers User { get; set; }
    }
}
