﻿using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzmData.Models
{
    public class PortalException
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Portal { get; set; }

        [Display(Name = "Servicio")]
        public ServiceType ServiceType { get; set; }

    }
}
