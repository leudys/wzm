﻿using System;
using System.Collections.Generic;
using System.Text;
using wzmData.Enum;

namespace wzmData.Models
{
   public class DevicesRoles
    {
        public int Id { get; set; }

        public Roles Role { get; set; }

        public virtual ICollection<AdminDevicesRoles> AdminDevices { get; set; }

        public DevicesRoles()
        {
            AdminDevices = new HashSet<AdminDevicesRoles>();
        }
    }
}
