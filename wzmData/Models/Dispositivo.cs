﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using wzmData.Enum;

namespace wzmData.Models
{
    public class Dispositivo
    {
        public int Id { get; set; }

        [Display(Name = "MAC")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Remote("IsDeviceMacValid", "Remote")]
        public string Mac { get; set; }

        [Display(Name = "Nombre del Equipo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Hostname { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Platform { get; set; }

        public string Firmware { get; set; }

        public string SSID { get; set; }

        [Display(Name = "Ip")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Remote("ValidateIp", "Remote")]
        public string Ip { get; set; }

        [Display(Name = "Ubicación")]
        public string Ubicacion { get; set; }

        public bool Alarm { get; set; }

        public string GetDispositivo
        {
            get { return Ip + " " + Hostname; }
        }

        public virtual ICollection<DeviceCredentials> DeviceCredential { get; set; }

        public virtual ICollection<DeviceSignal> DeviceSignals { get; set; }

        [Display(Name = "Modo de Wi-Fi")]
        public WirelessMode WirelessMode { get; set; }

        public virtual ICollection<ApplicationUsers> Usuarios { get; set; }

        public DeviceTypeEnum Fabricante { get; set; }

        [Display(Name = "Seguridad de Wi-Fi")]
        public WirelessSecurity WirelessSecurity { get; set; }

        public Dispositivo()
        {
            Usuarios = new HashSet<ApplicationUsers>();
            DeviceCredential = new HashSet<DeviceCredentials>();
            DeviceSignals = new HashSet<DeviceSignal>();
        }

        [NotMapped]
        public string GetFullName
        {
            get
            {
                return $"{Hostname} {Ip} {Ubicacion}";
            }
            
        }


        public override string ToString()
        {
            return $"{Hostname} {Ip} {Ubicacion}";
            
        }
    }
}
