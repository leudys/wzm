﻿using System.Collections.Generic;
using wzmData.Enum;

namespace wzmData.Models
{
    public class Service
    {
        public int Id { get; set; }

        public UserServiceType UserServiceType { get; set; }

        public virtual ICollection<ClientServices> ClientServices { get; set; }

        public Service()
        {
            ClientServices = new HashSet<ClientServices>();
        }
    }
}
