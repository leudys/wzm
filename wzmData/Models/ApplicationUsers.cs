﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using wzmData.Enum;

namespace wzmData.Models
{
    public class ApplicationUsers : IdentityUser
    {
        public virtual ICollection<UserDevice> UserDevices { get; set; }
        public ApplicationUsers()
        {
            UserDevices = new HashSet<UserDevice>();
            Pagos = new HashSet<Pago>();
            ClientServices = new HashSet<ClientServices>();
           
        }

        [Display(Name = "Cliente Desde")]
        public DateTime? ClienteDesde { get; set; }

        [Display(Name = "Estado del Pago")]
        public UserPaymentState UserPaymentState { get; set; }

        [Display(Name = "Dispositivo")]
        public int? DispositivoId { get; set; }

        [Display(Name = "Forma de Pago")]
        public int? FormaPagoId { get; set; }

        public string HotSpotUser { get; set; }

        public virtual FormasDePago FormasPago { get; set; }

        public virtual Dispositivo Dispositivo { get; set; }

        [Display(Name = "Clasificador de Pago")]
        public int? ClasificadorPagoId { get; set; }

        public virtual ClasificadorPago ClasificadorPago { get; set; }

        public virtual ICollection<Pago> Pagos { get; set; }

        public virtual ICollection<ClientServices> ClientServices { get; set; }

        public virtual ICollection<ExcentoPago> ExcentoPagos { get; set; }
               
        public StatusEnum Estado { get; set; }

    }
}
