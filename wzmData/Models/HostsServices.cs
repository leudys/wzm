﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace wzmData.Models
{
    public class HostsServices
    {
        public int Id { get; set; }

        [Display(Name = "Deshabiliado")]
        public bool Disabled { get; set; }

        [Display(Name = "Cambio de Portales")]
        public bool Vip { get; set; }

        [Display(Name = "Balanceo")]
        public bool Balanceo { get; set; }

        [Display(Name = "Tiene Internet")]
        public bool HasInternet { get; set; }

        [Display(Name = "Está en el balanceo")]
        public bool IsBalanced { get; set; }

        [Display(Name = "Limitar Dispositivo con Simple Queue")]
        public bool Limit { get; set; }

        public string PortalActual { get; set; }

        public int ApplicationServicesId { get; set; }

        public virtual ApplicationServices ApplicationServices { get; set; }
        /// <summary>
        /// Si está habilitado este usuario siempre tendrá una conexión persistente y no se moverá entre los portales
        /// </summary>
        [Display(Name = "Conexión Persistente")]
        public bool Vpn { get; set; }

        [Display(Name = "Dispositivo")]
        public int DeviceId { get; set; }

        public virtual UserDevice Device { get; set; }

        [Display(Name = "Sin acceso a Portal")]
        public bool NoPortal { get; set; }
        
        public virtual ICollection<ResetTime> ResetTime { get; set; }

        public HostsServices()
        {
            ResetTime = new HashSet<ResetTime>();
        }

        [NotMapped]
        public string Name
        {
            get
            {
                return Device != null ? Device.ApplicationUser.HotSpotUser : "";

                
            }
        }
    }
}
