﻿namespace wzmData.Models
{
    public class StationDevice : Dispositivo
    {
        public int? ApDeviceId { get; set; }

        public virtual ApDevice ApDevice { get; set; }
              
        /// <summary>
        /// Define si el equipo está conectado a un Proveedor Público
        /// </summary>
        public bool Publico { get; set; }

        /// <summary>
        /// MAC del AP Público
        /// </summary>
        public string MacApPublico { get; set; }
               
    }
}
