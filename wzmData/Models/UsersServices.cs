﻿namespace wzmData.Models
{
    public class ClientServices
    {
        public string UserId { get; set; }

        public virtual ApplicationUsers Usuarios { get; set; }

        public int ServiceId { get; set; }

        public virtual Service Service { get; set; }
    }
}
