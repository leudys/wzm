﻿using System;
using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
    public class MorososService : ApplicationServices
    {
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Redirigir A:")]
        public string RedirecTo { get; set; }
               
        public TimeSpan Horario { get; set; }
                
    }
}
