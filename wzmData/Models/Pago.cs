﻿using System;
using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
    public class Pago
    {
        public int Id { get; set; }

        [Display(Name = "Fecha del Pago")]
        public DateTime FechaPago { get; set; }

        [Display(Name = "Usuario")]
        public string UsuarioId { get; set; }

        public decimal? Pagado { get; set; }

        public virtual ApplicationUsers Usuario { get; set; }
    }
}
