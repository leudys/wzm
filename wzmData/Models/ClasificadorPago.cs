﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;
using wzmData.Enum;

namespace wzmData.Models
{
    public class ClasificadorPago
    {
        public int Id { get; set; }

        [Display(Name = "Día del Pago")]
        [Remote("CheckDiaPago", "Remote")]
        public int? DiaPago { get; set; }

        [Display(Name = "Tipo de Pago")]
        [Required(ErrorMessage = "El campo {0} es olbigatorio.")]
        public PaymentType PaymentType { get; set; }

        [Remote("CheckImporte", "Remote")]
        [DisplayFormat(DataFormatString = "{0:00.00}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "decimal(4, 2)")]
        public decimal? Importe { get; set; }

        public virtual ICollection<ApplicationUsers> Usuarios { get; set; }

        public ClasificadorPago()
        {
            Usuarios = new HashSet<ApplicationUsers>();
        }

        public string GetPago
        {
            get
            {
                return PaymentType + " Día: " + DiaPago.ToString() + " Importe: " + Importe + " ";
            }
        }
    }
}
