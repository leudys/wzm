﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace wzmData.Models
{
    public class AdminDevices : Dispositivo
    {     

        public virtual ICollection<UserDevice> UserDevices { get; set; }

        public virtual ICollection<AdminDevicesRoles> DevicesRoles { get; set; }

        public AdminDevices()
        {
            UserDevices = new HashSet<UserDevice>();
            DevicesRoles = new HashSet<AdminDevicesRoles>();
        }

        [NotMapped]
        public List<int> Roles { get; set; }

    }
}
