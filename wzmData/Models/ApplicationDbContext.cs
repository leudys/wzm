﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace wzmData.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUsers, IdentityRole, string>
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {


            base.OnModelCreating(builder);

            builder.Entity<ClientServices>()
                  .HasKey(bc => new { bc.UserId, bc.ServiceId });
            builder.Entity<ClientServices>()
                .HasOne(bc => bc.Usuarios)
                .WithMany(b => b.ClientServices)
                .HasForeignKey(bc => bc.UserId);
            builder.Entity<ClientServices>()
                .HasOne(bc => bc.Service)
                .WithMany(c => c.ClientServices)
                .HasForeignKey(bc => bc.ServiceId);

            builder.Entity<AdminDevicesRoles>()
               .HasKey(bc => new { bc.AdminDeviceId, bc.DeviceRoleId });
            builder.Entity<AdminDevicesRoles>()
                .HasOne(bc => bc.AdminDevice)
                .WithMany(b => b.DevicesRoles)
                .HasForeignKey(bc => bc.AdminDeviceId);
            builder.Entity<AdminDevicesRoles>()
                .HasOne(bc => bc.DeviceRole)
                .WithMany(c => c.AdminDevices)
                .HasForeignKey(bc => bc.DeviceRoleId);

            //base.OnModelCreating(builder);
            //builder.Entity<IdentityRole>().HasData(new IdentityRole[] {
            //        new IdentityRole {Id = "1", Name = "Admin", NormalizedName = "ADMIN" },
            //        new IdentityRole {Id = "2", Name = "User", NormalizedName = "User" },

            // });

            //builder.Entity<ApplicationUsers>().HasData(
            //new ApplicationUsers
            //{
            //    Id = "54205fe9-4890-460c-9cb4-4e510143b83e",
            //    UserName = "admin",

            //    NormalizedUserName = "ADMIN",
            //    Email = "wzm@gmail.com",
            //    NormalizedEmail = "WZM@GMAIL.COM",
            //    PasswordHash = "AQAAAAEAACcQAAAAECgzoE/Fv9DDLRiCv4xHrpYg3eYCS+lTcINI14COpE71hF5ZfCJum+7cue376R92yA==",
            //    SecurityStamp = "43ZFATDQSRL356THOAOZBLIT6BDRRQT5",
            //    ConcurrencyStamp = "882d70d6-70ac-47f5-9000-4e0e11deee36",
            //    Estado = StatusEnum.Habilitado

            //});
            //builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string> { UserId = "54205fe9-4890-460c-9cb4-4e510143b83e", RoleId = "1" });


        }

        public DbSet<MikrotikCredentials> MikrotikCredentials { get; set; }
        public DbSet<UbiquitiCredentials> UbiquitiCredentials { get; set; }
        public DbSet<HostsServices> HostsServices { get; set; }
        public DbSet<DeviceCredentials> DeviceCredentials { get; set; }
        public DbSet<StationDevice> StationDevices { get; set; }
        public DbSet<ApDevice> ApDevices { get; set; }
        public DbSet<Pago> Pagos { get; set; }
        public DbSet<ApplicationServices> ApplicationServices { get; set; }
        public DbSet<MorososService> MorososService { get; set; }        
        public DbSet<ClasificadorPago> ClasificadorPagos { get; set; }
        public DbSet<Dispositivo> Dispositivos { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ClientServices> ClientServices { get; set; }
        public DbSet<UserDevice> UsersDevices { get; set; }
        public DbSet<ApplicationUsers> Usuarios { get; set; }
        public DbSet<FormasDePago> FormasDePagos { get; set; }
        public DbSet<BestHost> TracerRoute { get; set; }
        public DbSet<AdminDevices> AdminDevices { get; set; }
        public DbSet<DevicesRoles> DevicesRoles { get; set; }
        public DbSet<AdminDevicesRoles> AdminDevicesRoles { get; set; }
        public DbSet<DeviceToNationalNetwork> DeviceToNationalNetworks { get; set; }
        public DbSet<DeviceSignal> DeviceSignals { get; set; }
        public DbSet<PortalException> PortalExceptions { get; set; }
        public DbSet<CuentaNauta> CuentaNautas { get; set; }
        public DbSet<PortalesEtecsa> Portales { get; set; }
        public DbSet<ResetTime> ResetTime { get; set; }
        public DbSet<ExcentoPago> ExcentoPagos { get; set; }
        public DbSet<Time> Time { get; set; }

    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var connectionString = configuration.GetConnectionString("Sqlite");
            builder.UseSqlite(connectionString);
            return new ApplicationDbContext(builder.Options);
        }
    }
}
