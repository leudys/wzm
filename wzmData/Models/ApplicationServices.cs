﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using wzmData.Enum;

namespace wzmData.Models
{

    public class ApplicationServices
    {
        public int Id { get; set; }

        [Display(Name = "Estado")]
        public ServicesStatus State { get; set; }

        [Display(Name = "Versión")]
        public string Version { get; set; }

        [Display(Name = "Servicio")]
        public ServiceType ServiceType { get; set; }

        public virtual ICollection<HostsServices> HostServices { get; set; }
              

        public ApplicationServices()
        {

            HostServices = new HashSet<HostsServices>();
        }

    }
}
