﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
    public class CuentaNauta
    {
        public int Id { get; set; }

        [Display(Name ="Usuario")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string User { get; set; }

        [Display(Name ="Contrasña")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string Password { get; set; }

        [Display(Name = "Portal")]
        public int PortalId { get; set; }

        public virtual PortalesEtecsa Portal { get; set; }

        public bool Connected { get; set; }
                              

    }
}
