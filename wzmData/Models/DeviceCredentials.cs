﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
    public class DeviceCredentials
    {
        public int Id { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public string User { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public int DispositivoId { get; set; }

        public virtual Dispositivo Dispositivo { get; set; }


    }
}
