﻿using System.Collections.Generic;

namespace wzmData.Models
{
    public class ApDevice : Dispositivo
    {
        public virtual ICollection<StationDevice> Estaciones { get; set; }

        public ApDevice()
        {
            Estaciones = new HashSet<StationDevice>();
        }
    }
}
