﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace wzmData.Models
{
    public class PortalesEtecsa
    {
        public int Id { get; set; }

        public string Interfaz { get; set; }

        [Display(Name = "DHCP Client")]
        public string DhcpClient { get; set; }

        [Display(Name = "NAT")]
        public string Nat { get; set; }

        [Display(Name = "Mangle")]
        public string Mangle { get; set; }

        [Display(Name = "Route")]
        public string Route { get; set; }

        public bool Internet { get; set; }

        public bool Enabled { get; set; }


        [NotMapped]
        public List<string> NatComment { get; set; }

        public virtual ICollection<CuentaNauta> CuentaNautas { get; set; }

        public PortalesEtecsa()
        {
            CuentaNautas = new HashSet<CuentaNauta>();
        }
    }
}
