﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wzmData.Models
{
    public class AdminDevicesRoles
    {
        public int AdminDeviceId { get; set; }

        public virtual AdminDevices AdminDevice { get; set; }

        public int DeviceRoleId { get; set; }

        public virtual DevicesRoles DeviceRole { get; set; }
    }
}
