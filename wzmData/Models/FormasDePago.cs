﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using wzmData.Enum;

namespace wzmData.Models
{
    public class FormasDePago
    {
        public int Id { get; set; }

        public UserPayClaissifier Clasificador { get; set; }

        public MonedaPago Moneda { get; set; }

        [Display(Name = "Número de Teléfono")]
        public string NumTelefono { get; set; }

        [Display(Name = "Cuenta Bancaria")]
        public string CtaBancaria { get; set; }

        public string Direccion { get; set; }

        public virtual ICollection<ApplicationUsers> Usuarios { get; set; }

        public FormasDePago()
        {
            Usuarios = new HashSet<ApplicationUsers>();
        }
    }
}
