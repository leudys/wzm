
using System.Collections;
using System.Collections.Generic;

namespace wzmData.Models
{
    public class PortalesInternet
    {

        public int Id { get; set; }

        public string Portal { get; set; }

        public string Route { get; set; }
               

    }
}