﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace wzmData.Models
{
    public class ResetTime
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public virtual HostsServices User { get; set; }

        public int TimeId { get; set; }

        public virtual Time Time { get; set; }
    }

    public class Time
    {
        public int Id { get; set; }

        [Display(Name = "Ejecución Diaria")]
        public bool Daily { get; set; }

        [Display(Name = "Ejecución Mensual")]
        public bool Monthly { get; set; }

        public int? DayOfMonth { get; set; }

        public int? Hora { get; set; }

        public int? Minutos { get; set; }

        public ICollection<ResetTime> ResetTime { get; set; }

        public Time()
        {
            ResetTime = new HashSet<ResetTime>();
        }

        [NotMapped]
        public string GetTime
        {
            get
            {
                string result = string.Empty;

                if (Daily)
                {
                    if (Minutos != null)
                    {
                        result = $"Diario Horario:{Hora}:{Minutos}";
                    }
                    else
                    {
                        result = $"Diario Horario:{Hora}:00";
                    }
                   
                }
                if (Monthly)
                {
                    result = $"Mensual Día:{DayOfMonth}";
                }

                return result;
            }            
        }
    }
}
