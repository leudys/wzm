﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using wzmData.Enum;


namespace wzmData.Models
{
    public class Seed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                if (!context.ApplicationServices.Any())
                {
                    var reconnection = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.ReconexionAutomatica, Version = "1.0.0" };
                    var antiClon = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.AntiClon, Version = "1.0.0" };
                    var vip = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.Vip, Version = "1.0.0" };
                    var balanceoNth = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.BalanceadorNTH, Version = "1.0.0" };
                    var balanceoPCC = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.BalanceadorPCC, Version = "1.0.0" };
                    var morosos = new MorososService { State = ServicesStatus.Stopped, ServiceType = ServiceType.Morosos, Version = "1.0.0", RedirecTo = "0.0.0.0", Horario = TimeSpan.FromHours(21) };
                    var simpleQueue = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.SimpleQueueLimit, Version = "1.0.0" };
                    var nautaConnector = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.NautaConnector, Version = "1.0.0" };
                    context.AddRange(reconnection, antiClon, vip, balanceoNth, balanceoPCC, morosos, simpleQueue, nautaConnector);
                    context.SaveChanges();
                }
                if (!context.ApplicationServices.Any(c => c.ServiceType == ServiceType.NautaConnector))
                {
                    var nautaConnector = new ApplicationServices { State = ServicesStatus.Stopped, ServiceType = ServiceType.NautaConnector, Version = "1.0.0" };
                    context.Add(nautaConnector);
                    context.SaveChanges();
                }
                if (!context.Services.Any())
                {
                    var services = new List<Service>()
                    {
                       new Service(){UserServiceType = UserServiceType.Games },

                       new Service(){ UserServiceType = UserServiceType.Internet},

                        new Service(){ UserServiceType = UserServiceType.Otros},
                    };

                    context.AddRange(services);
                    context.SaveChanges();
                }

                if (!context.DevicesRoles.Any())
                {
                    List<DevicesRoles> devicesRoles = new List<DevicesRoles>()
                    {
                        new DevicesRoles(){Role = Roles.FIREWALL_JUEGOS},
                        new DevicesRoles(){Role = Roles.DHCP},
                        new DevicesRoles(){Role=Roles.VLAN}
                    };
                    context.AddRange(devicesRoles);
                    context.SaveChanges();
                }

                if (!context.ApplicationServices.Any(c => c.ServiceType == ServiceType.BalanceadorNTH))
                {

                    //context.Add(balanceoNth);
                    //context.SaveChanges();
                }

            }




        }

        public static async void SeedUserAsync(UserManager<ApplicationUsers> userManager, RoleManager<IdentityRole> roleManager)
        {


            if (!await roleManager.RoleExistsAsync("Admin"))
            {
                IdentityRole adminRole = new IdentityRole()
                {
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                };

                roleManager.CreateAsync(adminRole).Wait();
            }

            if (!await roleManager.RoleExistsAsync("User"))
            {
                IdentityRole userRole = new IdentityRole()
                {
                    Name = "User",
                    NormalizedName = "USER"
                };

                roleManager.CreateAsync(userRole).Wait();
            }
            if (!await roleManager.RoleExistsAsync("Sub_Admin"))
            {
                IdentityRole userRole = new IdentityRole()
                {
                    Name = "Sub_Admin",
                    NormalizedName = "SUB_ADMIN"
                };

                roleManager.CreateAsync(userRole).Wait();
            }

            var users = await userManager.GetUsersInRoleAsync("Admin");
            if (users.Count() == 0)
            {
                ApplicationUsers user = new ApplicationUsers
                {
                    UserName = "admin",

                    Email = "wzm@gmail.com",

                    Estado = StatusEnum.Habilitado
                };

                IdentityResult result = userManager.CreateAsync
                (user, "Wzm.2019").Result;

                userManager.AddToRoleAsync(user, "Admin").Wait();

            }
        }
    }
}
