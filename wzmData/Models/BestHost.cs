﻿
namespace wzmData.Models
{
    public class BestHost
    {
        public int Id { get; set; }

        public string Host { get; set; }

        public string Latencia { get; set; }
    }
}
