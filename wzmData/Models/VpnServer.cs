﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace wzmData.Models
{
    public class VpnServer
    {
        public int Id { get; set; }
        public string User { get; set; }

        public string Password { get; set; }

        public string Host { get; set; }

        [Display(Name = "Deshabilitado")]
        public bool Desabled { get; set; }

        public string AddressList { get; set; }

        public bool Running { get; set; }

        public bool DisconnectOnPingFailed { get; set; }
                
       
    }
}
