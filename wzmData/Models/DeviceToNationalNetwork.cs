﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace wzmData.Models
{
    public class DeviceToNationalNetwork
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Ip Pública")]
        public string PublicIp { get; set; }
        
        public bool Disabled { get; set; }
         

        [Display(Name ="Dispositivo del Usuario")]
        public int UserDeviceId { get; set; }

        public virtual UserDevice UserDevice { get; set; }

   
                
    }
}
