﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wzmData.Models
{
    public class DeviceSignal
    {
        public int Id { get; set; }

        public int MinimalSignal { get; set; }

        public int MaximumSignal { get; set; }

        public int DispositivoId { get; set; }

        public bool Enabled { get; set; }

        public virtual Dispositivo Dispositivo { get; set; }
    }
}
