﻿using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
    public class VpnService : ApplicationServices
    {
        [Display(Name = "Utilizar Conexiones Persistentes")]

        public bool Persistent { get; set; }

        [Display(Name = "Adicionar Usuarios Premium")]
        public bool IncludeUsers { get; set; }
    }
}
