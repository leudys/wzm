﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace wzmData.Models
{
    public class VpnServerNacional : VpnServer
    {
        [Display(Name = "Cuenta Nauta")]
        public int CuentaNautaId { get; set; }

        public virtual CuentaNauta CuentaNauta { get; set; }

        [Display(Name = "Portal")]
        public int PortalId { get; set; }

        public virtual PortalesEtecsa Portal { get; set; }

        [Display(Name ="Gateway hacia Internet")]
        public bool AsInternet { get; set; }

        /// <summary>
        /// Significa que esta VPN se usará solamente para conexiones persistentes
        /// </summary>
        [Display(Name = "Conexón Persistente")]
        public bool PersistentConnection { get; set; }

        public virtual ICollection<HostsServices> Premium { get; set; }
        public virtual ICollection<VpnServerInternet> ServerInternet { get; set; }
        public VpnServerNacional()
        {
            ServerInternet = new HashSet<VpnServerInternet>();
            Premium = new HashSet<HostsServices>();
        }
    }
}
